FROM node:current-alpine

RUN mkdir -p /app
WORKDIR /app
COPY . /app

RUN npm i --production
RUN npm update
ENTRYPOINT npm run start
