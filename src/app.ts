import Koa from 'koa'
import koaBody from 'koa-body'
import helmet from 'koa-helmet'
import logger from 'koa-logger'
import json from 'koa-json'

import EdiApiSendRouter from './routes/edi-api-interface-router'
//import CronJob from './config/cronjob'

const PORT = 3002
const app = new Koa
app.use(json())
app.use(helmet())
app.use(logger())
app.use(koaBody({multipart:true}))
app.use(EdiApiSendRouter.routes())
app.use(EdiApiSendRouter.allowedMethods())
//app.use(CronJob.call)
app.listen(PORT, () => {
  console.log(`The server started on ${PORT}`)
})
