import Router from 'koa-router'
import SmtApiSendController from '../controllers/smt-api-send-controller'

const router = new Router()

//GET
router.get('/getToken', async (ctx, next) => {
  const token = await SmtApiSendController.getToken() 
  console.log(token)

  if (token !== 'Nothing') {
    ctx.body = await SmtApiSendController.sendDataJson(token)
  }
  //ctx.status = 200 
})


export default router