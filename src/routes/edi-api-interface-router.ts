import Router from 'koa-router'
import EdiApiInterfaceController from '../controllers/edi-api-interface-controller'


const router = new Router()

//POST API for booking request
router.post('/clv/inbound/requests/translate-to-e2f/partner/:partnerId/:terminalId', async (ctx) => {
  const { partnerId, terminalId } = ctx.params
  console.log(`param 1 : ${partnerId}`)
  console.log(`param 2 : ${terminalId}`)
  //const request = ctx.request.body
  //console.log(request)
  
  await EdiApiInterfaceController.translateE2F(ctx)
  
})

//POST API for booking request
router.post('/clv/inbound/requests/translate-to-j2f/partner/:partnerId', async (ctx) => {
  const { partnerId } = ctx.params
  console.log(`param : ${partnerId}`)
  //const request = ctx.request.body
  //console.log(request)
  
  await EdiApiInterfaceController.translateJ2F(`${partnerId}`, ctx)
  
})


export default router