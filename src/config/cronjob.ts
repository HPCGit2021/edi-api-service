// import cron from 'cron'
// import F9ApiInterfaceController from '../controllers/f9-api-interface-controller'

// const TEST_API = 'http://localhost:3002/clt/freight9/requests/booking-request/carriers/truongdo001'

// const {
//     API_F9_BKG_Confirm,
//     API_F9_Draft_BL,
//     API_F9_CGO_Tracking,
//     API_F9_FUN_ACK,
//     API_F9_APP_ACK,
//     PATH_TO_F9_BKG_Confirm,
//     PATH_TO_F9_Draft_BL,
//     PATH_TO_F9_CGO_Tracking,
//     PATH_TO_F9_FUN_ACK,
//     PATH_TO_F9_APP_ACK
//   } = require('../config/constants-path-to-f9');

// const cronjob = cron.CronJob

// console.log('Before job instantiation');
// const job = new cronjob('*/10 * * * * *', async function() {
// 	const d = new Date();
//     console.log('Every Tenth Second:', d);
//     //Booking Confirmation
//     await F9ApiInterfaceController.sendFileJsonToF9(PATH_TO_F9_BKG_Confirm, API_F9_BKG_Confirm)
//     //Draft BL
//     await F9ApiInterfaceController.sendFileJsonToF9(PATH_TO_F9_Draft_BL, API_F9_Draft_BL)
//     //Cargo Tracking
//     await F9ApiInterfaceController.sendFileJsonToF9(PATH_TO_F9_CGO_Tracking, API_F9_CGO_Tracking)
//     //Function Acknowledgement
//     await F9ApiInterfaceController.sendFileJsonToF9(PATH_TO_F9_FUN_ACK, API_F9_FUN_ACK)
//     //Application Acknowledgement
//     await F9ApiInterfaceController.sendFileJsonToF9(PATH_TO_F9_APP_ACK, API_F9_APP_ACK)
// });
// console.log('After job instantiation');
// job.start();


// export default cronjob