const API_F9_BKG_Confirm = 'http://34.64.253.200:80/api/f9/bkg/booking/confirm'
const API_F9_Draft_BL = 'http://34.64.253.200:80/api/f9/bkg/draft/bl'
const API_F9_CGO_Tracking = 'http://34.64.253.200:80/api/f9/bkg/cargo/tracking'
const API_F9_FUN_ACK = 'http://34.64.253.200:80/api/f9/bkg/functional/acknowledgement'
const API_F9_APP_ACK = 'http://34.64.253.200:80/api/f9/bkg/application/acknowlegement'
const PATH_TO_F9_BKG_Confirm = '/smartone/work/F9_TMP/TO_F9/Booking_Confirmation'
const PATH_TO_F9_Draft_BL = '/smartone/work/F9_TMP/TO_F9/Draft_BL'
const PATH_TO_F9_CGO_Tracking = '/smartone/work/F9_TMP/TO_F9/Cargo_Tracking'
const PATH_TO_F9_FUN_ACK = '/smartone/work/F9_TMP/TO_F9/Functional_Ack'
const PATH_TO_F9_APP_ACK = '/smartone/work/F9_TMP/TO_F9/Application_Ack'

module.exports = {
    API_F9_BKG_Confirm,
    API_F9_Draft_BL,
    API_F9_CGO_Tracking,
    API_F9_FUN_ACK,
    API_F9_APP_ACK,
    PATH_TO_F9_BKG_Confirm,
    PATH_TO_F9_Draft_BL,
    PATH_TO_F9_CGO_Tracking,
    PATH_TO_F9_FUN_ACK,
    PATH_TO_F9_APP_ACK
  };

  