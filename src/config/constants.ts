const STORE_PATH_INBOUND_REQUEST = '/edi-test/inbound'
const STORE_PATH_OUTBOUND_REQUEST = '/edi-test/outbound'

module.exports = {
  STORE_PATH_INBOUND_REQUEST,
  STORE_PATH_OUTBOUND_REQUEST
};