import fs from 'fs'
import CoparnD00bInboundHsdMapper from '../mapper/inbound/KHH/HSD/coparn_d00b_r_hsd_mapper'
import CoparnD00bInboundMapper from '../mapper/inbound/KHH/coparn_d95b_r_mapper'

class EdiInboundMapper {

    public static async processTranslateToOutput(data:any, partnerId:string, terminalId:string, msg:string, verId:string) {
        let flatFileData
        if (terminalId === "KHH") {
            if (partnerId === "HSD") {

            } else {
                flatFileData = await CoparnD00bInboundMapper.executeBizToFF(data)

            }
        
        } else if (terminalId === "Westports") {

        }

        return flatFileData
    }

}
export default EdiInboundMapper