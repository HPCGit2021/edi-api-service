import fs from 'fs'
import { isNotEmpty, makeGroupSegmentList, makeSegmentList, getSegmentExistGroup, makeGroupSegmentListForSecondGroup,  } from '../common/common-function'
import BGMSegment from '../segments/BGM-segment'
import UNBSegment from '../segments/UNB-segment'
import UNHSegment from '../segments/UNH-segment'
import ScannerSegments from '../segments/scanner-segments'

class EdiScanner {
  /**
   * scanner input data for inbound
   * 
   * @author truong.do
   * @param data 
   * @param partnerId 
   * @param msg 
   * @param version 
   * @param type 
   * @param code 
   */
  public static async scannerInputData(data:any, msg:string, version:string, type: string, code:string) {
    let dirPath: string = `/Algeta Mapper/EDI research/db`
    let bizData:string = ""
    if (code === "EDI") {
      dirPath = dirPath + "/edifact"
      if (type === "UN") {
        dirPath = dirPath + "/un" + "/" + version.toLowerCase()
      } else if (type === "KE") {
        dirPath = dirPath + "/ke" + "/" + version.toLowerCase()
      }

      let file:string = "msgtype"
      let msgList = fs.readFileSync(`${dirPath}/${file}`, 'utf8')
      
      file = "message"
      let messageData = fs.readFileSync(`${dirPath}/${file}`, 'utf8')

      if (msgList.indexOf(msg + "|") > -1) {
        let tabBiz:string = ""
        let levelGroup:number = 0
        let grpSegmentList:string[] = []
        let secondGrpSegmentList:string[] = []
        let thirdGrpSegmentList:string[] = []
        let fourthGrpSegmentList:string[] = []
        let groupFlag:boolean = false
        let firstGrpNm:string = ""
        let checkIdx:number = 0
        let firstGrpIdx:number = 0
        let secondGrpIdx:number = 0
        let thirdGrpIdx:number = 0
        let fourthGrpIdx:number = 0
        let lineList:string[] = data.split("'")
        for (let i_line = 0; i_line < lineList.length; i_line++) {
          let line = lineList[i_line]
          if (!isNotEmpty(line)) continue
          
          let segment = line.substr(0, 3)
          if (segment === "UNB") {
            bizData = bizData + await UNBSegment.scannerUNB(line)                
          } else if (segment === "UNH") {
            bizData = bizData + await UNHSegment.scannerUNH(line)
          } else if (segment === "BGM") {                
            bizData = bizData + await BGMSegment.scannerBGM(line, dirPath)
          } else {
            let segmentList = makeSegmentList(messageData, msg.toUpperCase(), code)                
            if (!groupFlag) {
              for (let i_segment = 2; i_segment < segmentList.length; i_segment++) {
                let segmentData:string = segmentList[i_segment]
                let itemList:string[] = segmentData.split("|")
                let segmentName:string = itemList[4]
                let segmentSeq:string = segmentName + itemList[2] + itemList[3].substr(0, 2)
                let groupName:string = itemList[8]
                if (i_segment < checkIdx) continue

                if (segmentName === segment) {
                  if (groupName !== "0") {
                    groupFlag = true
                    if (groupName === segmentName) {
                      if (!isNotEmpty(firstGrpNm)) {
                        if (levelGroup == 0) levelGroup = Number.parseInt(itemList[9])
                        firstGrpIdx = i_segment
                        checkIdx = i_segment
                      } else {
                        if (groupName !== firstGrpNm) {
                          checkIdx = i_segment
                          levelGroup = Number.parseInt(itemList[9])
                        }
                      }
                      
                      //check level group
                      if (levelGroup == 1) {
                        firstGrpNm = groupName
                        firstGrpIdx = i_segment
                        let list = makeGroupSegmentList(segmentList, groupName, firstGrpIdx, 1)                            
                        grpSegmentList = list.groupSegmentList
                        segmentSeq = segmentName + list.segmentSeq
                        // console.log("--------------start " + segment + " group segment list-------------------")
                        // console.log(grpSegmentList)
                        // console.log("--------------end " + segment + " group segment list-------------------")
                        secondGrpSegmentList = []
                        thirdGrpSegmentList = []
                        fourthGrpSegmentList = []

                        // processing check next line is exist in group
                        let nextLine:string = lineList[i_line + 1]
                        let nextSegment:string = nextLine.substr(0, 3)
                        let nextResultList = getSegmentExistGroup(nextSegment, grpSegmentList)
                        
                        let nextIdxGrp:number = nextResultList.idxGrp
                        if (nextIdxGrp == 0) {
                          groupFlag = false
                        }
                      } else if (levelGroup == 2) {
                        secondGrpIdx = i_segment
                        let list = makeGroupSegmentListForSecondGroup(segmentList, groupName, secondGrpIdx, 2)                            
                        secondGrpSegmentList = list.groupSegmentList
                        segmentSeq = segmentName + list.segmentSeq
                        // console.log("--------------start " + segment + " group segment list-------------------")
                        // console.log(secondGrpSegmentList)
                        // console.log("--------------end " + segment + " group segment list-------------------")
                        thirdGrpSegmentList = []
                        fourthGrpSegmentList = []
                        
                        // processing check next line is exist in group
                        let nextLine:string = lineList[i_line + 1]
                        let nextSegment:string = nextLine.substr(0, 3)
                        let nextResultList = getSegmentExistGroup(nextSegment, secondGrpSegmentList)
                        
                        let nextIdxGrp:number = nextResultList.idxGrp
                        if (nextIdxGrp == 0) {
                          groupFlag = false
                        }
                      } else if (levelGroup == 3) {
                        thirdGrpIdx = i_segment
                        let list = makeGroupSegmentListForSecondGroup(segmentList, groupName, thirdGrpIdx, 3)                            
                        thirdGrpSegmentList = list.groupSegmentList
                        segmentSeq = segmentName + list.segmentSeq
                        // console.log("--------------start " + segment + " group segment list-------------------")
                        // console.log(thirdGrpSegmentList)
                        // console.log("--------------end " + segment + " group segment list-------------------")                            
                        fourthGrpSegmentList = []

                        // processing check next line is exist in group
                        let nextLine:string = lineList[i_line + 1]
                        let nextSegment:string = nextLine.substr(0, 3)
                        let nextResultList = getSegmentExistGroup(nextSegment, thirdGrpSegmentList)
                        
                        let nextIdxGrp:number = nextResultList.idxGrp
                        if (nextIdxGrp == 0) {
                          groupFlag = false
                        }
                      } else if (levelGroup == 4) {
                        fourthGrpIdx = i_segment
                        let list = makeGroupSegmentListForSecondGroup(segmentList, groupName, fourthGrpIdx, 4)                            
                        fourthGrpSegmentList = list.groupSegmentList
                        segmentSeq = segmentName + list.segmentSeq
                        // console.log("--------------start " + segment + " group segment list-------------------")
                        // console.log(fourthGrpSegmentList)
                        // console.log("--------------end " + segment + " group segment list-------------------")
                        //check next line

                        // processing check next line is exist in group
                        let nextLine:string = lineList[i_line + 1]
                        let nextSegment:string = nextLine.substr(0, 3)
                        let nextResultList = getSegmentExistGroup(nextSegment, fourthGrpSegmentList)
                        
                        let nextIdxGrp:number = nextResultList.idxGrp
                        if (nextIdxGrp == 0) {
                          groupFlag = false
                        }
                      }
                      
                      bizData += "\n"                        
                      tabBiz = ""
                      for (let i_level = 0; i_level < levelGroup; i_level++) {
                        tabBiz += "    "
                      }
                    } else {
                      let resultList =  getSegmentExistGroup(segment, grpSegmentList)
                      let segmentData:string = resultList.segment
                      //console.log("----------Data:" + segmentData)
                      if (!isNotEmpty(segmentData)) break
                      
                      let itemList:string[] = segmentData.split("|")
                      levelGroup = Number.parseInt(itemList[9])
                      segmentSeq = segmentName + itemList[2] + itemList[3].substr(0, 2)

                      bizData += "\n"
                      tabBiz = ""
                      for (let i_level = 0; i_level < levelGroup; i_level++) {
                        tabBiz += "    "
                      }
                    }

                    bizData +=  await ScannerSegments.scannerGroupSegments(line, dirPath, "|" + segmentName + "|", segmentSeq, groupName, tabBiz)
                  } else { 
                    if (grpSegmentList.length > 0) {
                      bizData += "\n"
                      grpSegmentList = []
                      secondGrpSegmentList = []
                      thirdGrpSegmentList = []
                      fourthGrpSegmentList = []
                    }

                    bizData += await ScannerSegments.scannerSegments(line, dirPath, "|" + segmentName + "|", segmentSeq)
                  }
                  break
                }
              }
            } else {// processing child segment in group
              // get data segment is exist group
              let resultList =  getSegmentExistGroup(segment, grpSegmentList)
              if (levelGroup == 2) {
                resultList =  getSegmentExistGroup(segment, secondGrpSegmentList)
              } else if (levelGroup == 3) {
                resultList =  getSegmentExistGroup(segment, thirdGrpSegmentList)
              }

              let segmentData:string = resultList.segment
              if (isNotEmpty(segmentData)) {                    
                let itemList:string[] = segmentData.split("|")
                let segmentName:string = itemList[4]
                let segmentSeq:string = segmentName + itemList[2] + itemList[3].substr(0, 2)
                let groupName:string = itemList[8]
                bizData +=  await ScannerSegments.scannerGroupSegments(line, dirPath, "|" + segmentName + "|", segmentSeq, groupName, tabBiz)
                
                // check next line is exist in this group
                let nextLine:string = lineList[i_line + 1]
                let nextSegment:string = nextLine.substr(0, 3)
                let nextResultList:any
                if (levelGroup == 1) {
                  nextResultList = getSegmentExistGroup(nextSegment, grpSegmentList)
                } else if (levelGroup == 2) {
                  nextResultList = getSegmentExistGroup(nextSegment, secondGrpSegmentList)
                } else if (levelGroup == 3) {
                  nextResultList = getSegmentExistGroup(nextSegment, thirdGrpSegmentList)
                }

                // check next line is not exist in group
                let nextIdxGrp:number = nextResultList.idxGrp
                if (nextIdxGrp == 0) {
                  groupFlag = false
                }
              }                  
            }                
          }
        }
      }
    } else if (code === "X12") {
      dirPath = dirPath + "/x12" + "/" + version.toLowerCase()
      
      //console.log("------------Start Scanner X12-----------------")
      // let file:string = "msgtype"
      // let msgList = fs.readFileSync(`${dirPath}/${file}`, 'utf8')
      
      let file = "message"
      let messageData = fs.readFileSync(`${dirPath}/${file}`, 'utf8')

      let tabBiz:string = ""
      let levelGroup:number = 0
      let grpSegmentList:string[] = []
      let secondGrpSegmentList:string[] = []
      let thirdGrpSegmentList:string[] = []
      let fourthGrpSegmentList:string[] = []
      let groupFlag:boolean = false
      let firstGrpNm:string = ""
      let checkIdx:number = -1
      let firstGrpIdx:number = 0
      let secondGrpIdx:number = 0
      let thirdGrpIdx:number = 0
      let fourthGrpIdx:number = 0
      let lineList:string[] = data.split("~")

      for (let i_line = 0; i_line < lineList.length; i_line++) {
        let line = lineList[i_line]
        if (!isNotEmpty(line)) continue
        
        let itemList:string[] = line.split(type)
        let segment = itemList[0]
        if (segment === "ISA") {
          bizData = bizData + await ScannerSegments.scannerHeaderForX12(line, dirPath, "|" + segment + "|", "ISA0000", type)
        } else if (segment === "GS") {
          bizData = bizData + await ScannerSegments.scannerHeaderForX12(line, dirPath, "|" + segment + "|", "GS00000", type)
        } else if (segment === "GE") {
          bizData = bizData + await ScannerSegments.scannerHeaderForX12(line, dirPath, "|" + segment + "|", "GE00000", type)
        } else if (segment === "IEA") {
          bizData = bizData + await ScannerSegments.scannerHeaderForX12(line, dirPath, "|" + segment + "|", "IEA0000", type)
        } else {
          let segmentList = makeSegmentList(messageData, msg, code)
          if (!groupFlag) {
            for (let i_segment = 0; i_segment < segmentList.length; i_segment++) {
              let segmentData:string = segmentList[i_segment]
              let itemList:string[] = segmentData.split("|")
              let segmentName:string = itemList[4]
              
              let segmentSeq:string = segmentName + itemList[2] + itemList[3].substr(0, 2)
              if (segmentName.length == 2) {
                segmentSeq = segmentName + "0" + itemList[2] + itemList[3].substr(0, 2)
              }
              let groupName:string = itemList[8]
              if (i_segment < checkIdx) continue

              if (segmentName === segment) {
                if (groupName !== "0") {
                  groupFlag = true
                  if (groupName === segmentName) {
                    if (!isNotEmpty(firstGrpNm)) {
                      if (levelGroup == 0) levelGroup = Number.parseInt(itemList[9])
                      firstGrpIdx = i_segment
                      checkIdx = i_segment
                    } else {
                      if (groupName !== firstGrpNm) {
                        checkIdx = i_segment
                        levelGroup = Number.parseInt(itemList[9])
                      }
                    }
                    
                    //check level group
                    if (levelGroup == 1) {
                      firstGrpNm = groupName
                      firstGrpIdx = i_segment
                      let list = makeGroupSegmentList(segmentList, groupName, firstGrpIdx, 1)                            
                      grpSegmentList = list.groupSegmentList
                      if (segmentName.length == 2) {
                        segmentSeq = segmentName + "0" + list.segmentSeq
                      } else {
                        segmentSeq = segmentName + list.segmentSeq
                      }
                      
                      // console.log("--------------start " + segment + " group segment list-------------------")
                      // console.log(grpSegmentList)
                      // console.log("--------------end " + segment + " group segment list-------------------")
                      secondGrpSegmentList = []
                      thirdGrpSegmentList = []
                      fourthGrpSegmentList = []

                      // processing check next line is exist in group
                      let nextLine:string = lineList[i_line + 1]
                      let nextSegment:string = nextLine.substr(0, 3)
                      let nextResultList = getSegmentExistGroup(nextSegment, grpSegmentList)
                      
                      let nextIdxGrp:number = nextResultList.idxGrp
                      if (nextIdxGrp == 0) {
                        groupFlag = false
                      }
                    } else if (levelGroup == 2) {
                      secondGrpIdx = i_segment
                      let list = makeGroupSegmentListForSecondGroup(segmentList, groupName, secondGrpIdx, 2)                            
                      secondGrpSegmentList = list.groupSegmentList
                      if (segmentName.length == 2) {
                        segmentSeq = segmentName + "0" + list.segmentSeq
                      } else {
                        segmentSeq = segmentName + list.segmentSeq
                      }
                      // console.log("--------------start " + segment + " group segment list-------------------")
                      // console.log(secondGrpSegmentList)
                      // console.log("--------------end " + segment + " group segment list-------------------")
                      thirdGrpSegmentList = []
                      fourthGrpSegmentList = []
                      
                      // processing check next line is exist in group
                      let nextLine:string = lineList[i_line + 1]
                      let nextSegment:string = nextLine.substr(0, 3)
                      let nextResultList = getSegmentExistGroup(nextSegment, secondGrpSegmentList)
                      
                      let nextIdxGrp:number = nextResultList.idxGrp
                      if (nextIdxGrp == 0) {
                        groupFlag = false
                      }
                    } else if (levelGroup == 3) {
                      thirdGrpIdx = i_segment
                      let list = makeGroupSegmentListForSecondGroup(segmentList, groupName, thirdGrpIdx, 3)                            
                      thirdGrpSegmentList = list.groupSegmentList
                      if (segmentName.length == 2) {
                        segmentSeq = segmentName + "0" + list.segmentSeq
                      } else {
                        segmentSeq = segmentName + list.segmentSeq
                      }
                      // console.log("--------------start " + segment + " group segment list-------------------")
                      // console.log(thirdGrpSegmentList)
                      // console.log("--------------end " + segment + " group segment list-------------------")                            
                      fourthGrpSegmentList = []

                      // processing check next line is exist in group
                      let nextLine:string = lineList[i_line + 1]
                      let nextSegment:string = nextLine.substr(0, 3)
                      let nextResultList = getSegmentExistGroup(nextSegment, thirdGrpSegmentList)
                      
                      let nextIdxGrp:number = nextResultList.idxGrp
                      if (nextIdxGrp == 0) {
                        groupFlag = false
                      }
                    } else if (levelGroup == 4) {
                      fourthGrpIdx = i_segment
                      let list = makeGroupSegmentListForSecondGroup(segmentList, groupName, fourthGrpIdx, 4)                            
                      fourthGrpSegmentList = list.groupSegmentList
                      if (segmentName.length == 2) {
                        segmentSeq = segmentName + "0" + list.segmentSeq
                      } else {
                        segmentSeq = segmentName + list.segmentSeq
                      }
                      // console.log("--------------start " + segment + " group segment list-------------------")
                      // console.log(fourthGrpSegmentList)
                      // console.log("--------------end " + segment + " group segment list-------------------")
                      //check next line

                      // processing check next line is exist in group
                      let nextLine:string = lineList[i_line + 1]
                      let nextSegment:string = nextLine.substr(0, 3)
                      let nextResultList = getSegmentExistGroup(nextSegment, fourthGrpSegmentList)
                      
                      let nextIdxGrp:number = nextResultList.idxGrp
                      if (nextIdxGrp == 0) {
                        groupFlag = false
                      }
                    }
                    
                    bizData += "\n"                        
                    tabBiz = ""
                    for (let i_level = 0; i_level < levelGroup; i_level++) {
                      tabBiz += "    "
                    }
                  } else {
                    let resultList =  getSegmentExistGroup(segment, grpSegmentList)
                    let segmentData:string = resultList.segment
                    //console.log("----------Data:" + segmentData)
                    if (!isNotEmpty(segmentData)) break
                    
                    let itemList:string[] = segmentData.split("|")
                    levelGroup = Number.parseInt(itemList[9])
                    
                    if (segmentName.length == 2) {
                      segmentSeq = segmentName + "0" + itemList[2] + itemList[3].substr(0, 2)
                    } else {
                      segmentSeq = segmentName + itemList[2] + itemList[3].substr(0, 2)
                    }

                    bizData += "\n"
                    tabBiz = ""
                    for (let i_level = 0; i_level < levelGroup; i_level++) {
                      tabBiz += "    "
                    }
                  }

                  bizData +=  await ScannerSegments.scannerGroupSegmentsForX12(line, dirPath, "|" + segmentName + "|", segmentSeq, tabBiz, type)
                } else { 
                  if (grpSegmentList.length > 0) {
                    bizData += "\n"
                    grpSegmentList = []
                    secondGrpSegmentList = []
                    thirdGrpSegmentList = []
                    fourthGrpSegmentList = []
                  }

                  bizData += await ScannerSegments.scannerSegmentsForX12(line, dirPath, "|" + segmentName + "|", segmentSeq, type)
                }
                break
              }
            }
          } else {// processing child segment in group
            // get data segment is exist group
            let resultList =  getSegmentExistGroup(segment, grpSegmentList)
            if (levelGroup == 2) {
              resultList =  getSegmentExistGroup(segment, secondGrpSegmentList)
            } else if (levelGroup == 3) {
              resultList =  getSegmentExistGroup(segment, thirdGrpSegmentList)
            }

            let segmentData:string = resultList.segment
            if (isNotEmpty(segmentData)) {                    
              let itemList:string[] = segmentData.split("|")
              let segmentName:string = itemList[4]
              let segmentSeq:string = segmentName + itemList[2] + itemList[3].substr(0, 2)
              
              bizData +=  await ScannerSegments.scannerGroupSegmentsForX12(line, dirPath, "|" + segmentName + "|", segmentSeq, tabBiz, type)
              
              // check next line is exist in this group
              let nextLine:string = lineList[i_line + 1]
              let nextSegment:string = nextLine.substr(0, 3)
              let nextResultList:any
              if (levelGroup == 1) {
                nextResultList = getSegmentExistGroup(nextSegment, grpSegmentList)
              } else if (levelGroup == 2) {
                nextResultList = getSegmentExistGroup(nextSegment, secondGrpSegmentList)
              } else if (levelGroup == 3) {
                nextResultList = getSegmentExistGroup(nextSegment, thirdGrpSegmentList)
              }

              // check next line is not exist in group
              let nextIdxGrp:number = nextResultList.idxGrp
              if (nextIdxGrp == 0) {
                groupFlag = false
              }
            }                  
          }
        }
      }
    }
    console.log("----------------BIZ File--------------------")
    console.log(bizData)
    console.log("------------End Scanner-----------------")

    return bizData
  }
}
export default EdiScanner