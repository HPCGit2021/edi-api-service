import fs from 'fs'
import { makeElementList, getSegmentSeq } from '../common/common-function'

class BGMSegment {
    public static async scannerBGM(data:string, dirPath:string) {
        let bizData:string = ""
        let file = "segment"
        let segmentData = fs.readFileSync(`${dirPath}/${file}`, 'utf8')        
        let bgmList = makeElementList(segmentData, "BGM", "EDI")

        file = "message"
        let messageData = fs.readFileSync(`${dirPath}/${file}`, 'utf8')

        let BGM_SEQ: string = "BGM0001"
        let elementList:string[] = data.split("+")
        let elementPosition:number = 0;
        for (let i_element = 0; i_element < elementList.length; i_element++){
            if (i_element > 0) {
                let elementData = elementList[i_element]
                let itemList = getSegmentSeq(bgmList, BGM_SEQ, elementData, elementPosition)
                bizData += itemList.itemSeq
                elementPosition = itemList.index
            }
        }

        return bizData
    }
}
export default BGMSegment