import fs from 'fs'
import { isNotEmpty, makeElementList, makeSegmentList, getSegmentSeq } from '../common/common-function'

class UNHSegment {
    public static async scannerUNH(data:string) {
        let bizData:string = ""
        let elementList = data.split("+")
        for (let i_element = 0; i_element < elementList.length; i_element++){
            let compositeList = elementList[i_element].split(":")
            
            if (i_element == 1) {
              bizData = bizData + "UNH000000:"
              let compsoteData:string = compositeList[0]
              if (isNotEmpty(compsoteData)) {
                bizData = bizData + compsoteData
              }
              bizData = bizData + "\n"
            } else if (i_element == 2) {
              bizData = bizData + "UNH000002:"
              let compsoteData:string = compositeList[0]
              if (isNotEmpty(compsoteData)) {
                bizData = bizData + compsoteData
              }
              bizData = bizData + "\n"

              bizData = bizData + "UNH000003:"
              compsoteData = compositeList[1]
              if (isNotEmpty(compsoteData)) {
                bizData = bizData + compsoteData
              }
              bizData = bizData + "\n"  

              bizData = bizData + "UNH000004:"
              compsoteData = compositeList[2]
              if (isNotEmpty(compsoteData)) {
                bizData = bizData + compsoteData
              }
              bizData = bizData + "\n"  

              bizData = bizData + "UNH000005:"
              compsoteData = compositeList[3]
              if (isNotEmpty(compsoteData)) {
                bizData = bizData + compsoteData
              }
              bizData = bizData + "\n"  

              bizData = bizData + "UNH000006:"
              compsoteData = compositeList[4]
              if (isNotEmpty(compsoteData)) {
                bizData = bizData + compsoteData
              }
              bizData = bizData + "\n"  
            }            
        }
        return bizData
    }
}
export default UNHSegment