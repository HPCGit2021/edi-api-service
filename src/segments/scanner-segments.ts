import fs from 'fs'
import { makeElementList, getSegmentSeq, getGroupSegmentSeq } from '../common/common-function'
import { isNotEmpty } from '../common/common-function'

class ScannerSegments{
    /**
     * scanner segment based on segment name and sequence
     * 
     * @author truong.do
     * @param data 
     * @param dirPath 
     * @param segmentName 
     * @param segmentSeq 
     */
    public static async scannerSegments(data:string, dirPath:string, segmentName:string, segmentSeq:string) {
        let bizData:string = ""
        let file = "segment"
        let segmentData = fs.readFileSync(`${dirPath}/${file}`, 'utf8')
        
        let segmentItemList = makeElementList(segmentData, segmentName, "EDI")
        
        let replacer = /\?\+/g
        data = data.replace(replacer, "'")
        let elementList:string[] = data.split("+")
        let elementPosition:number = 0;
        for (let i_element = 0; i_element < elementList.length; i_element++){
            if (i_element > 0) {
                let elementData = elementList[i_element]
                replacer = /\'/g
                elementData = elementData.replace(replacer, "+")
                let itemList = getSegmentSeq(segmentItemList, segmentSeq, elementData, elementPosition)
                bizData += itemList.itemSeq
                elementPosition = itemList.index
            }
        }

        return bizData
    }

    /**
     * scanner segment in the group based on segment name and sequence
     * 
     * @author truong.do
     * @param dirPath 
     * @param segmentName 
     * @param segmentSeq 
     * @param groupName 
     * @param tabBiz 
     */
    public static async scannerGroupSegments(data:string, dirPath:string, segmentName:string, segmentSeq:string, groupName:string, tabBiz:string) {
        let bizData:string = ""
        let file = "segment"
        let segmentData = fs.readFileSync(`${dirPath}/${file}`, 'utf8')
        
        let segmentItemList = makeElementList(segmentData, segmentName, "EDI")

        let replacer = /\?\+/g
        data = data.replace(replacer, "'")
        let elementList:string[] = data.split("+")
        let elementPosition:number = 0;
        for (let i_element = 0; i_element < elementList.length; i_element++){
            if (i_element > 0) {
                let elementData = elementList[i_element]
                replacer = /\'/g
                elementData = elementData.replace(replacer, "+")
                let itemList = getGroupSegmentSeq(segmentItemList, segmentSeq, elementData, elementPosition, tabBiz)
                bizData += itemList.itemSeq
                elementPosition = itemList.index
            }
        }

        return bizData
    }

    /**
     * scanner segment for X12 based on segment name and sequence
     * 
     * @author truong.do
     * @param data 
     * @param dirPath 
     * @param segmentName 
     * @param segmentSeq 
     */
    public static async scannerSegmentsForX12(data:string, dirPath:string, segmentName:string, segmentSeq:string, type:string) {
        let bizData:string = ""
        let file = "segment"
        let segmentData = fs.readFileSync(`${dirPath}/${file}`, 'utf8')
        
        let segmentItemList = makeElementList(segmentData, segmentName, type)
        let elementList:string[] = data.split(type)
        let elementPosition:number = 0;
        for (let i_element = 0; i_element < elementList.length; i_element++){
            if (i_element > 0) {
                let elementData = elementList[i_element]
                let itemList = await this.getSegmentSeqForX12(segmentItemList, segmentSeq, elementData, elementPosition)
                bizData += itemList.itemSeq
                elementPosition = itemList.index
            }
        }

        return bizData
    }

    /**
     * scanner segment for X12 in the group based on segment name and sequence
     * 
     * @author truong.do
     * @param data 
     * @param dirPath 
     * @param segmentName 
     * @param segmentSeq 
     * @param tabBiz
     * @param type
     */
    public static async scannerGroupSegmentsForX12(data:string, dirPath:string, segmentName:string, segmentSeq:string, tabBiz:string, type:string) {
        let bizData:string = ""
        let file = "segment"
        let segmentData = fs.readFileSync(`${dirPath}/${file}`, 'utf8')
        
        let segmentItemList = makeElementList(segmentData, segmentName, type)        
        let elementList:string[] = data.split(type)        
        let elementPosition:number = 0;
        for (let i_element = 0; i_element < elementList.length; i_element++){
            if (i_element > 0) {
                let elementData = elementList[i_element]
                let itemList = await this.getGroupSegmentSeqForX12(segmentItemList, segmentSeq, elementData, elementPosition, tabBiz)
                
                bizData += itemList.itemSeq
                elementPosition = itemList.index
            }
        }
        return bizData
    }

    /**
     * get segment sequence for X12
     * 
     * @author truong.do
     * @param elementList 
     * @param segSeq 
     * @param data 
     * @param index 
     */
    static async getSegmentSeqForX12(elementList:string[], segSeq:string, data:string, index:number) {
        let itemID:string = ""
            
        for (let i_element = 0; i_element < elementList.length; i_element++) {
            if (i_element == index) {
                let line = elementList[i_element]
                let itemList:string[] = line.split("|")
                
                if (!isNotEmpty(data)) {
                    data = ""                         
                    itemID = segSeq + itemList[2] + ":" + data + "\n"
                } else {
                    itemID = segSeq + itemList[2] + ":" + data + "\n"
                }
                index = index + 1
                break                
            }
        }
        
        return {itemSeq: itemID, index: index}
    }

    /**
     * get segment sequence for X12
     * 
     * @author truong.do
     * @param elementList 
     * @param segSeq 
     * @param data 
     * @param index 
     * @param tabBiz
     */
    static async getGroupSegmentSeqForX12(elementList:string[], segSeq:string, data:string, index:number, tabBiz:string) {
        //console.log("-----------Segment SEQ---------------")
        let itemID:string = ""
        
        for (let i_element = 0; i_element < elementList.length; i_element++) {
            if (i_element == index) {
                let line = elementList[i_element]
                let itemList:string[] = line.split("|")
                
                if (!isNotEmpty(data)) {
                    data = ""                             
                    itemID = tabBiz+ segSeq + itemList[2] + ":" + data + "\n"                            
                } else {
                    itemID = tabBiz+ segSeq + itemList[2] + ":" + data + "\n"
                }
                index = index + 1
                break
            }
        }
    
        return {itemSeq: itemID, index: index}
    }

    /**
     * scanner header/footer for X12
     * 
     * @author truong.do
     * @param data 
     * @param dirPath 
     * @param segmentName 
     * @param segmentSeq 
     * @param type 
     */
    public static async scannerHeaderForX12(data:string, dirPath:string, segmentName:string, segmentSeq:string, type:string) {
        let bizData:string = ""
        let file = "segment"
        let segmentData = fs.readFileSync(`${dirPath}/${file}`, 'utf8')
        
        let segmentItemList = makeElementList(segmentData, segmentName, type)
        let elementList:string[] = data.split(type)
        let elementPosition:number = 0;
        for (let i_element = 0; i_element < elementList.length; i_element++){
            if (i_element > 0) {
                let elementData = elementList[i_element]
                let itemList = await this.getSegmentSeqForX12(segmentItemList, segmentSeq, elementData, elementPosition)
                bizData += itemList.itemSeq
                elementPosition = itemList.index
            }
        }

        return bizData
    }
}
export default ScannerSegments