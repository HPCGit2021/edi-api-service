import { isNotEmpty } from '../common/common-function'

class UNBSegment {
    public static async scannerUNB(data:string) {
        let bizData:string = ""
        let elementList = data.split("+")
        for (let i_element = 0; i_element < elementList.length; i_element++){
          let compositeList = elementList[i_element].split(":")
          
          if (i_element == 1) {
            bizData = bizData + "UNB000001:"
            let compsoteData:string = compositeList[0]
            if (isNotEmpty(compsoteData)) {
              bizData = bizData + compsoteData
            }
            bizData = bizData + "\n"
            
            bizData = bizData + "UNB000002:"
            compsoteData = compositeList[1]
            if (isNotEmpty(compsoteData)) {
              bizData = bizData + compsoteData
            }
            bizData = bizData + "\n"
          } else if (i_element == 2) {
            bizData = bizData + "UNB000004:"
            let compsoteData:string = compositeList[0]
            if (isNotEmpty(compsoteData)) {
              bizData = bizData + compsoteData
            }
            bizData = bizData + "\n"                 
          } else if (i_element == 3) {
            bizData = bizData + "UNB000008:"
            let compsoteData:string = compositeList[0]
            if (isNotEmpty(compsoteData)) {
              bizData = bizData + compsoteData
            }
            bizData = bizData + "\n"                 
          } else if (i_element == 4) {
            bizData = bizData + "UNB000012:"
            let compsoteData:string = compositeList[0]
            if (isNotEmpty(compsoteData)) {
              bizData = bizData + compsoteData
            }
            bizData = bizData + "\n"

            bizData = bizData + "UNB000013:"
            compsoteData = compositeList[1]
            if (isNotEmpty(compsoteData)) {
              bizData = bizData + compsoteData
            }
            bizData = bizData + "\n"   
          } else if (i_element == 5) {
            bizData = bizData + "UNB000014:"
            let compsoteData:string = compositeList[0]
            if (isNotEmpty(compsoteData)) {
              bizData = bizData + compsoteData
            }
            bizData = bizData + "\n"                 
          }
          
        }

        return bizData
    }
}
export default UNBSegment