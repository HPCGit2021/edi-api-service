class MapperCommon {

    public static async getDataFTX(lineList:string[], idx:number, segSeq:string) {
        let FTX_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let FTX_List:any[] = []
        let FTX_SEQ:string = "FTX" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(FTX_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                FTX_Item[FTX_SEQ] = itemList[1]
                flag = true;
            } else if (line.indexOf(FTX_SEQ) > -1 && flag) {
                FTX_List.push(FTX_Item)
                
                FTX_Item = {}
                itemList = line.split(":")
                FTX_Item[FTX_SEQ] = itemList[1]                
            } else {
                itemList = line.split(":")
                if (line.indexOf("FTX" + segSeq + "01") > -1) {
                    FTX_Item['FTX'+ segSeq +'01'] = itemList[1]
                } else if (line.indexOf("FTX" + segSeq + "03") > -1) {
                    FTX_Item['FTX'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("FTX" + segSeq + "04") > -1) {
                    FTX_Item['FTX'+ segSeq +'04'] = itemList[1]
                } else if (line.indexOf("FTX" + segSeq + "05") > -1) {
                    FTX_Item['FTX'+ segSeq +'05'] = itemList[1]
                } else if (line.indexOf("FTX" + segSeq + "07") > -1) {
                    FTX_Item['FTX'+ segSeq +'07'] = itemList[1]
                    if (itemList.length > 2) {
                        for (let i:number = 2; i < itemList.length; i++) {
                            FTX_Item['FTX'+ segSeq +'07'] += ":" + itemList[i]
                        }
                    }
                } else if (line.indexOf("FTX" + segSeq + "08") > -1) {
                    FTX_Item['FTX'+ segSeq +'08'] = itemList[1]
                    if (itemList.length > 2) {
                        for (let i:number = 2; i < itemList.length; i++) {
                            FTX_Item['FTX'+ segSeq +'08'] += ":" + itemList[i]
                        }
                    }
                } else if (line.indexOf("FTX" + segSeq + "09") > -1) {
                    FTX_Item['FTX'+ segSeq +'09'] = itemList[1]
                    if (itemList.length > 2) {
                        for (let i:number = 2; i < itemList.length; i++) {
                            FTX_Item['FTX'+ segSeq +'09'] += ":" + itemList[i]
                        }
                    }
                } else if (line.indexOf("FTX" + segSeq + "10") > -1) {
                    FTX_Item['FTX'+ segSeq +'10'] = itemList[1]
                    if (itemList.length > 2) {
                        for (let i:number = 2; i < itemList.length; i++) {
                            FTX_Item['FTX'+ segSeq +'10'] += ":" + itemList[i]
                        }
                    }
                } else if (line.indexOf("FTX" + segSeq + "11") > -1) {
                    FTX_Item['FTX'+ segSeq +'11'] = itemList[1]
                    if (itemList.length > 2) {
                        for (let i:number = 2; i < itemList.length; i++) {
                            FTX_Item['FTX'+ segSeq +'11'] += ":" + itemList[i]
                        }
                    }
                } else if (line.indexOf("FTX" + segSeq + "12") > -1) {
                    FTX_Item['FTX'+ segSeq +'12'] = itemList[1]
                } else if (line.indexOf("FTX" + segSeq + "13") > -1) {
                    FTX_Item['FTX'+ segSeq +'13'] = itemList[1]
                } else {
                    FTX_List.push(FTX_Item)
                    break
                }
            }
        }

        return FTX_List
    }

    public static async getDataRFF(lineList:string[], idx:number, segSeq:string) {
        let RFF_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let RFF_List:any[] = []
        let RFF_SEQ:string = "RFF" + segSeq + "01"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(RFF_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                RFF_Item[RFF_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(RFF_SEQ) > -1 && flag) {
                RFF_List.push(RFF_Item)
                
                RFF_Item = {}
                itemList = line.split(":")
                RFF_Item[RFF_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf("RFF") > -1) {
                    RFF_Item["RFF"+ itemSeq] = itemList[1]
                } else {
                    RFF_List.push(RFF_Item)
                    break
                }
            }
        }
        return RFF_List
    }

    public static async getDataCTA(lineList:string[], idx:number, segSeq:string) {
        let CTA_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let CTA_List:any[] = []
        let CTA_SEQ:string = "CTA" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(CTA_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                CTA_Item[CTA_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(CTA_SEQ) > -1 && flag) {
                CTA_List.push(CTA_Item)
                
                CTA_Item = {}
                itemList = line.split(":")
                CTA_Item[CTA_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf("CTA") > -1) {
                    CTA_Item['CTA'+ itemSeq] = itemList[1]
                } else {
                    CTA_List.push(CTA_Item)
                    break
                }
            }
        }
        return CTA_List
    }

    public static async getDataLOC(lineList:string[], idx:number, segSeq:string) {
        let LOC_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let LOC_List:any[] = []
        let LOC_SEQ:string = "LOC" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(LOC_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                LOC_Item[LOC_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(LOC_SEQ) > -1 && flag) {
                LOC_List.push(LOC_Item)
                
                LOC_Item = {}
                itemList = line.split(":")
                LOC_Item[LOC_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf("LOC" ) > -1) {
                    LOC_Item['LOC'+ itemSeq] = itemList[1]
                } else {
                    LOC_List.push(LOC_Item)
                    break
                }
            }
        }
        //console.log(LOC_List)
        return LOC_List
    }

    public static async getDataDTM(lineList:string[], idx:number, segSeq:string) {
        let DTM_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let DTM_List:any[] = []
        let DTM_SEQ:string = "DTM" + segSeq + "01"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DTM_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                DTM_Item[DTM_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(DTM_SEQ) > -1 && flag) {
                DTM_List.push(DTM_Item)
                
                DTM_Item = {}
                itemList = line.split(":")
                DTM_Item[DTM_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("DTM" + segSeq + "02") > -1) {
                    DTM_Item['DTM'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("DTM" + segSeq + "03") > -1) {
                    DTM_Item['DTM'+ segSeq +'03'] = itemList[1]
                } else {
                    DTM_List.push(DTM_Item)
                    break
                }
            }
        }
        return DTM_List
    }

    public static async getDataHAN(lineList:string[], idx:number, segSeq:string) {
        let HAN_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let HAN_List:any[] = []
        let HAN_SEQ:string = "HAN" + segSeq + "01"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(HAN_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                HAN_Item[HAN_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(HAN_SEQ) > -1 && flag) {
                HAN_List.push(HAN_Item)
                
                HAN_Item = {}
                itemList = line.split(":")
                HAN_Item[HAN_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf("HAN") > -1) {
                    HAN_Item['HAN'+ itemSeq] = itemList[1]
                } else {
                    HAN_List.push(HAN_Item)
                    break
                }
            }
        }
        return HAN_List
    }

    public static async getDataPIA(lineList:string[], idx:number, segSeq:string) {
        let PIA_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let PIA_List:any[] = []
        let PIA_SEQ:string = "PIA" + segSeq + "01"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(PIA_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                PIA_Item[PIA_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(PIA_SEQ) > -1 && flag) {
                PIA_List.push(PIA_Item)
                
                PIA_Item = {}
                itemList = line.split(":")
                PIA_Item[PIA_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("PIA" + segSeq + "02") > -1) {
                    PIA_Item['PIA'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("PIA" + segSeq + "03") > -1) {
                    PIA_Item['PIA'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("PIA" + segSeq + "04") > -1) {
                    PIA_Item['PIA'+ segSeq +'04'] = itemList[1]
                } else if (line.indexOf("PIA" + segSeq + "06") > -1) {
                    PIA_Item['PIA'+ segSeq +'06'] = itemList[1]
                } else if (line.indexOf("PIA" + segSeq + "07") > -1) {
                    PIA_Item['PIA'+ segSeq +'07'] = itemList[1]
                } else if (line.indexOf("PIA" + segSeq + "08") > -1) {
                    PIA_Item['PIA'+ segSeq +'08'] = itemList[1]
                } else {
                    PIA_List.push(PIA_Item)
                    break
                }
            }
        }
        return PIA_List
    }

    public static async getDataMEA(lineList:string[], idx:number, segSeq:string) {
        let MEA_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let MEA_List:any[] = []
        let MEA_SEQ:string = "MEA" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(MEA_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                MEA_Item[MEA_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(MEA_SEQ) > -1 && flag) {
                MEA_List.push(MEA_Item)
                
                MEA_Item = {}
                itemList = line.split(":")
                MEA_Item[MEA_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("MEA" + segSeq + "02") > -1) {
                    MEA_Item['MEA'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "03") > -1) {
                    MEA_Item['MEA'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "04") > -1) {
                    MEA_Item['MEA'+ segSeq +'04'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "05") > -1) {
                    MEA_Item['MEA'+ segSeq +'05'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "07") > -1) {
                    MEA_Item['MEA'+ segSeq +'07'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "08") > -1) {
                    MEA_Item['MEA'+ segSeq +'08'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "09") > -1) {
                    MEA_Item['MEA'+ segSeq +'09'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "10") > -1) {
                    MEA_Item['MEA'+ segSeq +'10'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "11") > -1) {
                    MEA_Item['MEA'+ segSeq +'11'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "12") > -1) {
                    MEA_Item['MEA'+ segSeq +'12'] = itemList[1]
                } else {
                    MEA_List.push(MEA_Item)
                    break
                }
            }
        }
        return MEA_List
    }

    public static async getDataDIM(lineList:string[], idx:number, segSeq:string) {
        let DIM_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let DIM_List:any[] = []
        let DIM_SEQ:string = "DIM" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DIM_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                DIM_Item[DIM_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(DIM_SEQ) > -1 && flag) {
                DIM_List.push(DIM_Item)
                
                DIM_Item = {}
                itemList = line.split(":")
                DIM_Item[DIM_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("DIM" + segSeq + "02") > -1) {
                    DIM_Item['DIM'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("DIM" + segSeq + "03") > -1) {
                    DIM_Item['DIM'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("DIM" + segSeq + "04") > -1) {
                    DIM_Item['DIM'+ segSeq +'04'] = itemList[1]
                } else if (line.indexOf("DIM" + segSeq + "05") > -1) {
                    DIM_Item['DIM'+ segSeq +'05'] = itemList[1]
                } else {
                    DIM_List.push(DIM_Item)
                    break
                }
            }
        }
        return DIM_List
    }

    public static async getDataDOC(lineList:string[], idx:number, segSeq:string) {
        let DOC_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let DOC_List:any[] = []
        let DOC_SEQ:string = "DOC" + segSeq + "01"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DOC_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                DOC_Item[DOC_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(DOC_SEQ) > -1 && flag) {
                DOC_List.push(DOC_Item)
                
                DOC_Item = {}
                itemList = line.split(":")
                DOC_Item[DOC_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("DOC" + segSeq + "02") > -1) {
                    DOC_Item['DOC'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("DOC" + segSeq + "03") > -1) {
                    DOC_Item['DOC'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("DOC" + segSeq + "04") > -1) {
                    DOC_Item['DOC'+ segSeq +'04'] = itemList[1]
                } else if (line.indexOf("DOC" + segSeq + "06") > -1) {
                    DOC_Item['DOC'+ segSeq +'06'] = itemList[1]
                } else if (line.indexOf("DOC" + segSeq + "07") > -1) {
                    DOC_Item['DOC'+ segSeq +'07'] = itemList[1]
                } else if (line.indexOf("DOC" + segSeq + "08") > -1) {
                    DOC_Item['DOC'+ segSeq +'08'] = itemList[1]
                } else if (line.indexOf("DOC" + segSeq + "09") > -1) {
                    DOC_Item['DOC'+ segSeq +'09'] = itemList[1]
                } else if (line.indexOf("DOC" + segSeq + "10") > -1) {
                    DOC_Item['DOC'+ segSeq +'10'] = itemList[1]
                } else if (line.indexOf("DOC" + segSeq + "11") > -1) {
                    DOC_Item['DOC'+ segSeq +'11'] = itemList[1]
                } else if (line.indexOf("DOC" + segSeq + "12") > -1) {
                    DOC_Item['DOC'+ segSeq +'12'] = itemList[1]
                } else {
                    DOC_List.push(DOC_Item)
                    break
                }
            }
        }
        return DOC_List
    }

    public static async getDataTMD(lineList:string[], idx:number, segSeq:string) {
        let TMD_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let TMD_List:any[] = []
        let TMD_SEQ:string = "TMD" + segSeq + "01"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TMD_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                TMD_Item[TMD_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(TMD_SEQ) > -1 && flag) {
                TMD_List.push(TMD_Item)
                
                TMD_Item = {}
                itemList = line.split(":")
                TMD_Item[TMD_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("TMD" + segSeq + "02") > -1) {
                    TMD_Item['TMD'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("TMD" + segSeq + "03") > -1) {
                    TMD_Item['TMD'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("TMD" + segSeq + "04") > -1) {
                    TMD_Item['TMD'+ segSeq +'04'] = itemList[1]
                } else {
                    TMD_List.push(TMD_Item)
                    break
                }
            }
        }
        return TMD_List
    }

    public static async getDataTSR(lineList:string[], idx:number, segSeq:string) {
        let TSR_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let TSR_List:any[] = []
        let TSR_SEQ:string = "TSR" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TSR_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                TSR_Item[TSR_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(TSR_SEQ) > -1 && flag) {
                TSR_List.push(TSR_Item)
                
                TSR_Item = {}
                itemList = line.split(":")
                TSR_Item[TSR_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("TSR" + segSeq + "01") > -1) {
                    TSR_Item['TSR'+ segSeq +'01'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "02") > -1) {
                    TSR_Item['TSR'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "03") > -1) {
                    TSR_Item['TSR'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "05") > -1) {
                    TSR_Item['TSR'+ segSeq +'05'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "06") > -1) {
                    TSR_Item['TSR'+ segSeq +'06'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "07") > -1) {
                    TSR_Item['TSR'+ segSeq +'07'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "08") > -1) {
                    TSR_Item['TSR'+ segSeq +'08'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "09") > -1) {
                    TSR_Item['TSR'+ segSeq +'09'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "10") > -1) {
                    TSR_Item['TSR'+ segSeq +'10'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "12") > -1) {
                    TSR_Item['TSR'+ segSeq +'12'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "13") > -1) {
                    TSR_Item['TSR'+ segSeq +'13'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "14") > -1) {
                    TSR_Item['TSR'+ segSeq +'14'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "16") > -1) {
                    TSR_Item['TSR'+ segSeq +'16'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "17") > -1) {
                    TSR_Item['TSR'+ segSeq +'17'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "18") > -1) {
                    TSR_Item['TSR'+ segSeq +'18'] = itemList[1]
                } else {
                    TSR_List.push(TSR_Item)
                    break
                }
            }
        }
        return TSR_List
    }

    public static async getDataTMP(lineList:string[], idx:number, segSeq:string) {
        let TMP_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let TMP_List:any[] = []
        let TMP_SEQ:string = "TMP" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TMP_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                TMP_Item[TMP_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(TMP_SEQ) > -1 && flag) {
                TMP_List.push(TMP_Item)
                
                TMP_Item = {}
                itemList = line.split(":")
                TMP_Item[TMP_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("TMP" + segSeq + "02") > -1) {
                    TMP_Item['TMP'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("TMP" + segSeq + "03") > -1) {
                    TMP_Item['TMP'+ segSeq +'03'] = itemList[1]
                } else {
                    TMP_List.push(TMP_Item)
                    break
                }
            }
        }
        return TMP_List
    }

    public static async getDataRNG(lineList:string[], idx:number, segSeq:string) {
        let RNG_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let RNG_List:any[] = []
        let RNG_SEQ:string = "RNG" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(RNG_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                RNG_Item[RNG_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(RNG_SEQ) > -1 && flag) {
                RNG_List.push(RNG_Item)
                
                RNG_Item = {}
                itemList = line.split(":")
                RNG_Item[RNG_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("RNG" + segSeq + "02") > -1) {
                    RNG_Item['RNG'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("RNG" + segSeq + "03") > -1) {
                    RNG_Item['RNG'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("RNG" + segSeq + "04") > -1) {
                    RNG_Item['RNG'+ segSeq +'04'] = itemList[1]
                } else {
                    RNG_List.push(RNG_Item)
                    break
                }
            }
        }
        return RNG_List
    }

    public static async getDataSEL(lineList:string[], idx:number, segSeq:string) {
        let SEL_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let SEL_List:any[] = []
        let SEL_SEQ:string = "SEL" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(SEL_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                SEL_Item[SEL_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(SEL_SEQ) > -1 && flag) {
                SEL_List.push(SEL_Item)
                
                SEL_Item = {}
                itemList = line.split(":")
                SEL_Item[SEL_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("SEL" + segSeq + "02") > -1) {
                    SEL_Item['SEL'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("SEL" + segSeq + "03") > -1) {
                    SEL_Item['SEL'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("SEL" + segSeq + "04") > -1) {
                    SEL_Item['SEL'+ segSeq +'04'] = itemList[1]
                } else if (line.indexOf("SEL" + segSeq + "05") > -1) {
                    SEL_Item['SEL'+ segSeq +'05'] = itemList[1]
                } else if (line.indexOf("SEL" + segSeq + "06") > -1) {
                    SEL_Item['SEL'+ segSeq +'06'] = itemList[1]
                } else {
                    SEL_List.push(SEL_Item)
                    break
                }
            }
        }
        return SEL_List
    }

    public static async getDataMOA(lineList:string[], idx:number, segSeq:string) {
        let MOA_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let MOA_List:any[] = []
        let MOA_SEQ:string = "MOA" + segSeq + "01"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(MOA_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                MOA_Item[MOA_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(MOA_SEQ) > -1 && flag) {
                MOA_List.push(MOA_Item)
                
                MOA_Item = {}
                itemList = line.split(":")
                MOA_Item[MOA_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("MOA" + segSeq + "02") > -1) {
                    MOA_Item['MOA'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("MOA" + segSeq + "03") > -1) {
                    MOA_Item['MOA'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("MOA" + segSeq + "04") > -1) {
                    MOA_Item['MOA'+ segSeq +'04'] = itemList[1]
                } else if (line.indexOf("MOA" + segSeq + "05") > -1) {
                    MOA_Item['MOA'+ segSeq +'05'] = itemList[1]
                } else {
                    MOA_List.push(MOA_Item)
                    break
                }
            }
        }
        return MOA_List
    }

    public static async getDataGOR(lineList:string[], idx:number, segSeq:string) {
        let GOR_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let GOR_List:any[] = []
        let GOR_SEQ:string = "GOR" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(GOR_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                GOR_Item[GOR_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(GOR_SEQ) > -1 && flag) {
                GOR_List.push(GOR_Item)
                
                GOR_Item = {}
                itemList = line.split(":")
                GOR_Item[GOR_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("GOR" + segSeq + "02") > -1) {
                    GOR_Item['GOR'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "03") > -1) {
                    GOR_Item['GOR'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "04") > -1) {
                    GOR_Item['GOR'+ segSeq +'04'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "05") > -1) {
                    GOR_Item['GOR'+ segSeq +'05'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "07") > -1) {
                    GOR_Item['GOR'+ segSeq +'07'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "08") > -1) {
                    GOR_Item['GOR'+ segSeq +'08'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "09") > -1) {
                    GOR_Item['GOR'+ segSeq +'09'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "10") > -1) {
                    GOR_Item['GOR'+ segSeq +'10'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "12") > -1) {
                    GOR_Item['GOR'+ segSeq +'12'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "13") > -1) {
                    GOR_Item['GOR'+ segSeq +'13'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "14") > -1) {
                    GOR_Item['GOR'+ segSeq +'14'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "15") > -1) {
                    GOR_Item['GOR'+ segSeq +'15'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "17") > -1) {
                    GOR_Item['GOR'+ segSeq +'17'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "18") > -1) {
                    GOR_Item['GOR'+ segSeq +'18'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "19") > -1) {
                    GOR_Item['GOR'+ segSeq +'19'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "20") > -1) {
                    GOR_Item['GOR'+ segSeq +'20'] = itemList[1]
                } else {
                    GOR_List.push(GOR_Item)
                    break
                }
            }
        }
        return GOR_List
    }
    
    public static async getDataDGS(lineList:string[], idx:number, segSeq:string) {
        let DGS_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let DGS_List:any[] = []
        let DGS_SEQ:string = "DGS" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DGS_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                DGS_Item[DGS_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(DGS_SEQ) > -1 && flag) {
                DGS_List.push(DGS_Item)
                
                DGS_Item = {}
                itemList = line.split(":")
                DGS_Item[DGS_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("DGS" + segSeq + "02") > -1) {
                    DGS_Item['DGS'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "03") > -1) {
                    DGS_Item['DGS'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "04") > -1) {
                    DGS_Item['DGS'+ segSeq +'04'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "06") > -1) {
                    DGS_Item['DGS'+ segSeq +'06'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "07") > -1) {
                    DGS_Item['DGS'+ segSeq +'07'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "09") > -1) {
                    DGS_Item['DGS'+ segSeq +'09'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "10") > -1) {
                    DGS_Item['DGS'+ segSeq +'10'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "12") > -1) {
                    DGS_Item['DGS'+ segSeq +'12'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "13") > -1) {
                    DGS_Item['DGS'+ segSeq +'13'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "14") > -1) {
                    DGS_Item['DGS'+ segSeq +'14'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "16") > -1) {
                    DGS_Item['DGS'+ segSeq +'16'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "17") > -1) {
                    DGS_Item['DGS'+ segSeq +'17'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "19") > -1) {
                    DGS_Item['DGS'+ segSeq +'19'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "20") > -1) {
                    DGS_Item['DGS'+ segSeq +'20'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "21") > -1) {
                    DGS_Item['DGS'+ segSeq +'21'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "22") > -1) {
                    DGS_Item['DGS'+ segSeq +'22'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "23") > -1) {
                    DGS_Item['DGS'+ segSeq +'23'] = itemList[1]
                } else if (line.indexOf("DGS" + segSeq + "24") > -1) {
                    DGS_Item['DGS'+ segSeq +'24'] = itemList[1]
                } else {
                    DGS_List.push(DGS_Item)
                    break
                }
            }
        }
        return DGS_List
    }

    public static async getDataCOM(lineList:string[], idx:number, segSeq:string) {
        let COM_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let COM_List:any[] = []
        let COM_SEQ:string = 'COM' + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(COM_SEQ + '01') > -1 && !flag) {
                itemList = line.split(":")
                COM_Item[COM_SEQ + '01'] = itemList[1]

                flag = true;
            } else if (line.indexOf(COM_SEQ + '01') > -1 && flag) {
                COM_List.push(COM_Item)
                
                COM_Item = {}
                itemList = line.split(":")
                COM_Item[COM_SEQ + '01'] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(COM_SEQ + "02") > -1) {
                    COM_Item[COM_SEQ + '02'] = itemList[1]
                } else {
                    COM_List.push(COM_Item)
                    break
                }
            }
        }
        return COM_List
    }

    public static async getDataValue(data:string) {
        if (!data) {
            return ""
        }
        return data.trim()
    }

    public static async getDataSGP(lineList:string[], idx:number, segSeq:string) {
        let SGP_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let SGP_List:any[] = []
        let SGP_SEQ:string = "SGP" + segSeq + "01"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(SGP_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                SGP_Item[SGP_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(SGP_SEQ) > -1 && flag) {
                SGP_List.push(SGP_Item)
                
                SGP_Item = {}
                itemList = line.split(":")
                SGP_Item[SGP_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("SGP" + segSeq + "02") > -1) {
                    SGP_Item['SGP'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("SGP" + segSeq + "03") > -1) {
                    SGP_Item['SGP'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("SGP" + segSeq + "04") > -1) {
                    SGP_Item['SGP'+ segSeq +'04'] = itemList[1]
                } else if (line.indexOf("SGP" + segSeq + "05") > -1) {
                    SGP_Item['SGP'+ segSeq +'05'] = itemList[1]
                } else {
                    SGP_List.push(SGP_Item)
                    break
                }
            }
        }
        return SGP_List
    }

    public static async getDataDAMGroup(lineList:string[], idx:number, segSeq:string) {
        let DAM_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let DAM_List:any[] = []
        let DAM_SEQ:string = "DAM" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DAM_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                DAM_Item[DAM_SEQ + "00"] = itemList[1]

                flag = true;
            } else if (line.indexOf(DAM_SEQ + "00") > -1 && flag) {
                DAM_List.push(DAM_Item)
                
                DAM_Item = {}
                itemList = line.split(":")
                DAM_Item[DAM_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(DAM_SEQ + "02") > -1) {
                    DAM_Item[DAM_SEQ + "02"] = itemList[1]
                } else if (line.indexOf(DAM_SEQ + "03") > -1) {
                    DAM_Item[DAM_SEQ + "03"] = itemList[1]
                } else if (line.indexOf(DAM_SEQ + "04") > -1) {
                    DAM_Item[DAM_SEQ + "04"] = itemList[1]
                } else if (line.indexOf(DAM_SEQ + "05") > -1) {
                    DAM_Item[DAM_SEQ + "05"] = itemList[1]
                } else if (line.indexOf(DAM_SEQ + "07") > -1) {
                    DAM_Item[DAM_SEQ + "07"] = itemList[1]
                } else if (line.indexOf(DAM_SEQ + "09") > -1) {
                    DAM_Item[DAM_SEQ + "09"] = itemList[1]
                } else if (line.indexOf(DAM_SEQ + "10") > -1) {
                    DAM_Item[DAM_SEQ + "10"] = itemList[1]
                } else if (line.indexOf(DAM_SEQ + "12") > -1) {
                    DAM_Item[DAM_SEQ + "12"] = itemList[1]
                } else if (line.indexOf(DAM_SEQ + "13") > -1) {
                    DAM_Item[DAM_SEQ + "13"] = itemList[1]
                } else if (line.indexOf(DAM_SEQ + "14") > -1) {
                    DAM_Item[DAM_SEQ + "14"] = itemList[1]
                } else if (line.indexOf(DAM_SEQ + "15") > -1) {
                    DAM_Item[DAM_SEQ + "15"] = itemList[1]
                } else if (line.indexOf(DAM_SEQ + "17") > -1) {
                    DAM_Item[DAM_SEQ + "17"] = itemList[1]
                } else if (line.indexOf(DAM_SEQ + "18") > -1) {
                    DAM_Item[DAM_SEQ + "18"] = itemList[1]
                } else if (line.indexOf(DAM_SEQ + "19") > -1) {
                    DAM_Item[DAM_SEQ + "19"] = itemList[1]
                } else if (line.indexOf(DAM_SEQ + "20") > -1) {
                    DAM_Item[DAM_SEQ + "20"] = itemList[1]
                } else if (line.indexOf("COD") > -1) {
                    let itemArray:string[] = line.split(":")
                    let COD_SEQ:string = itemArray[0].trim()
                    COD_SEQ = COD_SEQ.substr(3, 6) 
                    DAM_Item["COD" + COD_SEQ] = itemList[1]
                } else if (line.indexOf("TDT") > -1 || line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    DAM_List.push(DAM_Item)
                    break
                }
            }
            
        }

        return DAM_List
    }

    public static async getDataGIDGroup(lineList:string[], idx:number, segSeq:string) {
        let GID_Item:any = {}
        let line:string
        let flag:boolean = false
        let HAN_Flag:boolean = false
        let FTX_Flag:boolean = false
        let RFF_Flag:boolean = false
        let PIA_Flag:boolean = false
        let NAD_Flag:boolean = false
        let MEA_Flag:boolean = false
        let DIM_Flag:boolean = false
        let DOC_Flag:boolean = false
        let SGP_Flag:boolean = false
        let DGS_Flag:boolean = false
        let TMP_Flag:boolean = false
        let RNG_Flag:boolean = false
        let TMD_Flag:boolean = false
        let LOC_Flag:boolean = false
        let MOA_Flag:boolean = false
        let GDS_Flag:boolean = false
        let PCI_Flag:boolean = false
        let TPL_Flag:boolean = false
        let TCC_Flag:boolean = false
        let itemList:string[]
        let GID_List:any[] = []
        let GID_SEQ:string = "GID" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(GID_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                GID_Item[GID_SEQ + "00"] = itemList[1]

                flag = true;
            } else if (line.indexOf(GID_SEQ + "00") > -1 && flag) {
                GID_List.push(GID_Item)
                
                GID_Item = {}
                itemList = line.split(":")
                GID_Item[GID_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(GID_SEQ + "02") > -1) {
                    GID_Item[GID_SEQ + "02"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "03") > -1) {
                    GID_Item[GID_SEQ + "03"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "04") > -1) {
                    GID_Item[GID_SEQ + "04"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "05") > -1) {
                    GID_Item[GID_SEQ + "05"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "06") > -1) {
                    GID_Item[GID_SEQ + "06"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "08") > -1) {
                    GID_Item[GID_SEQ + "08"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "09") > -1) {
                    GID_Item[GID_SEQ + "09"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "10") > -1) {
                    GID_Item[GID_SEQ + "10"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "11") > -1) {
                    GID_Item[GID_SEQ + "11"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "12") > -1) {
                    GID_Item[GID_SEQ + "12"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "14") > -1) {
                    GID_Item[GID_SEQ + "14"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "15") > -1) {
                    GID_Item[GID_SEQ + "15"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "16") > -1) {
                    GID_Item[GID_SEQ + "16"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "17") > -1) {
                    GID_Item[GID_SEQ + "17"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "18") > -1) {
                    GID_Item[GID_SEQ + "18"] = itemList[1]
                } else if (line.indexOf("HAN") > -1 && !HAN_Flag) {
                    HAN_Flag = true
                    let itemArray:string[] = line.split(":")
                    let HAN_SEQ:string = itemArray[0].trim()
                    HAN_SEQ = HAN_SEQ.substr(3, 4)
                    GID_Item["HAN" + HAN_SEQ] = await this.getDataHAN(lineList, i, HAN_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_Flag) {
                    FTX_Flag = true
                    let itemArray:string[] = line.split(":")
                    let FTX_SEQ:string = itemArray[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)
                    GID_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("RFF") > -1 && !RFF_Flag) {
                    RFF_Flag = true
                    let itemArray:string[] = line.split(":")
                    let RFF_SEQ:string = itemArray[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4)
                    let GID_SEQ_GRP:string = segSeq.substr(0, 2)
                    let RFF_SEQ_GRP:string = RFF_SEQ.substr(0, 2) 
                    if (GID_SEQ_GRP === RFF_SEQ_GRP) {
                        GID_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                    } else {
                        GID_Item["RFF" + RFF_SEQ] = await this.getDataRFFGroup(lineList, i, RFF_SEQ)
                    }
                } else if (line.indexOf("PIA") > -1 && !PIA_Flag) {
                    PIA_Flag = true
                    let itemArray:string[] = line.split(":")
                    let PIA_SEQ:string = itemArray[0].trim()
                    PIA_SEQ = PIA_SEQ.substr(3, 4)
                    GID_Item["PIA" + PIA_SEQ] = await this.getDataPIA(lineList, i, PIA_SEQ)
                } else if (line.indexOf("NAD") > -1 && !NAD_Flag) {
                    NAD_Flag = true
                    let itemArray:string[] = line.split(":")
                    let NAD_SEQ:string = itemArray[0].trim()
                    NAD_SEQ = NAD_SEQ.substr(3, 4)
                    GID_Item["NAD" + NAD_SEQ] = await this.getDataNADGroup(lineList, i, NAD_SEQ)
                } else if (line.indexOf("MEA") > -1 && !MEA_Flag) {
                    MEA_Flag = true
                    let itemArray:string[] = line.split(":")
                    let MEA_SEQ:string = itemArray[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4)
                    let GID_SEQ_GRP:string = segSeq.substr(0, 2)
                    let MEA_SEQ_GRP:string = MEA_SEQ.substr(0, 2) 
                    if (GID_SEQ_GRP === MEA_SEQ_GRP) {
                        GID_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                    } else {
                        GID_Item["MEA" + MEA_SEQ] = await this.getDataMEAGroup(lineList, i, MEA_SEQ)
                    }
                } else if (line.indexOf("DIM") > -1 && !DIM_Flag) {
                    DIM_Flag = true
                    let itemArray:string[] = line.split(":")
                    let DIM_SEQ:string = itemArray[0].trim()
                    DIM_SEQ = DIM_SEQ.substr(3, 4)
                    let GID_SEQ_GRP:string = segSeq.substr(0, 2)
                    let DIM_SEQ_GRP:string = DIM_SEQ.substr(0, 2) 
                    if (GID_SEQ_GRP === DIM_SEQ_GRP) {
                        GID_Item["DIM" + DIM_SEQ] = await this.getDataDIM(lineList, i, DIM_SEQ)
                    } else {
                        GID_Item["DIM" + DIM_SEQ] = await this.getDataDIMGroup(lineList, i, DIM_SEQ)
                    }
                } else if (line.indexOf("DOC") > -1 && !DOC_Flag) {
                    DOC_Flag = true
                    let itemArray:string[] = line.split(":")
                    let DOC_SEQ:string = itemArray[0].trim()
                    DOC_SEQ = DOC_SEQ.substr(3, 4)
                    let GID_SEQ_GRP:string = segSeq.substr(0, 2)
                    let DOC_SEQ_GRP:string = DOC_SEQ.substr(0, 2) 
                    if (GID_SEQ_GRP === DOC_SEQ_GRP) {
                        GID_Item["DOC" + DOC_SEQ] = await this.getDataDOC(lineList, i, DOC_SEQ)
                    } else {
                        GID_Item["DOC" + DOC_SEQ] = await this.getDataDOCGroup(lineList, i, DOC_SEQ)
                    }
                } else if (line.indexOf("SGP") > -1 && !SGP_Flag) {
                    SGP_Flag = true
                    let itemArray:string[] = line.split(":")
                    let SGP_SEQ:string = itemArray[0].trim()
                    SGP_SEQ = SGP_SEQ.substr(3, 4)
                    GID_Item["SGP" + SGP_SEQ] = await this.getDataSGPGroup(lineList, i, SGP_SEQ)
                } else if (line.indexOf("DGS") > -1 && !DGS_Flag) {
                    DGS_Flag = true
                    let itemArray:string[] = line.split(":")
                    let DGS_SEQ:string = itemArray[0].trim()
                    DGS_SEQ = DGS_SEQ.substr(3, 4)
                    GID_Item["DGS" + DGS_SEQ] = await this.getDataDGSGroup(lineList, i, DGS_SEQ)
                } else if (line.indexOf("TMP") > -1 && !TMP_Flag) {
                    TMP_Flag = true
                    let itemArray:string[] = line.split(":")
                    let TMP_SEQ:string = itemArray[0].trim()
                    TMP_SEQ = TMP_SEQ.substr(3, 4)
                    GID_Item["TMP" + TMP_SEQ] = await this.getDataTMP(lineList, i, TMP_SEQ)
                } else if (line.indexOf("RNG") > -1 && !RNG_Flag) {
                    RNG_Flag = true
                    let itemArray:string[] = line.split(":")
                    let RNG_SEQ:string = itemArray[0].trim()
                    RNG_SEQ = RNG_SEQ.substr(3, 4)
                    GID_Item["RNG" + RNG_SEQ] = await this.getDataRNG(lineList, i, RNG_SEQ)
                } else if (line.indexOf("TMD") > -1 && !TMD_Flag) {
                    TMD_Flag = true
                    let itemArray:string[] = line.split(":")
                    let TMD_SEQ:string = itemArray[0].trim()
                    TMD_SEQ = TMD_SEQ.substr(3, 4)
                    GID_Item["TMD" + TMD_SEQ] = await this.getDataTMD(lineList, i, TMD_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_Flag) {
                    LOC_Flag = true
                    let itemArray:string[] = line.split(":")
                    let LOC_SEQ:string = itemArray[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)
                    GID_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("MOA") > -1 && !MOA_Flag) {
                    MOA_Flag = true
                    let itemArray:string[] = line.split(":")
                    let MOA_SEQ:string = itemArray[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4)
                    GID_Item["MOA" + MOA_SEQ] = await this.getDataMOA(lineList, i, MOA_SEQ)
                } else if (line.indexOf("GDS") > -1 && !GDS_Flag) {
                    GDS_Flag = true
                    let itemArray:string[] = line.split(":")
                    let GDS_SEQ:string = itemArray[0].trim()
                    GDS_SEQ = GDS_SEQ.substr(3, 4)
                    GID_Item["GDS" + GDS_SEQ] = await this.getDataGDS(lineList, i, GDS_SEQ)
                } else if (line.indexOf("PCI") > -1 && !PCI_Flag) {
                    PCI_Flag = true
                    let itemArray:string[] = line.split(":")
                    let PCI_SEQ:string = itemArray[0].trim()
                    PCI_SEQ = PCI_SEQ.substr(3, 4)
                    GID_Item["PCI" + PCI_SEQ] = await this.getDataPCIGroup(lineList, i, PCI_SEQ)
                } else if (line.indexOf("TPL") > -1 && !TPL_Flag) {
                    TPL_Flag = true
                    let itemArray:string[] = line.split(":")
                    let TPL_SEQ:string = itemArray[0].trim()
                    TPL_SEQ = TPL_SEQ.substr(3, 4)
                    let GID_SEQ_GRP:string = segSeq.substr(0, 2)
                    let TPL_SEQ_GRP:string = TPL_SEQ.substr(0, 2) 
                    if (GID_SEQ_GRP === TPL_SEQ_GRP) {
                        GID_Item["TPL" + TPL_SEQ] = await this.getDataTPL(lineList, i, TPL_SEQ)
                    } else {
                        GID_Item["TPL" + TPL_SEQ] = await this.getDataTPLGroup(lineList, i, TPL_SEQ)
                    }
                } else if (line.indexOf("TCC") > -1 && !TCC_Flag) {
                    TCC_Flag = true
                    let itemArray:string[] = line.split(":")
                    let TCC_SEQ:string = itemArray[0].trim()
                    TCC_SEQ = TCC_SEQ.substr(3, 4)
                    GID_Item["TCC" + TCC_SEQ] = await this.getDataTCCGroup(lineList, i, TCC_SEQ)
                } else if (line.indexOf("EQD") > -1 || line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    GID_List.push(GID_Item)
                    break
                }
            }
        }
        return GID_List
    }    

    public static async getDataTDTGroup(lineList:string[], idx:number, segSeq:string) {
        let TDT_Item:any = {}
        let line:string
        let flag:boolean = false
        let RFF_flag:boolean = false
        let LOC_flag:boolean = false
        let DTM_flag:boolean = false
        let FTX_flag:boolean = false
        let TSR_flag:boolean = false
        let itemList:string[]
        let TDT_List:any[] = []
        let TDT_SEQ:string = "TDT" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TDT_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                TDT_Item[TDT_SEQ + "00"] = itemList[1]

                flag = true;
            } else if (line.indexOf(TDT_SEQ + "00") > -1 && flag) {
                TDT_List.push(TDT_Item)
                
                TDT_Item = {}
                itemList = line.split(":")
                TDT_Item[TDT_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(TDT_SEQ + "01") > -1) {
                    TDT_Item[TDT_SEQ + "01"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "03") > -1) {
                    TDT_Item[TDT_SEQ + "03"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "04") > -1) {
                    TDT_Item[TDT_SEQ + "04"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "05") > -1) {
                    TDT_Item[TDT_SEQ + "05"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "06") > -1) {
                    TDT_Item[TDT_SEQ + "06"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "07") > -1) {
                    TDT_Item[TDT_SEQ + "07"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "09") > -1) {
                    TDT_Item[TDT_SEQ + "09"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "10") > -1) {
                    TDT_Item[TDT_SEQ + "10"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "11") > -1) {
                    TDT_Item[TDT_SEQ + "11"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "12") > -1) {
                    TDT_Item[TDT_SEQ + "12"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "13") > -1) {
                    TDT_Item[TDT_SEQ + "13"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "15") > -1) {
                    TDT_Item[TDT_SEQ + "15"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "16") > -1) {
                    TDT_Item[TDT_SEQ + "16"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "17") > -1) {
                    TDT_Item[TDT_SEQ + "17"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "19") > -1) {
                    TDT_Item[TDT_SEQ + "19"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "20") > -1) {
                    TDT_Item[TDT_SEQ + "20"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "21") > -1) {
                    TDT_Item[TDT_SEQ + "21"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "22") > -1) {
                    TDT_Item[TDT_SEQ + "22"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "23") > -1) {
                    TDT_Item[TDT_SEQ + "23"] = itemList[1]
                } else if (line.indexOf(TDT_SEQ + "24") > -1) {
                    TDT_Item[TDT_SEQ + "24"] = itemList[1]
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    let itemArray:string[] = line.split(":")
                    let RFF_SEQ:string = itemArray[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4) 
                    let TDT_SEQ_GRP:string = segSeq.substr(0, 2)
                    let RFF_SEQ_GRP:string = RFF_SEQ.substr(0, 2) 
                    if (RFF_SEQ_GRP === TDT_SEQ_GRP) {
                        TDT_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                    } else {
                        TDT_Item["RFF" + RFF_SEQ] = await this.getDataRFFGroup(lineList, i, RFF_SEQ)
                    }
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    let itemArray:string[] = line.split(":")
                    let LOC_SEQ:string = itemArray[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4) 
                    let TDT_SEQ_GRP:string = segSeq.substr(0, 2)
                    let LOC_SEQ_GRP:string = LOC_SEQ.substr(0, 2) 
                    if (LOC_SEQ_GRP === TDT_SEQ_GRP) {
                        TDT_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                    } else {
                        TDT_Item["LOC" + LOC_SEQ] = await this.getDataLOCGroup(lineList, i, LOC_SEQ)
                    }                    
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    let itemArray:string[] = line.split(":")
                    let DTM_SEQ:string = itemArray[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4) 
                    let TDT_SEQ_GRP:string = segSeq.substr(0, 2)
                    let DTM_SEQ_GRP:string = DTM_SEQ.substr(0, 2) 
                    if (DTM_SEQ_GRP === TDT_SEQ_GRP) {
                        TDT_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                    }
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    let itemArray:string[] = line.split(":")
                    let FTX_SEQ:string = itemArray[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4) 
                    TDT_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("TSR") > -1 && !TSR_flag) {
                    TSR_flag = true
                    let itemArray:string[] = line.split(":")
                    let TSR_SEQ:string = itemArray[0].trim()
                    TSR_SEQ = TSR_SEQ.substr(3, 4) 
                    TDT_Item["TSR" + TSR_SEQ] = await this.getDataTSR(lineList, i, TSR_SEQ)
                } else if (line.indexOf("NAD") > -1 || line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    TDT_List.push(TDT_Item)
                    break
                } 
            }
            
        }

        return TDT_List
    }

    public static async getDataNADGroup(lineList:string[], idx:number, segSeq:string) {
        let NAD_Item:any = {}
        let line:string
        let flag:boolean = false
        let CTA_flag:boolean = false
        let RFF_flag:boolean = false
        let DTM_flag:boolean = false
        let LOC_flag:boolean = false
        let DOC_flag:boolean = false
        let TSR_flag:boolean = false
        let CPI_flag:boolean = false
        let TCC_flag:boolean = false
        let MOA_flag:boolean = false
        let itemList:string[]
        let NAD_List:any[] = []
        let NAD_SEQ:string = "NAD" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(NAD_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                NAD_Item[NAD_SEQ + "00"] = itemList[1]

                flag = true;
            } else if (line.indexOf(NAD_SEQ + "00") > -1 && flag) {
                NAD_List.push(NAD_Item)
                
                NAD_Item = {}
                itemList = line.split(":")
                NAD_Item[NAD_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(NAD_SEQ + "02") > -1) {
                    NAD_Item[NAD_SEQ + "02"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "03") > -1) {
                    NAD_Item[NAD_SEQ + "03"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "04") > -1) {
                    NAD_Item[NAD_SEQ + "04"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "06") > -1) {
                    NAD_Item[NAD_SEQ + "06"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "07") > -1) {
                    NAD_Item[NAD_SEQ + "07"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "08") > -1) {
                    NAD_Item[NAD_SEQ + "08"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "09") > -1) {
                    NAD_Item[NAD_SEQ + "09"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "10") > -1) {
                    NAD_Item[NAD_SEQ + "10"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "12") > -1) {
                    NAD_Item[NAD_SEQ + "12"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "13") > -1) {
                    NAD_Item[NAD_SEQ + "13"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "14") > -1) {
                    NAD_Item[NAD_SEQ + "14"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "15") > -1) {
                    NAD_Item[NAD_SEQ + "15"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "16") > -1) {
                    NAD_Item[NAD_SEQ + "16"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "17") > -1) {
                    NAD_Item[NAD_SEQ + "17"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "19") > -1) {
                    NAD_Item[NAD_SEQ + "19"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "20") > -1) {
                    NAD_Item[NAD_SEQ + "20"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "21") > -1) {
                    NAD_Item[NAD_SEQ + "21"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "22") > -1) {
                    NAD_Item[NAD_SEQ + "22"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "23") > -1) {
                    NAD_Item[NAD_SEQ + "23"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "24") > -1) {
                    NAD_Item[NAD_SEQ + "24"] = itemList[1]
                } else if (line.indexOf(NAD_SEQ + "25") > -1) {
                    NAD_Item[NAD_SEQ + "25"] = itemList[1]
                } else if (line.indexOf("CTA") > -1 && !CTA_flag) {
                    CTA_flag = true
                    let itemArray:string[] = line.split(":")
                    let CTA_SEQ:string = itemArray[0].trim()
                    CTA_SEQ = CTA_SEQ.substr(3, 4) 
                    let NAD_SEQ_GRP:string = segSeq.substr(0, 2)
                    let CTA_SEQ_GRP:string = CTA_SEQ.substr(0, 2) 
                    if (CTA_SEQ_GRP === NAD_SEQ_GRP) {
                        NAD_Item["CTA" + CTA_SEQ] = await this.getDataCTA(lineList, i, CTA_SEQ)
                    } else {
                        NAD_Item["CTA" + CTA_SEQ] = await this.getDataCTAGroup(lineList, i, CTA_SEQ)
                    }
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    let itemArray:string[] = line.split(":")
                    let RFF_SEQ:string = itemArray[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4) 
                    let NAD_SEQ_GRP:string = segSeq.substr(0, 2)
                    let RFF_SEQ_GRP:string = RFF_SEQ.substr(0, 2) 
                    if (RFF_SEQ_GRP === NAD_SEQ_GRP) {
                        NAD_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                    } else {
                        NAD_Item["RFF" + RFF_SEQ] = await this.getDataRFFGroup(lineList, i, RFF_SEQ)
                    }
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    let itemArray:string[] = line.split(":")
                    let DTM_SEQ:string = itemArray[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4) 
                    NAD_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("COM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    let itemArray:string[] = line.split(":")
                    let COM_SEQ:string = itemArray[0].trim()
                    COM_SEQ = COM_SEQ.substr(3, 4) 
                    NAD_Item["COM" + COM_SEQ] = await this.getDataCOM(lineList, i, COM_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    let itemArray:string[] = line.split(":")
                    let LOC_SEQ:string = itemArray[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4) 
                    NAD_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("DOC") > -1 && !DOC_flag) {
                    DOC_flag = true
                    let itemArray:string[] = line.split(":")
                    let DOC_SEQ:string = itemArray[0].trim()
                    DOC_SEQ = DOC_SEQ.substr(3, 4) 
                    let NAD_SEQ_GRP:string = segSeq.substr(0, 2)
                    let DOC_SEQ_GRP:string = DOC_SEQ.substr(0, 2) 
                    if (DOC_SEQ_GRP === NAD_SEQ_GRP) {
                        NAD_Item["DOC" + DOC_SEQ] = await this.getDataDOC(lineList, i, DOC_SEQ)
                    } else {
                        NAD_Item["DOC" + DOC_SEQ] = await this.getDataDOCGroup(lineList, i, DOC_SEQ)
                    }
                } else if (line.indexOf("CPI") > -1 && !CPI_flag) {
                    CPI_flag = true
                    let itemArray:string[] = line.split(":")
                    let CPI_SEQ:string = itemArray[0].trim()
                    CPI_SEQ = CPI_SEQ.substr(3, 4) 
                    let NAD_SEQ_GRP:string = segSeq.substr(0, 2)
                    let CPI_SEQ_GRP:string = CPI_SEQ.substr(0, 2) 
                    if (CPI_SEQ_GRP === NAD_SEQ_GRP) {
                        NAD_Item["CPI" + CPI_SEQ] = await this.getDataCPI(lineList, i, CPI_SEQ)
                    } else {
                        NAD_Item["CPI" + CPI_SEQ] = await this.getDataCPIGroup(lineList, i, CPI_SEQ)
                    }
                } else if (line.indexOf("TSR") > -1 && !TSR_flag) {
                    TSR_flag = true
                    let itemArray:string[] = line.split(":")
                    let TSR_SEQ:string = itemArray[0].trim()
                    TSR_SEQ = TSR_SEQ.substr(3, 4) 
                    let NAD_SEQ_GRP:string = segSeq.substr(0, 2)
                    let TSR_SEQ_GRP:string = TSR_SEQ.substr(0, 2) 
                    if (TSR_SEQ_GRP === NAD_SEQ_GRP) {
                        NAD_Item["TSR" + TSR_SEQ] = await this.getDataTSR(lineList, i, TSR_SEQ)
                    } else {
                        NAD_Item["TSR" + TSR_SEQ] = await this.getDataTSRGroup(lineList, i, TSR_SEQ)
                    }
                } else if (line.indexOf("TCC") > -1 && !TCC_flag) {
                    TCC_flag = true
                    let itemArray:string[] = line.split(":")
                    let TCC_SEQ:string = itemArray[0].trim()
                    TCC_SEQ = TCC_SEQ.substr(3, 4) 
                    NAD_Item["TCC" + TCC_SEQ] = await this.getDataTCCGroup(lineList, i, TCC_SEQ)                    
                } else if (line.indexOf("MOA") > -1 && !MOA_flag) {
                    MOA_flag = true
                    let itemArray:string[] = line.split(":")
                    let MOA_SEQ:string = itemArray[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4) 
                    NAD_Item["MOA" + MOA_SEQ] = await this.getDataMOA(lineList, i, MOA_SEQ)
                } else if (line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    NAD_List.push(NAD_Item)
                    break
                }
            }
            
        }

        return NAD_List
    }

    public static async getDataSGPGroup(lineList:string[], idx:number, segSeq:string) {
        let SGP_Item:any = {}
        let line:string
        let flag:boolean = false
        let MEA_flag:boolean = false
        let itemList:string[]
        let SGP_List:any[] = []
        let SGP_SEQ:string = "SGP" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(SGP_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                SGP_Item[SGP_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(SGP_SEQ + "01") > -1 && flag) {
                SGP_List.push(SGP_Item)
                
                SGP_Item = {}
                itemList = line.split(":")
                SGP_Item[SGP_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(SGP_SEQ + "02") > -1) {
                    SGP_Item[SGP_SEQ + "02"] = itemList[1]
                } else if (line.indexOf(SGP_SEQ + "03") > -1) {
                    SGP_Item[SGP_SEQ + "03"] = itemList[1]
                } else if (line.indexOf(SGP_SEQ + "04") > -1) {
                    SGP_Item[SGP_SEQ + "04"] = itemList[1]
                } else if (line.indexOf(SGP_SEQ + "05") > -1) {
                    SGP_Item[SGP_SEQ + "05"] = itemList[1]
                } else if (line.indexOf("MEA") > -1 && !MEA_flag) {//MEA050100
                    MEA_flag = true
                    let itemArray:string[] = line.split(":")
                    let MEA_SEQ:string = itemArray[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4) 
                    SGP_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                } else if (line.indexOf("DGS") > -1 || line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    SGP_List.push(SGP_Item)
                    break
                }
            }
            
        }

        return SGP_List
    }

    public static async getDataDGSGroup(lineList:string[], idx:number, segSeq:string) {
        let DGS_Item:any = {}
        let line:string
        let flag:boolean = false
        let FTX_Flag:boolean = false
        let MEA_Flag:boolean = false
        let CTA_Flag:boolean = false
        let SGP_Flag:boolean = false
        let itemList:string[]
        let DGS_List:any[] = []
        let DGS_SEQ:string = "DGS" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DGS_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                DGS_Item[DGS_SEQ + "00"] = itemList[1]

                flag = true;
            } else if (line.indexOf(DGS_SEQ + "00") > -1 && flag) {
                DGS_List.push(DGS_Item)
                DGS_Item = {}
                itemList = line.split(":")
                DGS_Item[DGS_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(DGS_SEQ + "02") > -1) {
                    DGS_Item[DGS_SEQ + "02"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "03") > -1) {
                    DGS_Item[DGS_SEQ + "03"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "04") > -1) {
                    DGS_Item[DGS_SEQ + "04"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "06") > -1) {
                    DGS_Item[DGS_SEQ + "06"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "07") > -1) {
                    DGS_Item[DGS_SEQ + "07"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "09") > -1) {
                    DGS_Item[DGS_SEQ + "09"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "10") > -1) {
                    DGS_Item[DGS_SEQ + "10"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "11") > -1) {
                    DGS_Item[DGS_SEQ + "11"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "12") > -1) {
                    DGS_Item[DGS_SEQ + "12"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "13") > -1) {
                    DGS_Item[DGS_SEQ + "13"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "14") > -1) {
                    DGS_Item[DGS_SEQ + "14"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "16") > -1) {
                    DGS_Item[DGS_SEQ + "16"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "17") > -1) {
                    DGS_Item[DGS_SEQ + "17"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "19") > -1) {
                    DGS_Item[DGS_SEQ + "19"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "20") > -1) {
                    DGS_Item[DGS_SEQ + "20"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "21") > -1) {
                    DGS_Item[DGS_SEQ + "21"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "22") > -1) {
                    DGS_Item[DGS_SEQ + "22"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "23") > -1) {
                    DGS_Item[DGS_SEQ + "23"] = itemList[1]
                } else if (line.indexOf(DGS_SEQ + "24") > -1) {
                    DGS_Item[DGS_SEQ + "24"] = itemList[1]
                } else if (line.indexOf("FTX") > -1 && !FTX_Flag) {
                    FTX_Flag = true
                    let itemArray:string[] = line.split(":")
                    let FTX_SEQ:string = itemArray[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4) 
                    DGS_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("MEA") > -1 && !MEA_Flag) {
                    MEA_Flag = true
                    let itemArray:string[] = line.split(":")
                    let MEA_SEQ:string = itemArray[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4) 
                    let DGS_SEQ_GRP:string = segSeq.substr(0, 2)
                    let MEA_SEQ_GRP:string = MEA_SEQ.substr(0, 2) 
                    if (MEA_SEQ_GRP === DGS_SEQ_GRP) {
                        DGS_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                    } else {
                        DGS_Item["MEA" + MEA_SEQ] = await this.getDataMEAGroup(lineList, i, MEA_SEQ)
                    }
                } else if (line.indexOf("CTA") > -1 && !CTA_Flag) {
                    CTA_Flag = true
                    let itemArray:string[] = line.split(":")
                    let CTA_SEQ:string = itemArray[0].trim()
                    CTA_SEQ = CTA_SEQ.substr(3, 4) 
                    let DGS_SEQ_GRP:string = segSeq.substr(0, 2)
                    let CTA_SEQ_GRP:string = CTA_SEQ.substr(0, 2) 
                    if (CTA_SEQ_GRP === DGS_SEQ_GRP) {
                        DGS_Item["CTA" + CTA_SEQ] = await this.getDataCTA(lineList, i, CTA_SEQ)
                    } else {
                        DGS_Item["CTA" + CTA_SEQ] = await this.getDataCTAGroup(lineList, i, CTA_SEQ)
                    }
                } else if (line.indexOf("SGP") > -1 && !SGP_Flag) {
                    SGP_Flag = true
                    let itemArray:string[] = line.split(":")
                    let SGP_SEQ:string = itemArray[0].trim()
                    SGP_SEQ = SGP_SEQ.substr(3, 4) 
                    let DGS_SEQ_GRP:string = segSeq.substr(0, 2)
                    let SGP_SEQ_GRP:string = SGP_SEQ.substr(0, 2) 
                    if (SGP_SEQ_GRP === DGS_SEQ_GRP) {
                        DGS_Item["SGP" + SGP_SEQ] = await this.getDataSGP(lineList, i, SGP_SEQ)
                    } else {
                        DGS_Item["SGP" + SGP_SEQ] = await this.getDataSGPGroup(lineList, i, SGP_SEQ)
                    }
                } else if (line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    DGS_List.push(DGS_Item)
                    break
                }
            }
            
        }

        return DGS_List
    }

    public static async getDataEQDGroup(lineList:string[], idx:number, segSeq:string) {
        let EQD_Item:any = {}
        let line:string
        let flag:boolean = false
        let RFF_flag:boolean = false
        let TMD_flag:boolean = false
        let DTM_flag:boolean = false
        let TSR_flag:boolean = false
        let LOC_flag:boolean = false
        let MEA_flag:boolean = false
        let DIM_flag:boolean = false
        let TMP_flag:boolean = false
        let RGN_flag:boolean = false
        let SEL_flag:boolean = false
        let FTX_flag:boolean = false
        let DGS_flag:boolean = false
        let MOA_flag:boolean = false
        let GOR_flag:boolean = false
        let DAM_flag:boolean = false
        let TDT_flag:boolean = false
        let NAD_flag:boolean = false
        let EQA_flag:boolean = false
        let TPL_flag:boolean = false
        let HAN_flag:boolean = false
        let TCC_flag:boolean = false
        let itemList:string[]
        let EQD_List:any[] = []
        let EQD_SEQ:string = "EQD" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(EQD_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                EQD_Item[EQD_SEQ + "00"] = itemList[1]

                flag = true;
            } else if (line.indexOf(EQD_SEQ + "00") > -1 && flag) {
                EQD_List.push(EQD_Item)
                
                EQD_Item = {}
                itemList = line.split(":")
                EQD_Item[EQD_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(EQD_SEQ + "02") > -1) {
                    EQD_Item[EQD_SEQ + "02"] = itemList[1]
                } else if (line.indexOf(EQD_SEQ + "03") > -1) {
                    EQD_Item[EQD_SEQ + "03"] = itemList[1]
                } else if (line.indexOf(EQD_SEQ + "04") > -1) {
                    EQD_Item[EQD_SEQ + "04"] = itemList[1]
                } else if (line.indexOf(EQD_SEQ + "05") > -1) {
                    EQD_Item[EQD_SEQ + "05"] = itemList[1]
                } else if (line.indexOf(EQD_SEQ + "07") > -1) {
                    EQD_Item[EQD_SEQ + "07"] = itemList[1]
                } else if (line.indexOf(EQD_SEQ + "08") > -1) {
                    EQD_Item[EQD_SEQ + "08"] = itemList[1]
                } else if (line.indexOf(EQD_SEQ + "09") > -1) {
                    EQD_Item[EQD_SEQ + "09"] = itemList[1]
                } else if (line.indexOf(EQD_SEQ + "10") > -1) {
                    EQD_Item[EQD_SEQ + "10"] = itemList[1]
                } else if (line.indexOf(EQD_SEQ + "11") > -1) {
                    EQD_Item[EQD_SEQ + "11"] = itemList[1]
                } else if (line.indexOf(EQD_SEQ + "12") > -1) {
                    EQD_Item[EQD_SEQ + "12"] = itemList[1]
                } else if (line.indexOf(EQD_SEQ + "13") > -1) {
                    EQD_Item[EQD_SEQ + "13"] = itemList[1]
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    let itemArray:string[] = line.split(":")
                    let FTX_SEQ:string = itemArray[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4) 
                    EQD_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("EQN") > -1) {
                    let itemArray:string[] = line.split(":")
                    let EQN_SEQ:string = itemArray[0].trim()
                    EQN_SEQ = EQN_SEQ.substr(3, 6) 
                    EQD_Item["EQN" + EQN_SEQ] = itemList[1]
                } else if (line.indexOf("TMD") > -1 && !TMD_flag) {
                    TMD_flag = true
                    let itemArray:string[] = line.split(":")
                    let TMD_SEQ:string = itemArray[0].trim()
                    TMD_SEQ = TMD_SEQ.substr(3, 4) 
                    EQD_Item["TMD" + TMD_SEQ] = await this.getDataTMD(lineList, i, TMD_SEQ)
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    let itemArray:string[] = line.split(":")
                    let DTM_SEQ:string = itemArray[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)
                    EQD_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("TSR") > -1 && !TSR_flag) {
                    TSR_flag = true
                    let itemArray:string[] = line.split(":")
                    let TSR_SEQ:string = itemArray[0].trim()
                    TSR_SEQ = TSR_SEQ.substr(3, 4)
                    EQD_Item["TSR" + TSR_SEQ] = await this.getDataTSR(lineList, i, TSR_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    let itemArray:string[] = line.split(":")
                    let LOC_SEQ:string = itemArray[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)
                    EQD_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("MEA") > -1 && !MEA_flag) {
                    MEA_flag = true
                    let itemArray:string[] = line.split(":")
                    let MEA_SEQ:string = itemArray[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4)
                    EQD_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                } else if (line.indexOf("DIM") > -1 && DIM_flag) {
                    DIM_flag = true
                    let itemArray:string[] = line.split(":")
                    let DIM_SEQ:string = itemArray[0].trim()
                    DIM_SEQ = DIM_SEQ.substr(3, 4)
                    EQD_Item["DIM" + DIM_SEQ] = await this.getDataDIM(lineList, i, DIM_SEQ)
                } else if (line.indexOf("TMP") > -1 && !TMP_flag) {
                    TMP_flag = true
                    let itemArray:string[] = line.split(":")
                    let TMP_SEQ:string = itemArray[0].trim()
                    TMP_SEQ = TMP_SEQ.substr(3, 4)
                    EQD_Item["TMP" + TMP_SEQ] = await this.getDataTMP(lineList, i, TMP_SEQ)
                } else if (line.indexOf("RNG") > -1 && !RGN_flag) {
                    RGN_flag = true
                    let itemArray:string[] = line.split(":")
                    let RNG_SEQ:string = itemArray[0].trim()
                    RNG_SEQ = RNG_SEQ.substr(3, 4)
                    EQD_Item["RNG" + RNG_SEQ] = await this.getDataRNG(lineList, i, RNG_SEQ)
                } else if (line.indexOf("SEL") > -1 && !SEL_flag) {
                    SEL_flag = true
                    let itemArray:string[] = line.split(":")
                    let SEL_SEQ:string = itemArray[0].trim()
                    SEL_SEQ = SEL_SEQ.substr(3, 4)
                    EQD_Item["SEL" + SEL_SEQ] = await this.getDataSEL(lineList, i, SEL_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    let itemArray:string[] = line.split(":")
                    let FTX_SEQ:string = itemArray[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)
                    EQD_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("DGS") > -1 && !DGS_flag) {
                    DGS_flag = true
                    let itemArray:string[] = line.split(":")
                    let DGS_SEQ:string = itemArray[0].trim()
                    DGS_SEQ = DGS_SEQ.substr(3, 4)
                    EQD_Item["DGS" + DGS_SEQ] = await this.getDataDGS(lineList, i, DGS_SEQ)
                } else if (line.indexOf("MOA") > -1 && !MOA_flag) {
                    MOA_flag = true
                    let itemArray:string[] = line.split(":")
                    let MOA_SEQ:string = itemArray[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4)
                    EQD_Item["MOA" + MOA_SEQ] = await this.getDataMOA(lineList, i, MOA_SEQ)
                } else if (line.indexOf("GOR") > -1 && !GOR_flag) {
                    GOR_flag = true
                    let itemArray:string[] = line.split(":")
                    let GOR_SEQ:string = itemArray[0].trim()
                    GOR_SEQ = GOR_SEQ.substr(3, 4)
                    EQD_Item["GOR" + GOR_SEQ] = await this.getDataGOR(lineList, i, GOR_SEQ)
                } else if (line.indexOf("EQA") > -1 && !EQA_flag) {
                    EQA_flag = true
                    let itemArray:string[] = line.split(":")
                    let EQA_SEQ:string = itemArray[0].trim()
                    EQA_SEQ = EQA_SEQ.substr(3, 4)
                    EQD_Item["EQA" + EQA_SEQ] = await this.getDataEQA(lineList, i, EQA_SEQ)
                } else if (line.indexOf("DAM") > -1 && !DAM_flag) {
                    DAM_flag = true
                    let itemArray:string[] = line.split(":")
                    let DAM_SEQ:string = itemArray[0].trim()
                    DAM_SEQ = DAM_SEQ.substr(3, 4)
                    EQD_Item["DAM" + DAM_SEQ] = await this.getDataDAMGroup(lineList, i, DAM_SEQ)
                } else if (line.indexOf("TDT") > -1 && !TDT_flag) {
                    TDT_flag = true
                    let itemArray:string[] = line.split(":")
                    let TDT_SEQ:string = itemArray[0].trim()
                    TDT_SEQ = TDT_SEQ.substr(3, 4)
                    EQD_Item["TDT" + TDT_SEQ] = await this.getDataTDTGroup(lineList, i, TDT_SEQ)
                } else if (line.indexOf("NAD") > -1 && !NAD_flag) {
                    NAD_flag = true
                    let itemArray:string[] = line.split(":")
                    let NAD_SEQ:string = itemArray[0].trim()
                    NAD_SEQ = NAD_SEQ.substr(3, 4)
                    EQD_Item["NAD" + NAD_SEQ] = await this.getDataNADGroup(lineList, i, NAD_SEQ)
                } else if (line.indexOf("TPL") > -1 && !TPL_flag) {
                    TPL_flag = true
                    let itemArray:string[] = line.split(":")
                    let TPL_SEQ:string = itemArray[0].trim()
                    TPL_SEQ = TPL_SEQ.substr(3, 4)
                    EQD_Item["TPL" + TPL_SEQ] = await this.getDataTPL(lineList, i, TPL_SEQ)
                } else if (line.indexOf("HAN") > -1 && !HAN_flag) {
                    HAN_flag = true
                    let itemArray:string[] = line.split(":")
                    let HAN_SEQ:string = itemArray[0].trim()
                    HAN_SEQ = HAN_SEQ.substr(3, 4)
                    EQD_Item["HAN" + HAN_SEQ] = await this.getDataHAN(lineList, i, HAN_SEQ)
                } else if (line.indexOf("TCC") > -1 && !TCC_flag) {
                    TCC_flag = true
                    let itemArray:string[] = line.split(":")
                    let TCC_SEQ:string = itemArray[0].trim()
                    TCC_SEQ = TCC_SEQ.substr(3, 4)
                    EQD_Item["TCC" + TCC_SEQ] = await this.getDataTCCGroup(lineList, i, TCC_SEQ)
                } else if (line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    EQD_List.push(EQD_Item)
                    break
                }
            }
            
        }

        return EQD_List
    }

    public static async getDataLOCGroup(lineList:string[], idx:number, segSeq:string) {
        let LOC_Item:any = {}
        let line:string
        let flag:boolean = false
        let GID_flag:boolean = false
        let GDS_flag:boolean = false
        let FTX_flag:boolean = false
        let MEA_flag:boolean = false
        let DIM_flag:boolean = false
        let TMP_flag:boolean = false
        let RNG_flag:boolean = false
        let LOC_flag:boolean = false
        let RFF_flag:boolean = false
        let EQD_flag:boolean = false
        let DGS_flag:boolean = false
        let DTM_flag:boolean = false
        let itemList:string[]
        let LOC_List:any[] = []
        let LOC_SEQ:string = "LOC" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(LOC_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                LOC_Item[LOC_SEQ + "00"] = itemList[1]

                flag = true;
            } else if (line.indexOf(LOC_SEQ) > -1 && flag) {
                LOC_List.push(LOC_Item)
                
                LOC_Item = {}
                itemList = line.split(":")
                LOC_Item[LOC_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(LOC_SEQ + "02") > -1) {
                    LOC_Item[LOC_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(LOC_SEQ + "03") > -1) {
                    LOC_Item[LOC_SEQ +'03'] = itemList[1]
                } else if (line.indexOf(LOC_SEQ + "04") > -1) {
                    LOC_Item[LOC_SEQ +'04'] = itemList[1]
                } else if (line.indexOf(LOC_SEQ + "05") > -1) {
                    LOC_Item[LOC_SEQ +'05'] = itemList[1]
                } else if (line.indexOf(LOC_SEQ + "07") > -1) {
                    LOC_Item[LOC_SEQ +'07'] = itemList[1]
                } else if (line.indexOf(LOC_SEQ + "08") > -1) {
                    LOC_Item[LOC_SEQ +'08'] = itemList[1]
                } else if (line.indexOf(LOC_SEQ + "09") > -1) {
                    LOC_Item[LOC_SEQ +'09'] = itemList[1]
                } else if (line.indexOf(LOC_SEQ + "10") > -1) {
                    LOC_Item[LOC_SEQ +'10'] = itemList[1]
                } else if (line.indexOf(LOC_SEQ + "12") > -1) {
                    LOC_Item[LOC_SEQ +'12'] = itemList[1]
                } else if (line.indexOf(LOC_SEQ + "13") > -1) {
                    LOC_Item[LOC_SEQ +'13'] = itemList[1]
                } else if (line.indexOf(LOC_SEQ + "14") > -1) {
                    LOC_Item[LOC_SEQ +'14'] = itemList[1]
                } else if (line.indexOf(LOC_SEQ + "15") > -1) {
                    LOC_Item[LOC_SEQ +'15'] = itemList[1]
                } else if (line.indexOf(LOC_SEQ + "16") > -1) {
                    LOC_Item[LOC_SEQ +'16'] = itemList[1]
                } else if (line.indexOf("GID") > -1 && !GID_flag) {
                    GID_flag = true
                    let itemArray:string[] = line.split(":")
                    let GID_SEQ:string = itemArray[0].trim()
                    GID_SEQ = GID_SEQ.substr(3, 4)
                    LOC_Item["GID" + GID_SEQ] = await this.getDataGID(lineList, i, GID_SEQ)
                } else if (line.indexOf("GDS") > -1 && !GDS_flag) {
                    GDS_flag = true
                    let itemArray:string[] = line.split(":")
                    let GDS_SEQ:string = itemArray[0].trim()
                    GDS_SEQ = GDS_SEQ.substr(3, 4)
                    LOC_Item["GDS" + GDS_SEQ] = await this.getDataGDS(lineList, i, GDS_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    let itemArray:string[] = line.split(":")
                    let FTX_SEQ:string = itemArray[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)
                    LOC_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("MEA") > -1 && !MEA_flag) {
                    MEA_flag = true
                    let itemArray:string[] = line.split(":")
                    let MEA_SEQ:string = itemArray[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4)
                    LOC_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                } else if (line.indexOf("DIM") > -1 && !DIM_flag) {
                    DIM_flag = true
                    let itemArray:string[] = line.split(":")
                    let DIM_SEQ:string = itemArray[0].trim()
                    DIM_SEQ = DIM_SEQ.substr(3, 4)
                    LOC_Item["DIM" + DIM_SEQ] = await this.getDataDIM(lineList, i, DIM_SEQ)
                } else if (line.indexOf("TMP") > -1 && !TMP_flag) {
                    TMP_flag = true
                    let itemArray:string[] = line.split(":")
                    let TMP_SEQ:string = itemArray[0].trim()
                    TMP_SEQ = TMP_SEQ.substr(3, 4)
                    LOC_Item["TMP" + TMP_SEQ] = await this.getDataTMP(lineList, i, TMP_SEQ)
                } else if (line.indexOf("RNG") > -1 && !RNG_flag) {
                    RNG_flag = true
                    let itemArray:string[] = line.split(":")
                    let RNG_SEQ:string = itemArray[0].trim()
                    RNG_SEQ = RNG_SEQ.substr(3, 4)
                    LOC_Item["RNG" + RNG_SEQ] = await this.getDataRNG(lineList, i, RNG_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    let itemArray:string[] = line.split(":")
                    let LOC_SEQ:string = itemArray[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)
                    LOC_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    let itemArray:string[] = line.split(":")
                    let RFF_SEQ:string = itemArray[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4)
                    LOC_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                } else if (line.indexOf("EQD") > -1 && !EQD_flag) {
                    EQD_flag = true
                    let itemArray:string[] = line.split(":")
                    let EQD_SEQ:string = itemArray[0].trim()
                    EQD_SEQ = EQD_SEQ.substr(3, 4)
                    LOC_Item["EQD" + EQD_SEQ] = await this.getDataEQDGroup(lineList, i, EQD_SEQ)
                } else if (line.indexOf("DGS") > -1 && !DGS_flag) {
                    DGS_flag = true
                    let itemArray:string[] = line.split(":")
                    let DGS_SEQ:string = itemArray[0].trim()
                    DGS_SEQ = DGS_SEQ.substr(3, 4)
                    LOC_Item["DGS" + DGS_SEQ] = await this.getDataDGSGroup(lineList, i, DGS_SEQ)
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    let itemArray:string[] = line.split(":")
                    let DTM_SEQ:string = itemArray[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)
                    LOC_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("UNT") > -1) {
                    LOC_List.push(LOC_Item)
                    break
                }
            }
        }

        return LOC_List
    }

    public static async getDataGID(lineList:string[], idx:number, segSeq:string) {
        let GID_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let GID_List:any[] = []
        let GID_SEQ:string = "GID" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(GID_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                GID_Item[GID_SEQ + "00"] = itemList[1]

                flag = true;
            } else if (line.indexOf(GID_SEQ + "00") > -1 && flag) {
                GID_List.push(GID_Item)
                
                GID_Item = {}
                itemList = line.split(":")
                GID_Item[GID_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(GID_SEQ + "02") > -1) {
                    GID_Item[GID_SEQ + "02"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "03") > -1) {
                    GID_Item[GID_SEQ + "03"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "04") > -1) {
                    GID_Item[GID_SEQ + "04"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "05") > -1) {
                    GID_Item[GID_SEQ + "05"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "06") > -1) {
                    GID_Item[GID_SEQ + "06"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "08") > -1) {
                    GID_Item[GID_SEQ + "08"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "09") > -1) {
                    GID_Item[GID_SEQ + "09"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "10") > -1) {
                    GID_Item[GID_SEQ + "10"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "11") > -1) {
                    GID_Item[GID_SEQ + "11"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "12") > -1) {
                    GID_Item[GID_SEQ + "12"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "14") > -1) {
                    GID_Item[GID_SEQ + "14"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "15") > -1) {
                    GID_Item[GID_SEQ + "15"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "16") > -1) {
                    GID_Item[GID_SEQ + "16"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "17") > -1) {
                    GID_Item[GID_SEQ + "17"] = itemList[1]
                } else if (line.indexOf(GID_SEQ + "18") > -1) {
                    GID_Item[GID_SEQ + "18"] = itemList[1]
                } else if (line.indexOf("FTX") > -1 || line.indexOf("MEA") > -1 || line.indexOf("DIM") > -1 || line.indexOf("TMP") > -1 || 
                line.indexOf("RNG") > -1 || line.indexOf("LOC") > -1 || line.indexOf("RFF") > -1 || line.indexOf("EQD") > -1 ||
                line.indexOf("DGS") > -1|| line.indexOf("UNT") > -1) {
                    GID_List.push(GID_Item)
                    break
                }
            }
        }
        return GID_List
    } 

    public static async getDataGDS(lineList:string[], idx:number, segSeq:string) {
        let GDS_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let GDS_List:any[] = []
        let GDS_SEQ:string = "GDS" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(GDS_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                GDS_Item[GDS_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(GDS_SEQ + "01") > -1 && flag) {
                GDS_List.push(GDS_Item)
                
                GDS_Item = {}
                itemList = line.split(":")
                GDS_Item[GDS_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(GDS_SEQ + "02") > -1) {
                    GDS_Item[GDS_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(GDS_SEQ + "03") > -1) {
                    GDS_Item[GDS_SEQ +'03'] = itemList[1]
                } else {
                    GDS_List.push(GDS_Item)
                    break
                }
            }
        }
        return GDS_List
    }

    public static async getDataEQA(lineList:string[], idx:number, segSeq:string) {
        let EQA_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let EQA_List:any[] = []
        let EQA_SEQ:string = "EQA" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(EQA_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                EQA_Item[EQA_SEQ + "00"] = itemList[1]

                flag = true;
            } else if (line.indexOf(EQA_SEQ + "00") > -1 && flag) {
                EQA_List.push(EQA_Item)
                
                EQA_Item = {}
                itemList = line.split(":")
                EQA_Item[EQA_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(EQA_SEQ + "02") > -1) {
                    EQA_Item[EQA_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(EQA_SEQ + "03") > -1) {
                    EQA_Item[EQA_SEQ +'03'] = itemList[1]
                } else if (line.indexOf(EQA_SEQ + "04") > -1) {
                    EQA_Item[EQA_SEQ +'04'] = itemList[1]
                } else if (line.indexOf(EQA_SEQ + "05") > -1) {
                    EQA_Item[EQA_SEQ +'05'] = itemList[1]
                } else if (line.indexOf("EQN") > -1) {
                    let itemArray:string[] = line.split(":")
                    let EQN_SEQ:string = itemArray[0].trim()
                    EQN_SEQ = EQN_SEQ.substr(3, 6) 
                    EQA_Item["EQN" + EQN_SEQ] = itemList[1]
                } else if (line.indexOf("EQD") > -1 || line.indexOf("UNT") > -1) {
                    EQA_List.push(EQA_Item)
                    break
                }
            }
        }
        return EQA_List
    }

    public static async getDataCNT(lineList:string[], idx:number, segSeq:string) {
        let CNT_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let CNT_List:any[] = []
        let CNT_SEQ:string = "CNT" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(CNT_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                CNT_Item[CNT_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(CNT_SEQ + "01") > -1 && flag) {
                CNT_List.push(CNT_Item)
                
                CNT_Item = {}
                itemList = line.split(":")
                CNT_Item[CNT_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(CNT_SEQ + "02") > -1) {
                    CNT_Item[CNT_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(CNT_SEQ + "03") > -1) {
                    CNT_Item[CNT_SEQ +'03'] = itemList[1]
                } else {
                    CNT_List.push(CNT_Item)
                    break
                }
            }
        }
        return CNT_List
    }

    public static async getDataTODGroup(lineList:string[], idx:number, segSeq:string) {
        let TOD_Item:any = {}
        let line:string
        let flag:boolean = false
        let LOC_flag:boolean = false
        let itemList:string[]
        let TOD_List:any[] = []
        let TOD_SEQ:string = "TOD" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TOD_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                TOD_Item[TOD_SEQ + "00"] = itemList[1]

                flag = true;
            } else if (line.indexOf(TOD_SEQ + "00") > -1 && flag) {
                TOD_List.push(TOD_Item)
                
                TOD_Item = {}
                itemList = line.split(":")
                TOD_Item[TOD_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(TOD_SEQ + "01") > -1) {
                    TOD_Item[TOD_SEQ + "01"] = itemList[1]
                } else if (line.indexOf(TOD_SEQ + "03") > -1) {
                    TOD_Item[TOD_SEQ + "03"] = itemList[1]
                } else if (line.indexOf(TOD_SEQ + "04") > -1) {
                    TOD_Item[TOD_SEQ + "04"] = itemList[1]
                } else if (line.indexOf(TOD_SEQ + "05") > -1) {
                    TOD_Item[TOD_SEQ + "05"] = itemList[1]
                } else if (line.indexOf(TOD_SEQ + "06") > -1) {
                    TOD_Item[TOD_SEQ + "06"] = itemList[1]
                } else if (line.indexOf(TOD_SEQ + "07") > -1) {
                    TOD_Item[TOD_SEQ + "07"] = itemList[1]
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    let itemArray:string[] = line.split(":")
                    let LOC_SEQ:string = itemArray[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)
                    TOD_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("RFF") > -1 || line.indexOf("GOR") > -1 || line.indexOf("TCC") > -1 || line.indexOf("TDT") > -1 || 
                line.indexOf("NAD") > -1 || line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    TOD_List.push(TOD_Item)
                    break
                }
            }
            
        }

        return TOD_List
    }

    public static async getDataRFFGroup(lineList:string[], idx:number, segSeq:string) {
        let RFF_Item:any = {}
        let line:string
        let flag:boolean = false
        let DTM_flag:boolean = false
        let itemList:string[]
        let RFF_List:any[] = []
        let RFF_SEQ:string = "RFF" + segSeq + "01"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(RFF_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                RFF_Item[RFF_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(RFF_SEQ) > -1 && flag) {
                RFF_List.push(RFF_Item)
                
                RFF_Item = {}
                itemList = line.split(":")
                RFF_Item[RFF_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("RFF" + segSeq + "02") > -1) {
                    RFF_Item['RFF'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("RFF" + segSeq + "03") > -1) {
                    RFF_Item['RFF'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("RFF" + segSeq + "04") > -1) {
                    RFF_Item['RFF'+ segSeq +'04'] = itemList[1]
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    let itemArray:string[] = line.split(":")
                    let DTM_SEQ:string = itemArray[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)
                    RFF_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("GOR") > -1 || line.indexOf("TCC") > -1 || line.indexOf("TDT") > -1 || 
                line.indexOf("NAD") > -1 || line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    RFF_List.push(RFF_Item)
                    break
                }
            }
        }
        return RFF_List
    }

    public static async getDataTCCGroup(lineList:string[], idx:number, segSeq:string) {
        let TCC_Item:any = {}
        let line:string
        let flag:boolean = false
        let CUX_flag:boolean = false
        let LOC_flag:boolean = false
        let PRI_flag:boolean = false
        let FTX_flag:boolean = false
        let PCD_flag:boolean = false
        let MOA_flag:boolean = false
        let QTY_flag:boolean = false
        let itemList:string[]
        let TCC_List:any[] = []
        let TCC_SEQ:string = "TCC" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TCC_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                TCC_Item[TCC_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(TCC_SEQ + "01") > -1 && flag) {
                TCC_List.push(TCC_Item)
                
                TCC_Item = {}
                itemList = line.split(":")
                TCC_Item[TCC_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(TCC_SEQ + "01") > -1) {
                    TCC_Item[TCC_SEQ + "01"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "02") > -1) {
                    TCC_Item[TCC_SEQ + "02"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "03") > -1) {
                    TCC_Item[TCC_SEQ + "03"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "04") > -1) {
                    TCC_Item[TCC_SEQ + "04"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "05") > -1) {
                    TCC_Item[TCC_SEQ + "05"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "07") > -1) {
                    TCC_Item[TCC_SEQ + "07"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "09") > -1) {
                    TCC_Item[TCC_SEQ + "09"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "10") > -1) {
                    TCC_Item[TCC_SEQ + "10"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "11") > -1) {
                    TCC_Item[TCC_SEQ + "11"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "12") > -1) {
                    TCC_Item[TCC_SEQ + "12"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "13") > -1) {
                    TCC_Item[TCC_SEQ + "13"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "15") > -1) {
                    TCC_Item[TCC_SEQ + "15"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "16") > -1) {
                    TCC_Item[TCC_SEQ + "16"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "18") > -1) {
                    TCC_Item[TCC_SEQ + "18"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "19") > -1) {
                    TCC_Item[TCC_SEQ + "19"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "20") > -1) {
                    TCC_Item[TCC_SEQ + "20"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "22") > -1) {
                    TCC_Item[TCC_SEQ + "22"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "23") > -1) {
                    TCC_Item[TCC_SEQ + "23"] = itemList[1]
                } else if (line.indexOf(TCC_SEQ + "24") > -1) {
                    TCC_Item[TCC_SEQ + "24"] = itemList[1]
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    let itemArray:string[] = line.split(":")
                    let LOC_SEQ:string = itemArray[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4) 
                    TCC_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    let itemArray:string[] = line.split(":")
                    let FTX_SEQ:string = itemArray[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4) 
                    TCC_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("CUX") > -1 && !CUX_flag) {
                    CUX_flag = true
                    let itemArray:string[] = line.split(":")
                    let CUX_SEQ:string = itemArray[0].trim()
                    CUX_SEQ = CUX_SEQ.substr(3, 4) 
                    TCC_Item["CUX" + CUX_SEQ] = await this.getDataCUX(lineList, i, CUX_SEQ)
                } else if (line.indexOf("PRI") > -1 && !PRI_flag) {
                    PRI_flag = true
                    let itemArray:string[] = line.split(":")
                    let PRI_SEQ:string = itemArray[0].trim()
                    PRI_SEQ = PRI_SEQ.substr(3, 4) 
                    TCC_Item["PRI" + PRI_SEQ] = await this.getDataPRI(lineList, i, PRI_SEQ)
                } else if (line.indexOf("QTY") > -1 && !QTY_flag) {
                    QTY_flag = true
                    let itemArray:string[] = line.split(":")
                    let QTY_SEQ:string = itemArray[0].trim()
                    QTY_SEQ = QTY_SEQ.substr(3, 4) 
                    TCC_Item["QTY" + QTY_SEQ] = await this.getDataQTY(lineList, i, QTY_SEQ)
                } else if (line.indexOf("EQN") > -1) {
                    let itemArray:string[] = line.split(":")
                    let EQN_SEQ:string = itemArray[0].trim()
                    EQN_SEQ = EQN_SEQ.substr(3, 6) 
                    TCC_Item["EQN" + EQN_SEQ] = itemList[1]
                } else if (line.indexOf("CUX") > -1) {
                    let itemArray:string[] = line.split(":")
                    let CUX_SEQ:string = itemArray[0].trim()
                    CUX_SEQ = CUX_SEQ.substr(3, 6) 
                    TCC_Item["CUX" + CUX_SEQ] = itemList[1]
                } else if (line.indexOf("PCD") > -1 && !PCD_flag) {
                    PCD_flag = true
                    let itemArray:string[] = line.split(":")
                    let PCD_SEQ:string = itemArray[0].trim()
                    PCD_SEQ = PCD_SEQ.substr(3, 4) 
                    TCC_Item["PCD" + PCD_SEQ] = await this.getDataPCD(lineList, i, PCD_SEQ)
                } else if (line.indexOf("MOA") > -1 && !MOA_flag) {
                    MOA_flag = true
                    let itemArray:string[] = line.split(":")
                    let MOA_SEQ:string = itemArray[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4) 
                    TCC_Item["MOA" + MOA_SEQ] = await this.getDataMOA(lineList, i, MOA_SEQ)
                } else if (line.indexOf("TDT") > -1 || line.indexOf("NAD") > -1 || line.indexOf("DGS") > -1 || line.indexOf("GID") > -1 || 
                line.indexOf("EQD") > -1 ||line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    TCC_List.push(TCC_Item)
                    break
                } 
            }
            
        }

        return TCC_List
    }

    public static async getDataCUX(lineList:string[], idx:number, segSeq:string) {
        let CUX_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let CUX_List:any[] = []
        let CUX_SEQ:string = "CUX" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(CUX_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                CUX_Item[CUX_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(CUX_SEQ + "01") > -1 && flag) {
                CUX_List.push(CUX_Item)
                
                CUX_Item = {}
                itemList = line.split(":")
                CUX_Item[CUX_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(CUX_SEQ + "02") > -1) {
                    CUX_Item[CUX_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(CUX_SEQ + "03") > -1) {
                    CUX_Item[CUX_SEQ +'03'] = itemList[1]
                } else if (line.indexOf(CUX_SEQ + "04") > -1) {
                    CUX_Item[CUX_SEQ +'04'] = itemList[1]
                } else if (line.indexOf(CUX_SEQ + "06") > -1) {
                    CUX_Item[CUX_SEQ +'06'] = itemList[1]
                } else if (line.indexOf(CUX_SEQ + "07") > -1) {
                    CUX_Item[CUX_SEQ +'07'] = itemList[1]
                } else if (line.indexOf(CUX_SEQ + "08") > -1) {
                    CUX_Item[CUX_SEQ +'08'] = itemList[1]
                } else if (line.indexOf(CUX_SEQ + "09") > -1) {
                    CUX_Item[CUX_SEQ +'09'] = itemList[1]
                } else if (line.indexOf(CUX_SEQ + "10") > -1) {
                    CUX_Item[CUX_SEQ +'10'] = itemList[1]
                } else if (line.indexOf(CUX_SEQ + "11") > -1) {
                    CUX_Item[CUX_SEQ +'11'] = itemList[1]
                } else {
                    CUX_List.push(CUX_Item)
                    break
                }
            }
        }
        return CUX_List
    }

    public static async getDataPRI(lineList:string[], idx:number, segSeq:string) {
        let PRI_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let PRI_List:any[] = []
        let PRI_SEQ:string = "PRI" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(PRI_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                PRI_Item[PRI_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(PRI_SEQ + "01") > -1 && flag) {
                PRI_List.push(PRI_Item)
                
                PRI_Item = {}
                itemList = line.split(":")
                PRI_Item[PRI_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(PRI_SEQ + "02") > -1) {
                    PRI_Item[PRI_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(PRI_SEQ + "03") > -1) {
                    PRI_Item[PRI_SEQ +'03'] = itemList[1]
                } else if (line.indexOf(PRI_SEQ + "04") > -1) {
                    PRI_Item[PRI_SEQ +'04'] = itemList[1]
                } else if (line.indexOf(PRI_SEQ + "05") > -1) {
                    PRI_Item[PRI_SEQ +'05'] = itemList[1]
                } else if (line.indexOf(PRI_SEQ + "06") > -1) {
                    PRI_Item[PRI_SEQ +'06'] = itemList[1]
                } else if (line.indexOf(PRI_SEQ + "07") > -1) {
                    PRI_Item[PRI_SEQ +'07'] = itemList[1]
                } else {
                    PRI_List.push(PRI_Item)
                    break
                }
            }
        }
        return PRI_List
    }

    public static async getDataPCD(lineList:string[], idx:number, segSeq:string) {
        let PCD_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let PCD_List:any[] = []
        let PCD_SEQ:string = "PCD" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(PCD_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                PCD_Item[PCD_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(PCD_SEQ + "01") > -1 && flag) {
                PCD_List.push(PCD_Item)
                
                PCD_Item = {}
                itemList = line.split(":")
                PCD_Item[PCD_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(PCD_SEQ + "02") > -1) {
                    PCD_Item[PCD_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(PCD_SEQ + "03") > -1) {
                    PCD_Item[PCD_SEQ +'03'] = itemList[1]
                } else if (line.indexOf(PCD_SEQ + "04") > -1) {
                    PCD_Item[PCD_SEQ +'04'] = itemList[1]
                } else if (line.indexOf(PCD_SEQ + "05") > -1) {
                    PCD_Item[PCD_SEQ +'05'] = itemList[1]
                } else {
                    PCD_List.push(PCD_Item)
                    break
                }
            }
        }
        return PCD_List
    }

    public static async getDataQTY(lineList:string[], idx:number, segSeq:string) {
        let QTY_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let QTY_List:any[] = []
        let QTY_SEQ:string = "QTY" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(QTY_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                QTY_Item[QTY_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(QTY_SEQ + "01") > -1 && flag) {
                QTY_List.push(QTY_Item)
                
                QTY_Item = {}
                itemList = line.split(":")
                QTY_Item[QTY_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(QTY_SEQ + "02") > -1) {
                    QTY_Item[QTY_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(QTY_SEQ + "03") > -1) {
                    QTY_Item[QTY_SEQ +'03'] = itemList[1]
                } else {
                    QTY_List.push(QTY_Item)
                    break
                }
            }
        }
        return QTY_List
    }

    public static async getDataCTAGroup(lineList:string[], idx:number, segSeq:string) {
        let CTA_Item:any = {}
        let line:string
        let flag:boolean = false
        let COM_flag:boolean = false
        let itemList:string[]
        let CTA_List:any[] = []
        let CTA_SEQ:string = "CTA" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(CTA_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                CTA_Item[CTA_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(CTA_SEQ) > -1 && flag) {
                CTA_List.push(CTA_Item)
                
                CTA_Item = {}
                itemList = line.split(":")
                CTA_Item[CTA_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("CTA" + segSeq + "01") > -1) {
                    CTA_Item['CTA'+ segSeq +'01'] = itemList[1]
                } else if (line.indexOf("CTA" + segSeq + "02") > -1) {
                    CTA_Item['CTA'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("CTA" + segSeq + "03") > -1) {
                    CTA_Item['CTA'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("COM") > -1 && !COM_flag) {
                    COM_flag = true
                    let itemArray:string[] = line.split(":")
                    let COM_SEQ:string = itemArray[0].trim()
                    COM_SEQ = COM_SEQ.substr(3, 4) 
                    CTA_Item["COM" + COM_SEQ] = await this.getDataCOM(lineList, i, COM_SEQ)
                } else if (line.indexOf("DOC") > -1 || line.indexOf("NAD") > -1 || line.indexOf("RFF") > -1 || line.indexOf("GID") > -1 || 
                line.indexOf("EQD") > -1 || line.indexOf("UNT") > -1) {
                    CTA_List.push(CTA_Item)
                    break
                } 
            }
        }
        return CTA_List
    }

    public static async getDataDOCGroup(lineList:string[], idx:number, segSeq:string) {
        let DOC_Item:any = {}
        let line:string
        let flag:boolean = false
        let DTM_flag:boolean = false
        let itemList:string[]
        let DOC_List:any[] = []
        let DOC_SEQ:string = "DOC" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DOC_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                DOC_Item[DOC_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(DOC_SEQ + "01") > -1 && flag) {
                DOC_List.push(DOC_Item)
                
                DOC_Item = {}
                itemList = line.split(":")
                DOC_Item[DOC_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(DOC_SEQ + "02") > -1) {
                    DOC_Item[DOC_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(DOC_SEQ + "03") > -1) {
                    DOC_Item[DOC_SEQ +'03'] = itemList[1]
                } else if (line.indexOf(DOC_SEQ + "04") > -1) {
                    DOC_Item[DOC_SEQ +'04'] = itemList[1]
                } else if (line.indexOf(DOC_SEQ + "06") > -1) {
                    DOC_Item[DOC_SEQ +'06'] = itemList[1]
                } else if (line.indexOf(DOC_SEQ + "07") > -1) {
                    DOC_Item[DOC_SEQ +'07'] = itemList[1]
                } else if (line.indexOf(DOC_SEQ + "08") > -1) {
                    DOC_Item[DOC_SEQ +'08'] = itemList[1]
                } else if (line.indexOf(DOC_SEQ + "09") > -1) {
                    DOC_Item[DOC_SEQ +'09'] = itemList[1]
                } else if (line.indexOf(DOC_SEQ + "10") > -1) {
                    DOC_Item[DOC_SEQ +'10'] = itemList[1]
                } else if (line.indexOf(DOC_SEQ + "11") > -1) {
                    DOC_Item[DOC_SEQ +'11'] = itemList[1]
                } else if (line.indexOf(DOC_SEQ + "12") > -1) {
                    DOC_Item[DOC_SEQ +'12'] = itemList[1]
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    let itemArray:string[] = line.split(":")
                    let DTM_SEQ:string = itemArray[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4) 
                    DOC_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("NAD") > -1 || line.indexOf("RFF") > -1 || line.indexOf("GID") > -1 || 
                line.indexOf("EQD") > -1 || line.indexOf("UNT") > -1) {
                    DOC_List.push(DOC_Item)
                    break
                } 
            }
        }
        return DOC_List
    }

    public static async getDataTSRGroup(lineList:string[], idx:number, segSeq:string) {
        let TSR_Item:any = {}
        let line:string
        let flag:boolean = false
        let RFF_flag:boolean = false
        let LOC_flag:boolean = false
        let TPL_flag:boolean = false
        let FTX_flag:boolean = false
        let itemList:string[]
        let TSR_List:any[] = []
        let TSR_SEQ:string = "TSR" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TSR_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                TSR_Item[TSR_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(TSR_SEQ) > -1 && flag) {
                TSR_List.push(TSR_Item)
                
                TSR_Item = {}
                itemList = line.split(":")
                TSR_Item[TSR_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("TSR" + segSeq + "01") > -1) {
                    TSR_Item['TSR'+ segSeq +'01'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "02") > -1) {
                    TSR_Item['TSR'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "03") > -1) {
                    TSR_Item['TSR'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "05") > -1) {
                    TSR_Item['TSR'+ segSeq +'05'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "06") > -1) {
                    TSR_Item['TSR'+ segSeq +'06'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "07") > -1) {
                    TSR_Item['TSR'+ segSeq +'07'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "08") > -1) {
                    TSR_Item['TSR'+ segSeq +'08'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "09") > -1) {
                    TSR_Item['TSR'+ segSeq +'09'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "10") > -1) {
                    TSR_Item['TSR'+ segSeq +'10'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "12") > -1) {
                    TSR_Item['TSR'+ segSeq +'12'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "13") > -1) {
                    TSR_Item['TSR'+ segSeq +'13'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "14") > -1) {
                    TSR_Item['TSR'+ segSeq +'14'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "16") > -1) {
                    TSR_Item['TSR'+ segSeq +'16'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "17") > -1) {
                    TSR_Item['TSR'+ segSeq +'17'] = itemList[1]
                } else if (line.indexOf("TSR" + segSeq + "18") > -1) {
                    TSR_Item['TSR'+ segSeq +'18'] = itemList[1]
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    let itemArray:string[] = line.split(":")
                    let RFF_SEQ:string = itemArray[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4) 
                    TSR_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    let itemArray:string[] = line.split(":")
                    let LOC_SEQ:string = itemArray[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4) 
                    TSR_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("TPL") > -1 && !TPL_flag) {
                    TPL_flag = true
                    let itemArray:string[] = line.split(":")
                    let TPL_SEQ:string = itemArray[0].trim()
                    TPL_SEQ = TPL_SEQ.substr(3, 4) 
                    TSR_Item["TPL" + TPL_SEQ] = await this.getDataTPL(lineList, i, TPL_SEQ)
                } else if (line.indexOf("TPL") > -1 && !TPL_flag) {
                    TPL_flag = true
                    let itemArray:string[] = line.split(":")
                    let TPL_SEQ:string = itemArray[0].trim()
                    TPL_SEQ = TPL_SEQ.substr(3, 4) 
                    TSR_Item["TPL" + TPL_SEQ] = await this.getDataTPL(lineList, i, TPL_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    let itemArray:string[] = line.split(":")
                    let FTX_SEQ:string = itemArray[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4) 
                    TSR_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("NAD") > -1 || line.indexOf("GID") > -1 || 
                line.indexOf("EQD") > -1 || line.indexOf("UNT") > -1) {
                    TSR_List.push(TSR_Item)
                    break
                } 
            }
        }
        return TSR_List
    }

    public static async getDataCPI(lineList:string[], idx:number, segSeq:string) {
        let CPI_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let CPI_List:any[] = []
        let CPI_SEQ:string = "CPI" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(CPI_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                CPI_Item[CPI_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(CPI_SEQ + "01") > -1 && flag) {
                CPI_List.push(CPI_Item)
                
                CPI_Item = {}
                itemList = line.split(":")
                CPI_Item[CPI_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(CPI_SEQ + "02") > -1) {
                    CPI_Item[CPI_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(CPI_SEQ + "03") > -1) {
                    CPI_Item[CPI_SEQ +'03'] = itemList[1]
                } else if (line.indexOf(CPI_SEQ + "05") > -1) {
                    CPI_Item[CPI_SEQ +'05'] = itemList[1]
                } else if (line.indexOf(CPI_SEQ + "06") > -1) {
                    CPI_Item[CPI_SEQ +'06'] = itemList[1]
                } else if (line.indexOf(CPI_SEQ + "07") > -1) {
                    CPI_Item[CPI_SEQ +'07'] = itemList[1]
                } else if (line.indexOf(CPI_SEQ + "08") > -1) {
                    CPI_Item[CPI_SEQ +'08'] = itemList[1]
                } else {
                    CPI_List.push(CPI_Item)
                    break
                }
            }
        }
        return CPI_List
    }

    public static async getDataTPL(lineList:string[], idx:number, segSeq:string) {
        let TPL_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let TPL_List:any[] = []
        let TPL_SEQ:string = "TPL" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TPL_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                TPL_Item[TPL_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(TPL_SEQ + "01") > -1 && flag) {
                TPL_List.push(TPL_Item)
                
                TPL_Item = {}
                itemList = line.split(":")
                TPL_Item[TPL_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(TPL_SEQ + "02") > -1) {
                    TPL_Item[TPL_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(TPL_SEQ + "03") > -1) {
                    TPL_Item[TPL_SEQ +'03'] = itemList[1]
                } else if (line.indexOf(TPL_SEQ + "04") > -1) {
                    TPL_Item[TPL_SEQ +'04'] = itemList[1]
                } else if (line.indexOf(TPL_SEQ + "05") > -1) {
                    TPL_Item[TPL_SEQ +'05'] = itemList[1]
                } else {
                    TPL_List.push(TPL_Item)
                    break
                }
            }
        }
        return TPL_List
    }

    public static async getDataCPIGroup(lineList:string[], idx:number, segSeq:string) {
        let CPI_Item:any = {}
        let line:string
        let flag:boolean = false
        let CUX_flag:boolean = false
        let LOC_flag:boolean = false
        let MOA_flag:boolean = false
        let itemList:string[]
        let CPI_List:any[] = []
        let CPI_SEQ:string = "CPI" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(CPI_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                CPI_Item[CPI_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(CPI_SEQ + "01") > -1 && flag) {
                CPI_List.push(CPI_Item)
                
                CPI_Item = {}
                itemList = line.split(":")
                CPI_Item[CPI_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(CPI_SEQ + "02") > -1) {
                    CPI_Item[CPI_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(CPI_SEQ + "03") > -1) {
                    CPI_Item[CPI_SEQ +'03'] = itemList[1]
                } else if (line.indexOf(CPI_SEQ + "05") > -1) {
                    CPI_Item[CPI_SEQ +'05'] = itemList[1]
                } else if (line.indexOf(CPI_SEQ + "06") > -1) {
                    CPI_Item[CPI_SEQ +'06'] = itemList[1]
                } else if (line.indexOf(CPI_SEQ + "07") > -1) {
                    CPI_Item[CPI_SEQ +'07'] = itemList[1]
                } else if (line.indexOf(CPI_SEQ + "08") > -1) {
                    CPI_Item[CPI_SEQ +'08'] = itemList[1]
                } else if (line.indexOf("CUX") > -1 && !CUX_flag) {
                    CUX_flag = true
                    let itemArray:string[] = line.split(":")
                    let CUX_SEQ:string = itemArray[0].trim()
                    CUX_SEQ = CUX_SEQ.substr(3, 4) 
                    CPI_Item["CUX" + CUX_SEQ] = await this.getDataCUX(lineList, i, CUX_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    let itemArray:string[] = line.split(":")
                    let LOC_SEQ:string = itemArray[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4) 
                    CPI_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("MOA") > -1 && !MOA_flag) {
                    MOA_flag = true
                    let itemArray:string[] = line.split(":")
                    let MOA_SEQ:string = itemArray[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4) 
                    CPI_Item["MOA" + MOA_SEQ] = await this.getDataMOA(lineList, i, MOA_SEQ)
                } else if (line.indexOf("NAD") > -1 || line.indexOf("GID") > -1 || 
                line.indexOf("EQD") > -1 || line.indexOf("UNT") > -1) {
                    CPI_List.push(CPI_Item)
                    break
                } 
            }
        }
        return CPI_List
    }

    public static async getDataPCIGroup(lineList:string[], idx:number, segSeq:string) {
        let PCI_Item:any = {}
        let line:string
        let flag:boolean = false
        let RFF_Flag:boolean = false
        let DTM_Flag:boolean = false
        let itemList:string[]
        let PCI_List:any[] = []
        let PCI_SEQ:string = "PCI" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(PCI_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                PCI_Item[PCI_SEQ + "00"] = itemList[1]

                flag = true;
            } else if (line.indexOf(PCI_SEQ + "00") > -1 && flag) {
                PCI_List.push(PCI_Item)
                PCI_Item = {}
                itemList = line.split(":")
                PCI_Item[PCI_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(PCI_SEQ + "02") > -1) {
                    PCI_Item[PCI_SEQ + "02"] = itemList[1]
                } else if (line.indexOf(PCI_SEQ + "03") > -1) {
                    PCI_Item[PCI_SEQ + "03"] = itemList[1]
                } else if (line.indexOf(PCI_SEQ + "04") > -1) {
                    PCI_Item[PCI_SEQ + "04"] = itemList[1]
                } else if (line.indexOf(PCI_SEQ + "05") > -1) {
                    PCI_Item[PCI_SEQ + "05"] = itemList[1]
                } else if (line.indexOf(PCI_SEQ + "06") > -1) {
                    PCI_Item[PCI_SEQ + "06"] = itemList[1]
                } else if (line.indexOf(PCI_SEQ + "07") > -1) {
                    PCI_Item[PCI_SEQ + "07"] = itemList[1]
                } else if (line.indexOf(PCI_SEQ + "08") > -1) {
                    PCI_Item[PCI_SEQ + "08"] = itemList[1]
                } else if (line.indexOf(PCI_SEQ + "09") > -1) {
                    PCI_Item[PCI_SEQ + "09"] = itemList[1]
                } else if (line.indexOf(PCI_SEQ + "10") > -1) {
                    PCI_Item[PCI_SEQ + "10"] = itemList[1]
                } else if (line.indexOf(PCI_SEQ + "11") > -1) {
                    PCI_Item[PCI_SEQ + "11"] = itemList[1]
                } else if (line.indexOf(PCI_SEQ + "12") > -1) {
                    PCI_Item[PCI_SEQ + "12"] = itemList[1]
                } else if (line.indexOf(PCI_SEQ + "14") > -1) {
                    PCI_Item[PCI_SEQ + "14"] = itemList[1]
                } else if (line.indexOf(PCI_SEQ + "15") > -1) {
                    PCI_Item[PCI_SEQ + "15"] = itemList[1]
                } else if (line.indexOf(PCI_SEQ + "16") > -1) {
                    PCI_Item[PCI_SEQ + "16"] = itemList[1]
                } else if (line.indexOf("RFF") > -1 && !RFF_Flag) {
                    RFF_Flag = true
                    let itemArray:string[] = line.split(":")
                    let RFF_SEQ:string = itemArray[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4) 
                    PCI_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                } else if (line.indexOf("DTM") > -1 && !DTM_Flag) {
                    DTM_Flag = true
                    let itemArray:string[] = line.split(":")
                    let DTM_SEQ:string = itemArray[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4) 
                    PCI_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("SGP") > -1 || line.indexOf("DGS") > -1 ||line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    PCI_List.push(PCI_Item)
                    break
                }
            }
            
        }

        return PCI_List
    }

    public static async getDataMEAGroup(lineList:string[], idx:number, segSeq:string) {
        let MEA_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let MEA_List:any[] = []
        let MEA_SEQ:string = "MEA" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(MEA_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                MEA_Item[MEA_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(MEA_SEQ) > -1 && flag) {
                MEA_List.push(MEA_Item)
                
                MEA_Item = {}
                itemList = line.split(":")
                MEA_Item[MEA_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("MEA" + segSeq + "02") > -1) {
                    MEA_Item['MEA'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "03") > -1) {
                    MEA_Item['MEA'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "04") > -1) {
                    MEA_Item['MEA'+ segSeq +'04'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "05") > -1) {
                    MEA_Item['MEA'+ segSeq +'05'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "07") > -1) {
                    MEA_Item['MEA'+ segSeq +'07'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "08") > -1) {
                    MEA_Item['MEA'+ segSeq +'08'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "09") > -1) {
                    MEA_Item['MEA'+ segSeq +'09'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "10") > -1) {
                    MEA_Item['MEA'+ segSeq +'10'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "11") > -1) {
                    MEA_Item['MEA'+ segSeq +'11'] = itemList[1]
                } else if (line.indexOf("MEA" + segSeq + "12") > -1) {
                    MEA_Item['MEA'+ segSeq +'12'] = itemList[1]
                } else if (line.indexOf("EQN") > -1) {
                    let itemArray:string[] = line.split(":")
                    let EQN_SEQ:string = itemArray[0].trim()
                    EQN_SEQ = EQN_SEQ.substr(3, 6) 
                    MEA_Item["EQN" + EQN_SEQ] = itemList[1]
                } else if (line.indexOf("DIM") > -1 || line.indexOf("RFF") > -1 ||line.indexOf("PCI") > -1 || line.indexOf("TPL") > -1 || 
                line.indexOf("SGP") > -1 || line.indexOf("DGS") > -1 ||line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    MEA_List.push(MEA_Item)
                    break
                }
            }
        }
        return MEA_List
    }

    public static async getDataDIMGroup(lineList:string[], idx:number, segSeq:string) {
        let DIM_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let DIM_List:any[] = []
        let DIM_SEQ:string = "DIM" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DIM_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                DIM_Item[DIM_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(DIM_SEQ) > -1 && flag) {
                DIM_List.push(DIM_Item)
                
                DIM_Item = {}
                itemList = line.split(":")
                DIM_Item[DIM_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("DIM" + segSeq + "02") > -1) {
                    DIM_Item['DIM'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("DIM" + segSeq + "03") > -1) {
                    DIM_Item['DIM'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("DIM" + segSeq + "04") > -1) {
                    DIM_Item['DIM'+ segSeq +'04'] = itemList[1]
                } else if (line.indexOf("DIM" + segSeq + "05") > -1) {
                    DIM_Item['DIM'+ segSeq +'05'] = itemList[1]
                } else if (line.indexOf("EQN") > -1) {
                    let itemArray:string[] = line.split(":")
                    let EQN_SEQ:string = itemArray[0].trim()
                    EQN_SEQ = EQN_SEQ.substr(3, 6) 
                    DIM_Item["EQN" + EQN_SEQ] = itemList[1]
                } else if (line.indexOf("RFF") > -1 ||line.indexOf("PCI") > -1 || line.indexOf("TPL") > -1 || 
                line.indexOf("SGP") > -1 || line.indexOf("DGS") > -1 ||line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    DIM_List.push(DIM_Item)
                    break
                }
            }
        }
        return DIM_List
    }

    public static async getDataTPLGroup(lineList:string[], idx:number, segSeq:string) {
        let TPL_Item:any = {}
        let line:string
        let flag:boolean = false
        let MEA_flag:boolean = false
        let itemList:string[]
        let TPL_List:any[] = []
        let TPL_SEQ:string = "TPL" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TPL_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                TPL_Item[TPL_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(TPL_SEQ + "01") > -1 && flag) {
                TPL_List.push(TPL_Item)
                
                TPL_Item = {}
                itemList = line.split(":")
                TPL_Item[TPL_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(TPL_SEQ + "02") > -1) {
                    TPL_Item[TPL_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(TPL_SEQ + "03") > -1) {
                    TPL_Item[TPL_SEQ +'03'] = itemList[1]
                } else if (line.indexOf(TPL_SEQ + "04") > -1) {
                    TPL_Item[TPL_SEQ +'04'] = itemList[1]
                } else if (line.indexOf(TPL_SEQ + "05") > -1) {
                    TPL_Item[TPL_SEQ +'05'] = itemList[1]
                } else if (line.indexOf("MEA") > -1 && !MEA_flag) {
                    MEA_flag = true
                    let itemArray:string[] = line.split(":")
                    let MEA_SEQ:string = itemArray[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4)                     
                    let TPL_SEQ_GRP:string = segSeq.substr(0, 2)
                    let MEA_SEQ_GRP:string = MEA_SEQ.substr(0, 2) 
                    if (MEA_SEQ_GRP === TPL_SEQ_GRP) {
                        TPL_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                    } else {
                        TPL_Item["MEA" + MEA_SEQ] = await this.getDataMEAGroup(lineList, i, MEA_SEQ)
                    }
                } else if (line.indexOf("SGP") > -1 || line.indexOf("DGS") > -1 ||line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    TPL_List.push(TPL_Item)
                    break
                }
            }
        }
        return TPL_List
    }

    public static async getDataGORGroup(lineList:string[], idx:number, segSeq:string) {
        let GOR_Item:any = {}
        let line:string
        let flag:boolean = false
        let DTM_flag:boolean = false
        let LOC_flag:boolean = false
        let SEL_flag:boolean = false
        let FTX_flag:boolean = false
        let DOC_flag:boolean = false
        let itemList:string[]
        let GOR_List:any[] = []
        let GOR_SEQ:string = "GOR" + segSeq + "00"
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(GOR_SEQ) > -1 && !flag) {
                itemList = line.split(":")
                GOR_Item[GOR_SEQ] = itemList[1]

                flag = true;
            } else if (line.indexOf(GOR_SEQ) > -1 && flag) {
                GOR_List.push(GOR_Item)
                
                GOR_Item = {}
                itemList = line.split(":")
                GOR_Item[GOR_SEQ] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf("GOR" + segSeq + "02") > -1) {
                    GOR_Item['GOR'+ segSeq +'02'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "03") > -1) {
                    GOR_Item['GOR'+ segSeq +'03'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "04") > -1) {
                    GOR_Item['GOR'+ segSeq +'04'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "05") > -1) {
                    GOR_Item['GOR'+ segSeq +'05'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "07") > -1) {
                    GOR_Item['GOR'+ segSeq +'07'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "08") > -1) {
                    GOR_Item['GOR'+ segSeq +'08'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "09") > -1) {
                    GOR_Item['GOR'+ segSeq +'09'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "10") > -1) {
                    GOR_Item['GOR'+ segSeq +'10'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "12") > -1) {
                    GOR_Item['GOR'+ segSeq +'12'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "13") > -1) {
                    GOR_Item['GOR'+ segSeq +'13'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "14") > -1) {
                    GOR_Item['GOR'+ segSeq +'14'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "15") > -1) {
                    GOR_Item['GOR'+ segSeq +'15'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "17") > -1) {
                    GOR_Item['GOR'+ segSeq +'17'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "18") > -1) {
                    GOR_Item['GOR'+ segSeq +'18'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "19") > -1) {
                    GOR_Item['GOR'+ segSeq +'19'] = itemList[1]
                } else if (line.indexOf("GOR" + segSeq + "20") > -1) {
                    GOR_Item['GOR'+ segSeq +'20'] = itemList[1]
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    let itemArray:string[] = line.split(":")
                    let DTM_SEQ:string = itemArray[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)  
                    GOR_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    let itemArray:string[] = line.split(":")
                    let LOC_SEQ:string = itemArray[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)  
                    GOR_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("SEL") > -1 && !SEL_flag) {
                    SEL_flag = true
                    let itemArray:string[] = line.split(":")
                    let SEL_SEQ:string = itemArray[0].trim()
                    SEL_SEQ = SEL_SEQ.substr(3, 4)  
                    GOR_Item["SEL" + SEL_SEQ] = await this.getDataSEL(lineList, i, SEL_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    let itemArray:string[] = line.split(":")
                    let FTX_SEQ:string = itemArray[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)  
                    GOR_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("DOC") > -1 && !DOC_flag) {
                    DOC_flag = true
                    let itemArray:string[] = line.split(":")
                    let DOC_SEQ:string = itemArray[0].trim()
                    DOC_SEQ = DOC_SEQ.substr(3, 4)                     
                    let TPL_SEQ_GRP:string = segSeq.substr(0, 2)
                    let DOC_SEQ_GRP:string = DOC_SEQ.substr(0, 2) 
                    if (DOC_SEQ_GRP === TPL_SEQ_GRP) {
                        GOR_Item["DOC" + DOC_SEQ] = await this.getDataDOC(lineList, i, DOC_SEQ)
                    } else {
                        GOR_Item["DOC" + DOC_SEQ] = await this.getDataDOCGroup(lineList, i, DOC_SEQ)
                    }
                } else if (line.indexOf("TDT") > -1 || line.indexOf("NAD") > -1 ||line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    GOR_List.push(GOR_Item)
                    break
                }
            }
        }
        return GOR_List
    }

    public static async getDataCNIGroup(lineList:string[], idx:number, segSeq:string) {
        let CNI_Item:any = {}
        let line:string
        let flag:boolean = false
        let LOC_flag:boolean = false
        let CNT_flag:boolean = false
        let DTM_flag:boolean = false
        let TDT_flag:boolean = false
        let NAD_flag:boolean = false
        let GID_flag:boolean = false
        let STS_flag:boolean = false
        let itemList:string[]
        let CNI_List:any[] = []
        let CNI_SEQ:string = "CNI" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(CNI_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                CNI_Item[CNI_SEQ + "00"] = itemList[1]

                flag = true;
            } else if (line.indexOf(CNI_SEQ + "00") > -1 && flag) {
                CNI_List.push(CNI_Item)
                
                CNI_Item = {}
                itemList = line.split(":")
                CNI_Item[CNI_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(CNI_SEQ + "02") > -1) {
                    CNI_Item[CNI_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(CNI_SEQ + "03") > -1) {
                    CNI_Item[CNI_SEQ +'03'] = itemList[1]
                } else if (line.indexOf(CNI_SEQ + "04") > -1) {
                    CNI_Item[CNI_SEQ +'04'] = itemList[1]
                } else if (line.indexOf(CNI_SEQ + "05") > -1) {
                    CNI_Item[CNI_SEQ +'05'] = itemList[1]
                } else if (line.indexOf(CNI_SEQ + "06") > -1) {
                    CNI_Item[CNI_SEQ +'06'] = itemList[1]
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    let itemArray:string[] = line.split(":")
                    let DTM_SEQ:string = itemArray[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)  
                    CNI_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    let itemArray:string[] = line.split(":")
                    let LOC_SEQ:string = itemArray[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)  
                    CNI_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("CNT") > -1 && !CNT_flag) {
                    CNT_flag = true
                    let itemArray:string[] = line.split(":")
                    let CNT_SEQ:string = itemArray[0].trim()
                    CNT_SEQ = CNT_SEQ.substr(3, 4)  
                    CNI_Item["CNT" + CNT_SEQ] = await this.getDataCNT(lineList, i, CNT_SEQ)
                } else if (line.indexOf("STS") > -1 && !STS_flag) {
                    STS_flag = true
                    let itemArray:string[] = line.split(":")
                    let STS_SEQ:string = itemArray[0].trim()
                    STS_SEQ = STS_SEQ.substr(3, 4)                    
                    CNI_Item["STS" + STS_SEQ] = await this.getDataSTSGroup(lineList, i, STS_SEQ)
                } else if (line.indexOf("TDT") > -1 && !TDT_flag) {
                    let itemArray:string[] = line.split(":")
                    let TDT_SEQ:string = itemArray[0].trim()
                    TDT_SEQ = TDT_SEQ.substr(3, 6) 
                    CNI_Item["TDT" + TDT_SEQ] = itemList[1]
                } else if (line.indexOf("NAD") > -1 && !NAD_flag) {
                    NAD_flag = true
                    let itemArray:string[] = line.split(":")
                    let NAD_SEQ:string = itemArray[0].trim()
                    NAD_SEQ = NAD_SEQ.substr(3, 4)
                    CNI_Item["NAD" + NAD_SEQ] = await this.getDataNADGroup(lineList, i, NAD_SEQ)
                } else if (line.indexOf("GID") > -1 && !GID_flag) {
                    GID_flag = true
                    let itemArray:string[] = line.split(":")
                    let GID_SEQ:string = itemArray[0].trim()
                    GID_SEQ = GID_SEQ.substr(3, 4)                    
                    CNI_Item["GID" + GID_SEQ] = await this.getDataGIDGroup(lineList, i, GID_SEQ)
                } else if (line.indexOf("UNT") > -1) {
                    CNI_List.push(CNI_Item)
                    break
                }
            }
        }
        return CNI_List
    }

    public static async getDataGDSGroup(lineList:string[], idx:number, segSeq:string) {
        let GDS_Item:any = {}
        let line:string
        let flag:boolean = false
        let FTX_flag:boolean = false
        let itemList:string[]
        let GDS_List:any[] = []
        let GDS_SEQ:string = "GDS" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(GDS_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                GDS_Item[GDS_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(GDS_SEQ + "01") > -1 && flag) {
                GDS_List.push(GDS_Item)
                
                GDS_Item = {}
                itemList = line.split(":")
                GDS_Item[GDS_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(GDS_SEQ + "02") > -1) {
                    GDS_Item[GDS_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(GDS_SEQ + "03") > -1) {
                    GDS_Item[GDS_SEQ +'03'] = itemList[1]
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    let itemArray:string[] = line.split(":")
                    let FTX_SEQ:string = itemArray[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)  
                    GDS_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("TDT") > -1 || line.indexOf("NAD") > -1 ||line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    GDS_List.push(GDS_Item)
                    break
                }
            }
        }
        return GDS_List
    }

    public static async getDataSTSGroup(lineList:string[], idx:number, segSeq:string) {
        let STS_Item:any = {}
        let line:string
        let flag:boolean = false
        let LOC_flag:boolean = false
        let RFF_flag:boolean = false
        let DTM_flag:boolean = false
        let FTX_flag:boolean = false
        let NAD_flag:boolean = false
        let PCI_flag:boolean = false
        let TDT_flag:boolean = false
        let EQD_flag:boolean = false
        let GID_flag:boolean = false
        let itemList:string[]
        let STS_List:any[] = []
        let STS_SEQ:string = "STS" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(STS_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                STS_Item[STS_SEQ + "01"] = itemList[1]

                flag = true;
            } else if (line.indexOf(STS_SEQ + "01") > -1 && flag) {
                STS_List.push(STS_Item)
                
                STS_Item = {}
                itemList = line.split(":")
                STS_Item[STS_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(STS_SEQ + "02") > -1) {
                    STS_Item[STS_SEQ +'02'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "03") > -1) {
                    STS_Item[STS_SEQ +'03'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "04") > -1) {
                    STS_Item[STS_SEQ +'04'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "05") > -1) {
                    STS_Item[STS_SEQ +'05'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "06") > -1) {
                    STS_Item[STS_SEQ +'06'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "07") > -1) {
                    STS_Item[STS_SEQ +'07'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "08") > -1) {
                    STS_Item[STS_SEQ +'08'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "10") > -1) {
                    STS_Item[STS_SEQ +'10'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "11") > -1) {
                    STS_Item[STS_SEQ +'11'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "12") > -1) {
                    STS_Item[STS_SEQ +'12'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "13") > -1) {
                    STS_Item[STS_SEQ +'13'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "15") > -1) {
                    STS_Item[STS_SEQ +'15'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "16") > -1) {
                    STS_Item[STS_SEQ +'16'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "17") > -1) {
                    STS_Item[STS_SEQ +'17'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "18") > -1) {
                    STS_Item[STS_SEQ +'18'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "20") > -1) {
                    STS_Item[STS_SEQ +'20'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "21") > -1) {
                    STS_Item[STS_SEQ +'21'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "22") > -1) {
                    STS_Item[STS_SEQ +'22'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "23") > -1) {
                    STS_Item[STS_SEQ +'23'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "25") > -1) {
                    STS_Item[STS_SEQ +'25'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "26") > -1) {
                    STS_Item[STS_SEQ +'26'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "27") > -1) {
                    STS_Item[STS_SEQ +'27'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "28") > -1) {
                    STS_Item[STS_SEQ +'28'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "30") > -1) {
                    STS_Item[STS_SEQ +'30'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "31") > -1) {
                    STS_Item[STS_SEQ +'31'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "32") > -1) {
                    STS_Item[STS_SEQ +'32'] = itemList[1]
                } else if (line.indexOf(STS_SEQ + "33") > -1) {
                    STS_Item[STS_SEQ +'33'] = itemList[1]
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    let itemArray:string[] = line.split(":")
                    let LOC_SEQ:string = itemArray[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)  
                    STS_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    let itemArray:string[] = line.split(":")
                    let RFF_SEQ:string = itemArray[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4)  
                    STS_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    let itemArray:string[] = line.split(":")
                    let DTM_SEQ:string = itemArray[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)  
                    STS_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    let itemArray:string[] = line.split(":")
                    let FTX_SEQ:string = itemArray[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)  
                    STS_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("NAD") > -1 && !NAD_flag) {
                    NAD_flag = true
                    let itemArray:string[] = line.split(":")
                    let NAD_SEQ:string = itemArray[0].trim()
                    NAD_SEQ = NAD_SEQ.substr(3, 4)  
                    STS_Item["NAD" + NAD_SEQ] = await this.getDataNADGroup(lineList, i, NAD_SEQ)
                } else if (line.indexOf("PCI") > -1 && !PCI_flag) {
                    PCI_flag = true
                    let itemArray:string[] = line.split(":")
                    let PCI_SEQ:string = itemArray[0].trim()
                    PCI_SEQ = PCI_SEQ.substr(3, 4)  
                    STS_Item["PCI" + PCI_SEQ] = await this.getDataPCIGroup(lineList, i, PCI_SEQ)
                } else if (line.indexOf("TDT") > -1 && !TDT_flag) {
                    TDT_flag = true
                    let itemArray:string[] = line.split(":")
                    let TDT_SEQ:string = itemArray[0].trim()
                    TDT_SEQ = TDT_SEQ.substr(3, 4)                   
                    STS_Item["TDT" + TDT_SEQ] = await this.getDataTDTGroup(lineList, i, TDT_SEQ)
                } else if (line.indexOf("GID") > -1 && !GID_flag) {
                    GID_flag = true
                    let itemArray:string[] = line.split(":")
                    let GID_SEQ:string = itemArray[0].trim()
                    GID_SEQ = GID_SEQ.substr(3, 4)                   
                    STS_Item["GID" + GID_SEQ] = await this.getDataGIDGroup(lineList, i, GID_SEQ)
                } else if (line.indexOf("EQD") > -1 && !EQD_flag) {
                    EQD_flag = true
                    let itemArray:string[] = line.split(":")
                    let EQD_SEQ:string = itemArray[0].trim()
                    EQD_SEQ = EQD_SEQ.substr(3, 4)                   
                    STS_Item["EQD" + EQD_SEQ] = await this.getDataEQDGroup(lineList, i, EQD_SEQ)
                } else if (line.indexOf("UNT") > -1 || line.indexOf("CNI") > -1) {
                    STS_List.push(STS_Item)
                    break
                }
            }
        }
        return STS_List
    }
}
export default MapperCommon