class MapperCommon {

    public static async getDataFTX(lineList:string[], idx:number, segSeq:string) {
        let FTX_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let FTX_List:any[] = []
        let FTX_SEQ:string = "FTX" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(FTX_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                FTX_Item[FTX_SEQ + "00"] = itemList[1]
                flag = true
            } else if (line.indexOf(FTX_SEQ + "00") > -1 && flag) {
                FTX_List.push(FTX_Item)
                
                FTX_Item = {}
                itemList = line.split(":")
                FTX_Item[FTX_SEQ + "00"] = itemList[1]                
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(FTX_SEQ) > -1) {
                    FTX_Item['FTX'+ itemSeq] = itemList[1]
                    if (itemList.length > 2) {
                        for (let i:number = 2; i < itemList.length; i++) {
                            FTX_Item['FTX'+ itemSeq] += ":" + itemList[i]
                        }
                    }
                } else {
                    FTX_List.push(FTX_Item)
                    break
                }
            }
        }

        return FTX_List
    }

    public static async getDataRFF(lineList:string[], idx:number, segSeq:string) {
        let RFF_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let RFF_List:any[] = []
        let RFF_SEQ:string = "RFF" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(RFF_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                RFF_Item[RFF_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(RFF_SEQ + "01") > -1 && flag) {
                RFF_List.push(RFF_Item)
                
                RFF_Item = {}
                itemList = line.split(":")
                RFF_Item[RFF_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(RFF_SEQ) > -1) {
                    RFF_Item["RFF"+ itemSeq] = itemList[1]
                } else {
                    RFF_List.push(RFF_Item)
                    break
                }
            }
        }
        return RFF_List
    }

    public static async getDataCTA(lineList:string[], idx:number, segSeq:string) {
        let CTA_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let CTA_List:any[] = []
        let CTA_SEQ:string = "CTA" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(CTA_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                CTA_Item[CTA_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(CTA_SEQ + "00") > -1 && flag) {
                CTA_List.push(CTA_Item)
                
                CTA_Item = {}
                itemList = line.split(":")
                CTA_Item[CTA_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(CTA_SEQ) > -1) {
                    CTA_Item['CTA'+ itemSeq] = itemList[1]
                } else {
                    CTA_List.push(CTA_Item)
                    break
                }
            }
        }
        return CTA_List
    }

    public static async getDataLOC(lineList:string[], idx:number, segSeq:string) {
        let LOC_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let LOC_List:any[] = []
        let LOC_SEQ:string = "LOC" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(LOC_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                LOC_Item[LOC_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(LOC_SEQ + "00") > -1 && flag) {
                LOC_List.push(LOC_Item)
                
                LOC_Item = {}
                itemList = line.split(":")
                LOC_Item[LOC_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(LOC_SEQ) > -1) {
                    LOC_Item['LOC'+ itemSeq] = itemList[1]
                } else {
                    LOC_List.push(LOC_Item)
                    break
                }
            }
        }
        //console.log(LOC_List)
        return LOC_List
    }

    public static async getDataDTM(lineList:string[], idx:number, segSeq:string) {
        let DTM_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let DTM_List:any[] = []
        let DTM_SEQ:string = "DTM" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DTM_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                DTM_Item[DTM_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(DTM_SEQ + "01") > -1 && flag) {
                DTM_List.push(DTM_Item)
                
                DTM_Item = {}
                itemList = line.split(":")
                DTM_Item[DTM_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(DTM_SEQ) > -1) {
                    DTM_Item['DTM'+ itemSeq] = itemList[1]
                } else {
                    DTM_List.push(DTM_Item)
                    break
                }
            }
        }
        return DTM_List
    }

    public static async getDataHAN(lineList:string[], idx:number, segSeq:string) {
        let HAN_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let HAN_List:any[] = []
        let HAN_SEQ:string = "HAN" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(HAN_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                HAN_Item[HAN_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(HAN_SEQ + "01") > -1 && flag) {
                HAN_List.push(HAN_Item)
                
                HAN_Item = {}
                itemList = line.split(":")
                HAN_Item[HAN_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf("HAN") > -1) {
                    HAN_Item['HAN'+ itemSeq] = itemList[1]
                } else {
                    HAN_List.push(HAN_Item)
                    break
                }
            }
        }
        return HAN_List
    }

    public static async getDataPIA(lineList:string[], idx:number, segSeq:string) {
        let PIA_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let PIA_List:any[] = []
        let PIA_SEQ:string = "PIA" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(PIA_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                PIA_Item[PIA_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(PIA_SEQ + "01") > -1 && flag) {
                PIA_List.push(PIA_Item)
                
                PIA_Item = {}
                itemList = line.split(":")
                PIA_Item[PIA_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(PIA_SEQ) > -1) {
                    PIA_Item['PIA'+ itemSeq] = itemList[1]
                } else {
                    PIA_List.push(PIA_Item)
                    break
                }
            }
        }
        return PIA_List
    }

    public static async getDataMEA(lineList:string[], idx:number, segSeq:string) {
        let MEA_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let MEA_List:any[] = []
        let MEA_SEQ:string = "MEA" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(MEA_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                MEA_Item[MEA_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(MEA_SEQ + "00") > -1 && flag) {
                MEA_List.push(MEA_Item)
                
                MEA_Item = {}
                itemList = line.split(":")
                MEA_Item[MEA_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(MEA_SEQ) > -1) {
                    MEA_Item['MEA'+ itemSeq] = itemList[1]
                } else {
                    MEA_List.push(MEA_Item)
                    break
                }
            }
        }
        return MEA_List
    }

    public static async getDataDIM(lineList:string[], idx:number, segSeq:string) {
        let DIM_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let DIM_List:any[] = []
        let DIM_SEQ:string = "DIM" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DIM_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                DIM_Item[DIM_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(DIM_SEQ + "00") > -1 && flag) {
                DIM_List.push(DIM_Item)
                
                DIM_Item = {}
                itemList = line.split(":")
                DIM_Item[DIM_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(DIM_SEQ) > -1) {
                    DIM_Item['DIM'+ itemSeq] = itemList[1]
                } else {
                    DIM_List.push(DIM_Item)
                    break
                }
            }
        }
        return DIM_List
    }

    public static async getDataDOC(lineList:string[], idx:number, segSeq:string) {
        let DOC_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let DOC_List:any[] = []
        let DOC_SEQ:string = "DOC" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DOC_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                DOC_Item[DOC_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(DOC_SEQ + "01") > -1 && flag) {
                DOC_List.push(DOC_Item)
                
                DOC_Item = {}
                itemList = line.split(":")
                DOC_Item[DOC_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(DOC_SEQ) > -1) {
                    DOC_Item['DOC'+ itemSeq] = itemList[1]
                } else {
                    DOC_List.push(DOC_Item)
                    break
                }
            }
        }
        return DOC_List
    }

    public static async getDataTMD(lineList:string[], idx:number, segSeq:string) {
        let TMD_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let TMD_List:any[] = []
        let TMD_SEQ:string = "TMD" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TMD_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                TMD_Item[TMD_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(TMD_SEQ + "01") > -1 && flag) {
                TMD_List.push(TMD_Item)
                
                TMD_Item = {}
                itemList = line.split(":")
                TMD_Item[TMD_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(TMD_SEQ) > -1) {
                    TMD_Item['TMD'+ itemSeq] = itemList[1]
                } else {
                    TMD_List.push(TMD_Item)
                    break
                }
            }
        }
        return TMD_List
    }

    public static async getDataTSR(lineList:string[], idx:number, segSeq:string) {
        let TSR_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let TSR_List:any[] = []
        let TSR_SEQ:string = "TSR" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TSR_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                TSR_Item[TSR_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(TSR_SEQ + "00") > -1 && flag) {
                TSR_List.push(TSR_Item)
                
                TSR_Item = {}
                itemList = line.split(":")
                TSR_Item[TSR_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(TSR_SEQ) > -1) {
                    TSR_Item['TSR'+ itemSeq] = itemList[1]
                } else {
                    TSR_List.push(TSR_Item)
                    break
                }
            }
        }
        return TSR_List
    }

    public static async getDataTMP(lineList:string[], idx:number, segSeq:string) {
        let TMP_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let TMP_List:any[] = []
        let TMP_SEQ:string = "TMP" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TMP_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                TMP_Item[TMP_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(TMP_SEQ + "00") > -1 && flag) {
                TMP_List.push(TMP_Item)
                
                TMP_Item = {}
                itemList = line.split(":")
                TMP_Item[TMP_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(TMP_SEQ) > -1) {
                    TMP_Item['TMP'+ itemSeq] = itemList[1]
                } else if (line.indexOf("RNG") > -1) {                    
                    let RNG_SEQ:string = itemList[0].trim()
                    RNG_SEQ = RNG_SEQ.substr(3, 6) 
                    TMP_Item["RNG" + RNG_SEQ] = itemList[1]
                } else if (line.indexOf("GID") > -1 || line.indexOf("EQD") > -1|| line.indexOf("UNT") > -1) {
                    TMP_List.push(TMP_Item)
                    break
                }
            }
        }
        return TMP_List
    }

    public static async getDataRNG(lineList:string[], idx:number, segSeq:string) {
        let RNG_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let RNG_List:any[] = []
        let RNG_SEQ:string = "RNG" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(RNG_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                RNG_Item[RNG_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(RNG_SEQ + "00") > -1 && flag) {
                RNG_List.push(RNG_Item)
                
                RNG_Item = {}
                itemList = line.split(":")
                RNG_Item[RNG_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(RNG_SEQ) > -1) {
                    RNG_Item['RNG'+ itemSeq] = itemList[1]
                } else {
                    RNG_List.push(RNG_Item)
                    break
                }
            }
        }
        return RNG_List
    }

    public static async getDataSEL(lineList:string[], idx:number, segSeq:string) {
        let SEL_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let SEL_List:any[] = []
        let SEL_SEQ:string = "SEL" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(SEL_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                SEL_Item[SEL_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(SEL_SEQ + "00") > -1 && flag) {
                SEL_List.push(SEL_Item)
                
                SEL_Item = {}
                itemList = line.split(":")
                SEL_Item[SEL_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(SEL_SEQ) > -1) {
                    SEL_Item['SEL'+ itemSeq] = itemList[1]
                } else {
                    SEL_List.push(SEL_Item)
                    break
                }
            }
        }
        return SEL_List
    }

    public static async getDataMOA(lineList:string[], idx:number, segSeq:string) {
        let MOA_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let MOA_List:any[] = []
        let MOA_SEQ:string = "MOA" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(MOA_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                MOA_Item[MOA_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(MOA_SEQ + "01") > -1 && flag) {
                MOA_List.push(MOA_Item)
                
                MOA_Item = {}
                itemList = line.split(":")
                MOA_Item[MOA_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(MOA_SEQ) > -1) {
                    MOA_Item['MOA'+ itemSeq] = itemList[1]
                } else {
                    MOA_List.push(MOA_Item)
                    break
                }
            }
        }
        return MOA_List
    }

    public static async getDataGOR(lineList:string[], idx:number, segSeq:string) {
        let GOR_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let GOR_List:any[] = []
        let GOR_SEQ:string = "GOR" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(GOR_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                GOR_Item[GOR_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(GOR_SEQ + "00") > -1 && flag) {
                GOR_List.push(GOR_Item)
                
                GOR_Item = {}
                itemList = line.split(":")
                GOR_Item[GOR_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(GOR_SEQ) > -1) {
                    GOR_Item['GOR'+ itemSeq] = itemList[1]
                } else {
                    GOR_List.push(GOR_Item)
                    break
                }
            }
        }
        return GOR_List
    }
    
    public static async getDataDGS(lineList:string[], idx:number, segSeq:string) {
        let DGS_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let DGS_List:any[] = []
        let DGS_SEQ:string = "DGS" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DGS_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                DGS_Item[DGS_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(DGS_SEQ + "00") > -1 && flag) {
                DGS_List.push(DGS_Item)
                
                DGS_Item = {}
                itemList = line.split(":")
                DGS_Item[DGS_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(DGS_SEQ) > -1) {
                    DGS_Item['DGS'+ itemSeq] = itemList[1]
                } else {
                    DGS_List.push(DGS_Item)
                    break
                }
            }
        }
        return DGS_List
    }

    public static async getDataCOM(lineList:string[], idx:number, segSeq:string) {
        let COM_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let COM_List:any[] = []
        let COM_SEQ:string = 'COM' + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(COM_SEQ + '01') > -1 && !flag) {
                itemList = line.split(":")
                COM_Item[COM_SEQ + '01'] = itemList[1]

                flag = true
            } else if (line.indexOf(COM_SEQ + '01') > -1 && flag) {
                COM_List.push(COM_Item)
                
                COM_Item = {}
                itemList = line.split(":")
                COM_Item[COM_SEQ + '01'] = itemList[1]
            } else {
                itemList = line.split(":")
                if (line.indexOf(COM_SEQ + "02") > -1) {
                    COM_Item[COM_SEQ + '02'] = itemList[1]
                } else {
                    COM_List.push(COM_Item)
                    break
                }
            }
        }
        return COM_List
    }

    public static async getDataValue(data:string) {
        if (!data) {
            return ""
        }
        return data.trim()
    }

    public static async getDataSGP(lineList:string[], idx:number, segSeq:string) {
        let SGP_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let SGP_List:any[] = []
        let SGP_SEQ:string = "SGP" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(SGP_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                SGP_Item[SGP_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(SGP_SEQ + "01") > -1 && flag) {
                SGP_List.push(SGP_Item)
                
                SGP_Item = {}
                itemList = line.split(":")
                SGP_Item[SGP_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(SGP_SEQ) > -1) {
                    SGP_Item['SGP'+ itemSeq] = itemList[1]
                } else {
                    SGP_List.push(SGP_Item)
                    break
                }
            }
        }
        return SGP_List
    }

    public static async getDataDAMGroup(lineList:string[], idx:number, segSeq:string) {
        let DAM_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let DAM_List:any[] = []
        let DAM_SEQ:string = "DAM" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DAM_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                DAM_Item[DAM_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(DAM_SEQ + "00") > -1 && flag) {
                DAM_List.push(DAM_Item)
                
                DAM_Item = {}
                itemList = line.split(":")
                DAM_Item[DAM_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(DAM_SEQ) > -1) {
                    DAM_Item["DAM" + itemSeq] = itemList[1]
                } else if (line.indexOf("COD") > -1) {
                    
                    let COD_SEQ:string = itemList[0].trim()
                    COD_SEQ = COD_SEQ.substr(3, 6) 
                    DAM_Item["COD" + COD_SEQ] = itemList[1]
                } else if (line.indexOf("TDT") > -1 || line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    DAM_List.push(DAM_Item)
                    break
                }
            }
            
        }

        return DAM_List
    }

    public static async getDataGIDGroup(lineList:string[], idx:number, segSeq:string) {
        let GID_Item:any = {}
        let line:string
        let flag:boolean = false
        let HAN_Flag:boolean = false
        let FTX_Flag:boolean = false
        let RFF_Flag:boolean = false
        let PIA_Flag:boolean = false
        let NAD_Flag:boolean = false
        let MEA_Flag:boolean = false
        let DIM_Flag:boolean = false
        let DOC_Flag:boolean = false
        let SGP_Flag:boolean = false
        let DGS_Flag:boolean = false
        let TMP_Flag:boolean = false
        let RNG_Flag:boolean = false
        let TMD_Flag:boolean = false
        let LOC_Flag:boolean = false
        let MOA_Flag:boolean = false
        let GDS_Flag:boolean = false
        let PCI_Flag:boolean = false
        let TPL_Flag:boolean = false
        let TCC_Flag:boolean = false
        let itemList:string[]
        let GID_List:any[] = []
        let GID_SEQ:string = "GID" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(GID_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                GID_Item[GID_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(GID_SEQ + "00") > -1 && flag) {
                GID_List.push(GID_Item)
                
                GID_Item = {}
                itemList = line.split(":")
                GID_Item[GID_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(GID_SEQ) > -1) {
                    GID_Item["GID" + itemSeq] = itemList[1]
                } else if (line.indexOf("HAN") > -1 && !HAN_Flag) {
                    HAN_Flag = true
                    
                    let HAN_SEQ:string = itemList[0].trim()
                    HAN_SEQ = HAN_SEQ.substr(3, 4)
                    GID_Item["HAN" + HAN_SEQ] = await this.getDataHAN(lineList, i, HAN_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_Flag) {
                    FTX_Flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)
                    GID_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("RFF") > -1 && !RFF_Flag) {
                    RFF_Flag = true
                    
                    let RFF_SEQ:string = itemList[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4)
                    let GID_SEQ_GRP:string = segSeq.substr(0, 2)
                    let RFF_SEQ_GRP:string = RFF_SEQ.substr(0, 2) 
                    if (GID_SEQ_GRP === RFF_SEQ_GRP) {
                        GID_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                    } else {
                        GID_Item["RFF" + RFF_SEQ] = await this.getDataRFFGroup(lineList, i, RFF_SEQ)
                    }
                } else if (line.indexOf("PIA") > -1 && !PIA_Flag) {
                    PIA_Flag = true
                    
                    let PIA_SEQ:string = itemList[0].trim()
                    PIA_SEQ = PIA_SEQ.substr(3, 4)
                    GID_Item["PIA" + PIA_SEQ] = await this.getDataPIA(lineList, i, PIA_SEQ)
                } else if (line.indexOf("NAD") > -1 && !NAD_Flag) {
                    NAD_Flag = true
                    
                    let NAD_SEQ:string = itemList[0].trim()
                    NAD_SEQ = NAD_SEQ.substr(3, 4)
                    GID_Item["NAD" + NAD_SEQ] = await this.getDataNADGroup(lineList, i, NAD_SEQ)
                } else if (line.indexOf("MEA") > -1 && !MEA_Flag) {
                    MEA_Flag = true
                    
                    let MEA_SEQ:string = itemList[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4)
                    let GID_SEQ_GRP:string = segSeq.substr(0, 2)
                    let MEA_SEQ_GRP:string = MEA_SEQ.substr(0, 2) 
                    if (GID_SEQ_GRP === MEA_SEQ_GRP) {
                        GID_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                    } else {
                        GID_Item["MEA" + MEA_SEQ] = await this.getDataMEAGroup(lineList, i, MEA_SEQ)
                    }
                } else if (line.indexOf("DIM") > -1 && !DIM_Flag) {
                    DIM_Flag = true
                    
                    let DIM_SEQ:string = itemList[0].trim()
                    DIM_SEQ = DIM_SEQ.substr(3, 4)
                    let GID_SEQ_GRP:string = segSeq.substr(0, 2)
                    let DIM_SEQ_GRP:string = DIM_SEQ.substr(0, 2) 
                    if (GID_SEQ_GRP === DIM_SEQ_GRP) {
                        GID_Item["DIM" + DIM_SEQ] = await this.getDataDIM(lineList, i, DIM_SEQ)
                    } else {
                        GID_Item["DIM" + DIM_SEQ] = await this.getDataDIMGroup(lineList, i, DIM_SEQ)
                    }
                } else if (line.indexOf("DOC") > -1 && !DOC_Flag) {
                    DOC_Flag = true
                    
                    let DOC_SEQ:string = itemList[0].trim()
                    DOC_SEQ = DOC_SEQ.substr(3, 4)
                    let GID_SEQ_GRP:string = segSeq.substr(0, 2)
                    let DOC_SEQ_GRP:string = DOC_SEQ.substr(0, 2) 
                    if (GID_SEQ_GRP === DOC_SEQ_GRP) {
                        GID_Item["DOC" + DOC_SEQ] = await this.getDataDOC(lineList, i, DOC_SEQ)
                    } else {
                        GID_Item["DOC" + DOC_SEQ] = await this.getDataDOCGroup(lineList, i, DOC_SEQ)
                    }
                } else if (line.indexOf("SGP") > -1 && !SGP_Flag) {
                    SGP_Flag = true
                    
                    let SGP_SEQ:string = itemList[0].trim()
                    SGP_SEQ = SGP_SEQ.substr(3, 4)
                    GID_Item["SGP" + SGP_SEQ] = await this.getDataSGPGroup(lineList, i, SGP_SEQ)
                } else if (line.indexOf("DGS") > -1 && !DGS_Flag) {
                    DGS_Flag = true
                    
                    let DGS_SEQ:string = itemList[0].trim()
                    DGS_SEQ = DGS_SEQ.substr(3, 4)
                    GID_Item["DGS" + DGS_SEQ] = await this.getDataDGSGroup(lineList, i, DGS_SEQ)
                } else if (line.indexOf("TMP") > -1 && !TMP_Flag) {
                    TMP_Flag = true
                    
                    let TMP_SEQ:string = itemList[0].trim()
                    TMP_SEQ = TMP_SEQ.substr(3, 4)
                    GID_Item["TMP" + TMP_SEQ] = await this.getDataTMP(lineList, i, TMP_SEQ)
                } else if (line.indexOf("RNG") > -1 && !RNG_Flag) {
                    RNG_Flag = true
                    
                    let RNG_SEQ:string = itemList[0].trim()
                    RNG_SEQ = RNG_SEQ.substr(3, 4)
                    GID_Item["RNG" + RNG_SEQ] = await this.getDataRNG(lineList, i, RNG_SEQ)
                } else if (line.indexOf("TMD") > -1 && !TMD_Flag) {
                    TMD_Flag = true
                    
                    let TMD_SEQ:string = itemList[0].trim()
                    TMD_SEQ = TMD_SEQ.substr(3, 4)
                    GID_Item["TMD" + TMD_SEQ] = await this.getDataTMD(lineList, i, TMD_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_Flag) {
                    LOC_Flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)
                    GID_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("MOA") > -1 && !MOA_Flag) {
                    MOA_Flag = true
                    
                    let MOA_SEQ:string = itemList[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4)
                    GID_Item["MOA" + MOA_SEQ] = await this.getDataMOA(lineList, i, MOA_SEQ)
                } else if (line.indexOf("GDS") > -1 && !GDS_Flag) {
                    GDS_Flag = true
                    
                    let GDS_SEQ:string = itemList[0].trim()
                    GDS_SEQ = GDS_SEQ.substr(3, 4)
                    GID_Item["GDS" + GDS_SEQ] = await this.getDataGDS(lineList, i, GDS_SEQ)
                } else if (line.indexOf("PCI") > -1 && !PCI_Flag) {
                    PCI_Flag = true
                    
                    let PCI_SEQ:string = itemList[0].trim()
                    PCI_SEQ = PCI_SEQ.substr(3, 4)
                    GID_Item["PCI" + PCI_SEQ] = await this.getDataPCIGroup(lineList, i, PCI_SEQ)
                } else if (line.indexOf("TPL") > -1 && !TPL_Flag) {
                    TPL_Flag = true
                    
                    let TPL_SEQ:string = itemList[0].trim()
                    TPL_SEQ = TPL_SEQ.substr(3, 4)
                    let GID_SEQ_GRP:string = segSeq.substr(0, 2)
                    let TPL_SEQ_GRP:string = TPL_SEQ.substr(0, 2) 
                    if (GID_SEQ_GRP === TPL_SEQ_GRP) {
                        GID_Item["TPL" + TPL_SEQ] = await this.getDataTPL(lineList, i, TPL_SEQ)
                    } else {
                        GID_Item["TPL" + TPL_SEQ] = await this.getDataTPLGroup(lineList, i, TPL_SEQ)
                    }
                } else if (line.indexOf("TCC") > -1 && !TCC_Flag) {
                    TCC_Flag = true
                    
                    let TCC_SEQ:string = itemList[0].trim()
                    TCC_SEQ = TCC_SEQ.substr(3, 4)
                    GID_Item["TCC" + TCC_SEQ] = await this.getDataTCCGroup(lineList, i, TCC_SEQ)
                } else if (line.indexOf("EQD") > -1 || line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    GID_List.push(GID_Item)
                    break
                }
            }
        }
        return GID_List
    }    

    public static async getDataTDTGroup(lineList:string[], idx:number, segSeq:string) {
        let TDT_Item:any = {}
        let line:string
        let flag:boolean = false
        let RFF_flag:boolean = false
        let LOC_flag:boolean = false
        let DTM_flag:boolean = false
        let FTX_flag:boolean = false
        let TSR_flag:boolean = false
        let itemList:string[]
        let TDT_List:any[] = []
        let TDT_SEQ:string = "TDT" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TDT_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                TDT_Item[TDT_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(TDT_SEQ + "00") > -1 && flag) {
                TDT_List.push(TDT_Item)
                
                TDT_Item = {}
                itemList = line.split(":")
                TDT_Item[TDT_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(TDT_SEQ) > -1) {
                    TDT_Item["TDT" + itemSeq] = itemList[1]
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    
                    let RFF_SEQ:string = itemList[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4) 
                    let TDT_SEQ_GRP:string = segSeq.substr(0, 2)
                    let RFF_SEQ_GRP:string = RFF_SEQ.substr(0, 2) 
                    if (RFF_SEQ_GRP === TDT_SEQ_GRP) {
                        TDT_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                    } else {
                        TDT_Item["RFF" + RFF_SEQ] = await this.getDataRFFGroup(lineList, i, RFF_SEQ)
                    }
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4) 
                    let TDT_SEQ_GRP:string = segSeq.substr(0, 2)
                    let LOC_SEQ_GRP:string = LOC_SEQ.substr(0, 2) 
                    if (LOC_SEQ_GRP === TDT_SEQ_GRP) {
                        TDT_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                    } else {
                        TDT_Item["LOC" + LOC_SEQ] = await this.getDataLOCGroup(lineList, i, LOC_SEQ)
                    }                    
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4) 
                    let TDT_SEQ_GRP:string = segSeq.substr(0, 2)
                    let DTM_SEQ_GRP:string = DTM_SEQ.substr(0, 2) 
                    if (DTM_SEQ_GRP === TDT_SEQ_GRP) {
                        TDT_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                    }
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4) 
                    TDT_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("TSR") > -1 && !TSR_flag) {
                    TSR_flag = true
                    
                    let TSR_SEQ:string = itemList[0].trim()
                    TSR_SEQ = TSR_SEQ.substr(3, 4) 
                    TDT_Item["TSR" + TSR_SEQ] = await this.getDataTSR(lineList, i, TSR_SEQ)
                } else if (line.indexOf("NAD") > -1 || line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    TDT_List.push(TDT_Item)
                    break
                } 
            }
            
        }

        return TDT_List
    }

    public static async getDataNADGroup(lineList:string[], idx:number, segSeq:string) {
        let NAD_Item:any = {}
        let line:string
        let flag:boolean = false
        let CTA_flag:boolean = false
        let RFF_flag:boolean = false
        let DTM_flag:boolean = false
        let LOC_flag:boolean = false
        let DOC_flag:boolean = false
        let TSR_flag:boolean = false
        let CPI_flag:boolean = false
        let TCC_flag:boolean = false
        let MOA_flag:boolean = false
        let itemList:string[]
        let NAD_List:any[] = []
        let NAD_SEQ:string = "NAD" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(NAD_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                NAD_Item[NAD_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(NAD_SEQ + "00") > -1 && flag) {
                NAD_List.push(NAD_Item)
                
                NAD_Item = {}
                itemList = line.split(":")
                NAD_Item[NAD_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(NAD_SEQ) > -1) {
                    NAD_Item["NAD" + itemSeq] = itemList[1]
                } else if (line.indexOf("CTA") > -1 && !CTA_flag) {
                    CTA_flag = true
                    
                    let CTA_SEQ:string = itemList[0].trim()
                    CTA_SEQ = CTA_SEQ.substr(3, 4) 
                    let NAD_SEQ_GRP:string = segSeq.substr(0, 2)
                    let CTA_SEQ_GRP:string = CTA_SEQ.substr(0, 2) 
                    if (CTA_SEQ_GRP === NAD_SEQ_GRP) {
                        NAD_Item["CTA" + CTA_SEQ] = await this.getDataCTA(lineList, i, CTA_SEQ)
                    } else {
                        NAD_Item["CTA" + CTA_SEQ] = await this.getDataCTAGroup(lineList, i, CTA_SEQ)
                    }
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    
                    let RFF_SEQ:string = itemList[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4) 
                    let NAD_SEQ_GRP:string = segSeq.substr(0, 2)
                    let RFF_SEQ_GRP:string = RFF_SEQ.substr(0, 2) 
                    if (RFF_SEQ_GRP === NAD_SEQ_GRP) {
                        NAD_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                    } else {
                        NAD_Item["RFF" + RFF_SEQ] = await this.getDataRFFGroup(lineList, i, RFF_SEQ)
                    }
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4) 
                    NAD_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("COM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let COM_SEQ:string = itemList[0].trim()
                    COM_SEQ = COM_SEQ.substr(3, 4) 
                    NAD_Item["COM" + COM_SEQ] = await this.getDataCOM(lineList, i, COM_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4) 
                    NAD_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("DOC") > -1 && !DOC_flag) {
                    DOC_flag = true
                    
                    let DOC_SEQ:string = itemList[0].trim()
                    DOC_SEQ = DOC_SEQ.substr(3, 4) 
                    let NAD_SEQ_GRP:string = segSeq.substr(0, 2)
                    let DOC_SEQ_GRP:string = DOC_SEQ.substr(0, 2) 
                    if (DOC_SEQ_GRP === NAD_SEQ_GRP) {
                        NAD_Item["DOC" + DOC_SEQ] = await this.getDataDOC(lineList, i, DOC_SEQ)
                    } else {
                        NAD_Item["DOC" + DOC_SEQ] = await this.getDataDOCGroup(lineList, i, DOC_SEQ)
                    }
                } else if (line.indexOf("CPI") > -1 && !CPI_flag) {
                    CPI_flag = true
                    
                    let CPI_SEQ:string = itemList[0].trim()
                    CPI_SEQ = CPI_SEQ.substr(3, 4) 
                    let NAD_SEQ_GRP:string = segSeq.substr(0, 2)
                    let CPI_SEQ_GRP:string = CPI_SEQ.substr(0, 2) 
                    if (CPI_SEQ_GRP === NAD_SEQ_GRP) {
                        NAD_Item["CPI" + CPI_SEQ] = await this.getDataCPI(lineList, i, CPI_SEQ)
                    } else {
                        NAD_Item["CPI" + CPI_SEQ] = await this.getDataCPIGroup(lineList, i, CPI_SEQ)
                    }
                } else if (line.indexOf("TSR") > -1 && !TSR_flag) {
                    TSR_flag = true
                    
                    let TSR_SEQ:string = itemList[0].trim()
                    TSR_SEQ = TSR_SEQ.substr(3, 4) 
                    let NAD_SEQ_GRP:string = segSeq.substr(0, 2)
                    let TSR_SEQ_GRP:string = TSR_SEQ.substr(0, 2) 
                    if (TSR_SEQ_GRP === NAD_SEQ_GRP) {
                        NAD_Item["TSR" + TSR_SEQ] = await this.getDataTSR(lineList, i, TSR_SEQ)
                    } else {
                        NAD_Item["TSR" + TSR_SEQ] = await this.getDataTSRGroup(lineList, i, TSR_SEQ)
                    }
                } else if (line.indexOf("TCC") > -1 && !TCC_flag) {
                    TCC_flag = true
                    
                    let TCC_SEQ:string = itemList[0].trim()
                    TCC_SEQ = TCC_SEQ.substr(3, 4) 
                    NAD_Item["TCC" + TCC_SEQ] = await this.getDataTCCGroup(lineList, i, TCC_SEQ)                    
                } else if (line.indexOf("MOA") > -1 && !MOA_flag) {
                    MOA_flag = true
                    
                    let MOA_SEQ:string = itemList[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4) 
                    NAD_Item["MOA" + MOA_SEQ] = await this.getDataMOA(lineList, i, MOA_SEQ)
                } else if (line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    NAD_List.push(NAD_Item)
                    break
                }
            }
            
        }

        return NAD_List
    }

    public static async getDataSGPGroup(lineList:string[], idx:number, segSeq:string) {
        let SGP_Item:any = {}
        let line:string
        let flag:boolean = false
        let MEA_flag:boolean = false
        let itemList:string[]
        let SGP_List:any[] = []
        let SGP_SEQ:string = "SGP" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(SGP_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                SGP_Item[SGP_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(SGP_SEQ + "01") > -1 && flag) {
                SGP_List.push(SGP_Item)
                
                SGP_Item = {}
                itemList = line.split(":")
                SGP_Item[SGP_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(SGP_SEQ) > -1) {
                    SGP_Item["SGP" + itemSeq] = itemList[1]
                } else if (line.indexOf("MEA") > -1 && !MEA_flag) {//MEA050100
                    MEA_flag = true
                    
                    let MEA_SEQ:string = itemList[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4) 
                    SGP_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                } else if (line.indexOf("DGS") > -1 || line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    SGP_List.push(SGP_Item)
                    break
                }
            }
            
        }

        return SGP_List
    }

    public static async getDataDGSGroup(lineList:string[], idx:number, segSeq:string) {
        let DGS_Item:any = {}
        let line:string
        let flag:boolean = false
        let FTX_Flag:boolean = false
        let MEA_Flag:boolean = false
        let CTA_Flag:boolean = false
        let SGP_Flag:boolean = false
        let itemList:string[]
        let DGS_List:any[] = []
        let DGS_SEQ:string = "DGS" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DGS_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                DGS_Item[DGS_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(DGS_SEQ + "00") > -1 && flag) {
                DGS_List.push(DGS_Item)
                DGS_Item = {}
                itemList = line.split(":")
                DGS_Item[DGS_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(DGS_SEQ) > -1) {
                    DGS_Item["DGS" + itemSeq] = itemList[1]
                } else if (line.indexOf("FTX") > -1 && !FTX_Flag) {
                    FTX_Flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4) 
                    DGS_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("MEA") > -1 && !MEA_Flag) {
                    MEA_Flag = true
                    
                    let MEA_SEQ:string = itemList[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4) 
                    let DGS_SEQ_GRP:string = segSeq.substr(0, 2)
                    let MEA_SEQ_GRP:string = MEA_SEQ.substr(0, 2) 
                    if (MEA_SEQ_GRP === DGS_SEQ_GRP) {
                        DGS_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                    } else {
                        DGS_Item["MEA" + MEA_SEQ] = await this.getDataMEAGroup(lineList, i, MEA_SEQ)
                    }
                } else if (line.indexOf("CTA") > -1 && !CTA_Flag) {
                    CTA_Flag = true
                    
                    let CTA_SEQ:string = itemList[0].trim()
                    CTA_SEQ = CTA_SEQ.substr(3, 4) 
                    let DGS_SEQ_GRP:string = segSeq.substr(0, 2)
                    let CTA_SEQ_GRP:string = CTA_SEQ.substr(0, 2) 
                    if (CTA_SEQ_GRP === DGS_SEQ_GRP) {
                        DGS_Item["CTA" + CTA_SEQ] = await this.getDataCTA(lineList, i, CTA_SEQ)
                    } else {
                        DGS_Item["CTA" + CTA_SEQ] = await this.getDataCTAGroup(lineList, i, CTA_SEQ)
                    }
                } else if (line.indexOf("SGP") > -1 && !SGP_Flag) {
                    SGP_Flag = true
                    
                    let SGP_SEQ:string = itemList[0].trim()
                    SGP_SEQ = SGP_SEQ.substr(3, 4) 
                    let DGS_SEQ_GRP:string = segSeq.substr(0, 2)
                    let SGP_SEQ_GRP:string = SGP_SEQ.substr(0, 2) 
                    if (SGP_SEQ_GRP === DGS_SEQ_GRP) {
                        DGS_Item["SGP" + SGP_SEQ] = await this.getDataSGP(lineList, i, SGP_SEQ)
                    } else {
                        DGS_Item["SGP" + SGP_SEQ] = await this.getDataSGPGroup(lineList, i, SGP_SEQ)
                    }
                } else if (line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    DGS_List.push(DGS_Item)
                    break
                }
            }
            
        }

        return DGS_List
    }

    public static async getDataEQDGroup(lineList:string[], idx:number, segSeq:string) {
        let EQD_Item:any = {}
        let line:string
        let flag:boolean = false
        let RFF_flag:boolean = false
        let TMD_flag:boolean = false
        let DTM_flag:boolean = false
        let TSR_flag:boolean = false
        let LOC_flag:boolean = false
        let MEA_flag:boolean = false
        let DIM_flag:boolean = false
        let TMP_flag:boolean = false
        let RGN_flag:boolean = false
        let SEL_flag:boolean = false
        let FTX_flag:boolean = false
        let DGS_flag:boolean = false
        let MOA_flag:boolean = false
        let GOR_flag:boolean = false
        let DAM_flag:boolean = false
        let TDT_flag:boolean = false
        let NAD_flag:boolean = false
        let EQA_flag:boolean = false
        let TPL_flag:boolean = false
        let HAN_flag:boolean = false
        let TCC_flag:boolean = false
        let itemList:string[]
        let EQD_List:any[] = []
        let EQD_SEQ:string = "EQD" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(EQD_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                EQD_Item[EQD_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(EQD_SEQ + "00") > -1 && flag) {
                EQD_List.push(EQD_Item)
                
                EQD_Item = {}
                itemList = line.split(":")
                EQD_Item[EQD_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(EQD_SEQ) > -1) {
                    EQD_Item["EQD" + itemSeq] = itemList[1]
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4) 
                    EQD_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("EQN") > -1) {
                    
                    let EQN_SEQ:string = itemList[0].trim()
                    EQN_SEQ = EQN_SEQ.substr(3, 6) 
                    EQD_Item["EQN" + EQN_SEQ] = itemList[1]
                } else if (line.indexOf("TMD") > -1 && !TMD_flag) {
                    TMD_flag = true
                    
                    let TMD_SEQ:string = itemList[0].trim()
                    TMD_SEQ = TMD_SEQ.substr(3, 4) 
                    EQD_Item["TMD" + TMD_SEQ] = await this.getDataTMD(lineList, i, TMD_SEQ)
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)
                    EQD_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("TSR") > -1 && !TSR_flag) {
                    TSR_flag = true
                    
                    let TSR_SEQ:string = itemList[0].trim()
                    TSR_SEQ = TSR_SEQ.substr(3, 4)
                    EQD_Item["TSR" + TSR_SEQ] = await this.getDataTSR(lineList, i, TSR_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)
                    EQD_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("MEA") > -1 && !MEA_flag) {
                    MEA_flag = true
                    
                    let MEA_SEQ:string = itemList[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4)
                    EQD_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                } else if (line.indexOf("DIM") > -1 && DIM_flag) {
                    DIM_flag = true
                    
                    let DIM_SEQ:string = itemList[0].trim()
                    DIM_SEQ = DIM_SEQ.substr(3, 4)
                    EQD_Item["DIM" + DIM_SEQ] = await this.getDataDIM(lineList, i, DIM_SEQ)
                } else if (line.indexOf("TMP") > -1 && !TMP_flag) {
                    TMP_flag = true
                    
                    let TMP_SEQ:string = itemList[0].trim()
                    TMP_SEQ = TMP_SEQ.substr(3, 4)
                    EQD_Item["TMP" + TMP_SEQ] = await this.getDataTMP(lineList, i, TMP_SEQ)
                } else if (line.indexOf("RNG") > -1 && !RGN_flag) {
                    RGN_flag = true
                    
                    let RNG_SEQ:string = itemList[0].trim()
                    RNG_SEQ = RNG_SEQ.substr(3, 4)
                    EQD_Item["RNG" + RNG_SEQ] = await this.getDataRNG(lineList, i, RNG_SEQ)
                } else if (line.indexOf("SEL") > -1 && !SEL_flag) {
                    SEL_flag = true
                    
                    let SEL_SEQ:string = itemList[0].trim()
                    SEL_SEQ = SEL_SEQ.substr(3, 4)
                    EQD_Item["SEL" + SEL_SEQ] = await this.getDataSEL(lineList, i, SEL_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)
                    EQD_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("DGS") > -1 && !DGS_flag) {
                    DGS_flag = true
                    
                    let DGS_SEQ:string = itemList[0].trim()
                    DGS_SEQ = DGS_SEQ.substr(3, 4)
                    EQD_Item["DGS" + DGS_SEQ] = await this.getDataDGS(lineList, i, DGS_SEQ)
                } else if (line.indexOf("MOA") > -1 && !MOA_flag) {
                    MOA_flag = true
                    
                    let MOA_SEQ:string = itemList[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4)
                    EQD_Item["MOA" + MOA_SEQ] = await this.getDataMOA(lineList, i, MOA_SEQ)
                } else if (line.indexOf("GOR") > -1 && !GOR_flag) {
                    GOR_flag = true
                    
                    let GOR_SEQ:string = itemList[0].trim()
                    GOR_SEQ = GOR_SEQ.substr(3, 4)
                    EQD_Item["GOR" + GOR_SEQ] = await this.getDataGOR(lineList, i, GOR_SEQ)
                } else if (line.indexOf("EQA") > -1 && !EQA_flag) {
                    EQA_flag = true
                    
                    let EQA_SEQ:string = itemList[0].trim()
                    EQA_SEQ = EQA_SEQ.substr(3, 4)
                    EQD_Item["EQA" + EQA_SEQ] = await this.getDataEQA(lineList, i, EQA_SEQ)
                } else if (line.indexOf("DAM") > -1 && !DAM_flag) {
                    DAM_flag = true
                    
                    let DAM_SEQ:string = itemList[0].trim()
                    DAM_SEQ = DAM_SEQ.substr(3, 4)
                    EQD_Item["DAM" + DAM_SEQ] = await this.getDataDAMGroup(lineList, i, DAM_SEQ)
                } else if (line.indexOf("TDT") > -1 && !TDT_flag) {
                    TDT_flag = true
                    
                    let TDT_SEQ:string = itemList[0].trim()
                    TDT_SEQ = TDT_SEQ.substr(3, 4)
                    EQD_Item["TDT" + TDT_SEQ] = await this.getDataTDTGroup(lineList, i, TDT_SEQ)
                } else if (line.indexOf("NAD") > -1 && !NAD_flag) {
                    NAD_flag = true
                    
                    let NAD_SEQ:string = itemList[0].trim()
                    NAD_SEQ = NAD_SEQ.substr(3, 4)
                    EQD_Item["NAD" + NAD_SEQ] = await this.getDataNADGroup(lineList, i, NAD_SEQ)
                } else if (line.indexOf("TPL") > -1 && !TPL_flag) {
                    TPL_flag = true
                    
                    let TPL_SEQ:string = itemList[0].trim()
                    TPL_SEQ = TPL_SEQ.substr(3, 4)
                    EQD_Item["TPL" + TPL_SEQ] = await this.getDataTPL(lineList, i, TPL_SEQ)
                } else if (line.indexOf("HAN") > -1 && !HAN_flag) {
                    HAN_flag = true
                    
                    let HAN_SEQ:string = itemList[0].trim()
                    HAN_SEQ = HAN_SEQ.substr(3, 4)
                    EQD_Item["HAN" + HAN_SEQ] = await this.getDataHAN(lineList, i, HAN_SEQ)
                } else if (line.indexOf("TCC") > -1 && !TCC_flag) {
                    TCC_flag = true
                    
                    let TCC_SEQ:string = itemList[0].trim()
                    TCC_SEQ = TCC_SEQ.substr(3, 4)
                    EQD_Item["TCC" + TCC_SEQ] = await this.getDataTCCGroup(lineList, i, TCC_SEQ)
                } else if (line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    EQD_List.push(EQD_Item)
                    break
                }
            }
            
        }

        return EQD_List
    }

    public static async getDataLOCGroup(lineList:string[], idx:number, segSeq:string) {
        let LOC_Item:any = {}
        let line:string
        let flag:boolean = false
        let GID_flag:boolean = false
        let GDS_flag:boolean = false
        let FTX_flag:boolean = false
        let MEA_flag:boolean = false
        let DIM_flag:boolean = false
        let TMP_flag:boolean = false
        let RNG_flag:boolean = false
        let LOC_flag:boolean = false
        let RFF_flag:boolean = false
        let EQD_flag:boolean = false
        let DGS_flag:boolean = false
        let DTM_flag:boolean = false
        let itemList:string[]
        let LOC_List:any[] = []
        let LOC_SEQ:string = "LOC" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(LOC_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                LOC_Item[LOC_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(LOC_SEQ) > -1 && flag) {
                LOC_List.push(LOC_Item)
                
                LOC_Item = {}
                itemList = line.split(":")
                LOC_Item[LOC_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(LOC_SEQ) > -1) {
                    LOC_Item["LOC" + itemSeq] = itemList[1]
                } else if (line.indexOf("GID") > -1 && !GID_flag) {
                    GID_flag = true
                    
                    let GID_SEQ:string = itemList[0].trim()
                    GID_SEQ = GID_SEQ.substr(3, 4)
                    LOC_Item["GID" + GID_SEQ] = await this.getDataGID(lineList, i, GID_SEQ)
                } else if (line.indexOf("GDS") > -1 && !GDS_flag) {
                    GDS_flag = true
                    
                    let GDS_SEQ:string = itemList[0].trim()
                    GDS_SEQ = GDS_SEQ.substr(3, 4)
                    LOC_Item["GDS" + GDS_SEQ] = await this.getDataGDS(lineList, i, GDS_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)
                    LOC_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("MEA") > -1 && !MEA_flag) {
                    MEA_flag = true
                    
                    let MEA_SEQ:string = itemList[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4)
                    LOC_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                } else if (line.indexOf("DIM") > -1 && !DIM_flag) {
                    DIM_flag = true
                    
                    let DIM_SEQ:string = itemList[0].trim()
                    DIM_SEQ = DIM_SEQ.substr(3, 4)
                    LOC_Item["DIM" + DIM_SEQ] = await this.getDataDIM(lineList, i, DIM_SEQ)
                } else if (line.indexOf("TMP") > -1 && !TMP_flag) {
                    TMP_flag = true
                    
                    let TMP_SEQ:string = itemList[0].trim()
                    TMP_SEQ = TMP_SEQ.substr(3, 4)
                    LOC_Item["TMP" + TMP_SEQ] = await this.getDataTMP(lineList, i, TMP_SEQ)
                } else if (line.indexOf("RNG") > -1 && !RNG_flag) {
                    RNG_flag = true
                    
                    let RNG_SEQ:string = itemList[0].trim()
                    RNG_SEQ = RNG_SEQ.substr(3, 4)
                    LOC_Item["RNG" + RNG_SEQ] = await this.getDataRNG(lineList, i, RNG_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)
                    LOC_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    
                    let RFF_SEQ:string = itemList[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4)
                    LOC_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                } else if (line.indexOf("EQD") > -1 && !EQD_flag) {
                    EQD_flag = true
                    
                    let EQD_SEQ:string = itemList[0].trim()
                    EQD_SEQ = EQD_SEQ.substr(3, 4)
                    LOC_Item["EQD" + EQD_SEQ] = await this.getDataEQDGroup(lineList, i, EQD_SEQ)
                } else if (line.indexOf("DGS") > -1 && !DGS_flag) {
                    DGS_flag = true
                    
                    let DGS_SEQ:string = itemList[0].trim()
                    DGS_SEQ = DGS_SEQ.substr(3, 4)
                    LOC_Item["DGS" + DGS_SEQ] = await this.getDataDGSGroup(lineList, i, DGS_SEQ)
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)
                    LOC_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("UNT") > -1) {
                    LOC_List.push(LOC_Item)
                    break
                }
            }
        }

        return LOC_List
    }

    public static async getDataGID(lineList:string[], idx:number, segSeq:string) {
        let GID_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let GID_List:any[] = []
        let GID_SEQ:string = "GID" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(GID_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                GID_Item[GID_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(GID_SEQ + "00") > -1 && flag) {
                GID_List.push(GID_Item)
                
                GID_Item = {}
                itemList = line.split(":")
                GID_Item[GID_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(GID_SEQ) > -1) {
                    GID_Item["GID" + itemSeq] = itemList[1]
                } else if (line.indexOf("FTX") > -1 || line.indexOf("MEA") > -1 || line.indexOf("DIM") > -1 || line.indexOf("TMP") > -1 || 
                line.indexOf("RNG") > -1 || line.indexOf("LOC") > -1 || line.indexOf("RFF") > -1 || line.indexOf("EQD") > -1 ||
                line.indexOf("DGS") > -1|| line.indexOf("UNT") > -1) {
                    GID_List.push(GID_Item)
                    break
                }
            }
        }
        return GID_List
    } 

    public static async getDataGDS(lineList:string[], idx:number, segSeq:string) {
        let GDS_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let GDS_List:any[] = []
        let GDS_SEQ:string = "GDS" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(GDS_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                GDS_Item[GDS_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(GDS_SEQ + "01") > -1 && flag) {
                GDS_List.push(GDS_Item)
                
                GDS_Item = {}
                itemList = line.split(":")
                GDS_Item[GDS_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(GDS_SEQ) > -1) {
                    GDS_Item["GDS" + itemSeq] = itemList[1]
                } else {
                    GDS_List.push(GDS_Item)
                    break
                }
            }
        }
        return GDS_List
    }

    public static async getDataEQA(lineList:string[], idx:number, segSeq:string) {
        let EQA_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let EQA_List:any[] = []
        let EQA_SEQ:string = "EQA" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(EQA_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                EQA_Item[EQA_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(EQA_SEQ + "00") > -1 && flag) {
                EQA_List.push(EQA_Item)
                
                EQA_Item = {}
                itemList = line.split(":")
                EQA_Item[EQA_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(EQA_SEQ) > -1) {
                    EQA_Item["EQA" + itemSeq] = itemList[1]
                } else if (line.indexOf("EQN") > -1) {                    
                    let EQN_SEQ:string = itemList[0].trim()
                    EQN_SEQ = EQN_SEQ.substr(3, 6) 
                    EQA_Item["EQN" + EQN_SEQ] = itemList[1]
                } else if (line.indexOf("EQD") > -1 || line.indexOf("UNT") > -1) {
                    EQA_List.push(EQA_Item)
                    break
                }
            }
        }
        return EQA_List
    }

    public static async getDataCNT(lineList:string[], idx:number, segSeq:string) {
        let CNT_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let CNT_List:any[] = []
        let CNT_SEQ:string = "CNT" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(CNT_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                CNT_Item[CNT_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(CNT_SEQ + "01") > -1 && flag) {
                CNT_List.push(CNT_Item)
                
                CNT_Item = {}
                itemList = line.split(":")
                CNT_Item[CNT_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(CNT_SEQ) > -1) {
                    CNT_Item["CNT" + itemSeq] = itemList[1]
                } else {
                    CNT_List.push(CNT_Item)
                    break
                }
            }
        }
        return CNT_List
    }

    public static async getDataTODGroup(lineList:string[], idx:number, segSeq:string) {
        let TOD_Item:any = {}
        let line:string
        let flag:boolean = false
        let LOC_flag:boolean = false
        let itemList:string[]
        let TOD_List:any[] = []
        let TOD_SEQ:string = "TOD" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TOD_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                TOD_Item[TOD_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(TOD_SEQ + "00") > -1 && flag) {
                TOD_List.push(TOD_Item)
                
                TOD_Item = {}
                itemList = line.split(":")
                TOD_Item[TOD_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(TOD_SEQ) > -1) {
                    TOD_Item["TOD" + itemSeq] = itemList[1]
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)
                    TOD_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("RFF") > -1 || line.indexOf("GOR") > -1 || line.indexOf("TCC") > -1 || line.indexOf("TDT") > -1 || 
                line.indexOf("NAD") > -1 || line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    TOD_List.push(TOD_Item)
                    break
                }
            }
            
        }

        return TOD_List
    }

    public static async getDataRFFGroup(lineList:string[], idx:number, segSeq:string) {
        let RFF_Item:any = {}
        let line:string
        let flag:boolean = false
        let DTM_flag:boolean = false
        let itemList:string[]
        let RFF_List:any[] = []
        let RFF_SEQ:string = "RFF" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(RFF_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                RFF_Item[RFF_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(RFF_SEQ + "01") > -1 && flag) {
                RFF_List.push(RFF_Item)
                
                RFF_Item = {}
                itemList = line.split(":")
                RFF_Item[RFF_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(RFF_SEQ) > -1) {
                    RFF_Item['RFF'+ itemSeq] = itemList[1]
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)
                    RFF_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("GOR") > -1 || line.indexOf("TCC") > -1 || line.indexOf("TDT") > -1 || 
                line.indexOf("NAD") > -1 || line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    RFF_List.push(RFF_Item)
                    break
                }
            }
        }
        return RFF_List
    }

    public static async getDataTCCGroup(lineList:string[], idx:number, segSeq:string) {
        let TCC_Item:any = {}
        let line:string
        let flag:boolean = false
        let CUX_flag:boolean = false
        let LOC_flag:boolean = false
        let PRI_flag:boolean = false
        let FTX_flag:boolean = false
        let PCD_flag:boolean = false
        let MOA_flag:boolean = false
        let QTY_flag:boolean = false
        let itemList:string[]
        let TCC_List:any[] = []
        let TCC_SEQ:string = "TCC" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TCC_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                TCC_Item[TCC_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(TCC_SEQ + "01") > -1 && flag) {
                TCC_List.push(TCC_Item)
                
                TCC_Item = {}
                itemList = line.split(":")
                TCC_Item[TCC_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(TCC_SEQ) > -1) {
                    TCC_Item["TCC" + itemSeq] = itemList[1]
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4) 
                    TCC_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4) 
                    TCC_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("CUX") > -1 && !CUX_flag) {
                    CUX_flag = true
                    
                    let CUX_SEQ:string = itemList[0].trim()
                    CUX_SEQ = CUX_SEQ.substr(3, 4) 
                    TCC_Item["CUX" + CUX_SEQ] = await this.getDataCUX(lineList, i, CUX_SEQ)
                } else if (line.indexOf("PRI") > -1 && !PRI_flag) {
                    PRI_flag = true
                    
                    let PRI_SEQ:string = itemList[0].trim()
                    PRI_SEQ = PRI_SEQ.substr(3, 4) 
                    TCC_Item["PRI" + PRI_SEQ] = await this.getDataPRI(lineList, i, PRI_SEQ)
                } else if (line.indexOf("QTY") > -1 && !QTY_flag) {
                    QTY_flag = true
                    
                    let QTY_SEQ:string = itemList[0].trim()
                    QTY_SEQ = QTY_SEQ.substr(3, 4) 
                    TCC_Item["QTY" + QTY_SEQ] = await this.getDataQTY(lineList, i, QTY_SEQ)
                } else if (line.indexOf("EQN") > -1) {
                    
                    let EQN_SEQ:string = itemList[0].trim()
                    EQN_SEQ = EQN_SEQ.substr(3, 6) 
                    TCC_Item["EQN" + EQN_SEQ] = itemList[1]
                } else if (line.indexOf("CUX") > -1) {
                    
                    let CUX_SEQ:string = itemList[0].trim()
                    CUX_SEQ = CUX_SEQ.substr(3, 6) 
                    TCC_Item["CUX" + CUX_SEQ] = itemList[1]
                } else if (line.indexOf("PCD") > -1 && !PCD_flag) {
                    PCD_flag = true
                    
                    let PCD_SEQ:string = itemList[0].trim()
                    PCD_SEQ = PCD_SEQ.substr(3, 4) 
                    TCC_Item["PCD" + PCD_SEQ] = await this.getDataPCD(lineList, i, PCD_SEQ)
                } else if (line.indexOf("MOA") > -1 && !MOA_flag) {
                    MOA_flag = true
                    
                    let MOA_SEQ:string = itemList[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4) 
                    TCC_Item["MOA" + MOA_SEQ] = await this.getDataMOA(lineList, i, MOA_SEQ)
                } else if (line.indexOf("TDT") > -1 || line.indexOf("NAD") > -1 || line.indexOf("DGS") > -1 || line.indexOf("GID") > -1 || 
                line.indexOf("EQD") > -1 ||line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    TCC_List.push(TCC_Item)
                    break
                } 
            }
            
        }

        return TCC_List
    }

    public static async getDataCUX(lineList:string[], idx:number, segSeq:string) {
        let CUX_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let CUX_List:any[] = []
        let CUX_SEQ:string = "CUX" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(CUX_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                CUX_Item[CUX_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(CUX_SEQ + "01") > -1 && flag) {
                CUX_List.push(CUX_Item)
                
                CUX_Item = {}
                itemList = line.split(":")
                CUX_Item[CUX_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(CUX_SEQ) > -1) {
                    CUX_Item["CUX" + itemSeq] = itemList[1]
                } else {
                    CUX_List.push(CUX_Item)
                    break
                }
            }
        }
        return CUX_List
    }

    /**
     * PRICE DETAILS
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns PRI list
     */
    public static async getDataPRI(lineList:string[], idx:number, segSeq:string) {
        let PRI_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let PRI_List:any[] = []
        let PRI_SEQ:string = "PRI" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(PRI_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                PRI_Item[PRI_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(PRI_SEQ + "01") > -1 && flag) {
                PRI_List.push(PRI_Item)
                
                PRI_Item = {}
                itemList = line.split(":")
                PRI_Item[PRI_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(PRI_SEQ) > -1) {
                    PRI_Item["PRI" + itemSeq] = itemList[1]
                } else {
                    PRI_List.push(PRI_Item)
                    break
                }
            }
        }
        return PRI_List
    }

    public static async getDataPCD(lineList:string[], idx:number, segSeq:string) {
        let PCD_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let PCD_List:any[] = []
        let PCD_SEQ:string = "PCD" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(PCD_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                PCD_Item[PCD_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(PCD_SEQ + "01") > -1 && flag) {
                PCD_List.push(PCD_Item)
                
                PCD_Item = {}
                itemList = line.split(":")
                PCD_Item[PCD_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(PCD_SEQ) > -1) {
                    PCD_Item["PCD" + itemSeq] = itemList[1]
                } else {
                    PCD_List.push(PCD_Item)
                    break
                }
            }
        }
        return PCD_List
    }

    public static async getDataQTY(lineList:string[], idx:number, segSeq:string) {
        let QTY_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let QTY_List:any[] = []
        let QTY_SEQ:string = "QTY" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(QTY_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                QTY_Item[QTY_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(QTY_SEQ + "01") > -1 && flag) {
                QTY_List.push(QTY_Item)
                
                QTY_Item = {}
                itemList = line.split(":")
                QTY_Item[QTY_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(QTY_SEQ) > -1) {
                    QTY_Item["QTY" + itemSeq] = itemList[1]
                } else {
                    QTY_List.push(QTY_Item)
                    break
                }
            }
        }
        return QTY_List
    }

    public static async getDataCTAGroup(lineList:string[], idx:number, segSeq:string) {
        let CTA_Item:any = {}
        let line:string
        let flag:boolean = false
        let COM_flag:boolean = false
        let itemList:string[]
        let CTA_List:any[] = []
        let CTA_SEQ:string = "CTA" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(CTA_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                CTA_Item[CTA_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(CTA_SEQ + "00") > -1 && flag) {
                CTA_List.push(CTA_Item)
                
                CTA_Item = {}
                itemList = line.split(":")
                CTA_Item[CTA_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(CTA_SEQ) > -1) {
                    CTA_Item['CTA'+ itemSeq] = itemList[1]
                } else if (line.indexOf("COM") > -1 && !COM_flag) {
                    COM_flag = true
                    
                    let COM_SEQ:string = itemList[0].trim()
                    COM_SEQ = COM_SEQ.substr(3, 4) 
                    CTA_Item["COM" + COM_SEQ] = await this.getDataCOM(lineList, i, COM_SEQ)
                } else if (line.indexOf("DOC") > -1 || line.indexOf("NAD") > -1 || line.indexOf("RFF") > -1 || line.indexOf("GID") > -1 || 
                line.indexOf("EQD") > -1 || line.indexOf("UNT") > -1) {
                    CTA_List.push(CTA_Item)
                    break
                } 
            }
        }
        return CTA_List
    }

    public static async getDataDOCGroup(lineList:string[], idx:number, segSeq:string) {
        let DOC_Item:any = {}
        let line:string
        let flag:boolean = false
        let DTM_flag:boolean = false
        let itemList:string[]
        let DOC_List:any[] = []
        let DOC_SEQ:string = "DOC" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DOC_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                DOC_Item[DOC_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(DOC_SEQ + "01") > -1 && flag) {
                DOC_List.push(DOC_Item)
                
                DOC_Item = {}
                itemList = line.split(":")
                DOC_Item[DOC_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(DOC_SEQ) > -1) {
                    DOC_Item['DOC' + itemSeq] = itemList[1]
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4) 
                    DOC_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("NAD") > -1 || line.indexOf("RFF") > -1 || line.indexOf("GID") > -1 || 
                line.indexOf("EQD") > -1 || line.indexOf("UNT") > -1) {
                    DOC_List.push(DOC_Item)
                    break
                } 
            }
        }
        return DOC_List
    }

    public static async getDataTSRGroup(lineList:string[], idx:number, segSeq:string) {
        let TSR_Item:any = {}
        let line:string
        let flag:boolean = false
        let RFF_flag:boolean = false
        let LOC_flag:boolean = false
        let TPL_flag:boolean = false
        let FTX_flag:boolean = false
        let itemList:string[]
        let TSR_List:any[] = []
        let TSR_SEQ:string = "TSR" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TSR_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                TSR_Item[TSR_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(TSR_SEQ + "00") > -1 && flag) {
                TSR_List.push(TSR_Item)
                
                TSR_Item = {}
                itemList = line.split(":")
                TSR_Item[TSR_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(TSR_SEQ) > -1) {
                    TSR_Item['TSR'+ itemSeq] = itemList[1]
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    
                    let RFF_SEQ:string = itemList[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4) 
                    TSR_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4) 
                    TSR_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("TPL") > -1 && !TPL_flag) {
                    TPL_flag = true
                    
                    let TPL_SEQ:string = itemList[0].trim()
                    TPL_SEQ = TPL_SEQ.substr(3, 4) 
                    TSR_Item["TPL" + TPL_SEQ] = await this.getDataTPL(lineList, i, TPL_SEQ)
                } else if (line.indexOf("TPL") > -1 && !TPL_flag) {
                    TPL_flag = true
                    
                    let TPL_SEQ:string = itemList[0].trim()
                    TPL_SEQ = TPL_SEQ.substr(3, 4) 
                    TSR_Item["TPL" + TPL_SEQ] = await this.getDataTPL(lineList, i, TPL_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4) 
                    TSR_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("NAD") > -1 || line.indexOf("GID") > -1 || 
                line.indexOf("EQD") > -1 || line.indexOf("UNT") > -1) {
                    TSR_List.push(TSR_Item)
                    break
                } 
            }
        }
        return TSR_List
    }

    public static async getDataCPI(lineList:string[], idx:number, segSeq:string) {
        let CPI_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let CPI_List:any[] = []
        let CPI_SEQ:string = "CPI" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(CPI_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                CPI_Item[CPI_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(CPI_SEQ + "01") > -1 && flag) {
                CPI_List.push(CPI_Item)
                
                CPI_Item = {}
                itemList = line.split(":")
                CPI_Item[CPI_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(CPI_SEQ) > -1) {
                    CPI_Item["CPI" + itemSeq] = itemList[1]
                } else {
                    CPI_List.push(CPI_Item)
                    break
                }
            }
        }
        return CPI_List
    }

    public static async getDataTPL(lineList:string[], idx:number, segSeq:string) {
        let TPL_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let TPL_List:any[] = []
        let TPL_SEQ:string = "TPL" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TPL_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                TPL_Item[TPL_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(TPL_SEQ + "01") > -1 && flag) {
                TPL_List.push(TPL_Item)
                
                TPL_Item = {}
                itemList = line.split(":")
                TPL_Item[TPL_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(TPL_SEQ) > -1) {
                    TPL_Item["TPL" + itemSeq] = itemList[1]
                } else {
                    TPL_List.push(TPL_Item)
                    break
                }
            }
        }
        return TPL_List
    }

    public static async getDataCPIGroup(lineList:string[], idx:number, segSeq:string) {
        let CPI_Item:any = {}
        let line:string
        let flag:boolean = false
        let CUX_flag:boolean = false
        let LOC_flag:boolean = false
        let MOA_flag:boolean = false
        let itemList:string[]
        let CPI_List:any[] = []
        let CPI_SEQ:string = "CPI" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(CPI_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                CPI_Item[CPI_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(CPI_SEQ + "01") > -1 && flag) {
                CPI_List.push(CPI_Item)
                
                CPI_Item = {}
                itemList = line.split(":")
                CPI_Item[CPI_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(CPI_SEQ) > -1) {
                    CPI_Item["CPI" + itemSeq] = itemList[1]
                } else if (line.indexOf("CUX") > -1 && !CUX_flag) {
                    CUX_flag = true
                    
                    let CUX_SEQ:string = itemList[0].trim()
                    CUX_SEQ = CUX_SEQ.substr(3, 4) 
                    CPI_Item["CUX" + CUX_SEQ] = await this.getDataCUX(lineList, i, CUX_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4) 
                    CPI_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("MOA") > -1 && !MOA_flag) {
                    MOA_flag = true
                    
                    let MOA_SEQ:string = itemList[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4) 
                    CPI_Item["MOA" + MOA_SEQ] = await this.getDataMOA(lineList, i, MOA_SEQ)
                } else if (line.indexOf("NAD") > -1 || line.indexOf("GID") > -1 || 
                line.indexOf("EQD") > -1 || line.indexOf("UNT") > -1) {
                    CPI_List.push(CPI_Item)
                    break
                } 
            }
        }
        return CPI_List
    }

    public static async getDataPCIGroup(lineList:string[], idx:number, segSeq:string) {
        let PCI_Item:any = {}
        let line:string
        let flag:boolean = false
        let RFF_Flag:boolean = false
        let DTM_Flag:boolean = false
        let itemList:string[]
        let PCI_List:any[] = []
        let PCI_SEQ:string = "PCI" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(PCI_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                PCI_Item[PCI_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(PCI_SEQ + "00") > -1 && flag) {
                PCI_List.push(PCI_Item)
                PCI_Item = {}
                itemList = line.split(":")
                PCI_Item[PCI_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(PCI_SEQ) > -1) {
                    PCI_Item["PCI" + itemSeq] = itemList[1]
                } else if (line.indexOf("RFF") > -1 && !RFF_Flag) {
                    RFF_Flag = true
                    
                    let RFF_SEQ:string = itemList[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4) 
                    PCI_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                } else if (line.indexOf("DTM") > -1 && !DTM_Flag) {
                    DTM_Flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4) 
                    PCI_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("SGP") > -1 || line.indexOf("DGS") > -1 ||line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    PCI_List.push(PCI_Item)
                    break
                }
            }
            
        }

        return PCI_List
    }

    public static async getDataMEAGroup(lineList:string[], idx:number, segSeq:string) {
        let MEA_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let MEA_List:any[] = []
        let MEA_SEQ:string = "MEA" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(MEA_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                MEA_Item[MEA_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(MEA_SEQ + "00") > -1 && flag) {
                MEA_List.push(MEA_Item)
                
                MEA_Item = {}
                itemList = line.split(":")
                MEA_Item[MEA_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(MEA_SEQ) > -1) {
                    MEA_Item['MEA'+ itemSeq] = itemList[1]
                } else if (line.indexOf("EQN") > -1) {                    
                    let EQN_SEQ:string = itemList[0].trim()
                    EQN_SEQ = EQN_SEQ.substr(3, 6) 
                    MEA_Item["EQN" + EQN_SEQ] = itemList[1]
                } else if (line.indexOf("DIM") > -1 || line.indexOf("RFF") > -1 ||line.indexOf("PCI") > -1 || line.indexOf("TPL") > -1 || 
                line.indexOf("SGP") > -1 || line.indexOf("DGS") > -1 ||line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    MEA_List.push(MEA_Item)
                    break
                }
            }
        }
        return MEA_List
    }

    public static async getDataDIMGroup(lineList:string[], idx:number, segSeq:string) {
        let DIM_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let DIM_List:any[] = []
        let DIM_SEQ:string = "DIM" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(DIM_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                DIM_Item[DIM_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(DIM_SEQ + "00") > -1 && flag) {
                DIM_List.push(DIM_Item)
                
                DIM_Item = {}
                itemList = line.split(":")
                DIM_Item[DIM_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(DIM_SEQ) > -1) {
                    DIM_Item['DIM'+ itemSeq] = itemList[1]
                } else if (line.indexOf("EQN") > -1) {                    
                    let EQN_SEQ:string = itemList[0].trim()
                    EQN_SEQ = EQN_SEQ.substr(3, 6) 
                    DIM_Item["EQN" + EQN_SEQ] = itemList[1]
                } else if (line.indexOf("RFF") > -1 ||line.indexOf("PCI") > -1 || line.indexOf("TPL") > -1 || 
                line.indexOf("SGP") > -1 || line.indexOf("DGS") > -1 ||line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    DIM_List.push(DIM_Item)
                    break
                }
            }
        }
        return DIM_List
    }

    public static async getDataTPLGroup(lineList:string[], idx:number, segSeq:string) {
        let TPL_Item:any = {}
        let line:string
        let flag:boolean = false
        let MEA_flag:boolean = false
        let itemList:string[]
        let TPL_List:any[] = []
        let TPL_SEQ:string = "TPL" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TPL_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                TPL_Item[TPL_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(TPL_SEQ + "01") > -1 && flag) {
                TPL_List.push(TPL_Item)
                
                TPL_Item = {}
                itemList = line.split(":")
                TPL_Item[TPL_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(TPL_SEQ) > -1) {
                    TPL_Item["TPL" + itemSeq] = itemList[1]
                } else if (line.indexOf("MEA") > -1 && !MEA_flag) {
                    MEA_flag = true
                    
                    let MEA_SEQ:string = itemList[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4)                     
                    let TPL_SEQ_GRP:string = segSeq.substr(0, 2)
                    let MEA_SEQ_GRP:string = MEA_SEQ.substr(0, 2) 
                    if (MEA_SEQ_GRP === TPL_SEQ_GRP) {
                        TPL_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                    } else {
                        TPL_Item["MEA" + MEA_SEQ] = await this.getDataMEAGroup(lineList, i, MEA_SEQ)
                    }
                } else if (line.indexOf("SGP") > -1 || line.indexOf("DGS") > -1 ||line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    TPL_List.push(TPL_Item)
                    break
                }
            }
        }
        return TPL_List
    }

    public static async getDataGORGroup(lineList:string[], idx:number, segSeq:string) {
        let GOR_Item:any = {}
        let line:string
        let flag:boolean = false
        let DTM_flag:boolean = false
        let LOC_flag:boolean = false
        let SEL_flag:boolean = false
        let FTX_flag:boolean = false
        let DOC_flag:boolean = false
        let itemList:string[]
        let GOR_List:any[] = []
        let GOR_SEQ:string = "GOR" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(GOR_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                GOR_Item[GOR_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(GOR_SEQ + "00") > -1 && flag) {
                GOR_List.push(GOR_Item)
                
                GOR_Item = {}
                itemList = line.split(":")
                GOR_Item[GOR_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(GOR_SEQ) > -1) {
                    GOR_Item['GOR'+ itemSeq] = itemList[1]
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)  
                    GOR_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)  
                    GOR_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("SEL") > -1 && !SEL_flag) {
                    SEL_flag = true
                    
                    let SEL_SEQ:string = itemList[0].trim()
                    SEL_SEQ = SEL_SEQ.substr(3, 4)  
                    GOR_Item["SEL" + SEL_SEQ] = await this.getDataSEL(lineList, i, SEL_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)  
                    GOR_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("DOC") > -1 && !DOC_flag) {
                    DOC_flag = true
                    
                    let DOC_SEQ:string = itemList[0].trim()
                    DOC_SEQ = DOC_SEQ.substr(3, 4)                     
                    let TPL_SEQ_GRP:string = segSeq.substr(0, 2)
                    let DOC_SEQ_GRP:string = DOC_SEQ.substr(0, 2) 
                    if (DOC_SEQ_GRP === TPL_SEQ_GRP) {
                        GOR_Item["DOC" + DOC_SEQ] = await this.getDataDOC(lineList, i, DOC_SEQ)
                    } else {
                        GOR_Item["DOC" + DOC_SEQ] = await this.getDataDOCGroup(lineList, i, DOC_SEQ)
                    }
                } else if (line.indexOf("TDT") > -1 || line.indexOf("NAD") > -1 ||line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    GOR_List.push(GOR_Item)
                    break
                }
            }
        }
        return GOR_List
    }

    public static async getDataCNIGroup(lineList:string[], idx:number, segSeq:string) {
        let CNI_Item:any = {}
        let line:string
        let flag:boolean = false
        let LOC_flag:boolean = false
        let CNT_flag:boolean = false
        let DTM_flag:boolean = false
        let TDT_flag:boolean = false
        let NAD_flag:boolean = false
        let GID_flag:boolean = false
        let STS_flag:boolean = false
        let itemList:string[]
        let CNI_List:any[] = []
        let CNI_SEQ:string = "CNI" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(CNI_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                CNI_Item[CNI_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(CNI_SEQ + "00") > -1 && flag) {
                CNI_List.push(CNI_Item)
                
                CNI_Item = {}
                itemList = line.split(":")
                CNI_Item[CNI_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(CNI_SEQ) > -1) {
                    CNI_Item["CNI" + itemSeq] = itemList[1]
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)  
                    CNI_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)  
                    CNI_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("CNT") > -1 && !CNT_flag) {
                    CNT_flag = true
                    
                    let CNT_SEQ:string = itemList[0].trim()
                    CNT_SEQ = CNT_SEQ.substr(3, 4)  
                    CNI_Item["CNT" + CNT_SEQ] = await this.getDataCNT(lineList, i, CNT_SEQ)
                } else if (line.indexOf("STS") > -1 && !STS_flag) {
                    STS_flag = true
                    
                    let STS_SEQ:string = itemList[0].trim()
                    STS_SEQ = STS_SEQ.substr(3, 4)                    
                    CNI_Item["STS" + STS_SEQ] = await this.getDataSTSGroup(lineList, i, STS_SEQ)
                } else if (line.indexOf("TDT") > -1 && !TDT_flag) {
                    
                    let TDT_SEQ:string = itemList[0].trim()
                    TDT_SEQ = TDT_SEQ.substr(3, 6) 
                    CNI_Item["TDT" + TDT_SEQ] = itemList[1]
                } else if (line.indexOf("NAD") > -1 && !NAD_flag) {
                    NAD_flag = true
                    
                    let NAD_SEQ:string = itemList[0].trim()
                    NAD_SEQ = NAD_SEQ.substr(3, 4)
                    CNI_Item["NAD" + NAD_SEQ] = await this.getDataNADGroup(lineList, i, NAD_SEQ)
                } else if (line.indexOf("GID") > -1 && !GID_flag) {
                    GID_flag = true
                    
                    let GID_SEQ:string = itemList[0].trim()
                    GID_SEQ = GID_SEQ.substr(3, 4)                    
                    CNI_Item["GID" + GID_SEQ] = await this.getDataGIDGroup(lineList, i, GID_SEQ)
                } else if (line.indexOf("UNT") > -1) {
                    CNI_List.push(CNI_Item)
                    break
                }
            }
        }
        return CNI_List
    }

    public static async getDataGDSGroup(lineList:string[], idx:number, segSeq:string) {
        let GDS_Item:any = {}
        let line:string
        let flag:boolean = false
        let FTX_flag:boolean = false
        let itemList:string[]
        let GDS_List:any[] = []
        let GDS_SEQ:string = "GDS" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(GDS_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                GDS_Item[GDS_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(GDS_SEQ + "01") > -1 && flag) {
                GDS_List.push(GDS_Item)
                
                GDS_Item = {}
                itemList = line.split(":")
                GDS_Item[GDS_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(GDS_SEQ) > -1) {
                    GDS_Item["GDS" + itemSeq] = itemList[1]
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)  
                    GDS_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("TDT") > -1 || line.indexOf("NAD") > -1 ||line.indexOf("GID") > -1 || line.indexOf("EQD") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    GDS_List.push(GDS_Item)
                    break
                }
            }
        }
        return GDS_List
    }

    public static async getDataSTSGroup(lineList:string[], idx:number, segSeq:string) {
        let STS_Item:any = {}
        let line:string
        let flag:boolean = false
        let LOC_flag:boolean = false
        let RFF_flag:boolean = false
        let DTM_flag:boolean = false
        let FTX_flag:boolean = false
        let NAD_flag:boolean = false
        let PCI_flag:boolean = false
        let TDT_flag:boolean = false
        let EQD_flag:boolean = false
        let GID_flag:boolean = false
        let itemList:string[]
        let STS_List:any[] = []
        let STS_SEQ:string = "STS" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(STS_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                STS_Item[STS_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(STS_SEQ + "01") > -1 && flag) {
                STS_List.push(STS_Item)
                
                STS_Item = {}
                itemList = line.split(":")
                STS_Item[STS_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(STS_SEQ) > -1) {
                    STS_Item["STS" + itemSeq] = itemList[1]
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)  
                    STS_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    
                    let RFF_SEQ:string = itemList[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4)  
                    STS_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)  
                    STS_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)  
                    STS_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("NAD") > -1 && !NAD_flag) {
                    NAD_flag = true
                    
                    let NAD_SEQ:string = itemList[0].trim()
                    NAD_SEQ = NAD_SEQ.substr(3, 4)  
                    STS_Item["NAD" + NAD_SEQ] = await this.getDataNADGroup(lineList, i, NAD_SEQ)
                } else if (line.indexOf("PCI") > -1 && !PCI_flag) {
                    PCI_flag = true
                    
                    let PCI_SEQ:string = itemList[0].trim()
                    PCI_SEQ = PCI_SEQ.substr(3, 4)  
                    STS_Item["PCI" + PCI_SEQ] = await this.getDataPCIGroup(lineList, i, PCI_SEQ)
                } else if (line.indexOf("TDT") > -1 && !TDT_flag) {
                    TDT_flag = true
                    
                    let TDT_SEQ:string = itemList[0].trim()
                    TDT_SEQ = TDT_SEQ.substr(3, 4)                   
                    STS_Item["TDT" + TDT_SEQ] = await this.getDataTDTGroup(lineList, i, TDT_SEQ)
                } else if (line.indexOf("GID") > -1 && !GID_flag) {
                    GID_flag = true
                    
                    let GID_SEQ:string = itemList[0].trim()
                    GID_SEQ = GID_SEQ.substr(3, 4)                   
                    STS_Item["GID" + GID_SEQ] = await this.getDataGIDGroup(lineList, i, GID_SEQ)
                } else if (line.indexOf("EQD") > -1 && !EQD_flag) {
                    EQD_flag = true
                    
                    let EQD_SEQ:string = itemList[0].trim()
                    EQD_SEQ = EQD_SEQ.substr(3, 4)                   
                    STS_Item["EQD" + EQD_SEQ] = await this.getDataEQDGroup(lineList, i, EQD_SEQ)
                } else if (line.indexOf("UNT") > -1 || line.indexOf("CNI") > -1) {
                    STS_List.push(STS_Item)
                    break
                }
            }
        }
        return STS_List
    }

    public static async getDataHANGroup(lineList:string[], idx:number, segSeq:string) {
        let HAN_Item:any = {}
        let line:string
        let flag:boolean = false
        let LOC_flag:boolean = false
        let RFF_flag:boolean = false
        let FTX_flag:boolean = false
        let MEA_flag:boolean = false
        let DIM_flag:boolean = false
        let NAD_flag:boolean = false
        let TMP_flag:boolean = false
        let EQD_flag:boolean = false
        let EQA_flag:boolean = false
        let GID_flag:boolean = false
        let itemList:string[]
        let HAN_List:any[] = []
        let HAN_SEQ:string = "HAN" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(HAN_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                HAN_Item[HAN_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(HAN_SEQ + "01") > -1 && flag) {
                HAN_List.push(HAN_Item)
                
                HAN_Item = {}
                itemList = line.split(":")
                HAN_Item[HAN_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(HAN_SEQ) > -1) {
                    HAN_Item['HAN'+ itemSeq] = itemList[1]
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)  
                    HAN_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    
                    let RFF_SEQ:string = itemList[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4)
                    let HAN_SEQ_GRP:string = segSeq.substr(0, 2)
                    let RFF_SEQ_GRP:string = RFF_SEQ.substr(0, 2) 
                    if (HAN_SEQ_GRP === RFF_SEQ_GRP) {
                        HAN_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                    } else {
                        HAN_Item["RFF" + RFF_SEQ] = await this.getDataRFFGroup(lineList, i, RFF_SEQ)
                    }
                } else if (line.indexOf("MEA") > -1 && !MEA_flag) {
                    MEA_flag = true
                    
                    let MEA_SEQ:string = itemList[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4)  
                    HAN_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)  
                    HAN_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("DIM") > -1 && !DIM_flag) {
                    DIM_flag = true
                    
                    let DIM_SEQ:string = itemList[0].trim()
                    DIM_SEQ = DIM_SEQ.substr(3, 4)  
                    HAN_Item["DIM" + DIM_SEQ] = await this.getDataDIM(lineList, i, DIM_SEQ)
                } else if (line.indexOf("NAD") > -1 && !NAD_flag) {
                    NAD_flag = true
                    
                    let NAD_SEQ:string = itemList[0].trim()
                    NAD_SEQ = NAD_SEQ.substr(3, 4)  
                    HAN_Item["NAD" + NAD_SEQ] = await this.getDataNADGroup(lineList, i, NAD_SEQ)
                } else if (line.indexOf("TMP") > -1 && !TMP_flag) {
                    TMP_flag = true
                    
                    let TMP_SEQ:string = itemList[0].trim()
                    TMP_SEQ = TMP_SEQ.substr(3, 4)  
                    HAN_Item["TMP" + TMP_SEQ] = await this.getDataTMP(lineList, i, TMP_SEQ)
                } else if (line.indexOf("EQD") > -1 && !EQD_flag) {
                    EQD_flag = true
                    
                    let EQD_SEQ:string = itemList[0].trim()
                    EQD_SEQ = EQD_SEQ.substr(3, 4)  
                    HAN_Item["EQD" + EQD_SEQ] = await this.getDataEQDGroup(lineList, i, EQD_SEQ)
                } else if (line.indexOf("EQA") > -1 && !EQA_flag) {
                    EQA_flag = true
                    
                    let EQA_SEQ:string = itemList[0].trim()
                    EQA_SEQ = EQA_SEQ.substr(3, 4)  
                    HAN_Item["EQA" + EQA_SEQ] = await this.getDataEQA(lineList, i, EQA_SEQ)
                } else if (line.indexOf("GID") > -1 && !GID_flag) {
                    GID_flag = true
                    
                    let GID_SEQ:string = itemList[0].trim()
                    GID_SEQ = GID_SEQ.substr(3, 4)  
                    HAN_Item["GID" + GID_SEQ] = await this.getDataGIDGroup(lineList, i, GID_SEQ)
                } else  if (line.indexOf("UNT") > -1) {
                    HAN_List.push(HAN_Item)
                    break
                }
            }
        }
        return HAN_List
    }

    /**
     * PAYMENT INSTRUCTION
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns PAI list
     */
    public static async getDataPAI(lineList:string[], idx:number, segSeq:string) {
        let PAI_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let PAI_List:any[] = []
        let PAI_SEQ:string = "PAI" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(PAI_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                PAI_Item[PAI_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(PAI_SEQ + "01") > -1 && flag) {
                PAI_List.push(PAI_Item)
                
                PAI_Item = {}
                itemList = line.split(":")
                PAI_Item[PAI_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(PAI_SEQ) > -1) {
                    PAI_Item["PAI"+ itemSeq] = itemList[1]
                } else {
                    PAI_List.push(PAI_Item)
                    break
                }
            }
        }
        return PAI_List
    }

    /**
     * ADDITIONAL INFORMATION
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns ALI list
     */
    public static async getDataALI(lineList:string[], idx:number, segSeq:string) {
        let ALI_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let ALI_List:any[] = []
        let ALI_SEQ:string = "ALI" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(ALI_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                ALI_Item[ALI_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(ALI_SEQ + "00") > -1 && flag) {
                ALI_List.push(ALI_Item)
                
                ALI_Item = {}
                itemList = line.split(":")
                ALI_Item[ALI_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(ALI_SEQ) > -1) {
                    ALI_Item["ALI"+ itemSeq] = itemList[1]
                } else {
                    ALI_List.push(ALI_Item)
                    break
                }
            }
        }
        return ALI_List
    }

    /**
     * ITEM DESCRIPTION
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns IMD list
     */
    public static async getDataIMD(lineList:string[], idx:number, segSeq:string) {
        let IMD_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let IMD_List:any[] = []
        let IMD_SEQ:string = "IMD" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(IMD_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                IMD_Item[IMD_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(IMD_SEQ + "00") > -1 && flag) {
                IMD_List.push(IMD_Item)
                
                IMD_Item = {}
                itemList = line.split(":")
                IMD_Item[IMD_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(IMD_SEQ) > -1) {
                    IMD_Item["IMD"+ itemSeq] = itemList[1]
                } else {
                    IMD_List.push(IMD_Item)
                    break
                }
            }
        }
        return IMD_List
    }

    /**
     * FINANCIAL INSTITUTION INFORMATION
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns FII list
     */
    public static async getDataFII(lineList:string[], idx:number, segSeq:string) {
        let FII_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let FII_List:any[] = []
        let FII_SEQ:string = "FII" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(FII_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                FII_Item[FII_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(FII_SEQ + "00") > -1 && flag) {
                FII_List.push(FII_Item)
                
                FII_Item = {}
                itemList = line.split(":")
                FII_Item[FII_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(FII_SEQ) > -1) {
                    FII_Item["FII"+ itemSeq] = itemList[1]
                } else {
                    FII_List.push(FII_Item)
                    break
                }
            }
        }
        return FII_List
    }

    /**
     * DUTY/FAX/FEE DETAILS
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns TAX list
     */
    public static async getDataTAXGroup(lineList:string[], idx:number, segSeq:string) {
        let TAX_Item:any = {}
        let line:string
        let flag:boolean = false
        let MOA_flag:boolean = false
        let LOC_flag:boolean = false
        let itemList:string[]
        let TAX_List:any[] = []
        let TAX_SEQ:string = "TAX" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(TAX_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                TAX_Item[TAX_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(TAX_SEQ + "00") > -1 && flag) {
                TAX_List.push(TAX_Item)
                
                TAX_Item = {}
                itemList = line.split(":")
                TAX_Item[TAX_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(TAX_SEQ) > -1) {
                    TAX_Item["TAX"+ itemSeq] = itemList[1]
                } else if (line.indexOf("MOA") > -1 && !MOA_flag) {
                    MOA_flag = true
                    
                    let MOA_SEQ:string = itemList[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4)
                    TAX_Item["MOA" + MOA_SEQ] = await this.getDataMOA(lineList, i, MOA_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)
                    TAX_Item["LOC" + LOC_SEQ] = await this.getDataLOC(lineList, i, LOC_SEQ)
                } else  if (line.indexOf("EQD") > -1 || line.indexOf("ALC") > -1 || line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    TAX_List.push(TAX_Item)
                    break
                }
            }
        }
        return TAX_List
    }

    /**
     * PAYMENT TERMS BASICS
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns PAT list
     */
    public static async getDataPATGroup(lineList:string[], idx:number, segSeq:string) {
        let PAT_Item:any = {}
        let line:string
        let flag:boolean = false
        let MOA_flag:boolean = false
        let DTM_flag:boolean = false
        let PCD_flag:boolean = false
        let itemList:string[]
        let PAT_List:any[] = []
        let PAT_SEQ:string = "PAT" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(PAT_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                PAT_Item[PAT_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(PAT_SEQ + "00") > -1 && flag) {
                PAT_List.push(PAT_Item)
                
                PAT_Item = {}
                itemList = line.split(":")
                PAT_Item[PAT_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(PAT_SEQ) > -1) {
                    PAT_Item["PAT"+ itemSeq] = itemList[1]
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)
                    PAT_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("PCD") > -1 && !PCD_flag) {
                    PCD_flag = true
                    
                    let PCD_SEQ:string = itemList[0].trim()
                    PCD_SEQ = PCD_SEQ.substr(3, 4)
                    PAT_Item["PCD" + PCD_SEQ] = await this.getDataPCD(lineList, i, PCD_SEQ)
                } else if (line.indexOf("MOA") > -1 && !MOA_flag) {
                    MOA_flag = true
                    
                    let MOA_SEQ:string = itemList[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4)
                    PAT_Item["MOA" + MOA_SEQ] = await this.getDataMOA(lineList, i, MOA_SEQ)
                } else  if (line.indexOf("EQD") > -1 || line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    PAT_List.push(PAT_Item)
                    break
                }
            }
        }
        return PAT_List
    }

    /**
     * SCHEDULING CONDITIONS
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns SCC list
     */
    public static async getDataSCCGroup(lineList:string[], idx:number, segSeq:string) {
        let SCC_Item:any = {}
        let line:string
        let flag:boolean = false
        let QTY_flag:boolean = false
        let FTX_flag:boolean = false
        let RFF_flag:boolean = false
        let itemList:string[]
        let SCC_List:any[] = []
        let SCC_SEQ:string = "SCC" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(SCC_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                SCC_Item[SCC_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(SCC_SEQ + "00") > -1 && flag) {
                SCC_List.push(SCC_Item)
                
                SCC_Item = {}
                itemList = line.split(":")
                SCC_Item[SCC_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(SCC_SEQ) > -1) {
                    SCC_Item["SCC"+ itemSeq] = itemList[1]
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)
                    SCC_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    
                    let RFF_SEQ:string = itemList[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4)
                    SCC_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                } else if (line.indexOf("QTY") > -1 && !QTY_flag) {
                    QTY_flag = true
                    
                    let QTY_SEQ:string = itemList[0].trim()
                    QTY_SEQ = QTY_SEQ.substr(3, 4)
                    SCC_Item["QTY" + QTY_SEQ] = await this.getDataQTYGroup(lineList, i, QTY_SEQ)
                } else  if (line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    SCC_List.push(SCC_Item)
                    break
                }
            }
        }
        return SCC_List
    }

    /**
     * QUANTITY
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns QTY list
     */
    public static async getDataQTYGroup(lineList:string[], idx:number, segSeq:string) {
        let QTY_Item:any = {}
        let line:string
        let flag:boolean = false
        let DTM_flag:boolean = false
        let MOA_flag:boolean = false
        let itemList:string[]
        let QTY_List:any[] = []
        let QTY_SEQ:string = "QTY" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(QTY_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                QTY_Item[QTY_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(QTY_SEQ + "01") > -1 && flag) {
                QTY_List.push(QTY_Item)
                
                QTY_Item = {}
                itemList = line.split(":")
                QTY_Item[QTY_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(QTY_SEQ) > -1) {
                    QTY_Item["QTY" + itemSeq] = itemList[1]
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)
                    QTY_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("MOA") > -1 && !MOA_flag) {
                    MOA_flag = true
                    
                    let MOA_SEQ:string = itemList[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4)
                    QTY_Item["MOA" + MOA_SEQ] = await this.getDataMOA(lineList, i, MOA_SEQ)
                } else  if (line.indexOf("SCC") > -1 || line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    QTY_List.push(QTY_Item)
                    break
                }
            }
        }
        return QTY_List
    }

    /**
     * ADDITIONAL PRICE INFORMATION
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns APR list
     */
    public static async getDataAPRGroup(lineList:string[], idx:number, segSeq:string) {
        let APR_Item:any = {}
        let line:string
        let flag:boolean = false
        let DTM_flag:boolean = false
        let RNG_flag:boolean = false
        let itemList:string[]
        let APR_List:any[] = []
        let APR_SEQ:string = "APR" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(APR_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                APR_Item[APR_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(APR_SEQ + "00") > -1 && flag) {
                APR_List.push(APR_Item)
                
                APR_Item = {}
                itemList = line.split(":")
                APR_Item[APR_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(APR_SEQ) > -1) {
                    APR_Item["APR"+ itemSeq] = itemList[1]
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)
                    APR_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("RNG") > -1 && !RNG_flag) {
                    RNG_flag = true
                    
                    let RNG_SEQ:string = itemList[0].trim()
                    RNG_SEQ = RNG_SEQ.substr(3, 4)
                    APR_Item["RNG" + RNG_SEQ] = await this.getDataRNG(lineList, i, RNG_SEQ)
                } else  if (line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    APR_List.push(APR_Item)
                    break
                }
            }
        }
        return APR_List
    }

    /**
     * ALLOWANCE OR CHARGE
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns ALC list
     */
    public static async getDataALCGroup(lineList:string[], idx:number, segSeq:string) {
        let ALC_Item:any = {}
        let line:string
        let flag:boolean = false
        let DTM_flag:boolean = false
        let ALI_flag:boolean = false
        let QTY_flag:boolean = false
        let PCD_flag:boolean = false
        let MOA_flag:boolean = false
        let RTE_flag:boolean = false
        let TAX_flag:boolean = false
        let itemList:string[]
        let ALC_List:any[] = []
        let ALC_SEQ:string = "ALC" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(ALC_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                ALC_Item[ALC_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(ALC_SEQ + "00") > -1 && flag) {
                ALC_List.push(ALC_Item)
                
                ALC_Item = {}
                itemList = line.split(":")
                ALC_Item[ALC_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(ALC_SEQ) > -1) {
                    ALC_Item["ALC"+ itemSeq] = itemList[1]
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)
                    ALC_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("ALI") > -1 && !ALI_flag) {
                    ALI_flag = true
                    
                    let ALI_SEQ:string = itemList[0].trim()
                    ALI_SEQ = ALI_SEQ.substr(3, 4)
                    ALC_Item["ALI" + ALI_SEQ] = await this.getDataALI(lineList, i, ALI_SEQ)
                } else if (line.indexOf("QTY") > -1 && !QTY_flag) {
                    QTY_flag = true
                    
                    let QTY_SEQ:string = itemList[0].trim()
                    QTY_SEQ = QTY_SEQ.substr(3, 4)
                    ALC_Item["QTY" + QTY_SEQ] = await this.getDataQTY(lineList, i, QTY_SEQ)
                } else if (line.indexOf("PCD") > -1 && !PCD_flag) {
                    PCD_flag = true
                    
                    let PCD_SEQ:string = itemList[0].trim()
                    PCD_SEQ = PCD_SEQ.substr(3, 4)
                    ALC_Item["PCD" + PCD_SEQ] = await this.getDataPCDGroup(lineList, i, PCD_SEQ)
                } else if (line.indexOf("MOA") > -1 && !MOA_flag) {
                    MOA_flag = true
                    
                    let MOA_SEQ:string = itemList[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4)
                    ALC_Item["MOA" + MOA_SEQ] = await this.getDataMOAGroup(lineList, i, MOA_SEQ)
                } else if (line.indexOf("RTE") > -1 && !RTE_flag) {
                    RTE_flag = true
                    
                    let RTE_SEQ:string = itemList[0].trim()
                    RTE_SEQ = RTE_SEQ.substr(3, 4)
                    ALC_Item["RTE" + RTE_SEQ] = await this.getDataRTEGroup(lineList, i, RTE_SEQ)
                } else if (line.indexOf("TAX") > -1 && !TAX_flag) {
                    TAX_flag = true
                    
                    let TAX_SEQ:string = itemList[0].trim()
                    TAX_SEQ = TAX_SEQ.substr(3, 4)
                    ALC_Item["TAX" + TAX_SEQ] = await this.getDataTAXGroup(lineList, i, TAX_SEQ)
                } else  if (line.indexOf("RCS") > -1 || line.indexOf("LIN") > -1 || line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    ALC_List.push(ALC_Item)
                    break
                }
            }
        }
        return ALC_List
    }

    /**
     * RATE DETAILS
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns RTE list
     */
    public static async getDataRTEGroup(lineList:string[], idx:number, segSeq:string) {
        let RTE_Item:any = {}
        let line:string
        let flag:boolean = false
        let RNG_flag:boolean = false
        let itemList:string[]
        let RTE_List:any[] = []
        let RTE_SEQ:string = "RTE" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(RTE_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                RTE_Item[RTE_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(RTE_SEQ + "01") > -1 && flag) {
                RTE_List.push(RTE_Item)
                
                RTE_Item = {}
                itemList = line.split(":")
                RTE_Item[RTE_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(RTE_SEQ) > -1) {
                    RTE_Item["RTE" + itemSeq] = itemList[1]
                } else if (line.indexOf("RNG") > -1 && !RNG_flag) {
                    RNG_flag = true
                    
                    let RNG_SEQ:string = itemList[0].trim()
                    RNG_SEQ = RNG_SEQ.substr(3, 4)
                    RTE_Item["RNG" + RNG_SEQ] = await this.getDataRNG(lineList, i, RNG_SEQ)
                } else  if (line.indexOf("SCC") > -1 || line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    RTE_List.push(RTE_Item)
                    break
                }
            }
        }
        return RTE_List
    }

    /**
     * MONETARY AMOUNT
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns MOA list
     */
    public static async getDataMOAGroup(lineList:string[], idx:number, segSeq:string) {
        let MOA_Item:any = {}
        let line:string
        let flag:boolean = false
        let RNG_flag:boolean = false
        let itemList:string[]
        let MOA_List:any[] = []
        let MOA_SEQ:string = "MOA" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(MOA_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                MOA_Item[MOA_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(MOA_SEQ + "01") > -1 && flag) {
                MOA_List.push(MOA_Item)
                
                MOA_Item = {}
                itemList = line.split(":")
                MOA_Item[MOA_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(MOA_SEQ) > -1) {
                    MOA_Item['MOA'+ itemSeq] = itemList[1]
                } else if (line.indexOf("RNG") > -1 && !RNG_flag) {
                    RNG_flag = true
                    
                    let RNG_SEQ:string = itemList[0].trim()
                    RNG_SEQ = RNG_SEQ.substr(3, 4)
                    MOA_Item["RNG" + RNG_SEQ] = await this.getDataRNG(lineList, i, RNG_SEQ)
                } else if (line.indexOf("ALC") > -1 || line.indexOf("RTE") > -1 || line.indexOf("TAX") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    MOA_List.push(MOA_Item)
                    break
                }
            }
        }
        return MOA_List
    }

    /**
     * PERCENTAGE DETAILS
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns PCD list
     */
    public static async getDataPCDGroup(lineList:string[], idx:number, segSeq:string) {
        let PCD_Item:any = {}
        let line:string
        let flag:boolean = false
        let RNG_flag:boolean = false
        let itemList:string[]
        let PCD_List:any[] = []
        let PCD_SEQ:string = "PCD" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(PCD_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                PCD_Item[PCD_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(PCD_SEQ + "01") > -1 && flag) {
                PCD_List.push(PCD_Item)
                
                PCD_Item = {}
                itemList = line.split(":")
                PCD_Item[PCD_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(PCD_SEQ) > -1) {
                    PCD_Item["PCD" + itemSeq] = itemList[1]
                } else if (line.indexOf("RNG") > -1 && !RNG_flag) {
                    RNG_flag = true
                    
                    let RNG_SEQ:string = itemList[0].trim()
                    RNG_SEQ = RNG_SEQ.substr(3, 4)
                    PCD_Item["RNG" + RNG_SEQ] = await this.getDataRNG(lineList, i, RNG_SEQ)
                } else if (line.indexOf("MOA") > -1 || line.indexOf("ALC") > -1 || line.indexOf("RTE") > -1 || line.indexOf("TAX") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    PCD_List.push(PCD_Item)
                    break
                }
            }
        }
        return PCD_List
    }

    /**
     * REQUIREMENTS AND CONDITIONS
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns RCS list
     */
    public static async getDataRCSGroup(lineList:string[], idx:number, segSeq:string) {
        let RCS_Item:any = {}
        let line:string
        let flag:boolean = false
        let DTM_flag:boolean = false
        let RFF_flag:boolean = false
        let FTX_flag:boolean = false
        let itemList:string[]
        let RCS_List:any[] = []
        let RCS_SEQ:string = "RCS" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(RCS_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                RCS_Item[RCS_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(RCS_SEQ + "00") > -1 && flag) {
                RCS_List.push(RCS_Item)
                
                RCS_Item = {}
                itemList = line.split(":")
                RCS_Item[RCS_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(RCS_SEQ) > -1) {
                    RCS_Item["RCS"+ itemSeq] = itemList[1]
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)
                    RCS_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    
                    let RFF_SEQ:string = itemList[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4)
                    RCS_Item["RFF" + RFF_SEQ] = await this.getDataRFF(lineList, i, RFF_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)
                    RCS_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else  if (line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    RCS_List.push(RCS_Item)
                    break
                }
            }
        }
        return RCS_List
    }

    /**
     * LINE ITEM
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns LIN list
     */
    public static async getDataLINGroup(lineList:string[], idx:number, segSeq:string) {
        let LIN_Item:any = {}
        let line:string
        let flag:boolean = false
        let PIA_flag:boolean = false
        let IMD_flag:boolean = false
        let MEA_flag:boolean = false
        let QTY_flag:boolean = false
        let PCD_flag:boolean = false
        let ALI_flag:boolean = false
        let DTM_flag:boolean = false
        let MOA_flag:boolean = false
        let GIN_flag:boolean = false
        let GIR_flag:boolean = false
        let QVR_flag:boolean = false
        let DOC_flag:boolean = false
        let PAI_flag:boolean = false
        let FTX_flag:boolean = false
        let PAT_flag:boolean = false
        let PRI_flag:boolean = false
        let RFF_flag:boolean = false
        let PAC_flag:boolean = false
        let LOC_flag:boolean = false
        let TAX_flag:boolean = false
        let NAD_flag:boolean = false
        let ALC_flag:boolean = false
        let TDT_flag:boolean = false
        let TOD_flag:boolean = false
        let EQD_flag:boolean = false
        let SCC_flag:boolean = false
        let RCS_flag:boolean = false
        let STG_flag:boolean = false
        let itemList:string[]
        let LIN_List:any[] = []
        let LIN_SEQ:string = "LIN" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(LIN_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                LIN_Item[LIN_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(LIN_SEQ + "00") > -1 && flag) {
                LIN_List.push(LIN_Item)
                
                LIN_Item = {}
                itemList = line.split(":")
                LIN_Item[LIN_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(LIN_SEQ) > -1) {
                    LIN_Item["LIN"+ itemSeq] = itemList[1]
                } else if (line.indexOf("PIA") > -1 && !PIA_flag) {
                    PIA_flag = true
                    
                    let PIA_SEQ:string = itemList[0].trim()
                    PIA_SEQ = PIA_SEQ.substr(3, 4)
                    LIN_Item["PIA" + PIA_SEQ] = await this.getDataPIA(lineList, i, PIA_SEQ)
                } else if (line.indexOf("IMD") > -1 && !IMD_flag) {
                    IMD_flag = true
                    
                    let IMD_SEQ:string = itemList[0].trim()
                    IMD_SEQ = IMD_SEQ.substr(3, 4)
                    LIN_Item["IMD" + IMD_SEQ] = await this.getDataIMD(lineList, i, IMD_SEQ)
                } else if (line.indexOf("MEA") > -1 && !MEA_flag) {
                    MEA_flag = true
                    
                    let MEA_SEQ:string = itemList[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4)
                    LIN_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                } else if (line.indexOf("QTY") > -1 && !QTY_flag) {
                    QTY_flag = true
                    
                    let QTY_SEQ:string = itemList[0].trim()
                    QTY_SEQ = QTY_SEQ.substr(3, 4)
                    LIN_Item["QTY" + QTY_SEQ] = await this.getDataQTY(lineList, i, QTY_SEQ)
                } else if (line.indexOf("PCD") > -1 && !PCD_flag) {
                    PCD_flag = true
                    
                    let PCD_SEQ:string = itemList[0].trim()
                    PCD_SEQ = PCD_SEQ.substr(3, 4)
                    LIN_Item["PCD" + PCD_SEQ] = await this.getDataPCD(lineList, i, PCD_SEQ)
                } else if (line.indexOf("ALI") > -1 && !ALI_flag) {
                    ALI_flag = true
                    
                    let ALI_SEQ:string = itemList[0].trim()
                    ALI_SEQ = ALI_SEQ.substr(3, 4)
                    LIN_Item["ALI" + ALI_SEQ] = await this.getDataALI(lineList, i, ALI_SEQ)
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)
                    LIN_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("MOA") > -1 && !MOA_flag) {
                    MOA_flag = true
                    
                    let MOA_SEQ:string = itemList[0].trim()
                    MOA_SEQ = MOA_SEQ.substr(3, 4)
                    LIN_Item["MOA" + MOA_SEQ] = await this.getDataMOA(lineList, i, MOA_SEQ)
                } else if (line.indexOf("GIN") > -1 && !GIN_flag) {
                    GIN_flag = true
                    
                    let GIN_SEQ:string = itemList[0].trim()
                    GIN_SEQ = GIN_SEQ.substr(3, 4)
                    LIN_Item["GIN" + GIN_SEQ] = await this.getDataGIN(lineList, i, GIN_SEQ)
                } else if (line.indexOf("GIR") > -1 && !GIR_flag) {
                    GIR_flag = true
                    
                    let GIR_SEQ:string = itemList[0].trim()
                    GIR_SEQ = GIR_SEQ.substr(3, 4)
                    LIN_Item["GIR" + GIR_SEQ] = await this.getDataGIR(lineList, i, GIR_SEQ)
                } else if (line.indexOf("QVR") > -1 && !QVR_flag) {
                    QVR_flag = true
                    
                    let QVR_SEQ:string = itemList[0].trim()
                    QVR_SEQ = QVR_SEQ.substr(3, 4)
                    LIN_Item["QVR" + QVR_SEQ] = await this.getDataQVR(lineList, i, QVR_SEQ)
                } else if (line.indexOf("DOC") > -1 && !DOC_flag) {
                    DOC_flag = true
                    
                    let DOC_SEQ:string = itemList[0].trim()
                    DOC_SEQ = DOC_SEQ.substr(3, 4)
                    LIN_Item["DOC" + DOC_SEQ] = await this.getDataDOC(lineList, i, DOC_SEQ)
                } else if (line.indexOf("PAI") > -1 && !PAI_flag) {
                    PAI_flag = true
                    
                    let PAI_SEQ:string = itemList[0].trim()
                    PAI_SEQ = PAI_SEQ.substr(3, 4)
                    LIN_Item["PAI" + PAI_SEQ] = await this.getDataPAI(lineList, i, PAI_SEQ)
                } else if (line.indexOf("FTX") > -1 && !FTX_flag) {
                    FTX_flag = true
                    
                    let FTX_SEQ:string = itemList[0].trim()
                    FTX_SEQ = FTX_SEQ.substr(3, 4)
                    LIN_Item["FTX" + FTX_SEQ] = await this.getDataFTX(lineList, i, FTX_SEQ)
                } else if (line.indexOf("PAT") > -1 && !PAT_flag) {
                    PAT_flag = true
                    
                    let PAT_SEQ:string = itemList[0].trim()
                    PAT_SEQ = PAT_SEQ.substr(3, 4)
                    LIN_Item["PAT" + PAT_SEQ] = await this.getDataPATGroup(lineList, i, PAT_SEQ)
                } else if (line.indexOf("PRI") > -1 && !PRI_flag) {
                    PRI_flag = true
                    
                    let PRI_SEQ:string = itemList[0].trim()
                    PRI_SEQ = PRI_SEQ.substr(3, 4)
                    LIN_Item["PRI" + PRI_SEQ] = await this.getDataPRIGroup(lineList, i, PRI_SEQ)
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    
                    let RFF_SEQ:string = itemList[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4)
                    LIN_Item["RFF" + RFF_SEQ] = await this.getDataRFFGroup(lineList, i, RFF_SEQ)
                } else if (line.indexOf("PAC") > -1 && !PAC_flag) {
                    PAC_flag = true
                    
                    let PAC_SEQ:string = itemList[0].trim()
                    PAC_SEQ = PAC_SEQ.substr(3, 4)
                    LIN_Item["PAC" + PAC_SEQ] = await this.getDataPACGroup(lineList, i, PAC_SEQ)
                } else if (line.indexOf("LOC") > -1 && !LOC_flag) {
                    LOC_flag = true
                    
                    let LOC_SEQ:string = itemList[0].trim()
                    LOC_SEQ = LOC_SEQ.substr(3, 4)
                    LIN_Item["LOC" + LOC_SEQ] = await this.getDataLOCGroup(lineList, i, LOC_SEQ)
                } else if (line.indexOf("TAX") > -1 && !TAX_flag) {
                    TAX_flag = true
                    
                    let TAX_SEQ:string = itemList[0].trim()
                    TAX_SEQ = TAX_SEQ.substr(3, 4)
                    LIN_Item["TAX" + TAX_SEQ] = await this.getDataTAXGroup(lineList, i, TAX_SEQ)
                } else if (line.indexOf("NAD") > -1 && !NAD_flag) {
                    NAD_flag = true
                    
                    let NAD_SEQ:string = itemList[0].trim()
                    NAD_SEQ = NAD_SEQ.substr(3, 4)
                    LIN_Item["NAD" + NAD_SEQ] = await this.getDataNADGroup(lineList, i, NAD_SEQ)
                } else if (line.indexOf("ALC") > -1 && !ALC_flag) {
                    ALC_flag = true
                    
                    let ALC_SEQ:string = itemList[0].trim()
                    ALC_SEQ = ALC_SEQ.substr(3, 4)
                    LIN_Item["ALC" + ALC_SEQ] = await this.getDataALCGroup(lineList, i, ALC_SEQ)
                } else if (line.indexOf("TDT") > -1 && !TDT_flag) {
                    TDT_flag = true
                    
                    let TDT_SEQ:string = itemList[0].trim()
                    TDT_SEQ = TDT_SEQ.substr(3, 4)
                    LIN_Item["TDT" + TDT_SEQ] = await this.getDataTDTGroup(lineList, i, TDT_SEQ)
                } else if (line.indexOf("TOD") > -1 && !TOD_flag) {
                    TOD_flag = true
                    
                    let TOD_SEQ:string = itemList[0].trim()
                    TOD_SEQ = TOD_SEQ.substr(3, 4)
                    LIN_Item["TOD" + TOD_SEQ] = await this.getDataTODGroup(lineList, i, TOD_SEQ)
                } else if (line.indexOf("EQD") > -1 && !EQD_flag) {
                    EQD_flag = true
                    
                    let EQD_SEQ:string = itemList[0].trim()
                    EQD_SEQ = EQD_SEQ.substr(3, 4)
                    LIN_Item["EQD" + EQD_SEQ] = await this.getDataEQDGroup(lineList, i, EQD_SEQ)
                } else if (line.indexOf("SCC") > -1 && !SCC_flag) {
                    SCC_flag = true
                    
                    let SCC_SEQ:string = itemList[0].trim()
                    SCC_SEQ = SCC_SEQ.substr(3, 4)
                    LIN_Item["SCC" + SCC_SEQ] = await this.getDataSCCGroup(lineList, i, SCC_SEQ)
                } else if (line.indexOf("STG") > -1 && !STG_flag) {
                    STG_flag = true
                    
                    let STG_SEQ:string = itemList[0].trim()
                    STG_SEQ = STG_SEQ.substr(3, 4)
                    LIN_Item["STG" + STG_SEQ] = await this.getDataSTGGroup(lineList, i, STG_SEQ)
                } else if (line.indexOf("RCS") > -1 && !RCS_flag) {
                    RCS_flag = true
                    
                    let RCS_SEQ:string = itemList[0].trim()
                    RCS_SEQ = RCS_SEQ.substr(3, 4)
                    LIN_Item["RCS" + RCS_SEQ] = await this.getDataRCSGroup(lineList, i, RCS_SEQ)
                } else  if (line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    LIN_List.push(LIN_Item)
                    break
                }
            }
        }
        return LIN_List
    }

    /**
     * PRICE DETAILS
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns PRI list
     */
    public static async getDataPRIGroup(lineList:string[], idx:number, segSeq:string) {
        let PRI_Item:any = {}
        let line:string
        let flag:boolean = false
        let CUX_flag:boolean = false
        let APR_flag:boolean = false
        let RNG_flag:boolean = false
        let DTM_flag:boolean = false
        let itemList:string[]
        let PRI_List:any[] = []
        let PRI_SEQ:string = "PRI" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(PRI_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                PRI_Item[PRI_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(PRI_SEQ + "01") > -1 && flag) {
                PRI_List.push(PRI_Item)
                
                PRI_Item = {}
                itemList = line.split(":")
                PRI_Item[PRI_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(PRI_SEQ) > -1) {
                    PRI_Item["PRI"+ itemSeq] = itemList[1]
                } else if (line.indexOf("CUX") > -1 && !CUX_flag) {
                    CUX_flag = true
                    
                    let CUX_SEQ:string = itemList[0].trim()
                    CUX_SEQ = CUX_SEQ.substr(3, 4)
                    PRI_Item["CUX" + CUX_SEQ] = await this.getDataCUX(lineList, i, CUX_SEQ)
                } else if (line.indexOf("APR") > -1 && !APR_flag) {
                    APR_flag = true
                    
                    let APR_SEQ:string = itemList[0].trim()
                    APR_SEQ = APR_SEQ.substr(3, 4)
                    PRI_Item["APR" + APR_SEQ] = await this.getDataAPR(lineList, i, APR_SEQ)
                } else if (line.indexOf("RNG") > -1 && !RNG_flag) {
                    RNG_flag = true
                    
                    let RNG_SEQ:string = itemList[0].trim()
                    RNG_SEQ = RNG_SEQ.substr(3, 4)
                    PRI_Item["RNG" + RNG_SEQ] = await this.getDataRNG(lineList, i, RNG_SEQ)
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)
                    PRI_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("RFF") > -1 || line.indexOf("PAC") > -1 || line.indexOf("LOC") > -1 || 
                line.indexOf("TAX") > -1 || line.indexOf("NAD") > -1 || line.indexOf("ALC") > -1 || 
                line.indexOf("TDT") > -1 || line.indexOf("EQD") > -1 || line.indexOf("SCC") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    PRI_List.push(PRI_Item)
                    break                    
                }
            }
        }
        return PRI_List
    }

    /**
     * GOODS IDENTITY NUMBER
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns GIN list
     */
    public static async getDataGIN(lineList:string[], idx:number, segSeq:string) {
        let GIN_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let GIN_List:any[] = []
        let GIN_SEQ:string = "GIN" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(GIN_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                GIN_Item[GIN_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(GIN_SEQ + "00") > -1 && flag) {
                GIN_List.push(GIN_Item)
                
                GIN_Item = {}
                itemList = line.split(":")
                GIN_Item[GIN_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(GIN_SEQ) > -1) {
                    GIN_Item["GIN"+ itemSeq] = itemList[1]
                } else {
                    GIN_List.push(GIN_Item)
                    break
                }
            }
        }
        return GIN_List
    }

    /**
     * RELATED IDENTIFICATION NUMBERS
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns GIR list
     */
    public static async getDataGIR(lineList:string[], idx:number, segSeq:string) {
        let GIR_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let GIR_List:any[] = []
        let GIR_SEQ:string = "GIR" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(GIR_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                GIR_Item[GIR_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(GIR_SEQ + "00") > -1 && flag) {
                GIR_List.push(GIR_Item)
                
                GIR_Item = {}
                itemList = line.split(":")
                GIR_Item[GIR_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(GIR_SEQ) > -1) {
                    GIR_Item["GIR"+ itemSeq] = itemList[1]
                } else {
                    GIR_List.push(GIR_Item)
                    break
                }
            }
        }
        return GIR_List
    }

    /**
     * QUANTITY VARIANCES
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns QVR list
     */
    public static async getDataQVR(lineList:string[], idx:number, segSeq:string) {
        let QVR_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let QVR_List:any[] = []
        let QVR_SEQ:string = "QVR" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(QVR_SEQ + "01") > -1 && !flag) {
                itemList = line.split(":")
                QVR_Item[QVR_SEQ + "01"] = itemList[1]

                flag = true
            } else if (line.indexOf(QVR_SEQ + "01") > -1 && flag) {
                QVR_List.push(QVR_Item)
                
                QVR_Item = {}
                itemList = line.split(":")
                QVR_Item[QVR_SEQ + "01"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(QVR_SEQ) > -1) {
                    QVR_Item["QVR"+ itemSeq] = itemList[1]
                } else {
                    QVR_List.push(QVR_Item)
                    break
                }
            }
        }
        return QVR_List
    }

    /**
     * ADDITIONAL PRICE INFORMATION
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns APR list
     */
    public static async getDataAPR(lineList:string[], idx:number, segSeq:string) {
        let APR_Item:any = {}
        let line:string
        let flag:boolean = false
        let itemList:string[]
        let APR_List:any[] = []
        let APR_SEQ:string = "APR" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(APR_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                APR_Item[APR_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(APR_SEQ + "00") > -1 && flag) {
                APR_List.push(APR_Item)
                
                APR_Item = {}
                itemList = line.split(":")
                APR_Item[APR_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(APR_SEQ) > -1) {
                    APR_Item["APR"+ itemSeq] = itemList[1]
                } else {
                    APR_List.push(APR_Item)
                    break
                }
            }
        }
        return APR_List
    }

    /**
     * PACKAGE
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns PAC list
     */
    public static async getDataPACGroup(lineList:string[], idx:number, segSeq:string) {
        let PAC_Item:any = {}
        let line:string
        let flag:boolean = false
        let MEA_flag:boolean = false
        let QTY_flag:boolean = false
        let RFF_flag:boolean = false
        let DTM_flag:boolean = false
        let PCI_flag:boolean = false
        let itemList:string[]
        let PAC_List:any[] = []
        let PAC_SEQ:string = "PAC" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(PAC_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                PAC_Item[PAC_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(PAC_SEQ + "00") > -1 && flag) {
                PAC_List.push(PAC_Item)
                
                PAC_Item = {}
                itemList = line.split(":")
                PAC_Item[PAC_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(PAC_SEQ) > -1) {
                    PAC_Item["PAC"+ itemSeq] = itemList[1]
                } else if (line.indexOf("MEA") > -1 && !MEA_flag) {
                    MEA_flag = true
                    
                    let MEA_SEQ:string = itemList[0].trim()
                    MEA_SEQ = MEA_SEQ.substr(3, 4)
                    PAC_Item["MEA" + MEA_SEQ] = await this.getDataMEA(lineList, i, MEA_SEQ)
                } else if (line.indexOf("QTY") > -1 && !QTY_flag) {
                    QTY_flag = true
                    
                    let QTY_SEQ:string = itemList[0].trim()
                    QTY_SEQ = QTY_SEQ.substr(3, 4)
                    PAC_Item["QTY" + QTY_SEQ] = await this.getDataQTY(lineList, i, QTY_SEQ)
                } else if (line.indexOf("RFF") > -1 && !RFF_flag) {
                    RFF_flag = true
                    
                    let RFF_SEQ:string = itemList[0].trim()
                    RFF_SEQ = RFF_SEQ.substr(3, 4)
                    PAC_Item["RFF" + RFF_SEQ] = await this.getDataRFFGroup(lineList, i, RFF_SEQ)
                } else if (line.indexOf("DTM") > -1 && !DTM_flag) {
                    DTM_flag = true
                    
                    let DTM_SEQ:string = itemList[0].trim()
                    DTM_SEQ = DTM_SEQ.substr(3, 4)
                    PAC_Item["DTM" + DTM_SEQ] = await this.getDataDTM(lineList, i, DTM_SEQ)
                } else if (line.indexOf("PCI") > -1 && !PCI_flag) {
                    PCI_flag = true
                    
                    let PCI_SEQ:string = itemList[0].trim()
                    PCI_SEQ = PCI_SEQ.substr(3, 4)
                    PAC_Item["PCI" + PCI_SEQ] = await this.getDataPCIGroup(lineList, i, PCI_SEQ)
                } else  if (line.indexOf("LOC") > -1 || line.indexOf("TAX") > -1 || line.indexOf("NAD") > -1 || line.indexOf("ALC") > -1 || 
                line.indexOf("TDT") > -1 || line.indexOf("EQD") > -1 || line.indexOf("SCC") > -1 || 
                line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    PAC_List.push(PAC_Item)
                    break
                    
                }
            }
        }
        return PAC_List
    }

    /**
     * STAGES
     * @author Truong.Do
     * @param lineList 
     * @param idx 
     * @param segSeq 
     * @returns STG list
     */
    public static async getDataSTGGroup(lineList:string[], idx:number, segSeq:string) {
        let STG_Item:any = {}
        let line:string
        let flag:boolean = false
        let QTY_flag:boolean = false
        let itemList:string[]
        let STG_List:any[] = []
        let STG_SEQ:string = "STG" + segSeq
        for (let i = idx; i < lineList.length; i++) {
            line = lineList[i]
            if (line.indexOf(STG_SEQ + "00") > -1 && !flag) {
                itemList = line.split(":")
                STG_Item[STG_SEQ + "00"] = itemList[1]

                flag = true
            } else if (line.indexOf(STG_SEQ + "00") > -1 && flag) {
                STG_List.push(STG_Item)
                
                STG_Item = {}
                itemList = line.split(":")
                STG_Item[STG_SEQ + "00"] = itemList[1]
            } else {
                itemList = line.split(":")
                let itemSeq:string = itemList[0].trim().substr(3, 6)
                if (line.indexOf(STG_SEQ) > -1) {
                    STG_Item["STG"+ itemSeq] = itemList[1]
                } else if (line.indexOf("QTY") > -1 && !QTY_flag) {
                    QTY_flag = true
                    
                    let QTY_SEQ:string = itemList[0].trim()
                    QTY_SEQ = QTY_SEQ.substr(3, 4)
                    STG_Item["QTY" + QTY_SEQ] = await this.getDataQTYGroup(lineList, i, QTY_SEQ)
                } else  if (line.indexOf("LIN") > -1 || line.indexOf("UNS") > -1 || line.indexOf("CNT") > -1 || line.indexOf("UNT") > -1) {
                    STG_List.push(STG_Item)
                    break
                    
                }
            }
        }
        return STG_List
    }
}
export default MapperCommon