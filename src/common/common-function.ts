import { isNumber } from "underscore";

export function isNotEmpty(value:string) {
    if (!value || value.trim() === "") return false
    return true;
}

export function makeElementList(data:string, segment:string, code:string) {
    let dataList:string[] = data.split("\n")
    let elementList = []
    for (let i = 0; i < dataList.length; i++) {
        let lineData:string = dataList[i]
        if (code === "X12") {
            let itemList:string[] = lineData.split("|")
            let item:string = itemList[1]
            if (segment === item) elementList.push(lineData)
        } else {
            if (lineData.indexOf(segment) > -1) {
                elementList.push(lineData)
            }
        }
    }

    return elementList
}

export function makeSegmentList(data:string, msg:string, code:string) {
    let dataList:string[] = data.split("\n")
    let segmentList = []

    for (let i = 0; i < dataList.length; i++) {
        let lineData:string = dataList[i]
        if (code === "X12") {
            let itemList:string[] = lineData.split("|")
            let message:string = itemList[1]
            if (message === msg) segmentList.push(lineData)
        } else {
            if (lineData.indexOf(msg) > -1) {
                segmentList.push(lineData)
            }
        }
    }

    return segmentList
}

export function makeGroupSegmentList(segmentList:any, grpNm:string, segIdx:number, lvlGrp:number) {    
    let groupSegmentList = []
    let segmentSeq:string = ""
    let isFirst:boolean = false
    for (let i = 0; i < segmentList.length; i++) {
        if (i >= segIdx) {
            let segment:string = segmentList[i]
            let itemList:string[] = segment.split("|")
            let groupName:string = itemList[8]
            let levelGroup:number = Number.parseInt(itemList[9])
            if (groupName !== "0" && groupName === grpNm && lvlGrp == levelGroup) {
                groupSegmentList.push(segment)
                if (!isFirst) {
                    segmentSeq = itemList[2] + itemList[3].substr(0, 2)
                    isFirst = true
                }
            }
        }
    }

    return {groupSegmentList: groupSegmentList, segmentSeq: segmentSeq}
}

export function makeGroupSegmentListForSecondGroup(segmentList:any, grpNm:string, segIdx:number, secLvlGrp:number) {    
    let groupSegmentList = []
    let segmentSeq:string = "";
    for (let i = 0; i < segmentList.length; i++) {
        if (i >= segIdx) {
            let segment:string = segmentList[i]
            let itemList:string[] = segment.split("|")
            let groupName:string = itemList[8]
            //let segmentSeq:string = itemList[2]
            let levelGroup:number = Number.parseInt(itemList[9])
            if (groupName !== "0" && groupName === grpNm && secLvlGrp == levelGroup) {
                groupSegmentList.push(segment)
                segmentSeq = itemList[2] + "00"
                let nextSegment:string = segmentList[i + 1]
                itemList = nextSegment.split("|")
                let nextGroupName:string = itemList[8]
                if (nextGroupName !== groupName) break
            }
        }
    }

    return {groupSegmentList: groupSegmentList, segmentSeq: segmentSeq}
}


export function getSegmentSeq(elementList:string[], segSeq:string, data:string, index:number) {
    //console.log("-----------Segment SEQ---------------")
    let itemID:string = ""
    let compositeList:string[] = data.split(":")
    if (compositeList.length == 1) {
        let compositeID:string = compositeList[0]
        
        for (let i_element = 0; i_element < elementList.length; i_element++) {
            if (i_element == index) {
                let line = elementList[i_element]
                let itemList:string[] = line.split("|")
                let elementID = itemList[3]
                let childNo:number = 0
                if (Number.parseInt(elementID)) {
                    if (!isNotEmpty(compositeID)) {
                        compositeID = "" 
                        if (checkElementNextExist(index + 1, compositeList)) {
                            itemID = segSeq + itemList[2] + ":" + compositeID + "\n"
                        }   
                    } else {
                        itemID = segSeq + itemList[2] + ":" + compositeID + "\n"
                    }
                    index = index + 1
                    break
                } else {                    
                    childNo = Number.parseInt(itemList[6]) 
                    index = index + childNo

                    line = elementList[i_element + 1]
                    itemList = line.split("|")
                    if (!isNotEmpty(compositeID)) {
                        compositeID = "" 
                        if (checkElementNextExist(index + 1, compositeList)) {
                            itemID = segSeq + itemList[2] + ":" + compositeID + "\n"
                        }   
                    } else {
                        itemID = segSeq + itemList[2] + ":" + compositeID + "\n"
                    }
                    index = index + 1
                    break
                }
            }            
        }
    
    } else {
        for (let i_element = 0; i_element < elementList.length; i_element++) {
            if (i_element == index) {
                let line = elementList[i_element]
                let itemList:string[] = line.split("|")
                let elementID:string = itemList[3]
                let childNo:number = 0
                if (!isNumber(elementID)) {
                    childNo = Number.parseInt(itemList[6]) 
                    index = index + childNo                    
                    
                    for (let i = 0; i < childNo; i++) {
                        line = elementList[i_element + 1 + i]
                        itemList = line.split("|")
                        let compositeID:string = compositeList[i]
                        if (!isNotEmpty(compositeID)) {
                            compositeID = ""
                            if (checkElementNextExist(index + 1, compositeList)) {
                                itemID += segSeq + itemList[2] + ":" + compositeID + "\n"
                            } 
                        } else {
                            itemID = itemID + segSeq + itemList[2] + ":" + compositeID + "\n"
                        }
                    }
                    break
                }
            }            
        }
    }    
    
    return {itemSeq: itemID, index: index}
}

function checkElementNextExist(nextIndex:number, compositeList:string[]) : boolean{
    for (let i_composite = nextIndex; i_composite < compositeList.length; i_composite++) {
        let compositeID:string = compositeList[i_composite]
        if (isNotEmpty(compositeID)) return true
        else {
            nextIndex += 1
            return checkElementNextExist(nextIndex, compositeList)
        }
    }
    return false
}

export function getGroupSegmentSeq(elementList:string[], segSeq:string, data:string, index:number, tabBiz:string) {
    //console.log("-----------Segment SEQ---------------")
    let itemID:string = ""
    let replacer = /\?\:/g
    data = data.replace(replacer, "'")
    //let totalChild:number = 0
    let compositeList:string[] = data.split(":")
    if (compositeList.length == 1) {
        let compositeID:string = compositeList[0]        
        for (let i_element = 0; i_element < elementList.length; i_element++) {
            if (i_element == index) {
                let line = elementList[i_element]
                let itemList:string[] = line.split("|")
                let elementID = itemList[3]
                let childNo:number = 0
                let idxChild:number = 0
                if (Number.parseInt(elementID)) {
                    if (!isNotEmpty(compositeID)) {
                        compositeID = "" 
                        if (checkElementNextExist(index + 1, compositeList)) {
                            itemID = tabBiz+ segSeq + itemList[2] + ":" + compositeID + "\n"
                        }   
                    } else {
                        replacer = /\'/g
                        compositeID = compositeID.replace(replacer, ":")
                        itemID = tabBiz+ segSeq + itemList[2] + ":" + compositeID + "\n"
                    }
                    index = index + 1
                    break
                } else {                    
                    childNo = Number.parseInt(itemList[6])                    
                    index = index + childNo
                    line = elementList[i_element + 1]
                    itemList = line.split("|")
                    if (!isNotEmpty(compositeID)) {
                        compositeID = "" 
                        idxChild = index
                        if (checkElementNextExist(idxChild + 1, compositeList)) {
                            itemID = tabBiz + segSeq + itemList[2] + ":" + compositeID + "\n"
                        }   
                    } else {
                        replacer = /\'/g
                        compositeID = compositeID.replace(replacer, ":")
                        itemID = tabBiz + segSeq + itemList[2] + ":" + compositeID + "\n"
                    }
                    index = index + 1
                    break
                }
            }            
        }
    
    } else { // multi character ":"
        for (let i_element = 0; i_element < elementList.length; i_element++) {
            if (i_element == index) {
                let line = elementList[i_element]
                let itemList:string[] = line.split("|")
                let elementID:string = itemList[3]
                let childNo:number = 0
                let idxChild:number = 0
                if (!isNumber(elementID)) {
                    childNo = Number.parseInt(itemList[6]) 
                    index = index + childNo
                    
                    for (let i = 0; i < childNo; i++) {
                        line = elementList[i_element + 1 + i]
                        itemList = line.split("|")
                        let compositeID:string = compositeList[i]
                        idxChild = index
                        if (!isNotEmpty(compositeID)) {
                            compositeID = ""
                            if (checkElementNextExist(idxChild + 1, compositeList)) {
                                itemID += tabBiz + segSeq + itemList[2] + ":" + compositeID + "\n"
                            } 
                        } else {
                            let replacer = /\'/g
                            compositeID = compositeID.replace(replacer, ":")
                            itemID += tabBiz + segSeq + itemList[2] + ":" + compositeID + "\n"
                        }
                    }
                    
                    index = index + 1 
                    break
                }
            }            
        }
    }

    return {itemSeq: itemID, index: index}
}

export function getSegmentExistGroup(segmentName:string, grpSegLst:any) {
    for (let i_seg = 0; i_seg < grpSegLst.length; i_seg++){
        let segment:string = grpSegLst[i_seg]
        let itemList:string[] = segment.split("|")
        let segNm:string = itemList[4]
        if (segmentName === segNm) return {segment: segment, idxGrp: i_seg}
    }

    return {segment: "", idxGrp: 0}
}

