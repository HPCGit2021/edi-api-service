import fs from 'fs'
import moment from 'moment'
import EdiScanner from '../models/edi-scanner'
import EdiInboundMapper from '../models/edi-inbound-mapper'
import JsonScanner from '../models/json-scanner'

const { STORE_PATH_INBOUND_REQUEST} = require('../config/constants')

class EdiApiInterfaceController {  

  /**
   * get message information
   * 
   * @author truong.do
   * @param data 
   */
  static getMessageType(data : string) {
    let re = /[\r\n]+/gm
    data = data.replace(re, "")

    let lineList:string[] = data.split("'")
    //console.log("-----getMessageType------")
    for (let i = 0; i < lineList.length; i++) {
      let line:string = lineList[i]
      if (line.indexOf("UNH\+") > -1) {
        // console.log("-----EDI file------")
        let elementList: string[] = line.split("+")
        let compositeList:string[] = elementList[2].split(":")
        // console.log(compositeList)
        let version:string = compositeList[1]+compositeList[2]
        console.log(version)
        return {status: 200, code: "EDI", message: compositeList[0], version: version, type: compositeList[3], }      
      }
    }

    // EDI message is X12 
    lineList = data.split("~")
    for (let i = 0; i < lineList.length; i++) {
      let line:string = lineList[i]
      if (line.indexOf("ST\^") > -1) {
        console.log("-----X12 file------")
        let elementList: string[] = line.split("^")
        console.log(elementList)
        let version:string = this.getVersonForX12(lineList)
        console.log(version)
        return {status: 200, code: "X12", message: elementList[1], version: version, type: "^", }
      } else if (line.indexOf("ST\*") > -1) {
        console.log("-----X12 file------")
        let elementList: string[] = line.split("*")
        console.log(elementList)
        let version:string = this.getVersonForX12(lineList)
        console.log(version)
        return {status: 200, code: "X12", message: elementList[1], version: version, type: "*", }
      }
    }
    return {code: 400}
  }

  /**
   * get version of message for X12
   * 
   * @author truong.do
   * @param lineList 
   */
  static getVersonForX12(lineList:string[]) {
    let version:string = ""
    for (let i = 0; i < lineList.length; i++) {
      let line:string = lineList[i]
      if (line.indexOf("GS\^") > -1) {
        let elementList: string[] = line.split("^")
        version = elementList[8].substr(2, 3)
        break
      } else if (line.indexOf("GS\*") > -1) {
        let elementList: string[] = line.split("*")
        version = elementList[8].substr(2, 3)
        break
      }
    }

    return version
  }

  static async translateE2F(ctx:any) {
    let data = ctx.request.body
    const { partnerId, terminalId } = ctx.params
    
    let bizData
    let flatFileData
    if (data !== undefined) {
      let re = /[\r\n]+/gm
      data = data.replace(re, "")
      //console.log(data)
      let checkData = this.getMessageType(data)
      console.log(checkData)
      if (checkData !== undefined && checkData.status === 200) {
        // store input data 
        //this.storeInputDataToServer(data, partnerId, checkData.message, checkData.code)

        // processing scanner function
        bizData = await EdiScanner.scannerInputData(data, checkData.message, checkData.version, checkData.type, checkData.code)

        // store biz data
        //this.storeBizDataToServer(bizData, partnerId, checkData.message)

        // processing mapper function
        flatFileData = await EdiInboundMapper.processTranslateToOutput(bizData, partnerId, terminalId, checkData.message, checkData.version)
        
        // store output data
        //this.storeOutputDataToServer(flatFileData, partnerId, checkData.message, "FF")
      }

      ctx.status = 200
      ctx.body = {
        success: true,
        result: flatFileData
      }
    }
  }
  
  /**
   * store input data into server
   * 
   * @author truong.do
   * @param data 
   * @param partnerId 
   * @param msg 
   * @param code 
   */
  static async storeInputDataToServer(data:any, partnerId: string, msg:string, code:string) {
    let dateFolder = moment().format("YYYYMMDD")
    console.log("-----storeInputDataToServer------")
    const saveDirPath = `${STORE_PATH_INBOUND_REQUEST}/${partnerId}/in/${dateFolder}`
    await fs.mkdirSync(saveDirPath, { recursive: true })
    let filename = moment().format("YYYYMMDDHHmmssSSS")
    filename = partnerId.toUpperCase() + "_" + msg.toUpperCase() + "_" + code.toUpperCase() + "_" + filename
    console.log('file name is ' + filename)

    let extendsionFile:string = "txt"
    if (code === "FF" || code === "EDI" || code === "X12") {
      const saveFilePath = `${saveDirPath}/${filename}.txt`
      fs.writeFile(saveFilePath, data, 'UTF8', err =>{
          if (err) {
              console.log('Error writing input file', err)
              
          } else {
              console.log('Successfully wrote input file')                
          }
      })      
    } else if (code === "JSON") {
      const jsonString = JSON.stringify(data)
      const saveFilePath = `${saveDirPath}/${filename}.json`
      fs.writeFile(saveFilePath, jsonString, 'UTF8', err =>{
          if (err) {
              console.log('Error writing input file', err)
              
          } else {
              console.log('Successfully wrote input file')                
          }
      })
    }    
  }
  
  static async translateJ2F(partnerId: string, ctx:any) {
    let data = ctx.request.body
    
    let bizData
    
    data = JSON.parse(data);

    //console.log(data)
    let checkData = this.getMessageType(data)
    bizData = checkData.code
    console.log(bizData)
    if (checkData !== undefined && checkData.status === 200) {
      // store input data 
      //this.storeInputDataToServer(data, partnerId, checkData.message, checkData.code)

      // processing scanner function
      bizData = await JsonScanner.scannerInputData(data, partnerId, checkData.message)

      // store biz data into server
      //this.storeBizDataToServer(bizData, partnerId, checkData.message)
    }

    ctx.status = 200
    ctx.body = {
      success: true,
      result: bizData
    }
    
  }

  /**
   * store biz data into server
   * 
   * @author Truong.do
   * @param data 
   * @param partnerId 
   * @param msg 
   */
  static async storeBizDataToServer(data:any, partnerId: string, msg:string) {
    let dateFolder = moment().format("YYYYMMDD")
    const saveDirPath = `${STORE_PATH_INBOUND_REQUEST}/${partnerId}/biz/${dateFolder}`
    await fs.mkdirSync(saveDirPath, { recursive: true })
    let filename = moment().format("YYYYMMDDHHmmssSSS")
    filename = partnerId.toUpperCase() + "_" + msg.toUpperCase() + "_BIZ_" + filename
    console.log('file name is ' + filename)
    
    console.log(data)
    const saveFilePath = `${saveDirPath}/${filename}.biz`
    fs.writeFile(saveFilePath, data, 'UTF8', err =>{
      if (err) {
        console.log('Error writing biz file', err)
      } else {
        console.log('Successfully wrote biz file')
      }
    })
    return {filename: filename, path: saveDirPath}
  }

  /**
   * store output data into server
   * 
   * @author truong.do
   * @param data 
   * @param partnerId 
   * @param msg 
   * @param code 
   */
  static async storeOutputDataToServer(data:any, partnerId: string, msg:string, code:string) {
    let dateFolder = moment().format("YYYYMMDD")
    console.log("-----storeOutputDataToServer------")
    const saveDirPath = `${STORE_PATH_INBOUND_REQUEST}/${partnerId}/out/${dateFolder}`
    await fs.mkdirSync(saveDirPath, { recursive: true })
    let filename = moment().format("YYYYMMDDHHmmssSSS")
    filename = partnerId.toUpperCase() + "_" + msg.toUpperCase() + "_" + code.toUpperCase() + "_" + filename
    console.log('file name is ' + filename)

    let extendsionFile:string = "txt"
    if (code === "FF" || code === "EDI" || code === "X12") {
      const saveFilePath = `${saveDirPath}/${filename}.txt`
      fs.writeFile(saveFilePath, data, 'UTF8', err =>{
          if (err) {
              console.log('Error writing output file', err)
              
          } else {
              console.log('Successfully wrote output file')                
          }
      })      
    } else if (code === "JSON") {
      const jsonString = JSON.stringify(data)
      const saveFilePath = `${saveDirPath}/${filename}.json`
      fs.writeFile(saveFilePath, jsonString, 'UTF8', err =>{
          if (err) {
              console.log('Error writing output file', err)
              
          } else {
              console.log('Successfully wrote output file')                
          }
      })
    }    
  }
}


export default EdiApiInterfaceController