import fs from 'fs'
import path from 'path'
import _ from 'underscore'
import request from 'request-promise'

const STORE_PATH = '/smartone/work/F9_TMP/TO_F9/Booking_Confirmation'
const F9_API_BKG_Confirm = 'http://34.64.253.200:80/api/f9/bkg/booking/confirm'
//const F9_API_BKG_Confirm = 'http://localhost:3001/clt/freight9/requests/booking-request/carriers/'

class SmtApiSendController {

  static async sendDataJson(tokenID:string) {
    const file = _.min(fs.readdirSync(STORE_PATH), file => (fs.statSync(path.join(STORE_PATH, file)).mtimeMs));

    if(file === Infinity) {
        console.log(`Not found anything at ${STORE_PATH}`)
        return
    }

    console.log(`Read file ${file}`)
    let data = await fs.readFileSync(`${STORE_PATH}/${file}`)
    if (data) {
        console.log(`Send JSON data to F9`)
        return request({
            method: 'POST',
            url: F9_API_BKG_Confirm,
            headers: {
              'Content-Type':'application/json',
              'Authorization': tokenID
            },
            body: data
            // formData:{
            //   file: {
            //     value:fs.createReadStream(`${STORE_PATH}/${file}`, 'utf8'), 
            //     options:{
            //       filename:`${file}`
            //     }
            //   }
            // }
        }).then(function (body) {
            //console.log(response)
            //console.log(body)
            
            
            // Delete file
            // fs.unlink(`${STORE_PATH}/${file}`, (err) => {
            //     if (err) throw err;
            //     console.log(`[${STORE_PATH}/${file}] file was deleted`);
            // });

            return 'OK'
        })
        .catch(function (err) {
          // POST failed...
          console.log(err)
          return 'Failing post to F9'
      })
    } else {
      console.log(`Failed to read [${file}] file in ${STORE_PATH}/`);
      return 'Fail'
    }
  }

  static getToken() {
    //let token: string = ''
    return request({
        method: 'POST',
        url: 'http://34.64.253.200/auth/realms/freight9/protocol/openid-connect/token',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        form: {
            'grant_type': 'password',
            'client_id': 'OPUS365-client',
            'client_secret': '260cd456-24cc-445b-8551-d13d0f869149',
            'scope': 'openid',
            'username': 'USR00',
            'password': 'USR00'
        }
    })
        .then(function (body) {
            // POST succeeded...
            body = JSON.parse(body)
            //console.log(body.id_token)
            return body.id_token
        })
        .catch(function (err) {
            // POST failed...
            return 'Nothing'
        })

    //return token
}
}

export default SmtApiSendController