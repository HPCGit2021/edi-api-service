import MapperCommon from '../../../common/mapper-common-function'
import {isNotEmpty} from '../../../common/common-function'
import moment from 'moment'

class CoparnD95bInboundMapper {
    public static async executeBizToFF(bizData:string) {
        let re = /[\r\n]+/gm
        bizData = bizData.replace(re, "'")
        
        let otp:string = ""
        let dtp:string = ""
        let msg:string = ""
        let BGM_CODE:string = ""
        let BGM_NO:string = ""
        let BGM_FUNCTION:string = ""
        let RFF_Flag:boolean = false
        let FTX_Flag:boolean = false
        let TDT0100_Flag:boolean = false
        let NAD0200_Flag:boolean = false
        let GID0300_Flag:boolean = false
        let EQD0700_Flag:boolean = false

        let TDT0100_List:any[] = []
        let NAD0200_List:any[] = []
        let RFF0005_List:any[] = []
        let FTX0004_List:any[] = []
        let GID0300_List:any[] = []
        let EQD0700_List:any[] = []

        let CNT110001:string = ""
        let CNT110002:string = ""
        let FlatFileData:string = ""
        let RFF_BOOKINGNO:string = ""
        let RFF_ABT:string = ""
        let TDT_VSL_VOYAGE:string = ""
        let TDT_VSL_OPERATOR:string = ""
        let TDT_VSL_CALLSIGN:string = ""
        let TDT_VSL_NAME:string = ""
        let DTM_ETD:string = ""
        let NAD_CARRIER:string = ""
        let FTX_GOODS:string = ""
        let DGS_IMDG:string = ""
        let DGS_UNNO:string = ""
        let ICR_NO:string = ""
        let EQD_CONTNO:string = ""
        let EQD_ISO:string = ""
        let EQD_STATUS:string = ""
        let EQD_FULLEMPTY:string = ""
        let TMD_MVMT:string = ""

        let i_RFF0005:number = 0
        let i_TDT0100:number = 0
        let i_DTM0103:number = 0
        let i_NAD0200:number = 0
        let i_GID0300:number = 0
        let i_FTX0302:number = 0
        let i_DGS0600:number = 0
        let i_EQD0700:number = 0
        let i_RFF0701:number = 0 
        let i_TMD0703:number = 0
        let i_DTM0704:number = 0
        let i_LOC0706:number = 0
        let i_MEA0707:number = 0
        let i_DIM0708:number = 0
        let i_TMP0709:number = 0
        let i_DGS0713:number = 0

        let itemList:string[] = []
        let nsysdate = moment().format('YYYYMMDDHHmmss')
        let milSec = moment().millisecond()
        nsysdate = nsysdate + milSec
        console.log("[START] system date & time = " + nsysdate)
        /*******************************/
        /*       1. READING PART       */
        /*******************************/
        let lineList:string[] = bizData.split("'")
        for (let i = 0; i < lineList.length; i++) {
            let line:string = lineList[i]
            //console.log("line_" + i + "\t" + line)
            itemList = line.split(":")
            if (line.indexOf("UNB000004") > -1) {
                otp = await MapperCommon.getDataValue(itemList[1])
            } else if (line.indexOf("UNB000008") > -1) {
                dtp = await MapperCommon.getDataValue(itemList[1])
            } else if (line.indexOf("UNB000014") > -1) {
                ICR_NO = await MapperCommon.getDataValue(itemList[1])
            } else if (line.indexOf("UNH000002") > -1) {
                msg = await MapperCommon.getDataValue(itemList[1])
            } else if (line.indexOf("BGM000101") > -1) {
                BGM_CODE = await MapperCommon.getDataValue(itemList[1])
            } else if (line.indexOf("BGM000105") > -1) {
                BGM_NO = await MapperCommon.getDataValue(itemList[1])
            } else if (line.indexOf("BGM000106") > -1) {
                BGM_FUNCTION = await MapperCommon.getDataValue(itemList[1])
            } else if (line.indexOf("FTX000401") > -1 && !FTX_Flag) {
                FTX_Flag = true
                FTX0004_List = await MapperCommon.getDataFTX(lineList, i, "0004")
            } else if (line.indexOf("RFF000501") > -1 && !RFF_Flag) {
                RFF_Flag = true
                RFF0005_List = await MapperCommon.getDataRFF(lineList, i, "0005")
            } else if (line.indexOf("TDT010000") > -1 && !TDT0100_Flag) {
                TDT0100_Flag = true
                TDT0100_List = await MapperCommon.getDataTDTGroup(lineList, i, "0100")
            } else if (line.indexOf("NAD020000") > -1 && !NAD0200_Flag) {
                NAD0200_Flag = true
                NAD0200_List = await MapperCommon.getDataNADGroup(lineList, i, "0200")
            } else if (line.indexOf("GID030000") > -1 && !GID0300_Flag) {
                GID0300_Flag = true
                GID0300_List = await MapperCommon.getDataGIDGroup(lineList, i, "0300")
            } else if (line.indexOf("EQD070000") > -1 && !EQD0700_Flag) {
                EQD0700_Flag = true
                EQD0700_List = await MapperCommon.getDataEQDGroup(lineList, i, "0700")
            } else if (line.indexOf("CNT110001") > -1) {
                itemList = line.split(":")
                CNT110001 = await MapperCommon.getDataValue(itemList[1])
            } else if (line.indexOf("CNT110002") > -1) {
                itemList = line.split(":")
                CNT110002 = await MapperCommon.getDataValue(itemList[1])
            }
        }

        /*******************************/
        /*       2. WRITING PART I     */
        /*******************************/
        nsysdate = moment().format('YYYYMMDDHHmmss')
        milSec = moment().millisecond()
        nsysdate = nsysdate + milSec
        console.log("[MIDDLE] system date & time = " + nsysdate)

        if (otp === "MAEU" && dtp === "HJS") {
            otp = "MSK"
            dtp = "HJKHH_MSK"
        } else if (otp === "MSC" && dtp === "KAOXPRES") {
            otp = "MSC"
            dtp = "HJKHH_MSC"
        } else if (otp === "TSL" && dtp === "KS078") {
            otp = "TSL"
            dtp = "KS078"
        } else if (otp === "HSD" && dtp === "TWHAMMPAC") {
            otp = "HSD"
            dtp = "TWHMMPAC"
        }

        otp = otp.padEnd(14, " ")
        dtp = dtp.padEnd(14, " ")
        FlatFileData = "COPARNSP" + nsysdate.substr(0, 12) + otp + dtp + nsysdate.substr(0, 8) + "\n"
        FlatFileData += "{BOOKING_HEADER\n"
        FlatFileData += "UNH_ID:" + msg + "\n"
        FlatFileData += "BGM_CODE:" + BGM_CODE + "\n"
        FlatFileData += "BGM_NO:" + BGM_NO + "\n"
        FlatFileData += "BGM_FUNCTION:" + BGM_FUNCTION + "\n"
        if (RFF0005_List.length > 0) {
            for (i_RFF0005 = 0; i_RFF0005 < RFF0005_List.length; i_RFF0005++) {
                let rff_data = RFF0005_List[i_RFF0005]
                if (rff_data["RFF000501"] === "BN") {
                    RFF_BOOKINGNO = rff_data["RFF000502"]
                } else if (rff_data["RFF000501"] === "ABT") {
                    RFF_ABT = rff_data["RFF000502"]
                }
            }
        }

        FlatFileData += "RFF_BOOKINGNO:" + RFF_BOOKINGNO + "\n"
        FlatFileData += "RFF_ABT:" + RFF_ABT + "\n"

        /* TDT Segment Vessel Voyage Info */
        //console.log(TDT0100_List)
        for (i_TDT0100 = 0; i_TDT0100 < TDT0100_List.length; i_TDT0100++) {
            let tdt_data = TDT0100_List[i_TDT0100]
            TDT_VSL_VOYAGE = await MapperCommon.getDataValue(tdt_data["TDT010001"])
            TDT_VSL_OPERATOR = await MapperCommon.getDataValue(tdt_data["TDT010009"])
            if (tdt_data["TDT010020"] === "103") {
                TDT_VSL_CALLSIGN = await MapperCommon.getDataValue(tdt_data["TDT010019"])
            }
            TDT_VSL_NAME = await MapperCommon.getDataValue(tdt_data["TDT010022"])

            FlatFileData += "TDT_VSL_VOYAGE:" + TDT_VSL_VOYAGE + "\n"
            FlatFileData += "TDT_VSL_OPERATOR:" + TDT_VSL_OPERATOR + "\n"
            FlatFileData += "TDT_VSL_CALLSIGN:" + TDT_VSL_CALLSIGN + "\n"
            FlatFileData += "TDT_VSL_NAME:" + TDT_VSL_NAME + "\n"

            let DTM0103_list:any[] = tdt_data["DTM0103"]? tdt_data["DTM0103"]:[]
            for (i_DTM0103 = 0; i_DTM0103 < DTM0103_list.length; i_DTM0103++) {
                let dtm_data = DTM0103_list[i_DTM0103]
                if (dtm_data["DTM010301"] === "133") {
                    DTM_ETD = await MapperCommon.getDataValue(dtm_data["DTM010302"])

                    FlatFileData += "DTM_ETD:" + DTM_ETD + "\n"
                }
            }
        }        

        // NAD
        for (i_NAD0200 = 0; i_NAD0200 < NAD0200_List.length; i_NAD0200++) {
            let nad_data = NAD0200_List[i_NAD0200]
            if (nad_data["NAD020000"] === "CA") {
                NAD_CARRIER = await MapperCommon.getDataValue(nad_data["NAD020002"])

                FlatFileData += "NAD_CARRIER:" + NAD_CARRIER + "\n"
            }
        }
        
        // GID
        for (i_GID0300 = 0; i_GID0300 < GID0300_List.length; i_GID0300++) {
            let gid_data = GID0300_List[i_GID0300]
            let FTX0302_list:any[] = gid_data["FTX0302"]? gid_data["FTX0302"]:[]
            for (i_FTX0302 = 0; i_FTX0302 < FTX0302_list.length; i_FTX0302++) {
                let ftx_data = FTX0302_list[i_FTX0302]
                if (ftx_data["FTX030200"] === "AAA") {
                    FTX_GOODS = await MapperCommon.getDataValue(ftx_data["FTX030207"])

                    FlatFileData += "FTX_GOODS:" + FTX_GOODS + "\n"
                }
            }
            
            let DGS0600_list:any[] = gid_data["DGS0600"]? gid_data["DGS0600"]:[]
            for (i_DGS0600 = 0; i_DGS0600 < DGS0600_list.length; i_DGS0600++) {
                let dgs_data = DGS0600_list[i_DGS0600]
                DGS_IMDG = await MapperCommon.getDataValue(dgs_data["DGS060002"])
                DGS_UNNO = await MapperCommon.getDataValue(dgs_data["DGS060006"])

                FlatFileData += "DGS_IMDG:" + DGS_IMDG + "\n"
                FlatFileData += "DGS_UNNO:" + DGS_UNNO + "\n"
            }
        }
        
        FlatFileData += "ICR_NO:" + ICR_NO + "\n"
        FlatFileData += "}BOOKING_HEADER\n"
        
        // EQD
        for (i_EQD0700 = 0; i_EQD0700 < EQD0700_List.length; i_EQD0700++) {
            let eqd_data = EQD0700_List[i_EQD0700]

            EQD_CONTNO = await MapperCommon.getDataValue(eqd_data["EQD070002"])
            EQD_ISO = await MapperCommon.getDataValue(eqd_data["EQD070007"])
            EQD_STATUS = await MapperCommon.getDataValue(eqd_data["EQD070012"])
            EQD_FULLEMPTY = await MapperCommon.getDataValue(eqd_data["EQD070013"])

            FlatFileData += "{BOOKING_BODY\n"
            FlatFileData += "EQD_CONTNO:" + EQD_CONTNO + "\n"
            FlatFileData += "EQD_ISO:" + EQD_ISO + "\n"
            FlatFileData += "EQD_STATUS:" + EQD_STATUS + "\n"
            FlatFileData += "EQD_FULLEMPTY:" + EQD_FULLEMPTY + "\n"

            let RFF0701_list:any[] = eqd_data["RFF0701"]? eqd_data["RFF0701"]:[]
            for (i_RFF0701 = 0; i_RFF0701 < RFF0701_list.length; i_RFF0701++) {
                let rff_data = RFF0701_list[i_RFF0701]
                if (rff_data["RFF070101"] === "BN") {
                    FlatFileData += "RFF_BOOKINGNO:" + await MapperCommon.getDataValue(rff_data["RFF070102"]) + "\n"
                } else if (rff_data["RFF070101"] === "BST") {
                    FlatFileData += "RFF_BST:" + await MapperCommon.getDataValue(rff_data["RFF070102"]) + "\n"
                } else if (rff_data["RFF070101"] === "SQ") {
                    FlatFileData += "RFF_SQ:" + await MapperCommon.getDataValue(rff_data["RFF070102"]) + "\n"
                } else if (rff_data["RFF070101"] === "UCN") {
                    FlatFileData += "RFF_UCN:" + await MapperCommon.getDataValue(rff_data["RFF070102"]) + "\n"
                } else if (rff_data["RFF070101"] === "ACD") {
                    FlatFileData += "RFF_ACD:" + await MapperCommon.getDataValue(rff_data["RFF070102"]) + "\n"
                }
            }

            FlatFileData += "EQN_QUANTITY:" + eqd_data["EQN070201"] + "\n"

            let TMD0703_list:any[] = eqd_data["TMD0703"]? eqd_data["TMD0703"]:[]
            for (i_TMD0703 = 0; i_TMD0703 < TMD0703_list.length; i_TMD0703++) {
                let tmd_data = TMD0703_list[i_TMD0703]
                if (tmd_data["DTM070401"] === "5") {
                    TMD_MVMT = await MapperCommon.getDataValue(tmd_data["TMD070301"])
                }
            }

            let DTM0704_list:any[] = eqd_data["DTM0704"]? eqd_data["DTM0704"]:[]
            for (i_DTM0704 = 0; i_DTM0704 < DTM0704_list.length; i_DTM0704++) {
                let dtm_data = DTM0704_list[i_DTM0704]
                if (dtm_data["DTM070401"] === "181") {
                    FlatFileData += "DTM_DELIVERY:" + await MapperCommon.getDataValue(dtm_data["DTM070402"]) + "\n"
                } else if (dtm_data["DTM070401"] === "201") {
                    FlatFileData += "DTM_PICKUP:" + await MapperCommon.getDataValue(dtm_data["DTM070402"]) + "\n"
                }
            }

            //console.log(eqd_data["LOC0706"])
            let LOC0706_list:any[] = eqd_data["LOC0706"]? eqd_data["LOC0706"]:[]
            for (i_LOC0706 = 0; i_LOC0706 < LOC0706_list.length; i_LOC0706++) {
                let loc_data = LOC0706_list[i_LOC0706]
                if (loc_data["LOC070600"] === "8") {
                    FlatFileData += "LOC_FND:" + await MapperCommon.getDataValue(loc_data["LOC070602"]) + "\n"
                } else if (loc_data["LOC070600"] === "9") {
                    FlatFileData += "LOC_POL:" + await MapperCommon.getDataValue(loc_data["LOC070602"]) + "\n"
                } else if (loc_data["LOC070600"] === "11") {
                    FlatFileData += "LOC_POD:" + await MapperCommon.getDataValue(loc_data["LOC070602"]) + "\n"
                } else if (loc_data["LOC070600"] === "98") {
                    FlatFileData += "LOC_EMPTYPICKUP:" + await MapperCommon.getDataValue(loc_data["LOC070602"]) + "\n"
                }
            }

            let MEA0707_list:any[] = eqd_data["MEA0707"]? eqd_data["MEA0707"]:[]
            for (i_MEA0707 = 0; i_MEA0707 < MEA0707_list.length; i_MEA0707++) {
                let mea_data = MEA0707_list[i_MEA0707]
                if (mea_data["MEA070702"] === "AAS") {
                    FlatFileData += "MEA_AIRVENT:" + await MapperCommon.getDataValue(mea_data["MEA070708"]) + "\n"
                }
            }
            
            let DIM0708_list:any[] = eqd_data["DIM0708"]? eqd_data["DIM0708"]:[]
            for (i_DIM0708 = 0; i_DIM0708 < DIM0708_list.length; i_DIM0708++) {
                let dim_data = DIM0708_list[i_DIM0708]
                if (dim_data["DIM070800"] === "5") {
                    FlatFileData += "DIM_FRONT:" + await MapperCommon.getDataValue(dim_data["DIM070803"]) + "\n"
                } else if (dim_data["DIM070800"] === "6") {
                    FlatFileData += "DIM_BACK:" + await MapperCommon.getDataValue(dim_data["DIM070803"]) + "\n"
                } else if (dim_data["DIM070800"] === "7") {
                    FlatFileData += "DIM_RIGHT:" + await MapperCommon.getDataValue(dim_data["DIM070804"]) + "\n"
                } else if (dim_data["DIM070800"] === "8") {
                    FlatFileData += "DIM_LEFT:" + await MapperCommon.getDataValue(dim_data["DIM070804"]) + "\n"
                } else if (dim_data["DIM070800"] === "9") {
                    FlatFileData += "DIM_HEIGHT:" + await MapperCommon.getDataValue(dim_data["DIM070805"]) + "\n"
                }
            }

            let TMP0709_list:any[] = eqd_data["TMP0709"]? eqd_data["TMP0709"]:[]
            for (i_TMP0709 = 0; i_TMP0709 < TMP0709_list.length; i_TMP0709++) {
                let tmp_data = TMP0709_list[i_TMP0709]
                
                FlatFileData += "TMP_TEMP:" + await MapperCommon.getDataValue(tmp_data["TMP070902"]) + "\n"
                FlatFileData += "TMP_QUALIFIER:" + await MapperCommon.getDataValue(tmp_data["TMP070903"]) + "\n"
            }

            let DGS0713_list:any[] = eqd_data["DGS0713"]? eqd_data["DGS0713"]:[]
            for (i_DGS0713 = 0; i_DGS0713 < DGS0713_list.length; i_DGS0713++) {
                let dgs_data = DGS0713_list[i_DGS0713]
                
                FlatFileData += "DGS_IMDG:" + await MapperCommon.getDataValue(dgs_data["DGS071302"]) + "\n"
                FlatFileData += "DGS_UNNO:" + await MapperCommon.getDataValue(dgs_data["DGS071306"]) + "\n"
            }

            if (isNotEmpty(TMD_MVMT)) {
                FlatFileData += "TMD_MVMT:" + TMD_MVMT + "\n"
            }

            FlatFileData += "}BOOKING_BODY\n"
        }

        //FlatFileData += "" +  + "\n"
        //FlatFileData += "" +  + "\n"

        
        console.log("------------------------")
        console.log(FlatFileData)
        nsysdate = moment().format('YYYYMMDDHHmmss')
        milSec = moment().millisecond()
        nsysdate = nsysdate + milSec
        console.log("[END] system date & time = " + nsysdate)

        return FlatFileData
    }

    
}
export default CoparnD95bInboundMapper