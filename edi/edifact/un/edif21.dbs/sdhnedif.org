|CTY|00|3500|Country|C|0|C00||00||
|CTY|01|C519|Related Location one Identification|C|2|C00||01||
|CTY|02|3835|Country|C|0|C00||01||
|CTY|03|3834|Country|C|0|C01||01||
|DGS|00|8273|DANGEROUS GOODS REGULATIONS, CODED|C|0|C00||00||
|DGS|01|C205|HAZARD CODE|C|3|C00||01||
|DGS|02|8755|Hazard code identification|M|0|M00||01||
|DGS|03|8702|Hazard substance/item/page number|C|0|C01||01||
|DGS|04|8704|Hazard code version number|C|0|C02||01||
|DGS|05|C234|UNDG INFORMATION|C|2|C00||02||
|DGS|06|7124|UNDG number|C|0|C00||02||
|DGS|07|7088|Dangerous goods flashpoint|C|0|C01||02||
|DGS|08|C223|DANGEROUS GOODS SHIPMENT FLASHPOINT|C|2|C00||03||
|DGS|09|7712|Shipment flashpoint|C|0|C00||03||
|DGS|10|6411|Measure unit qualifier|C|0|C01||03||
|DGS|11|8725|PACKING GROUP, CODED|C|0|C00||04||
|DGS|12|8756|EMS NUMBER|C|0|C00||05||
|DGS|13|8758|MFAG|C|0|C00||06||
|DGS|14|8706|TREM CARD NUMBER|C|0|C00||07||
|DGS|15|C235|HAZARD IDENTIFICATION|C|2|C00||08||
|DGS|16|8708|Hazard identification number, upper part|C|0|C00||08||
|DGS|17|8710|Substance identification number, lower part|C|0|C01||08||
|DGS|18|C236|DANGEROUS GOODS LABEL|C|3|C00||09||
|DGS|19|8712|Dangerous goods label marking|C|0|C00||09||
|DGS|20|8712|Dangerous goods label marking|C|0|C01||09||
|DGS|21|8712|Dangerous goods label marking|C|0|C02||09||
|DGS|22|8715|PACKING INSTRUCTION, CODED|C|0|C00||10||
|DGS|23|8717|CATEGORY OF MEANS OF TRANSPORT, CODED|C|0|C00||11||
|DGS|24|8719|PERMISSION FOR TRANSPORT, CODED|C|0|C00||12||
|DTM|00|2875|Date/Time qualifier|M|0|M00||00||
|DTM|01|2871|Date|M|0|M00||01||
|DTM|02|2873|Time|C|0|C00||02||
|   DTM|03|2845|Time zone specifier|C|0|C00||03||
|  EQF|00|8501|Equipment Qualifier|C|0|C00||00||
|EQF|01|8512|Equipment Qualifier|C|0|C00||01||
|EQF|02|C522|Equipment size and type|C|2|C00||02||
|EQF|03|8155|Container size and type|C|0|C00||02||
|EQF|04|8507|Eq.size/type|C|0|C01||02||
|EQF|05|8502|shipper supplied Equipment Ind|C|0|C00||03||
|EQF|06|8516|Equipment status|C|0|C00||04||
|EQF|07|8517|Full/Empty Indicator|C|0|C00||05||
|EQF|08|8524|status Reefer|C|0|C00||06||
|EQF|09|8520|Length of container(feet)|C|0|C00||07||
|EQF|10|8521|Width of cnr|C|0|C00||08||
|EQF|11|8522|Height of cnr|C|0|C00||09||
|EQF|12|8523|Container type|C|0|C00||10||
|LOC|00|3505|Location Function Qualifier|C|0|C00||00||
|LOC|01|C524|Location,code|C|2|C00||01||
|LOC|02|3913|Location,coded|C|0|C00||01||
|LOC|03|3915|code list|C|0|C01||01||
|LOC|04|3912|Location clear text|C|0|C00||02||
|MSE|00|6500|Measurement Qualifer|C|0|C00||00||
|MSE|01|C174|Value/Range|C|2|C00||01||
|MSE|02|6842|Measurement Value|C|0|C00||01||
|MSE|03|6410|Measuer unit specifier|C|0|C01||01||
|MSE|04|6834|Number of units|C|0|C00||02||
|RFF|00|1503|REFERENCE|M|4|M00||00||
|RFF|01|C517|Reference qualifier|C|2|C00||01||
|RFF|02|1826|Reference number|C|0|C00||01||
|RFF|03|1501|Line number|C|0|C01||01||
|SEL|00|9308|SEAL NUMBER|M|0|M00||00||
|SEL|01|C215|SEAL ISSUER|C|4|C00||01||
|SEL|02|9303|Sealing party, coded|C|0|C00||01||
|SEL|03|1131|Code list qualifier|C|0|C01||01||
|SEL|04|3055|Code list responsible agency, coded|C|0|C02||01||
|SEL|05|9302|Sealing party|C|0|C03||01||
|STA|00|8519|Status|C|0|C00||00||
|STA|01|4506|Message use specifier|C|0|C00||01||
|   SUP|00|4507||C|0|C00||00||
|SUP|01|C530||C|2|C00||01||
|SUP|02|4508|supp1 . info . coded|C|0|C00||01||
|SUP|03|4510||C|0|C01||01||
|SUP|04|C510||C|4|C00||02||
|SUP|05|4509||C|0|C00||02||
|SUP|06|4509||C|0|C01||02||
|SUP|07|4509||C|0|C02||02||
|SUP|08|4509||C|0|C03||02||
|TDT|00|8505|Transport Qualifier|C|0|C00||00||
|TDT|01|C526|Mode of Transport|C|2|C00||01||
|TDT|02|8059|Mode of Transport,code|C|0|C00||01||
|TDT|03|8066|Mode of Transport|C|0|C01||01||
|TDT|04|8028|Conveyance Reference number|C|0|C00||02||
|TDT|05|C511|Transport Identification|C|2|C00||03||
|TDT|06|8213|Id of Means of Transport Idendtification|C|0|C00||03||
|TDT|07|8212|Identification of the means of transport|C|0|C01||03||
|TDT|08|C512|Transport Means|C|2|C00||04||
|TDT|09|8510|Transport means,code|C|0|C00||04||
|TDT|10|8511|Transport means,clear|C|0|C01||04||
|TDT|11|C513|Carrier|C|3|C00||05||
|TDT|12|3127|Carrier identification|C|0|C00||05||
|TDT|13|3503|Carrier,code id|C|0|C01||05||
|TDT|14|3128|Carrrier Name|C|0|C02||05||
|TMP|00|6501|Tmerature 6501|C|1|C00||00||
| TMP|01|C514|Tmerature|C|2|C00||01||
| TMP|02|6502|Tmerature 6502|C|0|C00||01||
| TMP|03|6503|Tmerature 6503|C|0|C01||01||
| TMP|04|6504|Temperature 6504|C|0|C00||02||
| UNB|00|S001|SYNTAX IDENTIFIER|M|2|M00||00||
|UNB|01|0001|Syntax identifier|M|0|M00||00||
|UNB|02|0002|Syntax version number|M|0|M01||00||
|UNB|03|S002|INTERCHANGE SENDER|M|3|M00||01||
|UNB|04|0004|Sender identification|M|0|M00||01||
|UNB|05|0007|Partner identification code qualifier|C|0|C01||01||
|UNB|06|0008|Address for reverse routing|C|0|C02||01||
|UNB|07|S003|INTERCHANGE RECIPIENT|M|3|M00||02||
|UNB|08|0010|Recipient Identification|M|0|M00||02||
|UNB|09|0007|Partner identification code qualifier|C|0|C01||02||
|UNB|10|0014|Routing address|C|0|C02||02||
|UNB|11|S004|DATE/TIME OF PREPARATION|M|2|M00||03||
|UNB|12|0017|Date|M|0|M00||03||
|UNB|13|0019|Time|M|0|M01||03||
|UNB|14|0020|INTERCHANGE CONTROL REFERENCE|M|0|M00||04||
|UNB|15|S005|RECIPIENTS REFERENCE, PASSWORD|C|2|C00||05||
|UNB|16|0022|Recipient's reference/password|M|0|M00||05||
|UNB|17|0025|Recipient's reference/password qualifier|C|0|C01||05||
|UNB|18|0026|APPLICATION REFERENCE|C|0|C00||06||
|UNB|19|0029|PROCESSING PRIORITY CODE|C|0|C00||07||
|UNB|20|0031|ACKNOWLEDGEMENT REQUEST|C|0|C00||08||
|UNB|21|0032|COMMUNICATIONS AGREEMENT ID|C|0|C00||09||
|UNB|22|0035|TEST INDICATOR|C|0|C00||10||
|UNH|00|0062|MESSAGE REFERENCE NUMBER|M|0|M00||00||
|UNH|01|S009|MESSAGE IDENTIFIER|M|5|M00||01||
|UNH|02|0065|Message type|M|0|M00||01||
|UNH|03|0052|Message version number|M|0|M01||01||
|UNH|04|0054|Message release number|M|0|M02||01||
|UNH|05|0051|Controlling agency|M|0|M03||01||
|UNH|06|0057|Association assigned code|C|0|C04||01||
|UNH|07|0068|COMMON ACCESS REFERENCE|C|0|C00||02||
|UNH|08|S010|STATUS OF THE TRANSFER|C|2|C00||03||
|UNH|09|0070|Sequence of transfers|M|0|M00||03||
|UNH|10|0073|First and last transfer|C|0|C01||03||
|UNT|00|0074|NUMBER OF SEGMENTS IN THE MESSAGE|M|0|M00||00||
|UNT|01|0062|MESSAGE REFERENCE NUMBER|M|0|M00||01||
|UNZ|00|0036|INTERCHANGE CONTROL COUNT|M|0|M00||00||
|UNZ|01|0020|INTERCHANGE CONTROL REFERENCE|M|0|M00||01||
