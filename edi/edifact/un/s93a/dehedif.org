|0001|Syntax identifier|4|4|a|||||||
|0002|Syntax version number|1|1|n|||||||
|0004|Sender identifier|1|35|an|||||||
|0007|Partner identification code qualifier|1|4|an|||||||
|0008|Address for reverse routing|1|14|an|||||||
|0010|Recipient identification|1|35|an|||||||
|0014|Routing address|1|14|an|||||||
|0017|Date|6|6|n|||||||
|0019|Time|4|4|n|||||||
|0020|Interchaneg control reference|1|14|an|||||||
|0022|Recipient's referencr password|1|14|an|||||||
|0025|Recipient's referencr/password qualifier|2|2|an|||||||
|0026|Application reference|1|14|an|||||||
|0029|Processing priority code|1|1|an|||||||
|0031|Acknowledgement request|1|1|n|||||||
|0032|Communicatios agreement|1|35|an|||||||
|0035|Test indication|1|1|n|||||||
|0036|Interchange control count|1|6|n|||||||
|0038|Functional group identifcation|1|6|an|||||||
|0040|Sender's identification|1|35|an|||||||
|0044|Recipient's ientification|1|35|an|||||||
|0048|Functional group reference number|1|14|an|||||||
|0051|Controlling agency|1|2|an|||||||
|0052|Message version number|1|3|an|||||||
|0054|Message release number|1|3|an|||||||
|0057|Association assigned code|1|6|an|||||||
|0058|Application password|1|14|an|||||||
|0060|Number of message|1|6|n|||||||
|0062|Message reference number|1|14|an|||||||
|0065|Message type identifier|1|6|an|||||||
|0068|Common access reference|1|35|an|||||||
|0070|Sequence of transfers|1|2|n|||||||
|0073|First and last transfer|1|1|a|||||||
|0074|Number of segments in the message|1|6|n|||||||
|0077|Text reference code|3|3|an|||||||
|0078|Free form text|1|70|an|||||||
|0081|Section identification|1|1|an|||||||
|0083|Action , coded|1|3|an|||||||
|0085|Error type, coded|1|3|an|||||||
|1000|Document/message name|1|35|an|||||||
|1001|Document/message name, coded|1|3|an|||||||
|1004|Document/message number|1|35|an|||||||
|1049|Message section, coded|1|3|an|||||||
|1050|Sequence number|1|6|an|||||||
|1052|Message item number|1|35|an|||||||
|1054|Message sub-item number|1|6|n|||||||
|1073|Document line indicator, coded|1|3|an|||||||
|1082|Line item number|1|6|n|||||||
|1131|Code list qualifier|1|3|an|||||||
|1153|Reference qualifier|1|3|an|||||||
|1154|Reference number|1|35|an|||||||
|1156|Line number|1|6|an|||||||
|1159|Sequence number source, coded|1|3|an|||||||
|1218|Number of originals of document required|1|2|n|||||||
|1220|Number of copies of document required|1|2|n|||||||
|1222|Configuration level|1|2|n|||||||
|1225|Message function, coded|1|3|an|||||||
|1227|Calculation sequence indicator, coded|1|3|an|||||||
|1229|Action request/notification, coded|1|3|an|||||||
|1230|Allowance or charge number|1|35|an|||||||
|1233|Relational qualifier|1|3|an|||||||
|1243|Missing document indicator, coded|1|3|an|||||||
|1245|Status indicator, coded|1|3|an|||||||
|1312|Consignment load sequence number|1|4|n|||||||
|1366|Document/message source|1|35|an|||||||
|1373|Document/message status, coded|1|3|an|||||||
|1490|Consolidation item number|1|4|n|||||||
|1496|Goods item number|1|5|n|||||||
|2005|Date/time/period qualifier|1|3|an|||||||
|2009|Time relation, coded|1|3|an|||||||
|2013|Frequency, coded|1|3|an|||||||
|2015|Despatch pattern, coded|1|3|an|||||||
|2017|Despatch pattern timing, coded|1|3|an|||||||
|2151|Type of period, coded|1|3|an|||||||
|2152|Number of periods|1|3|n|||||||
|2379|Date/time/period format qualifier|1|3|an|||||||
|2380|Date/time/period|1|35|an|||||||
|2387|Lead time reference, coded|1|3|an|||||||
|2475|Payment time reference, coded|1|3|an|||||||
|3035|Party qualifier|1|3|an|||||||
|3036|Party name|1|35|an|||||||
|3039|Party id identification|1|17|an|||||||
|3042|Street and number/P.O. Box|1|35|an|||||||
|3045|Party name format, coded|1|3|an|||||||
|3055|Code list responsible agency, coded|1|3|an|||||||
|3077|Test media, coded|1|3|an|||||||
|3124|Name and address line|1|35|an|||||||
|3127|Carrier identification|1|17|an|||||||
|3128|Carrier name|1|35|an|||||||
|3139|Contact function, coded|1|3|an|||||||
|3148|Communication number|1|25|an|||||||
|3153|Communication channel identifier, coded|1|3|an|||||||
|3155|Communication channel qualifier|1|3|an|||||||
|3164|City name|1|35|an|||||||
|3192|Account holder name|1|35|an|||||||
|3194|Account holder number|1|17|an|||||||
|3207|Country, coded|1|3|an|||||||
|3222|Related place/location one|1|70|an|||||||
|3223|Related place/location one identification|1|25|an|||||||
|3224|Place/location|1|17|an|||||||
|3225|Place/location identification|1|25|an|||||||
|3227|Place/location qualifier|1|3|an|||||||
|3229|Country sub-entity identification|1|9|an|||||||
|3232|Related place/location two|1|70|an|||||||
|3233|Related place/location two identification|1|25|an|||||||
|3236|Sample location|1|35|an|||||||
|3237|Sample location, coded|1|3|an|||||||
|3239|Country of origin, coded|1|3|an|||||||
|3251|Postcode identification|1|9|an|||||||
|3279|Geographic environment, coded|1|3|an|||||||
|3285|Recipient of the instruction identification|1|17|an|||||||
|3301|Party enacting instruction identification|1|17|an|||||||
|3412|Department or employee|1|35|an|||||||
|3413|Department or employee identification|1|17|an|||||||
|3432|Institution name|1|70|an|||||||
|3433|Institution name identification|1|11|an|||||||
|3434|Institution branch number|1|17|an|||||||
|3436|Institution branch place|1|70|an|||||||
|3446|Party tax identification number|1|20|an|||||||
|3453|Language, coded|1|3|an|||||||
|3494|Job title|1|35|an|||||||
|4000|Reference version number|1|35|an|||||||
|4017|Delivery plan status indicator, coded|1|3|an|||||||
|4022|Business description|1|70|an|||||||
|4025|Business function, coded|1|3|an|||||||
|4027|Business function qualifier|1|3|an|||||||
|4043|Class of trade, coded|1|3|an|||||||
|4052|Terms of delivery|1|70|an|||||||
|4053|Terms of delivery, coded|1|3|an|||||||
|4055|Terms of delivery function, coded|1|3|an|||||||
|4065|Contract and carriage condition, coded|1|3|an|||||||
|4078|Handling instructions|1|70|an|||||||
|4079|Handling instructions, coded|1|3|an|||||||
|4183|Special conditions, coded|1|3|an|||||||
|4215|Transport charges method of payment, coded|1|3|an|||||||
|4219|Transport priority, coded|1|3|an|||||||
|4221|Discrepancy, coded|1|3|an|||||||
|4233|Marking instructions, coded|1|3|an|||||||
|4237|Prepaid/collect indicator, coded|1|3|an|||||||
|4276|Terms of payment|1|35|an|||||||
|4277|Terms of payment identification|1|17|an|||||||
|4279|Payment terms type qualifier|1|3|an|||||||
|4294|Change reason|1|35|an|||||||
|4295|Change reason, coded|1|3|an|||||||
|4343|Response type, coded|1|3|an|||||||
|4347|Product id function qualifier|1|3|an|||||||
|4383|Bank operation, coded|1|3|an|||||||
|4400|Instruction|1|35|an|||||||
|4401|Instruction, coded|1|3|an|||||||
|4403|Instruction qualifier|1|3|an|||||||
|4405|Status, coded|1|3|an|||||||
|4407|Sample process status, coded|1|3|an|||||||
|4415|Test method identification|1|17|an|||||||
|4416|Test description|1|70|an|||||||
|4419|Test route of administering, coded|1|3|an|||||||
|4424|Test reason|1|35|an|||||||
|4425|Test reason identification|1|17|an|||||||
|4431|Payment guarantee, coded|1|3|an|||||||
|4435|Payment channel, coded|1|3|an|||||||
|4439|Payment conditions, coded|1|3|an|||||||
|4440|Free text|1|70|an|||||||
|4441|Free text, coded|1|3|an|||||||
|4451|Text subject qualifier|1|3|an|||||||
|4453|Text function, coded|1|3|an|||||||
|4455|Back order, coded|1|3|an|||||||
|4457|Product/service substitution, coded|1|3|an|||||||
|4461|Payment means, coded|1|3|an|||||||
|4463|Intra-company payment, coded|1|3|an|||||||
|4465|Adjustment reason, coded|1|3|an|||||||
|4471|Settlement, coded|1|3|an|||||||
|4487|Type of financial transaction, coded|1|3|an|||||||
|4493|Delivery requirements, coded|1|3|an|||||||
|5004|Monetary amount|1|18|n|||||||
|5007|Monetary function, coded|1|3|an|||||||
|5013|Index qualifier|1|3|an|||||||
|5025|Monetary amount type qualifier|1|3|an|||||||
|5027|Index type, coded|1|3|an|||||||
|5030|Index value|1|6|n|||||||
|5039|Index value representation, coded|1|3|an|||||||
|5047|Contribution qualifier|1|3|an|||||||
|5048|Contribution type|1|35|an|||||||
|5049|Contribution type, coded|1|3|an|||||||
|5118|Price|1|15|n|||||||
|5125|Price qualifier|1|3|an|||||||
|5135|ULD rate class type, coded|1|3|an|||||||
|5152|Duty/tax/fee type|1|35|an|||||||
|5153|Duty/tax/fee type, coded|1|3|an|||||||
|5189|Charge/allowance description, coded|1|3|an|||||||
|5213|Sub-line price change, coded|1|3|an|||||||
|5237|Charge category, coded|1|3|an|||||||
|5242|Rate/tariff class|1|35|an|||||||
|5243|Rate/tariff class identification|1|9|an|||||||
|5245|Percentage qualifier|1|3|an|||||||
|5249|Percentage basis, coded|1|3|an|||||||
|5273|Duty/tax/fee rate basis identification|1|12|an|||||||
|5275|Supplementary rate/tariff basis identification|1|6|an|||||||
|5278|Duty/tax/fee rate|1|17|an|||||||
|5279|Duty/tax/fee rate identification|1|7|an|||||||
|5283|Duty/tax/fee function qualifier|1|3|an|||||||
|5284|Unit price basis|1|9|n|||||||
|5286|Duty/tax/fee assessment basis|1|15|an|||||||
|5289|Duty/tax/fee account identification|1|6|an|||||||
|5305|Duty/tax/fee category, coded|1|3|an|||||||
|5375|Price type, coded|1|3|an|||||||
|5377|Price change indicator, coded|1|3|an|||||||
|5379|Price/tariff type, coded|1|3|an|||||||
|5387|Price type qualifier|1|3|an|||||||
|5388|Pricing group|1|35|an|||||||
|5389|Pricing group, coded|1|3|an|||||||
|5393|Price multiplier qualifier|1|3|an|||||||
|5394|Price multiplier|1|12|n|||||||
|5402|Rate of exchange|1|12|n|||||||
|5419|Rate type qualifier|1|3|an|||||||
|5420|Rate per unit|1|15|n|||||||
|5463|Allowance or charge qualifier|1|3|an|||||||
|5479|Relation, coded|1|3|an|||||||
|5482|Percentage|1|8|n|||||||
|5495|Sub-line indicator, coded|1|3|an|||||||
|6008|Height dimension|1|15|n|||||||
|6060|Quantity|1|15|n|||||||
|6063|Quantity qualifier|1|3|an|||||||
|6064|Quantity difference|1|15|n|||||||
|6066|Control value|1|18|n|||||||
|6069|Control qualifier|1|3|an|||||||
|6071|Frequency qualifier|1|3|an|||||||
|6072|Frequency value|1|9|n|||||||
|6074|Confidence limit|1|6|n|||||||
|6140|Width dimension|1|15|n|||||||
|6145|Dimension qualifier|1|3|an|||||||
|6152|Range maximum|1|18|n|||||||
|6155|Measurement attribute, coded|1|3|an|||||||
|6162|Range minimum|1|18|n|||||||
|6165|Weight qualifier|1|3|an|||||||
|6167|Range type qualifier|1|3|an|||||||
|6168|Length dimension|1|15|n|||||||
|6173|Size qualifier|1|3|an|||||||
|6174|Size|1|15|n|||||||
|6245|Temperature qualifier|1|3|an|||||||
|6246|Temperature setting|3|3|n|||||||
|6311|Measurement application qualifier|1|3|an|||||||
|6313|Measurement dimension, coded|1|3|an|||||||
|6314|Measurement value|1|18|n|||||||
|6321|Measurement significance, coded|1|3|an|||||||
|6331|Statistic type, coded|1|3|an|||||||
|6341|Currency market exchange, coded|1|3|an|||||||
|6343|Currency qualifier|1|3|an|||||||
|6345|Currency, coded|1|3|an|||||||
|6347|Currency details qualifier|1|3|an|||||||
|6348|Currency rate base|1|4|n|||||||
|6350|Number of units|1|15|n|||||||
|6353|Number of units qualifier|1|3|an|||||||
|6411|Measure unit qualifier|1|3|id|||||||
|6424|Sample frequency value|1|9|n|||||||
|7008|Item description|1|35|an|||||||
|7009|Item description identification|1|7|an|||||||
|7011|Article availability, coded|1|3|an|||||||
|7023|Article number identifier, coded|1|3|an|||||||
|7036|Characteristic|1|35|an|||||||
|7037|Characteristic identification|1|17|an|||||||
|7039|Sample selection method, coded|1|3|an|||||||
|7045|Sample description, coded|1|3|an|||||||
|7047|Sample direction, coded|1|3|an|||||||
|7059|Property class, coded|1|3|an|||||||
|7064|Type of packages|1|35|an|||||||
|7065|Type of packages identification|1|7|id|||||||
|7073|Packaging terms and conditions, coded|1|3|an|||||||
|7075|Packaging level, coded|1|3|an|||||||
|7077|Item description type, coded|1|3|an|||||||
|7081|Item characteristic, coded|1|3|an|||||||
|7083|Configuration, coded|1|3|an|||||||
|7085|Nature of cargo, coded|1|3|an|||||||
|7088|Dangerous goods flashpoint|1|8|an|||||||
|7102|Shipping marks|1|35|an|||||||
|7106|Shipment flashpoint|3|3|n|||||||
|7124|UNDG number|4|4|n|||||||
|7130|Customer authorization number|1|17|an|||||||
|7139|Item qualifier|1|3|an|||||||
|7140|Item number|1|35|an|||||||
|7143|Item number type, coded|1|3|an|||||||
|7161|Special services, coded|1|3|an|||||||
|7164|Hierarchical ID number|1|12|an|||||||
|7166|Hierarchical parent ID|1|12|an|||||||
|7186|Process type|1|35|an|||||||
|7187|Process type identification|1|17|an|||||||
|7188|Test revision number|1|30|an|||||||
|7224|Number of packages|1|8|n|||||||
|7233|Packaging related information, coded|1|3|an|||||||
|7240|Total number of items|1|15|n|||||||
|7273|Service requirement, coded|1|3|an|||||||
|7293|Sector/subject identification qualifier|1|3|an|||||||
|7295|Requirement/condition identification|1|17|an|||||||
|7297|Set identification qualifier|1|3|an|||||||
|7357|Commodity/rate identification|1|18|an|||||||
|7361|Customs code identification|1|18|an|||||||
|7365|Processing indicator, coded|1|3|an|||||||
|7383|Surface/layer indicator, coded|1|3|an|||||||
|7402|Identity number|1|35|an|||||||
|7405|Identity number qualifier|1|3|an|||||||
|7419|Hazardous material class code, identification|1|4|an|||||||
|7429|Indexing structure qualifier|1|3|an|||||||
|7431|Agreement type qualifier|1|3|an|||||||
|7433|Agreement type, coded|1|3|an|||||||
|7434|Agreement type description|1|70|an|||||||
|7436|Level one ID|1|17|an|||||||
|7438|Level two ID|1|17|an|||||||
|7440|Level three ID|1|17|an|||||||
|7442|Level four ID|1|17|an|||||||
|7444|Level five ID|1|17|an|||||||
|7446|Level six ID|1|17|an|||||||
|7449|Membership qualifier|1|3|an|||||||
|7450|Membership category|1|35|an|||||||
|7451|Membership category identification|1|4|an|||||||
|7452|Membership status|1|35|an|||||||
|7453|Membership status, coded|1|3|an|||||||
|7455|Membership level qualifier|1|3|an|||||||
|7456|Membership level|1|35|an|||||||
|7457|Membership level identification|1|9|an|||||||
|8022|Freight and charges|1|26|an|||||||
|8023|Freight and charges identification|1|17|an|||||||
|8028|Conveyance reference number|1|17|an|||||||
|8051|Transport stage qualifier|1|3|an|||||||
|8053|Equipment qualifier|1|3|an|||||||
|8066|Mode of transport|1|17|an|||||||
|8067|Mode of transport, coded|1|3|an|||||||
|8077|Equipment supplier, coded|1|3|an|||||||
|8078|Hazard substance/item/page number|1|7|an|||||||
|8092|Hazard code version number|1|10|an|||||||
|8101|Transit direction, coded|1|3|an|||||||
|8126|Trem card number|1|10|an|||||||
|8154|Equipment size and type|1|35|an|||||||
|8155|Equipment size and type identification|1|4|an|||||||
|8158|Hazard identification number, upper part|1|4|an|||||||
|8169|Full/empty indicator, coded|1|3|an|||||||
|8178|Type of means of transport|1|17|an|||||||
|8179|Type of means of transport identification|1|8|an|||||||
|8186|Substance identification number, lower part|4|4|an|||||||
|8211|Permission for transport, coded|1|3|an|||||||
|8212|Id of the means of transport|1|17|an|||||||
|8213|Id of means of transport identification|1|9|an|||||||
|8246|Dangerous goods label marking|1|4|an|||||||
|8249|Equipment status, coded|1|3|an|||||||
|8255|Packing instruction, coded|1|3|an|||||||
|8260|Equipment identification number|1|17|an|||||||
|8273|Dangerous goods regulations, coded|1|3|an|||||||
|8275|Container/package status, coded|1|3|an|||||||
|8323|Transport movement, coded|1|3|an|||||||
|8325|Category of means of transport, coded|1|3|an|||||||
|8332|Equipment plan|1|26|an|||||||
|8334|Movement type|1|35|an|||||||
|8335|Movement type, coded|1|3|an|||||||
|8339|Packing group, coded|1|3|an|||||||
|8341|Haulage arrangements, coded|1|3|an|||||||
|8351|Hazard code identification|1|7|an|||||||
|8364|EMS number|1|6|an|||||||
|8393|Returnable package load contents, coded|1|3|an|||||||
|8395|Returnable package freight payment responsibility, coded|1|3|an|||||||
|8410|MFAG|1|4|an|||||||
|8453|Nationality of means of transport, coded|1|3|id|||||||
|8457|Excess transportation reason, coded|1|3|an|||||||
|8459|Excess transportation responsibility, coded|1|3|an|||||||
|9003|Employment qualifier|1|3|an|||||||
|9004|Employment category|1|35|an|||||||
|9005|Employment category, coded|1|3|an|||||||
|9006|Qualification classification|1|35|an|||||||
|9007|Qualification classification, coded|1|3|an|||||||
|9008|Occupation|1|35|an|||||||
|9009|Occupation, coded|1|3|an|||||||
|9011|Status event, coded|1|3|an|||||||
|9012|Status reason|1|35|an|||||||
|9013|Status reason, coded|1|3|an|||||||
|9015|Status type, coded|1|3|an|||||||
|9017|Attribute function qualifier|1|3|an|||||||
|9018|Attribute|1|35|an|||||||
|9019|Attribute, coded|1|3|an|||||||
|9021|Attribute type, coded|1|3|an|||||||
|9141|Relationship qualifier|1|3|an|||||||
|9142|Relationship|1|35|an|||||||
|9143|Relationship, coded|1|3|an|||||||
|9213|Type of duty regime, coded|1|3|an|||||||
|9280|Validation result|1|35|an|||||||
|9282|Validation key identification|1|35|an|||||||
|9302|Sealing party|1|17|an|||||||
|9303|Sealing party, coded|1|3|an|||||||
|9308|Seal number|1|10|an|||||||
|9321|Application error, coded|1|3|an|||||||
|9353|Government procedure, coded|1|3|an|||||||
|9411|Government involvement, coded|1|3|an|||||||
|9415|Government agency, coded|1|3|an|||||||
|9417|Government action, coded|1|3|an|||||||
|9419|Service layer, coded|1|3|an|||||||
