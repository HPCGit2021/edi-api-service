|AUTHOR|Authorization message||||
|BANSTA|Banking status message||||
|BAPLIE|Bayplan/stowage plan occupied and empty locations message||||
|BAPLTE|Bayplan/stowage plan total numbers message||||
|BOPBNK|Bank transactions and portfolio transactions report message||||
|BOPCUS|Balance of payment customer transaction report message||||
|BOPDIR|Direct balance of payment declaration message||||
|BOPINF|Balance of payment information from customer message||||
|COMDIS|Commercial dispute message||||
|CONAPW|Advice on pending works message||||
|CONDPV|Direct payment valuation message||||
|CONEST|Establishment of contract message||||
|CONITT|Invitation to tender message||||
|CONPVA|Payment valuation message||||
|CONQVA|Quantity valuation message||||
|CONRPW|Response of pending works message||||
|CONTEN|Tender message||||
|CONTRL|Syntax and service report message||||
|CONWQD|Work item quantity determination message||||
|CREADV|Credit advice message||||
|CREEXT|Extended credit advice message||||
|CUSCAR|Customs cargo report message||||
|CUSDEC|Customs declaration message||||
|CUSEXP|Customs express consignment declaration message||||
|CUSREP|Customs conveyance report message||||
|CUSRES|Customs response message||||
|DEBADV|Debit advice message||||
|DELFOR|Delivery schedule message||||
|DELJIT|Delivery just-in-time message||||
|DESADV|Despatch advice message||||
|DIRDEB|Direct debit message||||
|DOCADV|Documentary credit advice message||||
|DOCAPP|Documentary credit application message||||
|DOCINF|Documentary credit issuance information message||||
|HANMOV|Cargo/goods handling and movement message||||
|IFCSUM|International forwarding and consolidation summary message||||
|IFTCCA|Forwarding and transport shipment charge calculation message||||
|IFTDGN|Dangerous goods notification message||||
|IFTMAN|Arrival notice message||||
|IFTMBC|Booking confirmation message||||
|IFTMBF|Firm booking message||||
|IFTMBP|Provisional booking message||||
|IFTMCS|Instruction contract status message||||
|IFTMIN|Instruction message||||
|IFTRIN|Forwarding and transport rate information message||||
|IFTSAI|Forwarding and transport schedule and availability||||
|IFTSTA|International multimodal status report message||||
|IFTSTQ|International multimodal status request message||||
|INSPRE|Insurance premium message||||
|INVOIC|Invoice message||||
|INVRPT|Inventory report message||||
|MOVINS|Stowage instruction message||||
|ORDCHG|Purchase order change message||||
|ORDERS|Purchase order message||||
|ORDRSP|Purchase order response message||||
|PARTIN|Party information message||||
|PAXLST|Passenger list message||||
|PAYDUC|Payroll deductions advice message||||
|PAYEXT|Extended payment order message||||
|PAYMUL|Multiple payment order message||||
|PAYORD|Payment order message||||
|PRICAT|Price/sales catalogue message||||
|PRODEX|Product exchange reconciliation message||||
|PRPAID|Insurance premium payment message||||
|QALITY|Quality data message||||
|QUOTES|Quote message||||
|RECECO|Credit risk cover message||||
|REMADV|Remittance advice message||||
|REQDOC|Request for document message||||
|REQOTE|Request for quote message||||
|SANCRT|Sanitary/phytosanitary certificate message||||
|SLSFCT|Sales forecast message||||
|SLSRPT|Sales data report message||||
|STATAC|Statement of account message||||
|SUPCOT|Superannuation contributions advice message||||
|SUPMAN|Superannuation maintenance message||||
