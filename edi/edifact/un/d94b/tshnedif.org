|IFTMBC|00|000|UNH|Message header|M|1|0|0|0|||||
|IFTMBC|00|010|BGM|Beginning of message|M|1|0|0|0|||||
|IFTMBC|00|020|CTA|Contact information|C|1|0|0|0|||||
|IFTMBC|00|030|COM|Communication contact|C|9|0|0|0|||||
|IFTMBC|00|040|DTM|Date/time/period|C|9|0|0|0|||||
|IFTMBC|00|050|TSR|Transport service requirements|C|9|0|0|0|||||
|IFTMBC|00|060|FTX|Free text|C|99|0|0|0|||||
|IFTMBC|00|070|CNT|Control total|C|9|0|0|0|||||
|IFTMBC|01|000|LOC|Place/location identification|M|1|LOC|1|99|||||
|IFTMBC|01|010|DTM|Date/time/period|C|9|LOC|1|0|||||
|IFTMBC|02|000|RFF|Reference|M|1|RFF|1|99|||||
|IFTMBC|02|010|DTM|Date/time/period|C|9|RFF|1|0|||||
|IFTMBC|03|000|TCC|Transport charge/rate calculations|C|9|0|0|0|||||
|IFTMBC|04|000|TDT|Details of transport|M|1|TDT|1|99|||||
|IFTMBC|04|010|DTM|Date/time/period|C|9|TDT|1|0|||||
|IFTMBC|04|020|TSR|Transport service requirements|C|9|TDT|1|0|||||
|IFTMBC|05|000|LOC|Place/location identification|M|1|LOC|2|99|||||
|IFTMBC|05|010|DTM|Date/time/period|C|1|LOC|2|0|||||
|IFTMBC|06|000|RFF|Reference|M|1|RFF|2|9|||||
|IFTMBC|06|010|DTM|Date/time/period|C|1|RFF|2|0|||||
|IFTMBC|07|000|NAD|Name and address|M|1|NAD|1|99|||||
|IFTMBC|07|010|LOC|Place/location identification|C|9|NAD|1|0|||||
|IFTMBC|08|000|TSR|Transport service requirements|M|1|TSR|2|99|||||
|IFTMBC|08|010|RFF|Reference|C|1|TSR|2|0|||||
|IFTMBC|08|020|LOC|Place/location identification|C|1|TSR|2|0|||||
|IFTMBC|08|030|TPL|Transport placement|C|1|TSR|2|0|||||
|IFTMBC|08|040|FTX|Free text|C|9|TSR|2|0|||||
|IFTMBC|09|000|GID|Goods item details|M|1|GID|1|999|||||
|IFTMBC|09|010|HAN|Handling instructions|C|1|GID|1|0|||||
|IFTMBC|09|020|TMP|Temperature|C|1|GID|1|0|||||
|IFTMBC|09|030|RNG|Range details|C|1|GID|1|0|||||
|IFTMBC|09|040|TMD|Transport movement details|C|1|GID|1|0|||||
|IFTMBC|09|050|LOC|Place/location identification|C|9|GID|1|0|||||
|IFTMBC|09|060|FTX|Free text|C|9|GID|1|0|||||
|IFTMBC|10|000|NAD|Name and address|M|1|NAD|2|9|||||
|IFTMBC|10|010|DTM|Date/time/period|C|1|NAD|2|0|||||
|IFTMBC|10|020|GDS|Nature of cargo|C|9|NAD|1|0|||||
|IFTMBC|11|000|MEA|Measurements|M|1|MEA|2|99|||||
|IFTMBC|11|010|EQN|Number of units|C|1|MEA|2|0|||||
|IFTMBC|12|000|DIM|Dimensions|M|1|DIM|2|99|||||
|IFTMBC|12|010|EQN|Number of units|C|1|DIM|2|0|||||
|IFTMBC|13|000|RFF|Reference|M|1|RFF|2|9|||||
|IFTMBC|13|010|DTM|Date/time/period|C|9|RFF|2|0|||||
|IFTMBC|14|000|DOC|Document/message details|M|1|DOC|2|9|||||
|IFTMBC|14|010|DTM|Date/time/period|C|9|DOC|2|0|||||
|IFTMBC|15|000|DGS|Dangerous goods|M|1|DGS|2|9|||||
|IFTMBC|15|010|FTX|Free text|C|99|DGS|2|0|||||
|IFTMBC|16|000|MEA|Measurements|M|1|MEA|3|9|||||
|IFTMBC|16|010|EQN|Number of units|C|1|MEA|3|0|||||
|IFTMBC|17|000|EQD|Equipment details|M|1|EQD|1|999|||||
|IFTMBC|17|010|EQN|Number of units|C|1|EQD|1|0|||||
|IFTMBC|17|020|TMD|Transport movement details|C|1|EQD|1|0|||||
|IFTMBC|17|030|MEA|Measurements|C|9|EQD|1|0|||||
|IFTMBC|17|040|DIM|Dimensions|C|9|EQD|1|0|||||
|IFTMBC|17|050|HAN|Handling instructions|C|1|EQD|1|0|||||
|IFTMBC|17|060|TMP|Temperature|C|1|EQD|1|0|||||
|IFTMBC|17|070|FTX|Free text|C|9|EQD|1|0|||||
|IFTMBC|18|000|NAD|Name and address|M|1|NAD|2|9|||||
|IFTMBC|18|010|DTM|Date/time/period|C|1|NAD|2|0|||||
|IFTMBC|19|000|UNT|Message trailer|M|1|0|0|0|||||
|IFTMBF|00|000|UNH|Message header|M|1|0|0|0|||||
|IFTMBF|00|010|BGM|Beginning of message|M|1|0|0|0|||||
|IFTMBF|00|020|CTA|Contact information|C|1|0|0|0|||||
|IFTMBF|00|030|COM|Communication contact|C|9|0|0|0|||||
|IFTMBF|00|040|DTM|Date/time/period|C|9|0|0|0|||||
|IFTMBF|00|050|TSR|Transport service requirements|C|9|0|0|0|||||
|IFTMBF|00|060|FTX|Free text|C|99|0|0|0|||||
|IFTMBF|00|070|CNT|Control total|C|9|0|0|0|||||
|IFTMBF|01|000|LOC|Place/location identification|M|1|LOC|1|99|||||
|IFTMBF|01|010|DTM|Date/time/period|C|9|LOC|1|0|||||
|IFTMBF|02|000|TOD|Terms of delivery or transport|M|1|TOD|1|2|||||
|IFTMBF|02|010|LOC|Place/location identification|C|9|TOD|1|0|||||
|IFTMBF|03|000|RFF|Reference|M|1|RFF|1|99|||||
|IFTMBF|03|010|DTM|Date/time/period|C|9|RFF|1|0|||||
|IFTMBF|04|000|GOR|Governmental requirements|M|1|GOR|1|9|||||
|IFTMBF|04|010|FTX|Free text|C|9|GOR|1|0|||||
|IFTMBF|05|000|DOC|Document/message details|M|1|DOC|2|9|||||
|IFTMBF|05|010|DTM|Date/time/period|C|1|DOC|2|0|||||
|IFTMBF|06|000|TCC|Transport charge/rate calculations|M|1|TCC|1|99|||||
|IFTMBF|06|010|LOC|Place/location identification|C|1|TCC|1|0|||||
|IFTMBF|06|020|FTX|Free text|C|1|TCC|1|0|||||
|IFTMBF|06|030|CUX|Currencies|C|1|TCC|1|0|||||
|IFTMBF|06|040|PRI|Price details|C|1|TCC|1|0|||||
|IFTMBF|06|050|EQN|Number of units|C|1|TCC|1|0|||||
|IFTMBF|06|060|PCD|Percentage details|C|1|TCC|1|0|||||
|IFTMBF|06|070|MOA|Monetary amount|C|9|TCC|1|0|||||
|IFTMBF|06|080|QTY|Quantity|C|9|TCC|1|0|||||
|IFTMBF|07|000|TDT|Details of transport|m|1|TDT|1|99|||||
|IFTMBF|07|010|DTM|Date/time/period|C|9|TDT|1|0|||||
|IFTMBF|07|020|TSR|Transport service requirements|C|9|TDT|1|0|||||
|IFTMBF|08|000|LOC|Place/location identification|M|1|LOC|2|99|||||
|IFTMBF|08|010|DTM|Date/time/period|C|1|LOC|2|0|||||
|IFTMBF|09|000|RFF|Reference|M|1|RFF|2|9|||||
|IFTMBF|09|010|DTM|Date/time/period|C|1|RFF|2|0|||||
|IFTMBF|10|000|NAD|Name and address|M|1|NAD|1|99|||||
|IFTMBF|10|010|LOC|Place/location identification|C|9|NAD|1|0|||||
|IFTMBF|11|000|CTA|Contact information|M|1|CTA|2|9|||||
|IFTMBF|11|010|COM|Communication contact|C|9|CTA|2|0|||||
|IFTMBF|12|000|DOC|Document/message details|M|1|DOC|2|9|||||
|IFTMBF|12|010|DTM|Date/time/period|C|1|DOC|2|0|||||
|IFTMBF|13|000|RFF|Reference|M|1|RFF|2|9|||||
|IFTMBF|13|010|DTM|Date/time/period|C|9|RFF|2|0|||||
|IFTMBF|14|000|CPI|Charge payment instructions|M|1|CPI|2|9|||||
|IFTMBF|14|010|CUX|Currencies|C|1|CPI|2|0|||||
|IFTMBF|14|020|LOC|Place/location identification|C|9|CPI|2|0|||||
|IFTMBF|14|030|MOA|Monetary amount|C|9|CPI|2|0|||||
|IFTMBF|15|000|TSR|Transport service requirements|M|1|TSR|2|99|||||
|IFTMBF|15|010|RFF|Reference|C|1|TSR|2|0|||||
|IFTMBF|15|020|LOC|Place/location identification|C|1|TSR|2|0|||||
|IFTMBF|15|030|TPL|Transport placement|C|1|TSR|2|0|||||
|IFTMBF|15|040|FTX|Free text|C|9|TSR|2|0|||||
|IFTMBF|16|000|GID|Goods item details|M|1|GID|1|999|||||
|IFTMBF|16|010|HAN|Handling instructions|C|1|GID|1|0|||||
|IFTMBF|16|020|TMP|Temperature|C|1|GID|1|0|||||
|IFTMBF|16|030|RNG|Range details|C|1|GID|1|0|||||
|IFTMBF|16|040|TMD|Transport movement details|C|1|GID|1|0|||||
|IFTMBF|16|050|LOC|Place/location identification|C|9|GID|1|0|||||
|IFTMBF|16|060|MOA|Monetary amount|C|9|GID|1|0|||||
|IFTMBF|16|070|PIA|Additional product id|C|9|GID|1|0|||||
|IFTMBF|16|080|FTX|Free text|C|9|GID|1|0|||||
|IFTMBF|17|000|NAD|Name and address|M|1|NAD|2|9|||||
|IFTMBF|17|010|DTM|Date/time/period|C|1|NAD|2|0|||||
|IFTMBF|17|020|GDS|Nature of cargo|C|9|NAD|1|0|||||
|IFTMBF|18|000|MEA|Measurements|M|1|MEA|2|99|||||
|IFTMBF|18|010|EQN|Number of units|C|1|MEA|2|0|||||
|IFTMBF|19|000|DIM|Dimensions|M|1|DIM|2|99|||||
|IFTMBF|19|010|EQN|Number of units|C|1|DIM|2|0|||||
|IFTMBF|20|000|RFF|Reference|M|1|RFF|2|9|||||
|IFTMBF|20|010|DTM|Date/time/period|C|9|RFF|2|0|||||
|IFTMBF|21|000|PCI|Package identification|M|1|PCI|2|9|||||
|IFTMBF|21|010|RFF|Reference|C|1|PCI|2|0|||||
|IFTMBF|21|020|DTM|Date/time/period|C|1|PCI|2|0|||||
|IFTMBF|22|000|DOC|Document/message details|M|1|DOC|2|9|||||
|IFTMBF|22|010|DTM|Date/time/period|C|9|DOC|2|0|||||
|IFTMBF|23|000|TPL|Transport placement|M|1|TPL|2|9|||||
|IFTMBF|24|000|MEA|Measurements|M|1|MEA|3|9|||||
|IFTMBF|24|010|EQN|Number of units|C|1|MEA|3|0|||||
|IFTMBF|25|000|SGP|Split goods placement|M|1|SGP|2|999|||||
|IFTMBF|26|000|MEA|Measurements|M|1|MEA|3|9|||||
|IFTMBF|26|010|EQN|Number of units|C|1|MEA|3|0|||||
|IFTMBF|27|000|DGS|Dangerous goods|M|1|DGS|2|9|||||
|IFTMBF|27|010|FTX|Free text|C|99|DGS|2|0|||||
|IFTMBF|28|000|CTA|Contact information|M|1|CTA|3|9|||||
|IFTMBF|28|010|COM|Communication contact|C|9|CTA|3|0|||||
|IFTMBF|29|000|MEA|Measurements|M|1|MEA|3|9|||||
|IFTMBF|29|010|EQN|Number of units|C|1|MEA|3|0|||||
|IFTMBF|30|000|SGP|Split goods placement|M|1|SGP|3|999|||||
|IFTMBF|31|000|MEA|Measurements|M|1|MEA|4|9|||||
|IFTMBF|31|010|EQN|Number of units|C|1|MEA|4|0|||||
|IFTMBF|32|000|EQD|Equipment details|M|1|EQD|1|999|||||
|IFTMBF|32|010|EQN|Number of units|C|1|EQD|1|0|||||
|IFTMBF|32|020|TMD|Transport movement details|C|1|EQD|1|0|||||
|IFTMBF|32|030|MEA|Measurements|C|9|EQD|1|0|||||
|IFTMBF|32|040|DIM|Dimensions|C|9|EQD|1|0|||||
|IFTMBF|32|050|TPL|Transport placement|C|9|EQD|1|0|||||
|IFTMBF|32|060|HAN|Handling instructions|C|1|EQD|1|0|||||
|IFTMBF|32|070|TMP|Temperature|C|1|EQD|1|0|||||
|IFTMBF|32|080|FTX|Free text|C|9|EQD|1|0|||||
|IFTMBF|33|000|NAD|Name and address|M|1|NAD|2|9|||||
|IFTMBF|33|010|DTM|Date/time/period|C|1|NAD|2|0|||||
|IFTMBF|34|000|CTA|Contact information|M|1|CTA|3|9|||||
|IFTMBF|34|010|COM|Communication contact|C|9|CTA|3|0|||||
|IFTMBF|35|000|UNT|Message trailer|M|1|0|0|0|||||
|IFTMCS|00|000|UNH|Message header|M|1|0|0|0|||||
|IFTMCS|00|010|BGM|Beginning of message|M|1|0|0|0|||||
|IFTMCS|00|020|CTA|Contact information|C|1|0|0|0|||||
|IFTMCS|00|030|COM|Communication contact|C|9|0|0|0|||||
|IFTMCS|00|040|DTM|Date/time/period|C|9|0|0|0|||||
|IFTMCS|00|050|TSR|Transport service requirements|C|9|0|0|0|||||
|IFTMCS|00|060|CUX|Currencies|C|9|0|0|0|||||
|IFTMCS|00|070|MOA|Monetary amount|C|99|0|0|0|||||
|IFTMCS|00|080|FTX|Free text|C|99|0|0|0|||||
|IFTMCS|00|090|CNT|Control total|C|9|0|0|0|||||
|IFTMCS|01|000|LOC|Place/location identification|M|1|LOC|1|99|||||
|IFTMCS|01|010|DTM|Date/time/period|C|9|LOC|1|0|||||
|IFTMCS|02|000|TOD|Terms of delivery or transport|M|1|TOD|1|2|||||
|IFTMCS|02|010|LOC|Place/location identification|C|9|TOD|1|0|||||
|IFTMCS|03|000|RFF|Reference|M|1|RFF|1|99|||||
|IFTMCS|03|010|DTM|Date/time/period|C|9|RFF|1|0|||||
|IFTMCS|04|000|GOR|Governmental requirements|M|1|GOR|1|9|||||
|IFTMCS|04|010|DTM|Date/time/period|C|9|GOR|1|0|||||
|IFTMCS|04|020|LOC|Place/location identification|C|9|GOR|1|0|||||
|IFTMCS|04|030|SEL|Seal number|C|9|GOR|1|0|||||
|IFTMCS|04|040|FTX|Free text|C|9|GOR|1|0|||||
|IFTMCS|05|000|DOC|Document/message details|M|1|DOC|2|9|||||
|IFTMCS|05|010|DTM|Date/time/period|C|1|DOC|2|0|||||
|IFTMCS|06|000|CPI|Charge payment instructions|M|1|CPI|1|9|||||
|IFTMCS|06|010|CUX|Currencies|C|1|CPI|1|0|||||
|IFTMCS|06|020|LOC|Place/location identification|C|9|CPI|1|0|||||
|IFTMCS|06|030|MOA|Monetary amount|C|9|CPI|1|0|||||
|IFTMCS|07|000|TCC|Transport charge/rate calculations|M|1|TCC|1|99|||||
|IFTMCS|07|010|LOC|Place/location identification|C|1|TCC|1|0|||||
|IFTMCS|07|020|FTX|Free text|C|1|TCC|1|0|||||
|IFTMCS|07|030|CUX|Currencies|C|1|TCC|1|0|||||
|IFTMCS|07|040|PRI|Price details|C|1|TCC|1|0|||||
|IFTMCS|07|050|EQN|Number of units|C|1|TCC|1|0|||||
|IFTMCS|07|060|PCD|Percentage details|C|1|TCC|1|0|||||
|IFTMCS|07|070|MOA|Monetary amount|C|9|TCC|1|0|||||
|IFTMCS|07|080|QTY|Quantity|C|9|TCC|1|0|||||
|IFTMCS|08|000|TDT|Details of transport|M|1|TDT|1|99|||||
|IFTMCS|08|010|DTM|Date/time/period|C|9|TDT|1|0|||||
|IFTMCS|08|020|TSR|Transport service requirements|C|9|TDT|1|0|||||
|IFTMCS|09|000|LOC|Place/location identification|M|1|LOC|2|99|||||
|IFTMCS|09|010|DTM|Date/time/period|C|1|LOC|2|0|||||
|IFTMCS|10|000|RFF|Reference|M|1|RFF|2|9|||||
|IFTMCS|10|010|DTM|Date/time/period|C|1|RFF|2|0|||||
|IFTMCS|11|000|NAD|Name and address|M|1|NAD|1|99|||||
|IFTMCS|11|010|LOC|Place/location identification|C|9|NAD|1|0|||||
|IFTMCS|11|020|MOA|Monetary amount|C|9|NAD|1|0|||||
|IFTMCS|12|000|CTA|Contact information|M|1|CTA|2|9|||||
|IFTMCS|12|010|COM|Communication contact|C|9|CTA|2|0|||||
|IFTMCS|13|000|DOC|Document/message details|M|1|DOC|2|9|||||
|IFTMCS|13|010|DTM|Date/time/period|C|1|DOC|2|0|||||
|IFTMCS|14|000|TCC|Transport charge/rate calculations|M|1|TCC|2|99|||||
|IFTMCS|14|010|PRI|Price details|C|1|TCC|2|0|||||
|IFTMCS|14|020|EQN|Number of units|C|1|TCC|2|0|||||
|IFTMCS|14|030|PCD|Percentage details|C|1|TCC|2|0|||||
|IFTMCS|14|040|MOA|Monetary amount|C|9|TCC|2|0|||||
|IFTMCS|14|050|QTY|Quantity|C|9|TCC|2|0|||||
|IFTMCS|15|000|RFF|Reference|M|1|RFF|2|9|||||
|IFTMCS|15|010|DTM|Date/time/period|C|9|RFF|2|0|||||
|IFTMCS|16|000|CPI|Charge payment instructions|M|1|CPI|2|9|||||
|IFTMCS|16|010|CUX|Currencies|C|1|CPI|2|0|||||
|IFTMCS|16|020|LOC|Place/location identification|C|9|CPI|2|0|||||
|IFTMCS|16|030|MOA|Monetary amount|C|9|CPI|2|0|||||
|IFTMCS|17|000|TSR|Transport service requirements|M|1|TSR|2|99|||||
|IFTMCS|17|010|RFF|Reference|C|1|TSR|2|0|||||
|IFTMCS|17|020|LOC|Place/location identification|C|1|TSR|2|0|||||
|IFTMCS|17|030|TPL|Transport placement|C|1|TSR|2|0|||||
|IFTMCS|17|040|FTX|Free text|C|9|TSR|2|0|||||
|IFTMCS|18|000|GID|Goods item details|M|1|GID|1|999|||||
|IFTMCS|18|010|HAN|Handling instructions|C|1|GID|1|0|||||
|IFTMCS|18|020|TMP|Temperature|C|1|GID|1|0|||||
|IFTMCS|18|030|RNG|Range details|C|1|GID|1|0|||||
|IFTMCS|18|040|TMD|Transport movement details|C|1|GID|1|0|||||
|IFTMCS|18|050|LOC|Place/location identification|C|9|GID|1|0|||||
|IFTMCS|18|060|MOA|Monetary amount|C|9|GID|1|0|||||
|IFTMCS|18|070|PIA|Additional product id|C|9|GID|1|0|||||
|IFTMCS|18|080|FTX|Free text|C|9|GID|1|0|||||
|IFTMCS|19|000|NAD|Name and address|M|1|NAD|2|9|||||
|IFTMCS|19|010|DTM|Date/time/period|C|1|NAD|2|0|||||
|IFTMCS|19|020|GDS|Nature of cargo|M|9|NAD|1|0|||||
|IFTMCS|20|000|MEA|Measurements|M|1|MEA|2|99|||||
|IFTMCS|20|010|EQN|Number of units|C|1|MEA|2|0|||||
|IFTMCS|21|000|DIM|Dimensions|M|1|DIM|2|99|||||
|IFTMCS|21|010|EQN|Number of units|C|1|DIM|2|0|||||
|IFTMCS|22|000|RFF|Reference|M|1|RFF|2|9|||||
|IFTMCS|22|010|DTM|Date/time/period|C|9|RFF|2|0|||||
|IFTMCS|23|000|PCI|Package identification|M|1|PCI|2|9|||||
|IFTMCS|23|010|RFF|Reference|C|1|PCI|2|0|||||
|IFTMCS|23|020|DTM|Date/time/period|C|1|PCI|2|0|||||
|IFTMCS|24|000|DOC|Document/message details|M|1|DOC|2|9|||||
|IFTMCS|24|010|DTM|Date/time/period|C|9|DOC|2|0|||||
|IFTMCS|25|000|TPL|Transport placement|M|1|TPL|2|9|||||
|IFTMCS|26|000|MEA|Measurements|M|1|MEA|3|9|||||
|IFTMCS|26|010|EQN|Number of units|C|1|MEA|3|0|||||
|IFTMCS|27|000|SGP|Split goods placement|M|1|SGP|2|999|||||
|IFTMCS|28|000|MEA|Measurements|M|1|MEA|3|9|||||
|IFTMCS|28|010|EQN|Number of units|C|1|MEA|3|0|||||
|IFTMCS|29|000|TCC|Transport charge/rate calculations|M|1|TCC|2|99|||||
|IFTMCS|29|010|PRI|Price details|C|1|TCC|2|0|||||
|IFTMCS|29|020|EQN|Number of units|C|1|TCC|2|0|||||
|IFTMCS|29|030|PCD|Percentage details|C|1|TCC|2|0|||||
|IFTMCS|29|040|MOA|Monetary amount|C|9|TCC|2|0|||||
|IFTMCS|29|050|QTY|Quantity|C|9|TCC|2|0|||||
|IFTMCS|29|060|LOC|Place/location identification|C|9|TCC|2|0|||||
|IFTMCS|30|000|DGS|Dangerous goods|M|1|DGS|2|9|||||
|IFTMCS|30|010|FTX|Free text|C|99|DGS|2|0|||||
|IFTMCS|31|000|CTA|Contact information|M|1|CTA|3|9|||||
|IFTMCS|31|010|COM|Communication contact|C|9|CTA|3|0|||||
|IFTMCS|32|000|MEA|Measurements|M|1|MEA|3|9|||||
|IFTMCS|32|010|EQN|Number of units|C|1|MEA|3|0|||||
|IFTMCS|33|000|SGP|Split goods placement|M|1|SGP|3|999|||||
|IFTMCS|34|000|MEA|Measurements|M|1|MEA|4|9|||||
|IFTMCS|34|010|EQN|Number of units|C|1|MEA|4|0|||||
|IFTMCS|35|000|EQD|Equipment details|M|1|EQD|1|999|||||
|IFTMCS|35|010|EQN|Number of units|C|1|EQD|1|0|||||
|IFTMCS|35|020|TMD|Transport movement details|C|1|EQD|1|0|||||
|IFTMCS|35|030|MEA|Measurements|C|9|EQD|1|0|||||
|IFTMCS|35|040|DIM|Dimensions|C|9|EQD|1|0|||||
|IFTMCS|35|050|SEL|Seal number|C|99|EQD|1|0|||||
|IFTMCS|35|060|TPL|Transport placement|C|9|EQD|1|0|||||
|IFTMCS|35|070|HAN|Handling instructions|C|1|EQD|1|0|||||
|IFTMCS|35|080|TMP|Temperature|C|1|EQD|1|0|||||
|IFTMCS|35|090|FTX|Free text|C|9|EQD|1|0|||||
|IFTMCS|36|000|TCC|Transport charge/rate calculations|M|1|TCC|2|99|||||
|IFTMCS|36|010|PRI|Price details|C|1|TCC|2|0|||||
|IFTMCS|36|020|EQN|Number of units|C|1|TCC|2|0|||||
|IFTMCS|36|030|PCD|Percentage details|C|1|TCC|2|0|||||
|IFTMCS|36|040|MOA|Monetary amount|C|9|TCC|2|0|||||
|IFTMCS|36|050|QTY|Quantity|C|9|TCC|2|0|||||
|IFTMCS|37|000|NAD|Name and address|M|1|NAD|2|9|||||
|IFTMCS|37|010|DTM|Date/time/period|C|1|NAD|2|0|||||
|IFTMCS|38|000|EQA|Attached equipment|M|1|EQA|2|99|||||
|IFTMCS|38|010|EQN|Number of units|C|1|EQA|2|0|||||
|IFTMCS|39|000|UNT|Message trailer|M|1|0|0|0|||||
|IFTMIN|00|000|UNH|Message header|M|1|0|0|0|||||
|IFTMIN|00|010|BGM|Beginning of message|M|1|0|0|0|||||
|IFTMIN|00|020|CTA|Contact information|C|1|0|0|0|||||
|IFTMIN|00|030|COM|Communication contact|C|9|0|0|0|||||
|IFTMIN|00|040|DTM|Date/time/period|C|9|0|0|0|||||
|IFTMIN|00|050|TSR|Transport service requirements|C|9|0|0|0|||||
|IFTMIN|00|060|CUX|Currencies|C|9|0|0|0|||||
|IFTMIN|00|070|MOA|Monetary amount|C|99|0|0|0|||||
|IFTMIN|00|080|FTX|Free text|C|99|0|0|0|||||
|IFTMIN|00|090|CNT|Control total|C|9|0|0|0|||||
|IFTMIN|00|100|DOC|Document/message details|C|9|0|0|0|||||
|IFTMIN|01|000|LOC|Place/location identification|M|1|LOC|1|99|||||
|IFTMIN|01|010|DTM|Date/time/period|C|9|LOC|1|0|||||
|IFTMIN|02|000|TOD|Terms of delivery or transport|M|1|TOD|1|2|||||
|IFTMIN|02|010|LOC|Place/location identification|C|9|TOD|1|0|||||
|IFTMIN|03|000|RFF|Reference|M|1|RFF|1|99|||||
|IFTMIN|03|010|DTM|Date/time/period|C|9|RFF|1|0|||||
|IFTMIN|04|000|GOR|Governmental requirements|M|1|GOR|1|9|||||
|IFTMIN|04|010|DTM|Date/time/period|C|9|GOR|1|0|||||
|IFTMIN|04|020|LOC|Place/location identification|C|9|GOR|1|0|||||
|IFTMIN|04|030|SEL|Seal number|C|9|GOR|1|0|||||
|IFTMIN|04|040|FTX|Free text|C|9|GOR|1|0|||||
|IFTMIN|05|000|DOC|Document/message details|M|1|DOC|2|9|||||
|IFTMIN|05|010|DTM|Date/time/period|C|1|DOC|2|0|||||
|IFTMIN|06|000|CPI|Charge payment instructions|M|1|CPI|1|9|||||
|IFTMIN|06|010|CUX|Currencies|C|1|CPI|1|0|||||
|IFTMIN|06|020|LOC|Place/location identification|C|9|CPI|1|0|||||
|IFTMIN|06|030|MOA|Monetary amount|C|9|CPI|1|0|||||
|IFTMIN|07|000|TCC|Transport charge/rate calculations|M|1|TCC|1|99|||||
|IFTMIN|07|010|LOC|Place/location identification|C|1|TCC|1|0|||||
|IFTMIN|07|020|FTX|Free text|C|1|TCC|1|0|||||
|IFTMIN|07|030|CUX|Currencies|C|1|TCC|1|0|||||
|IFTMIN|07|040|PRI|Price details|C|1|TCC|1|0|||||
|IFTMIN|07|050|EQN|Number of units|C|1|TCC|1|0|||||
|IFTMIN|07|060|PCD|Percentage details|C|1|TCC|1|0|||||
|IFTMIN|07|070|MOA|Monetary amount|C|9|TCC|1|0|||||
|IFTMIN|07|080|QTY|Quantity|C|9|TCC|1|0|||||
|IFTMIN|08|000|TDT|Details of transport|M|1|TDT|1|99|||||
|IFTMIN|08|010|DTM|Date/time/period|C|9|TDT|1|0|||||
|IFTMIN|08|020|TSR|Transport service requirements|C|9|TDT|1|0|||||
|IFTMIN|09|000|LOC|Place/location identification|M|1|LOC|2|99|||||
|IFTMIN|09|010|DTM|Date/time/period|C|1|LOC|2|0|||||
|IFTMIN|10|000|RFF|Reference|M|1|RFF|2|9|||||
|IFTMIN|10|010|DTM|Date/time/period|C|1|RFF|2|0|||||
|IFTMIN|11|000|NAD|Name and address|m|1|NAD|1|99|||||
|IFTMIN|11|010|LOC|Place/location identification|C|9|NAD|1|0|||||
|IFTMIN|11|020|MOA|Monetary amount|C|9|NAD|1|0|||||
|IFTMIN|12|000|CTA|Contact information|M|1|CTA|2|9|||||
|IFTMIN|12|010|COM|Communication contact|C|9|CTA|2|0|||||
|IFTMIN|13|000|DOC|Document/message details|M|1|DOC|2|9|||||
|IFTMIN|13|010|DTM|Date/time/period|C|1|DOC|2|0|||||
|IFTMIN|14|000|TCC|Transport charge/rate calculations|M|1|TCC|2|99|||||
|IFTMIN|14|010|PRI|Price details|C|1|TCC|2|0|||||
|IFTMIN|14|020|EQN|Number of units|C|1|TCC|2|0|||||
|IFTMIN|14|030|PCD|Percentage details|C|1|TCC|2|0|||||
|IFTMIN|14|040|MOA|Monetary amount|C|9|TCC|2|0|||||
|IFTMIN|14|050|QTY|Quantity|C|9|TCC|2|0|||||
|IFTMIN|15|000|RFF|Reference|M|1|RFF|2|9|||||
|IFTMIN|15|010|DTM|Date/time/period|C|9|RFF|2|0|||||
|IFTMIN|16|000|CPI|Charge payment instructions|M|1|CPI|2|9|||||
|IFTMIN|16|010|CUX|Currencies|C|1|CPI|2|0|||||
|IFTMIN|16|020|LOC|Place/location identification|C|9|CPI|2|0|||||
|IFTMIN|16|030|MOA|Monetary amount|C|9|CPI|2|0|||||
|IFTMIN|17|000|TSR|Transport service requirements|M|1|TSR|2|99|||||
|IFTMIN|17|010|RFF|Reference|C|1|TSR|2|0|||||
|IFTMIN|17|020|LOC|Place/location identification|C|1|TSR|2|0|||||
|IFTMIN|17|030|TPL|Transport placement|C|1|TSR|2|0|||||
|IFTMIN|17|040|FTX|Free text|C|9|TSR|2|0|||||
|IFTMIN|18|000|GID|Goods item details|M|1|GID|1|999|||||
|IFTMIN|18|010|HAN|Handling instructions|C|1|GID|1|0|||||
|IFTMIN|18|020|TMP|Temperature|C|1|GID|1|0|||||
|IFTMIN|18|030|RNG|Range details|C|1|GID|1|0|||||
|IFTMIN|18|040|TMD|Transport movement details|C|1|GID|1|0|||||
|IFTMIN|18|050|LOC|Place/location identification|C|9|GID|1|0|||||
|IFTMIN|18|060|MOA|Monetary amount|C|9|GID|1|0|||||
|IFTMIN|18|070|PIA|Additional product id|C|9|GID|1|0|||||
|IFTMIN|18|080|FTX|Free text|C|9|GID|1|0|||||
|IFTMIN|19|000|NAD|Name and address|M|1|NAD|2|9|||||
|IFTMIN|19|010|DTM|Date/time/period|C|1|NAD|2|0|||||
|IFTMIN|19|020|GDS|Nature of cargo|C|9|NAD|1|0|||||
|IFTMIN|20|000|MEA|Measurements|M|1|MEA|2|99|||||
|IFTMIN|20|010|EQN|Number of units|C|1|MEA|2|0|||||
|IFTMIN|21|000|DIM|Dimensions|M|1|DIM|2|99|||||
|IFTMIN|21|010|EQN|Number of units|C|1|DIM|2|0|||||
|IFTMIN|22|000|RFF|Reference|M|1|RFF|2|9|||||
|IFTMIN|22|010|DTM|Date/time/period|C|9|RFF|2|0|||||
|IFTMIN|23|000|PCI|Package identification|M|1|PCI|2|9|||||
|IFTMIN|23|010|RFF|Reference|C|1|PCI|2|0|||||
|IFTMIN|23|020|DTM|Date/time/period|C|1|PCI|2|0|||||
|IFTMIN|24|000|DOC|Document/message details|M|1|DOC|2|9|||||
|IFTMIN|24|010|DTM|Date/time/period|C|9|DOC|2|0|||||
|IFTMIN|25|000|GOR|Governmental requirements|M|1|GOR|2|9|||||
|IFTMIN|25|010|DTM|Date/time/period|C|9|GOR|2|0|||||
|IFTMIN|25|020|LOC|Place/location identification|C|9|GOR|2|0|||||
|IFTMIN|25|030|SEL|Seal number|C|9|GOR|2|0|||||
|IFTMIN|25|040|FTX|Free text|C|9|GOR|2|0|||||
|IFTMIN|26|000|DOC|Document/message details|M|1|DOC|3|9|||||
|IFTMIN|26|010|DTM|Date/time/period|C|1|DOC|3|0|||||
|IFTMIN|27|000|TPL|Transport placement|M|1|TPL|2|9|||||
|IFTMIN|28|000|MEA|Measurements|M|1|MEA|3|9|||||
|IFTMIN|28|010|EQN|Number of units|C|1|MEA|3|0|||||
|IFTMIN|29|000|SGP|Split goods placement|M|1|SGP|2|999|||||
|IFTMIN|30|000|MEA|Measurements|M|1|MEA|3|9|||||
|IFTMIN|30|010|EQN|Number of units|C|1|MEA|3|0|||||
|IFTMIN|31|000|TCC|Transport charge/rate calculations|M|1|TCC|2|99|||||
|IFTMIN|31|010|PRI|Price details|C|1|TCC|2|0|||||
|IFTMIN|31|020|EQN|Number of units|C|1|TCC|2|0|||||
|IFTMIN|31|030|PCD|Percentage details|C|1|TCC|2|0|||||
|IFTMIN|31|040|MOA|Monetary amount|C|9|TCC|2|0|||||
|IFTMIN|31|050|QTY|Quantity|C|9|TCC|2|0|||||
|IFTMIN|31|060|LOC|Place/location identification|C|9|TCC|2|0|||||
|IFTMIN|32|000|DGS|Dangerous goods|M|1|DGS|2|9|||||
|IFTMIN|32|010|FTX|Free text|C|99|DGS|2|0|||||
|IFTMIN|33|000|CTA|Contact information|M|1|CTA|3|9|||||
|IFTMIN|33|010|COM|Communication contact|C|9|CTA|3|0|||||
|IFTMIN|34|000|MEA|Measurements|M|1|MEA|3|9|||||
|IFTMIN|34|010|EQN|Number of units|C|1|MEA|3|0|||||
|IFTMIN|35|000|SGP|Split goods placement|M|1|SGP|3|999|||||
|IFTMIN|36|000|MEA|Measurements|M|1|MEA|4|9|||||
|IFTMIN|36|010|EQN|Number of units|C|1|MEA|4|0|||||
|IFTMIN|37|000|EQD|Equipment details|M|1|EQD|1|999|||||
|IFTMIN|37|010|EQN|Number of units|C|1|EQD|1|0|||||
|IFTMIN|37|020|TMD|Transport movement details|C|1|EQD|1|0|||||
|IFTMIN|37|030|MEA|Measurements|C|9|EQD|1|0|||||
|IFTMIN|37|040|DIM|Dimensions|C|9|EQD|1|0|||||
|IFTMIN|37|050|SEL|Seal number|C|99|EQD|1|0|||||
|IFTMIN|37|060|TPL|Transport placement|C|9|EQD|1|0|||||
|IFTMIN|37|070|HAN|Handling instructions|C|1|EQD|1|0|||||
|IFTMIN|37|080|TMP|Temperature|C|1|EQD|1|0|||||
|IFTMIN|37|090|FTX|Free text|C|9|EQD|1|0|||||
|IFTMIN|38|000|TCC|Transport charge/rate calculations|M|1|TCC|2|99|||||
|IFTMIN|38|010|PRI|Price details|C|1|TCC|2|0|||||
|IFTMIN|38|020|EQN|Number of units|C|1|TCC|2|0|||||
|IFTMIN|38|030|PCD|Percentage details|C|1|TCC|2|0|||||
|IFTMIN|38|040|MOA|Monetary amount|C|9|TCC|2|0|||||
|IFTMIN|38|050|QTY|Quantity|C|9|TCC|2|0|||||
|IFTMIN|39|000|NAD|Name and address|M|1|NAD|2|9|||||
|IFTMIN|39|010|DTM|Date/time/period|C|1|NAD|2|0|||||
|IFTMIN|40|000|CTA|Contact information|M|1|CTA|3|9|||||
|IFTMIN|40|010|COM|Communication contact|C|9|CTA|3|0|||||
|IFTMIN|41|000|EQA|Attached equipment|M|1|EQA|2|99|||||
|IFTMIN|41|010|EQN|Number of units|C|1|EQA|2|0|||||
|IFTMIN|42|000|UNT|Message trailer|M|1|0|0|0|||||
|IFTSTA|00|000|UNH|Message header|M|1|0|0|0|||||
|IFTSTA|00|010|BGM|Beginning of message|M|1|0|0|0|||||
|IFTSTA|00|020|DTM|Date/time/period|C|9|0|0|0|||||
|IFTSTA|00|030|TSR|Transport service requirements|C|1|0|0|0|||||
|IFTSTA|01|000|NAD|Name and address|m|1|NAD|1|1|||||
|IFTSTA|02|000|CTA|Contact information|M|1|CTA|2|9|||||
|IFTSTA|02|010|COM|Communication contact|C|9|CTA|2|0|||||
|IFTSTA|03|000|RFF|Reference|C|9|0|0|0|||||
|IFTSTA|03|010|LOC|Place/location identification|C|9|0|0|0|||||
|IFTSTA|03|020|FTX|Free text|C|9|0|0|0|||||
|IFTSTA|03|030|CNT|Control total|C|9|0|0|0|||||
|IFTSTA|04|000|CNI|Consignment information|M|1|CNI|1|999|||||
|IFTSTA|04|010|LOC|Place/location identification|C|9|CNI|1|0|||||
|IFTSTA|04|020|CNT|Control total|C|9|CNI|1|0|||||
|IFTSTA|05|000|STS|Status|m|1|STS|2|99|||||
|IFTSTA|05|010|RFF|Reference|C|9|STS|2|0|||||
|IFTSTA|05|020|DTM|Date/time/period|C|1|STS|2|0|||||
|IFTSTA|05|030|FTX|Free text|C|9|STS|2|0|||||
|IFTSTA|05|040|NAD|Name and address|C|9|STS|2|0|||||
|IFTSTA|05|050|LOC|Place/location identification|C|1|STS|2|0|||||
|IFTSTA|05|060|PCI|Package identification|C|99|STS|2|0|||||
|IFTSTA|06|000|TDT|Details of transport|M|1|TDT|3|99|||||
|IFTSTA|06|010|RFF|Reference|C|9|TDT|3|0|||||
|IFTSTA|06|020|LOC|Place/location identification|C|9|TDT|3|0|||||
|IFTSTA|06|030|DTM|Date/time/period|C|9|TDT|3|0|||||
|IFTSTA|07|000|EQD|Equipment details|M|1|EQD|3|99|||||
|IFTSTA|07|010|MEA|Measurements|C|9|EQD|3|0|||||
|IFTSTA|07|020|DIM|Dimensions|C|9|EQD|3|0|||||
|IFTSTA|07|030|SEL|Seal number|C|9|EQD|3|0|||||
|IFTSTA|08|000|EQA|Attached equipment|M|1|EQA|4|99|||||
|IFTSTA|08|010|SEL|Seal number|C|9|EQA|4|0|||||
|IFTSTA|09|000|GID|Goods item details|M|1|GID|3|99|||||
|IFTSTA|09|010|HAN|Handling instructions|C|9|GID|3|0|||||
|IFTSTA|09|020|PCI|Package identification|C|99|GID|3|0|||||
|IFTSTA|10|000|MEA|Measurements|M|1|MEA|4|99|||||
|IFTSTA|10|010|EQN|Number of units|C|1|MEA|4|0|||||
|IFTSTA|11|000|DIM|Dimensions|M|1|DIM|4|99|||||
|IFTSTA|11|010|EQN|Number of units|C|1|DIM|4|0|||||
|IFTSTA|12|000|UNT|Message trailer|M|1|0|0|0|||||
