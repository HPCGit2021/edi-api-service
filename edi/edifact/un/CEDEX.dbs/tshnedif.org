|ACCEPT|00|000|UNH|Message Header|M|1|0|0|0|||||
|ACCEPT|00|010|DTM|Date/Time Reference|M|1|0|0|0|||||
|ACCEPT|00|020|RFF|Reference|M|1|0|0|0|||||
|ACCEPT|00|030|TAD|Transaction Details|C|1|0|0|0|||||
|ACCEPT|00|040|CUI|Current Usage Information|C|1|0|0|0|||||
|ACCEPT|00|050|NAD|Name and Address|M|2|0|0|0|||||
|ACCEPT|00|060|CTA|Contact|C|2|0|0|0|||||
|ACCEPT|01|000|EQF|Equipment|C|1|0|0|0|||||
|ACCEPT|01|010|ACA|Alternative Currency Amount|C|1|0|0|0|||||
|ACCEPT|01|020|CUI|Current Usage Information|C|1|0|0|0|||||
|ACCEPT|02|000|TXT|Text|C|2|0|0|0|||||
|ACCEPT|02|010|UNT|Message Trailer|M|1|0|0|0|||||
|BOOKIN|00|000|UNH|Message Header|M|1|0|0|0|||||
|BOOKIN|00|010|DTM|Date/Time Reference|M|1|0|0|0|||||
|BOOKIN|00|020|RFF|Reference|M|1|0|0|0|||||
|BOOKIN|00|030|TAD|Transaction Details|C|1|0|0|0|||||
|BOOKIN|00|040|CUI|Current Usage Information|C|1|0|0|0|||||
|BOOKIN|00|050|NAD|Name and Address|M|2|0|0|0|||||
|BOOKIN|00|060|CTA|Contact|C|2|0|0|0|||||
|BOOKIN|00|070|EQF|Equipment|M|1|0|0|0|||||
|BOOKIN|00|080|TXT|Text|C|2|0|0|0|||||
|BOOKIN|00|090|UNT|Message Trailer|M|1|0|0|0|||||
|GATEIN|00|000|UNH|Message Header|M|1|0|0|0|||||
|GATEIN|00|010|DTM|Date/Time Reference|M|1|0|0|0|||||
|GATEIN|00|020|RFF|Reference|M|2|0|0|0|||||
|GATEIN|00|030|NAD|Name and Address|M|6|0|0|0|||||
|GATEIN|00|040|CTA|Contact|M|2|0|0|0|||||
|GATEIN|01|000|EQF|Equipment|M|2|0|0|0|||||
|GATEIN|01|010|ERI|Equipment Related Information|C|1|0|0|0|||||
|GATEIN|01|020|ECI|Equipment Condition|C|1|0|0|0|||||
|GATEIN|02|000|CUI|Current Usage Information|C|2|0|0|0|||||
|GATEIN|02|010|TAD|Transaction Details|C|1|0|0|0|||||
|GATEIN|02|020|TXT|Text|C|2|0|0|0|||||
|GATEIN|02|030|UNT|Message Trailer|M|1|0|0|0|||||
|GATOUT|00|000|UNH|Message Header|M|1|0|0|0|||||
|GATOUT|00|010|DTM|Date/Time Reference|M|1|0|0|0|||||
|GATOUT|00|020|RFF|Reference|M|2|0|0|0|||||
|GATOUT|00|030|NAD|Name and Address|M|6|0|0|0|||||
|GATOUT|00|040|CTA|Contact|M|2|0|0|0|||||
|GATOUT|01|000|EQF|Equipment|M|2|0|0|0|||||
|GATOUT|01|010|ERI|Equipment Related Information|C|1|0|0|0|||||
|GATOUT|01|020|ECI|Equipment Condition|C|1|0|0|0|||||
|GATOUT|02|000|CUI|Current Usage Information|C|2|0|0|0|||||
|GATOUT|02|010|TAD|Transaction Details|C|1|0|0|0|||||
|GATOUT|02|020|TXT|Text|C|2|0|0|0|||||
|GATOUT|02|030|UNT|Message Trailer|M|1|0|0|0|||||
|RELEAS|00|000|UNH|Message Header|M|1|0|0|0|||||
|RELEAS|00|010|DTM|Date/Time Reference|M|1|0|0|0|||||
|RELEAS|00|020|RFF|Reference|M|1|0|0|0|||||
|RELEAS|00|030|TAD|Transaction Details|C|1|0|0|0|||||
|RELEAS|00|040|CUI|Current Usage Information|C|1|0|0|0|||||
|RELEAS|00|050|NAD|Name and Address|M|2|0|0|0|||||
|RELEAS|00|060|CTA|Contact|C|2|0|0|0|||||
|RELEAS|01|000|EQF|Equipment|C|1|0|0|0|||||
|RELEAS|01|010|ACA|Alternative Currency Amount|C|1|0|0|0|||||
|RELEAS|01|020|CUI|Current Usage Information|C|1|0|0|0|||||
|RELEAS|02|000|TXT|Text|C|2|0|0|0|||||
|RELEAS|02|010|UNT|Message Trailer|M|1|0|0|0|||||
|TERMIN|00|000|UNH|Message Header|M|1|0|0|0|||||
|TERMIN|00|010|DTM|Date/Time Reference|M|1|0|0|0|||||
|TERMIN|00|020|RFF|Reference|M|1|0|0|0|||||
|TERMIN|00|030|TAD|Transaction Details|C|1|0|0|0|||||
|TERMIN|00|040|CUI|Current Usage Information|C|1|0|0|0|||||
|TERMIN|00|050|NAD|Name and Address|M|2|0|0|0|||||
|TERMIN|00|060|CTA|Contact|C|2|0|0|0|||||
|TERMIN|01|000|EQF|Equipment|C|1|0|0|0|||||
|TERMIN|01|010|ACA|Alternative Currency Amount|C|1|0|0|0|||||
|TERMIN|01|020|CUI|Current Usage Information|C|1|0|0|0|||||
|TERMIN|02|000|TXT|Text|C|2|0|0|0|||||
|TERMIN|02|010|UNT|Message Trailer|M|1|0|0|0|||||
|WESTIM|00|000|UNH|Message Header|M|1|0|0|0|||||
|WESTIM|00|010|DTM|Date/Time Reference|M|1|0|0|0|||||
|WESTIM|00|020|RFF|Reference|C|4|0|0|0|||||
|WESTIM|00|030|ACA|Alternative Currency Amount|C|2|0|0|0|||||
|WESTIM|00|040|LBR|Labour|M|1|0|0|0|||||
|WESTIM|00|050|NAD|Name and Address|C|3|0|0|0|||||
|WESTIM|00|060|CTA|Contact|C|2|0|0|0|||||
|WESTIM|01|000|EQF|Equipment|M|1|0|0|0|||||
|WESTIM|01|010|ERI|Equipment Related Information|C|1|0|0|0|||||
|WESTIM|01|020|ECI|Equipment Condition|C|1|0|0|0|||||
|WESTIM|01|030|CUI|Current Usage Information|C|2|0|0|0|||||
|WESTIM|02|000|DAM|Damage|C|1|DAM|1|99|||||
|WESTIM|02|010|WOR|Work|C|1|DAM|1|0|||||
|WESTIM|02|020|COS|Cost|C|1|DAM|1|0|||||
|WESTIM|03|000|CTO|Cost Totals|C|5|0|0|0|||||
|WESTIM|04|000|TAD|Transaction Details|C|1|0|0|0|||||
|WESTIM|04|010|TMA|Total Message Amount|M|1|0|0|0|||||
|WESTIM|04|020|TXT|Text|C|5|0|0|0|||||
|WESTIM|04|030|UNT|Message Trailer|M|1|0|0|0|||||
|WINVOI|00|000|UNH|Message Header|M|1|0|0|0|||||
|WINVOI|00|010|DTM|Date/Time Reference|M|1|0|0|0|||||
|WINVOI|00|020|RFF|Reference|C|3|0|0|0|||||
|WINVOI|00|030|ACA|Alternative Currency Amount|C|2|0|0|0|||||
|WINVOI|00|040|LBR|Labour|M|1|0|0|0|||||
|WINVOI|00|050|NAD|Name and Address|C|3|0|0|0|||||
|WINVOI|00|060|CTA|Contact|C|2|0|0|0|||||
|WINVOI|01|000|EQF|Equipment|M|1|0|0|0|||||
|WINVOI|01|010|ERI|Equipment Related Information|C|1|0|0|0|||||
|WINVOI|01|020|ECI|Equipment Condition Information|C|1|0|0|0|||||
|WINVOI|01|030|CUI|Current Usage Information|C|2|0|0|0|||||
|WINVOI|02|000|DAM|Damage|C|99|DAM|1|99|||||
|WINVOI|02|010|WOR|Work|C|99|DAM|1|0|||||
|WINVOI|02|020|COS|Cost|C|99|DAM|1|0|||||
|WINVOI|03|000|CTO|Cost Totals|C|1|0|0|0|||||
|WINVOI|04|000|TAD|Transaction Details|C|1|0|0|0|||||
|WINVOI|04|010|TMA|Total Message Amount|M|1|0|0|0|||||
|WINVOI|04|020|TXT|Text|C|3|0|0|0|||||
|WINVOI|04|030|UNT|Message Trailer|M|1|0|0|0|||||
|WORDER|00|000|UNH|Message Header|M|1|0|0|0|||||
|WORDER|00|010|DTM|Date/Time Reference|M|1|0|0|0|||||
|WORDER|00|020|RFF|Reference|M|4|0|0|0|||||
|WORDER|00|030|ACA|Alternative Currency Amount|C|2|0|0|0|||||
|WORDER|00|040|LBR|Labour|M|1|0|0|0|||||
|WORDER|00|050|NAD|Name and Address|M|2|0|0|0|||||
|WORDER|00|060|CTA|Contact|C|2|0|0|0|||||
|WORDER|01|000|EQF|Equipment|M|1|0|0|0|||||
|WORDER|01|010|ERI|Equipment Related Information|C|1|0|0|0|||||
|WORDER|02|000|DAM|Damage|C|1|DAM|1|99|||||
|WORDER|02|010|WOR|Work|C|1|DAM|1|0|||||
|WORDER|02|020|COS|Cost|C|1|DAM|1|0|||||
|WORDER|03|000|CTO|Cost Totals|C|1|0|0|0|||||
|WORDER|04|000|TAD|Transaction Details|C|1|0|0|0|||||
|WORDER|04|010|TMA|Total Message Amount|M|1|0|0|0|||||
|WORDER|04|020|TXT|Text|C|3|0|0|0|||||
|WORDER|04|030|UNT|Message Trailer|M|1|0|0|0|||||
