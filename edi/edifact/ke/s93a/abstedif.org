|ABCSAR|입찰/계약보증금수납통보서||||
|ASSIGN|조합배정물량서||||
|AWBADV|Air Waybill Advice||||
|AWBINV|운임청구용 INVOICE||||
|BIDDOC|입찰서||||
|BIDRES|입찰응답서||||
|CCAREQ|공사계약내용변경요청서||||
|CCARES|공사계약내용변경응답서||||
|CUSAGD|하선신고서||||
|CUSAPE|오류통보||||
|CUSAVE|평균세액증명||||
|CUSBCR|이적허기신청||||
|CUSBRR|반출신고서||||
|CUSCAR|보세화물 양수도 신고서/빈압신고서/반송물품반입보고/반출통고목록보||||
|CUSCED|선원휴대품목록||||
|CUSCRA|반출승인내역||||
|CUSDAD|정산신청||||
|CUSDEC|수입신고수리필정보(Customs declaration message)||||
|CUSDRW|환급신청/기초원재료납세증명/분할증명||||
|CUSINF|보세구역반입예정정보||||
|CUSINS|하선신고수리통보/체화예정정보 누락통보||||
|CUSMAN|해상수입적하목록외||||
|CUSMOV|보세운송신고/보세운송승인신청||||
|CUSQAR|검사.검역결과  접수통지||||
|CUSREP|입항보고/해상(항공)적하목록/해상입출항신고(Customs conveyance rep||||
|CUSRES|접수통보/완료통보/환급금지급결정통지(신청인용)/서류제출요구/통관�||||
|CUSRGD|하선적재결과 이상보고||||
|CUSSSD|선기용품신고서외||||
|DVAREQ|분할납품변경요청서||||
|DVARES|분할납품변경요청응답서||||
|DVSREQ|분할납품요구 및 통보서||||
|DVSRES|분할납품요구 및 통보응답서||||
|EXMBSA|입찰보증금면제각서||||
|IMPAIC|식품등의 수입신고서||||
|IMPICA|식품등의 수입신고필증||||
|IMPNTY|식품등의 수입신고서||||
|IMPPAS|식품등의 수입신고필증||||
|LTCREQ|장기계속공사계약요청서||||
|LTCRES|장기계속공사계약응답서||||
|ORDCHG|Purchase order change message||||
|ORDERS|Purchase order message||||
|ORDRSP|Purchase order response message||||
|PARTIN|해외공급자부호신청/통관고유부호 부여신청(Party information messag||||
|PAXLST|Passenger list message||||
|RBCSAR|입찰/계약보증금수납통보응답서||||
|REFNTY|부적합통보서||||
|REMBSA|입찰보증금면제각서응답서||||
|REQINS|검수요청서||||
|RESINS|검수결과통보서||||
|SAITIN|수입화물정보||||
|SANCRT|검사.검역 결과통지||||
|SHPRQS|Shpping Request||||
|SPLACK|청구및 불출확인서||||
|SPLREQ|청구및 불출요청서||||
|STOCKS|업체재고현황서||||
|SUAREQ|조달변경요청서||||
|SUARES|조달변경요청응답서||||
|SUPREQ|조달요청서||||
|SUPRES|조달요청응답서||||
|WORACK|공사 지시 확인서||||
|WORINS|공사 지시 요청서||||
