|APERAK|Application error and acknowledgement message||||
|AUTACK|Secure authentication and acknowledgement message||||
|AUTHOR|Authorization message||||
|BANSTA|Banking status message||||
|BAPLIE|Bayplan/stowage plan occupied and empty locations message||||
|BAPLTE|Bayplan/stowage plan total numbers message||||
|BOPBNK|Bank transactions and portfolio transactions report message||||
|BOPCUS|Balance of payment customer transaction report message||||
|BOPDIR|Direct balance of payment declaration message||||
|BOPINF|Balance of payment information from customer message||||
|CALINF|Vessel call information message||||
|CASINT|Request for legal administration action in civil proceedings||||
|CASRES|Legal administration response in civil proceedings message||||
|COARRI|Container discharge/loading report message||||
|CODECO|Container gate-in/gate-out report message||||
|CODENO|Permit expiration/clearance ready notice message||||
|COEDOR|Container stock report message||||
|COHAOR|Container special handling order message||||
|COMDIS|Commercial dispute message||||
|CONAPW|Advice on pending works message||||
|CONDPV|Direct payment valuation message||||
|CONDRA|Drawing administration message||||
|CONDRO|Drawing organisation message||||
|CONEST|Establishment of contract message||||
|CONITT|Invitation to tender message||||
|CONPVA|Payment valuation message||||
|CONQVA|Quantity valuation message||||
|CONRPW|Response of pending works message||||
|CONTEN|Tender message||||
|CONTRL|Syntax and service report message||||
|CONWQD|Work item quantity determination message||||
|COPARN|Container announcement message||||
|COPINO|Container pre-notification message||||
|COPRAR|Container discharge/loading order message||||
|COREOR|Container release order message||||
|COSTCO|Container stuffing/stripping confirmation message||||
|COSTOR|Container stuffing/stripping order message||||
|CREADV|Credit advice message||||
|CREEXT|Extended credit advice message||||
|CREMUL|Multiple credit advice message||||
|CUSCAR|Customs cargo report message||||
|CUSDEC|Customs declaration message||||
|CUSEXP|Customs express consignment declaration message||||
|CUSREP|Customs conveyance report message||||
|CUSRES|Customs response message||||
|DEBADV|Debit advice message||||
|DEBMUL|Multiple debit advice message||||
|DELFOR|Delivery schedule message||||
|DELJIT|Delivery just in time message||||
|DESADV|Despatch advice message||||
|DESTIM|Equipment damage and repair estimate message||||
|DGRECA|Dangerous goods recapitulation message||||
|DIRDEB|Direct debit message||||
|DIRDEF|Directory definition message||||
|DOCADV|Documentary credit advice message||||
|DOCAMA|Advice of an amendment of a documentary credit message||||
|DOCAMI|Documentary credit amendment information message||||
|DOCAMR|Request for an amendment of a documentary credit message||||
|DOCAPP|Documentary credit application message||||
|DOCARE|Response to an amendment of a documentary credit message||||
|DOCINF|Documentary credit issuance information message||||
|FINCAN|Financial cancellation message||||
|FINSTA|Financial statement of an account message||||
|GENRAL|General purpose message||||
|GESMES|Generic statistical message||||
|HANMOV|Cargo/goods handling and movement message||||
|IFCSUM|Forwarding and consolidation summary message||||
|IFTCCA|Forwarding and transport shipment charge calculation message||||
|IFTDGN|Dangerous goods notification message||||
|IFTFCC|International transport freight costs and other charges||||
|IFTIAG|Dangerous cargo list message||||
|IFTMAN|Arrival notice message||||
|IFTMBC|Booking confirmation message||||
|IFTMBF|Firm booking message||||
|IFTMBP|Provisional booking message||||
|IFTMCS|Instruction contract status message||||
|IFTMIN|Instruction message||||
|IFTRIN|Forwarding and transport rate information message||||
|IFTSAI|Forwarding and transport schedule and availability||||
|IFTSTA|International multimodal status report message||||
|IFTSTQ|International multimodal status request message||||
|INSPRE|Insurance premium message||||
|INVOIC|Invoice message||||
|INVRPT|Inventory report message||||
|ITRRPT|In transit report detail message||||
|JAPRES|Job application result message||||
|JINFDE|Job information demand message||||
|JOBAPP|Job application proposal message||||
|JOBCON|Job order confirmation message||||
|JOBMOD|Job order modification message||||
|JOBOFF|Job order message||||
|MEDPID|Person identification message||||
|MEDREQ|Medical service request message||||
|MEDRPT|Medical service report message||||
|MEQPOS|Means of transport and equipment position message||||
|MOVINS|Stowage instruction message||||
|MSCONS|Metered services consumption report message||||
|ORDCHG|Purchase order change request message||||
|ORDERS|Purchase order message||||
|ORDRSP|Purchase order response message||||
|OSTENQ|Order status enquiry message||||
|OSTRPT|Order status report message||||
|PARTIN|Party information message||||
|PAXLST|Passenger list message||||
|PAYDUC|Payroll deductions advice message||||
|PAYEXT|Extended payment order message||||
|PAYMUL|Multiple payment order message||||
|PAYORD|Payment order message||||
|PRICAT|Price/sales catalogue message||||
|PRODAT|Product data message||||
|PRODEX|Product exchange reconciliation message||||
|PROINQ|Product inquiry message||||
|PRPAID|Insurance premium payment message||||
|QALITY|Quality data message||||
|QUOTES|Quote message||||
|RDRMES|Raw data reporting message||||
|REBORD|Reinsurance bordereau message||||
|RECADV|Receiving advice message||||
|RECALC|Reinsurance calculation message||||
|RECECO|Credit risk cover message||||
|RECLAM|Reinsurance claims message||||
|REMADV|Remittance advice message||||
|REPREM|Reinsurance premium message||||
|REQDOC|Request for document message||||
|REQOTE|Request for quote message||||
|RESETT|Reinsurance settlement message||||
|RESMSG|Reservation message||||
|RETACC|Reinsurance technical account message||||
|SAFHAZ|Safety and hazard data message||||
|SANCRT|International movement of goods governmental regulatory||||
|SLSFCT|Sales forecast message||||
|SLSRPT|Sales data report message||||
|SSIMOD|Modification of identity details message||||
|SSRECH|Worker's insurance history message||||
|SSREGW|Notification of registration of a worker message||||
|STATAC|Statement of account message||||
|SUPCOT|Superannuation contributions advice message||||
|SUPMAN|Superannuation maintenance message||||
|SUPRES|Supplier response message||||
|TANSTA|Tank status report message||||
|VATDEC|Value added tax message||||
|VESDEP|Vessel departure message||||
|WASDIS|Waste disposal information message||||
|WKGRDC|Work grant decision message||||
|WKGRRE|Work grant request message||||
||||||
|ABCSAR|입찰/계약보증금수납통보서||||
|ASSIGN|조합배정물량서||||
|AWBADV|Air Waybill Advice||||
|AWBINV|운임청구용 INVOICE||||
|BIDDOC|입찰서||||
|BIDRES|입찰응답서||||
|CCAREQ|공사계약내용변경요청서||||
|CCARES|공사계약내용변경응답서||||
|CUSAGD|하선신고서||||
|CUSAPE|오류통보||||
|CUSAVE|평균세액증명||||
|CUSBCR|이적허기신청||||
|CUSBRR|반출신고서||||
|CUSCED|선원휴대품목록||||
|CUSCRA|반출승인내역||||
|CUSDAD|정산신청||||
|CUSDRW|환급신청/기초원재료납세증명/분할증명||||
|CUSINF|보세구역반입예정정보||||
|CUSINS|하선신고수리통보/체화예정정보 누락통보||||
|CUSMAN|해상수입적하목록외||||
|CUSMOV|보세운송신고/보세운송승인신청||||
|CUSQAR|검사.검역결과  접수통지||||
|CUSRGD|하선적재결과 이상보고||||
|CUSSSD|선기용품신고서외||||
|DVAREQ|분할납품변경요청서||||
|DVARES|분할납품변경요청응답서||||
|DVSREQ|분할납품요구 및 통보서||||
|DVSRES|분할납품요구 및 통보응답서||||
|EXMBSA|입찰보증금면제각서||||
|IMPAIC|식품등의 수입신고서||||
|IMPICA|식품등의 수입신고필증||||
|IMPNTY|식품등의 수입신고서||||
|IMPPAS|식품등의 수입신고필증||||
|LTCREQ|장기계속공사계약요청서||||
|LTCRES|장기계속공사계약응답서||||
|RBCSAR|입찰/계약보증금수납통보응답서||||
|REFNTY|부적합통보서||||
|REMBSA|입찰보증금면제각서응답서||||
|REQINS|검수요청서||||
|RESINS|검수결과통보서||||
|SAITIN|수입화물정보||||
|SHPRQS|Shpping Request||||
|SPLACK|청구및 불출확인서||||
|SPLREQ|청구및 불출요청서||||
|STOCKS|업체재고현황서||||
|SUAREQ|조달변경요청서||||
|SUARES|조달변경요청응답서||||
|SUPREQ|조달요청서||||
|WORACK|공사 지시 확인서||||
|WORINS|공사 지시 요청서||||
