|BAPLIE|00|000|UNH|전자문서 머리|M|1|0|0|0|||||
|BAPLIE|00|010|BGM|전자문서 시작|M|1|0|0|0|||||
|BAPLIE|00|020|DTM|일자/시간/기간|M|1|0|0|0|||||
|BAPLIE|00|030|RFF|참조번호|C|1|0|0|0|||||
|BAPLIE|00|040|NAD|상호 및 주소|C|3|0|0|0|||||
|BAPLIE|01|000|TDT|운송정보 내역|M|1|TDT|1|3|||||
|BAPLIE|01|010|LOC|장소/위치 식별|M|2|TDT|1|0|||||
|BAPLIE|01|020|DTM|일자/시간/기간|M|99|TDT|1|0|||||
|BAPLIE|01|030|RFF|참조번호|C|1|TDT|1|0|||||
|BAPLIE|01|040|FTX|평문기술|C|1|TDT|1|0|||||
|BAPLIE|02|000|LOC|장소/위치 식별|M|1|LOC|1|9999|||||
|BAPLIE|02|010|GID|상품항목 명세|C|1|LOC|1|0|||||
|BAPLIE|02|020|GDS|화물특성|C|1|LOC|1|0|||||
|BAPLIE|02|030|FTX|평문기술|C|9|LOC|1|0|||||
|BAPLIE|02|040|MEA|측정치|M|9|LOC|1|0|||||
|BAPLIE|02|050|DIM|규격|C|9|LOC|1|0|||||
|BAPLIE|02|060|TMP|온도|C|1|LOC|1|0|||||
|BAPLIE|02|070|RNG|범위|C|1|LOC|1|0|||||
|BAPLIE|02|080|LOC|장소/위치 식별|C|9|LOC|1|0|||||
|BAPLIE|02|090|RFF|참조번호|M|1|LOC|1|0|||||
|BAPLIE|03|000|EQD|장비내역|M|1|EQD|2|3|||||
|BAPLIE|03|010|EQA|부가장비|C|9|EQD|2|0|||||
|BAPLIE|03|020|NAD|상호 및 주소|C|1|EQD|2|0|||||
|BAPLIE|04|000|DGS|위험화물 명세|M|1|DGS|2|999|||||
|BAPLIE|04|010|FTX|평문기술|C|1|DGS|2|0|||||
|BAPLIE|05|000|UNT|전자문서 꼬리|M|1|0|0|0|||||
|CALINF|00|000|UNH|전자문서 머리|M|1|0|0|0|||||
|CALINF|00|010|BGM|전자문서 시작|M|1|0|0|0|||||
|CALINF|00|020|RFF|참조번호|C|9|0|0|0|||||
|CALINF|01|000|NAD|상호 및 주소|m|1|NAD|1|9|||||
|CALINF|01|010|CTA|연락정보|C|9|NAD|1|0|||||
|CALINF|02|000|TDT|운송정보내역|m|1|TDT|1|1|||||
|CALINF|02|010|RFF|참조번호|C|9|TDT|1|0|||||
|CALINF|02|020|LOC|장소/위치 식별|M|9|TDT|1|0|||||
|CALINF|02|030|DTM|일자/시간/기간|M|9|TDT|1|0|||||
|CALINF|02|040|FTX|평문기술|C|9|TDT|1|0|||||
|CALINF|03|000|QTY|수량|M|1|QTY|1|9|||||
|CALINF|03|010|FTX|평문기술|C|1|QTY|1|0|||||
|CALINF|04|000|UNT|전자문서 꼬리|M|1|0|0|0|||||
|COARRI|00|000|UNH|Message header|M|1|0|0|0|||||
|COARRI|00|010|BGM|Beginning of message|M|1|0|0|0|||||
|COARRI|00|020|FTX|Free text|C|9|0|0|0|||||
|COARRI|00|030|RFF|Reference|M|9|0|0|0|||||
|COARRI|01|000|TDT|Details of transport|M|1|TDT|1|0|||||
|COARRI|01|010|RFF|Reference|C|9|TDT|1|0|||||
|COARRI|01|020|LOC|Place/location identification|C|9|TDT|1|0|||||
|COARRI|01|030|DTM|Date/time/period|C|9|TDT|1|0|||||
|COARRI|02|000|NAD|Name and address|M|1|NAD|1|9|||||
|COARRI|02|010|CTA|Contact information|C|9|NAD|1|0|||||
|COARRI|03|000|EQD|Equipment details|M|1|EQD|1|9999|||||
|COARRI|03|010|RFF|Reference|C|9|EQD|1|0|||||
|COARRI|03|020|EQN|Number of units|C|1|EQD|1|0|||||
|COARRI|03|030|TMD|Transport movement details|C|9|EQD|1|0|||||
|COARRI|03|040|DTM|Date/time/period|C|9|EQD|1|0|||||
|COARRI|03|050|LOC|Place/location identification|C|9|EQD|1|0|||||
|COARRI|03|060|MEA|Measurements|C|9|EQD|1|0|||||
|COARRI|03|070|DIM|Dimensions|C|9|EQD|1|0|||||
|COARRI|03|080|TMP|Temperature|C|9|EQD|1|0|||||
|COARRI|03|090|RNG|Range details|C|9|EQD|1|0|||||
|COARRI|03|100|SEL|Seal number|C|9|EQD|1|0|||||
|COARRI|03|110|FTX|Free text|C|9|EQD|1|0|||||
|COARRI|03|120|DGS|Dangerous goods|C|9|EQD|1|0|||||
|COARRI|03|130|EQA|Attached equipment|C|9|EQD|1|0|||||
|COARRI|03|140|PIA|Additional product id|C|9|EQD|1|0|||||
|COARRI|03|150|NAD|Name and address|C|1|EQD|1|0|||||
|COARRI|04|000|DAM|Damage|M|1|DAM|2|9|||||
|COARRI|04|010|COD|Component details|C|1|DAM|2|0|||||
|COARRI|05|000|TDT|Details of transport|M|1|TDT|2|1|||||
|COARRI|05|010|LOC|Place/location identification|C|9|TDT|2|0|||||
|COARRI|05|020|DTM|Date/time/period|C|9|TDT|2|0|||||
|COARRI|06|000|CNT|Control total|M|1|0|0|0|||||
|COARRI|07|000|UNT|Meaasge trailer|M|1|0|0|0|||||
|CODECO|00|000|UNH|전자문서 머리|M|1|0|0|0|||||
|CODECO|00|010|BGM|전자문서 시작|M|1|0|0|0|||||
|CODECO|00|020|FTX|평문기술|C|9|0|0|0|||||
|CODECO|00|030|RFF|참조번호|C|9|0|0|0|||||
|CODECO|01|000|TDT|운송정보내역|M|1|TDT|1|1|||||
|CODECO|01|010|RFF|참조번호|C|9|TDT|1|0|||||
|CODECO|01|020|LOC|장소/위치 식별|C|9|TDT|1|0|||||
|CODECO|01|030|DTM|일자/시간/기간|C|9|TDT|1|0|||||
|CODECO|02|000|NAD|상호 및 주소|m|1|NAD|1|9|||||
|CODECO|02|010|CTA|연락정보|C|9|NAD|1|0|||||
|CODECO|03|000|GID|상품항목명세|M|1|GID|1|999|||||
|CODECO|03|010|HAN|취급지시|C|9|GID|1|0|||||
|CODECO|03|020|FTX|평문기술|C|9|GID|1|0|||||
|CODECO|03|030|PIA|부가화물식별|C|9|GID|1|0|||||
|CODECO|03|040|MEA|측정치|C|9|GID|1|0|||||
|CODECO|03|050|TMP|온도|C|9|GID|1|0|||||
|CODECO|03|060|RNG|범위|C|9|GID|1|0|||||
|CODECO|03|070|SGP|화물배치정보|C|999|GID|1|0|||||
|CODECO|04|000|DGS|위험화물명세|M|1|DGS|2|9|||||
|CODECO|04|010|FTX|평문기술|C|9|DGS|2|0|||||
|CODECO|05|000|EQD|장비내역|m|1|EQD|1|999|||||
|CODECO|05|010|RFF|참조번호|C|9|EQD|1|0|||||
|CODECO|05|020|TMD|운송이동명세|C|9|EQD|1|0|||||
|CODECO|05|030|DTM|일자/시간/기간|C|9|EQD|1|0|||||
|CODECO|05|040|LOC|장소/위치 식별|C|9|EQD|1|0|||||
|CODECO|05|050|MEA|측정치|C|9|EQD|1|0|||||
|CODECO|05|060|DIM|규격|C|9|EQD|1|0|||||
|CODECO|05|070|SEL|봉인번호|C|9|EQD|1|0|||||
|CODECO|05|080|FTX|평문기술|C|9|EQD|1|0|||||
|CODECO|05|090|EQA|부가장비|C|9|EQD|1|0|||||
|CODECO|06|000|DAM|Damage|M|1|DAM|2|9|||||
|CODECO|06|010|COD|Component details|C|1|DAM|2|0|||||
|CODECO|07|000|TDT|운송정보내역|M|1|TDT|2|9|||||
|CODECO|07|010|LOC|장소/위치 식별|C|1|TDT|2|0|||||
|CODECO|07|020|DTM|일자/시간/기간|C|1|TDT|2|0|||||
|CODECO|07|030|NAD|상호 및 주소|C|9|EQD|1|0|||||
|CODECO|08|000|CNT|제어 총계|M|1|0|0|0|||||
|CODECO|09|000|UNT|전자문서 꼬리|M|1|0|0|0|||||
|COPRAR|00|000|UNH|Message header|M|1|0|0|0|||||
|COPRAR|00|010|BGM|Beginning of message|M|1|0|0|0|||||
|COPRAR|00|020|FTX|Free text|C|9|0|0|0|||||
|COPRAR|00|030|RFF|Reference|M|9|0|0|0|||||
|COPRAR|01|000|TDT|Details of transport|M|1|TDT|1|0|||||
|COPRAR|01|010|RFF|Reference|C|9|TDT|1|0|||||
|COPRAR|01|020|LOC|Place/location identification|C|9|TDT|1|0|||||
|COPRAR|01|030|DTM|Date/time/period|C|9|TDT|1|0|||||
|COPRAR|01|040|FTX|Free text|C|9|TDT|1|0|||||
|COPRAR|02|000|NAD|Name and address|M|1|NAD|1|9|||||
|COPRAR|02|010|CTA|Contact information|C|9|NAD|1|0|||||
|COPRAR|03|000|EQD|Equipment details|M|1|EQD|1|9999|||||
|COPRAR|03|010|RFF|Reference|C|9|EQD|1|0|||||
|COPRAR|03|020|EQN|Number of units|C|1|EQD|1|0|||||
|COPRAR|03|030|TMD|Transport movement details|C|9|EQD|1|0|||||
|COPRAR|03|040|DTM|Date/time/period|C|9|EQD|1|0|||||
|COPRAR|03|050|LOC|Place/location identification|C|9|EQD|1|0|||||
|COPRAR|03|060|MEA|Measurements|C|9|EQD|1|0|||||
|COPRAR|03|070|DIM|Dimensions|C|9|EQD|1|0|||||
|COPRAR|03|080|TMP|Temperature|C|9|EQD|1|0|||||
|COPRAR|03|090|RNG|Range details|C|9|EQD|1|0|||||
|COPRAR|03|100|SEL|Seal number|C|9|EQD|1|0|||||
|COPRAR|03|110|FTX|Free text|C|9|EQD|1|0|||||
|COPRAR|03|120|DGS|Dangerous goods|C|9|EQD|1|0|||||
|COPRAR|03|130|EQA|Attached equipment|C|9|EQD|1|0|||||
|COPRAR|03|140|NAD|Name and address|C|1|EQD|1|0|||||
|COPRAR|04|000|TDT|Details of transport|M|1|TDT|2|1|||||
|COPRAR|04|010|RFF|References|C|9|TDT|2|0|||||
|COPRAR|04|020|LOC|Place/location identification|C|9|TDT|2|0|||||
|COPRAR|04|030|DTM|Date/time/period|C|9|TDT|2|0|||||
|COPRAR|05|000|CNT|Control total|M|1|0|0|0|||||
|COPRAR|06|000|UNT|Meaasge trailer|M|1|0|0|0|||||
|MOVINS|00|000|UNH|전자문서 머리|M|1|0|0|0|||||
|MOVINS|00|010|BGM|전자문서 시작|M|1|0|0|0|||||
|MOVINS|00|020|DTM|일자/시간/기간|M|1|0|0|0|||||
|MOVINS|00|030|RFF|참조번호|C|1|0|0|0|||||
|MOVINS|00|040|NAD|상호 및 주소|C|3|0|0|0|||||
|MOVINS|01|000|TDT|운송정보내역|M|1|TDT|1|3|||||
|MOVINS|01|010|LOC|장소/위치 식별|C|99|TDT|1|0|||||
|MOVINS|01|020|DTM|일자/시간/기간|C|99|TDT|1|0|||||
|MOVINS|01|030|RFF|참조번호|C|1|TDT|1|0|||||
|MOVINS|01|040|FTX|평문기술|C|1|TDT|1|0|||||
|MOVINS|02|000|HAN|취급설명|M|1|HAN|1|9999|||||
|MOVINS|03|000|LOC|장소 및 위치 식별|M|1|LOC|2|9999|||||
|MOVINS|03|010|RFF|Reference|C|99|LOC|2|0|||||
|MOVINS|03|020|FTX|평문기술|C|9|LOC|2|0|||||
|MOVINS|03|030|MEA|측정치|C|9|LOC|2|0|||||
|MOVINS|03|040|DIM|규  격|C|9|LOC|2|0|||||
|MOVINS|03|050|LOC|Place/location identification|C|9|LOC|2|0|||||
|MOVINS|03|060|NAD|Name and address|C|99|LOC|2|0|||||
|MOVINS|04|000|TMP|Temperature|C|1|TMP|3|1|||||
|MOVINS|04|010|RNG|Range details|C|1|TMP|3|0|||||
|MOVINS|05|000|EQD|Equipment details|C|999|EQD|3|999|||||
|MOVINS|05|010|EQN|Number of units|C|1|EQD|3|0|||||
|MOVINS|06|000|EQA|Attached equipment|C|1|EQA|3|99|||||
|MOVINS|06|010|EQN|Number of units|C|1|EQD|3|0|||||
|MOVINS|07|000|GID|Goods item details|C|1|GID|3|9999|||||
|MOVINS|07|010|GDS|Nature of cargo|C|1|GID|3|0|||||
|MOVINS|08|000|RFF|Reference|C|1|RFF|3|999|||||
|MOVINS|09|000|DGS|Dangerous goods|C|1|DGS|4|99|||||
|MOVINS|09|010|FTX|Free text|C|1|DGS|4|0|||||
|CODECO|05|100|NAD|상호 및 주소|C|9|EQD|1|0|||||
|COARRI|08|000|UNT|Meaasge trailer|M|1|0|0|0|||||
|CODECO|10|000|UNT|전자문서 꼬리|M|1|0|0|0|||||
|COPRAR|07|000|UNT|Meaasge trailer|M|1|0|0|0|||||
|MOVINS|03|070|RNG|범  위|C|1|LOC|2|0|||||
|MOVINS|03|080|LOC|장소/위치 식별|C|9|LOC|2|0|||||
|MOVINS|03|090|RFF|참조번호|C|9|LOC|2|0|||||
|MOVINS|04|020|NAD|상호 및 주소|C|1|EQD|3|0|||||
|COREOR|00|000|UNH|전자문서 머리|M|1|0|0|0|||||
|COREOR|00|010|BGM|전자문서 시작|M|1|0|0|0|||||
|COREOR|00|020|FTX|평문기술|C|9|0|0|0|||||
|COREOR|00|030|RFF|참조번호|C|9|0|0|0|||||
|COREOR|01|000|TDT|운송정보내역|M|1|TDT|1|1|||||
|COREOR|01|010|RFF|참조번호|M|9|TDT|1|0|||||
|COREOR|01|020|LOC|장소/위치 식별|C|9|TDT|1|0|||||
|COREOR|01|030|DTM|일자/시간/기간|C|9|TDT|1|0|||||
|COREOR|02|000|NAD|상호 및 주소|M|1|NAD|1|9|||||
|COREOR|02|010|CTA|연락정보|C|9|NAD|1|0|||||
|COREOR|02|020|RFF|참조번호|C|9|NAD|1|0|||||
|COREOR|02|030|DTM|일자/시간/기간|C|9|NAD|1|0|||||
|COREOR|03|000|GID|상품항목명세|C|1|GID|1|999|||||
|COREOR|03|010|HAN|취급지시|C|9|GID|1|0|||||
|COREOR|03|020|FTX|평문기술|C|9|GID|1|0|||||
|COREOR|04|000|NAD|상호 및 주소|C|9|NAD|1|0|||||
|COREOR|04|010|DTM|일자/시간/기간|C|9|NAD|1|0|||||
|COREOR|04|020|RFF|참조번호|C|9|NAD|1|0|||||
|COREOR|04|030|MEA|측정치|C|9|NAD|1|0|||||
|COREOR|04|040|PCI|측정가격|C|9|NAD|1|0|||||
|COREOR|05|000|SGP|화물배치정보|C|999|SGP|1|0|||||
|COREOR|05|010|MEA|측정치|C|9|SGP|1|0|||||
|COREOR|06|000|DGS|위험화물명세|M|1|DGS|2|9|||||
|COREOR|06|010|FTX|평문기술|C|9|DGS|2|0|||||
|COREOR|06|020|MEA|측정치|C|9|DGS|1|0|||||
|COREOR|07|000|EQD|장비내역|M|1|EQD|1|999|||||
|COREOR|07|010|RFF|참조번호|C|9|EQD|1|0|||||
|COREOR|07|020|TSR|운송이동명세|C|9|EQD|1|0|||||
|COREOR|07|030|MEA|측정치|C|9|EQD|1|0|||||
|COREOR|07|040|DIM|규격|C|9|EQD|1|0|||||
|COREOR|07|050|TMP|온도|C|1|EQD|1|0|||||
|COREOR|07|060|RNG|범위|C|1|EQD|1|0|||||
|COREOR|07|070|SEL|봉인번호|C|9|EQD|1|0|||||
|COREOR|07|080|FTX|평문기술|C|9|EQD|1|0|||||
|COREOR|07|090|EQA|부가장비|C|9|EQD|1|0|||||
|COREOR|07|100|DTM|일자/시간/기간|C|9|EQD|1|0|||||
|COREOR|07|110|LOC|장소/위치 식별|C|9|EQD|1|0|||||
|COREOR|08|000|TDT|운송정보내역|C|1|TDT|2|9|||||
|COREOR|08|010|LOC|장소/위치 식별|C|9|TDT|2|0|||||
|COREOR|08|020|DTM|일자/시간/기간|C|9|TDT|2|0|||||
|COREOR|09|000|NAD|상호 및 주소|C|9|NAD|2|9|||||
|COREOR|09|010|DTM|일자/시간/기간|C|1|NAD|2|0|||||
|COREOR|09|020|CTA|연락정보|C|9|NAD|2|0|||||
|COREOR|09|030|COM|Communication contact|C|9|NAD|2|0|||||
|COREOR|10|000|CNT|제어 총계|M|1|0|0|0|||||
|COREOR|10|010|UNT|전자문서 꼬리|M|1|0|0|0|||||
