|104|Air Shipment Information||||
|110|Air Freight Details and Invoice||||
|120|Vehicle Shipping Order||||
|121|Vehicle Service||||
|125|Multilevel Railcar Load Details||||
|126|Vehicle Application Advice||||
|127|Vehicle Baying Order||||
|128|Dealer Information||||
|129|Vehicle Carrier Rate Update||||
|130|Student Educational Record (Transcript)||||
|131|Student Educational Record (Transcript) Acknowledgment||||
|135|Student Loan Application||||
|139|Student Loan Guarantee Result||||
|140|Product Registration||||
|141|Product Service Claim Response||||
|142|Product Service Claim||||
|143|Product Service Notification||||
|144|Student Loan Transfer and Status Verification||||
|146|Request for Student Educational Record (Transcript)||||
|147|Response to Request for Student Educational Record (Transcript)||||
|148|Report of Injury or Illness||||
|150|Tax Rate Notification||||
|151|Electronic Filing of Tax Return Data Acknowledgment||||
|152|Statistical Government Information||||
|154|Uniform Commercial Code Filing||||
|161|Train Sheet||||
|170|Revenue Receipts Statement||||
|175|Court Notice||||
|176|Bankruptcy Proof of Claim||||
|180|Return Merchandise Authorization and Notification||||
|186|Laboratory Reporting||||
|190|Student Enrollment Verification||||
|191|Student Loan Preclaims||||
|196|Contractor Cost Data Reporting||||
|204|Motor Carrier Shipment Information||||
|210|Motor Carrier Freight Details and Invoice||||
|213|Motor Carrier Shipment Status Inquiry||||
|214|Transportation Carrier Shipment Status Message||||
|217|Motor Carrier Loading and Route Guide||||
|218|Motor Carrier Tariff Information||||
|250|Purchase Order Shipment Management Document||||
|251|Pricing Support||||
|260|Application for Mortgage Insurance Benefits||||
|263|Residential Mortgage Insurance Application Response||||
|264|Mortgage Loan Default Status||||
|265|Real Estate Title Insurance Services Order||||
|270|Health Care Eligibility/Benefit Inquiry||||
|271|Health Care Eligibility/Benefit Information||||
|272|Property and Casualty Loss Notification||||
|276|Health Care Claim Status Request||||
|277|Health Care Claim Status Notification||||
|290|Cooperative Advertising Agreements||||
|300|Reservation (Booking Request) (Ocean)||||
|301|Confirmation (Ocean)||||
|303|Booking Cancellation (Ocean)||||
|304|Shipping Instructions||||
|309|U.S. Customs Manifest||||
|310|Freight Receipt and Invoice (Ocean)||||
|311|Canadian Customs Information||||
|312|Arrival Notice (Ocean)||||
|313|Shipment Status Inquiry (Ocean)||||
|315|Status Details (Ocean)||||
|317|Delivery/Pickup Order||||
|319|Terminal Information||||
|322|Terminal Operations Activity (Ocean)||||
|323|Vessel Schedule and Itinerary (Ocean)||||
|324|Vessel Stow Plan (Ocean)||||
|325|Consolidation of Goods In Container||||
|326|Consignment Summary List||||
|350|U.S. Customs Release Information||||
|352|U.S. Customs Carrier General Order Status||||
|353|U.S. Customs Events Advisory Details||||
|354|U.S. Customs Automated Manifest Archive Status||||
|355|U.S. Customs Manifest Acceptance/Rejection||||
|356|Permit To Transfer Request||||
|361|Carrier Interchange Agreement (Ocean)||||
|362|Cargo Insurance Advice of Shipment||||
|404|Rail Carrier Shipment Information||||
|410|Rail Carrier Freight Details and Invoice||||
|414|Rail Carhire Settlements||||
|417|Rail Carrier Waybill Interchange||||
|418|Rail Advance Interchange Consist||||
|419|Advance Car Disposition||||
|420|Car Handling Information||||
|421|Estimated Time of Arrival and Car Scheduling||||
|422|Shipper's Car Order||||
|425|Rail Waybill Request||||
|426|Rail Revenue Waybill||||
|429|Railroad Retirement Activity||||
|431|Railroad Station Master File||||
|440|Shipment Weights||||
|466|Rate Request||||
|468|Rate Docket Journal Log||||
|475|Rail Route File Maintenance||||
|485|Ratemaking Action||||
|490|Rate Group Definition||||
|492|Miscellaneous Rates||||
|494|Scale Rate Table||||
|511|Requisition||||
|517|Material Obligation Validation||||
|527|Material Due-In and Receipt||||
|536|Logistics Reassignment||||
|561|Contract Abstract||||
|567|Contract Completion Status||||
|568|Contract Payment Management Report||||
|601|Shipper's Export Declaration||||
|602|Transportation Services Tender||||
|622|Intermodal Ramp Activity||||
|805|Contract Pricing Proposal||||
|806|Project Schedule Reporting||||
|810|Invoice||||
|811|Consolidated Service Invoice/Statement||||
|812|Credit/Debit Adjustment||||
|813|Electronic Filing of Tax Return Data||||
|815|Cryptographic Service Message||||
|816|Organizational Relationships||||
|818|Commission Sales Report||||
|819|Operating Expense Statement||||
|820|Payment Order/Remittance Advice||||
|821|Financial Information Reporting||||
|822|Customer Account Analysis||||
|823|Lockbox||||
|824|Application Advice||||
|826|Tax Information Reporting||||
|827|Financial Return Notice||||
|828|Debit Authorization||||
|829|Payment Cancellation Request||||
|830|Planning Schedule with Release Capability||||
|831|Application Control Totals||||
|832|Price/Sales Catalog||||
|833|Residential Mortgage Credit Report Order||||
|834|Benefit Enrollment and Maintenance||||
|835|Health Care Claim Payment/Advice||||
|836|Contract Award||||
|837|Health Care Claim||||
|838|Trading Partner Profile||||
|839|Project Cost Reporting||||
|840|Request for Quotation||||
|841|Specifications/Technical Information||||
|842|Nonconformance Report||||
|843|Response to Request for Quotation||||
|844|Product Transfer Account Adjustment||||
|845|Price Authorization Acknowledgment/Status||||
|846|Inventory Inquiry/Advice||||
|847|Material Claim||||
|848|Material Safety Data Sheet||||
|849|Response to Product Transfer Account Adjustment||||
|850|Purchase Order||||
|851|Asset Schedule||||
|852|Product Activity Data||||
|853|Routing and Carrier Instruction||||
|854|Shipment Delivery Discrepancy Information||||
|855|Purchase Order Acknowledgment||||
|856|Ship Notice/Manifest||||
|857|Shipment and Billing Notice||||
|858|Shipment Information||||
|859|Freight Invoice||||
|860|Purchase Order Change Request - Buyer Initiated||||
|861|Receiving Advice/Acceptance Certificate||||
|862|Shipping Schedule||||
|863|Report of Test Results||||
|864|Text Message||||
|865|Purchase Order Change Acknowledgment/Request - Seller Initiated||||
|866|Production Sequence||||
|867|Product Transfer and Resale Report||||
|868|Electronic Form Structure||||
|869|Order Status Inquiry||||
|870|Order Status Report||||
|872|Residential Mortgage Insurance Application||||
|875|Grocery Products Purchase Order||||
|876|Grocery Products Purchase Order Change||||
|878|Product Authorization/Deauthorization||||
|879|Price Change||||
|880|Grocery Products Invoice||||
|882|Direct Store Delivery Summary Information||||
|883|Market Development Fund Allocation||||
|884|Market Development Fund Settlement||||
|888|Item Maintenance||||
|889|Promotion Announcement||||
|893|Item Information Request||||
|894|Delivery/Return Base Record||||
|895|Delivery/Return Acknowledgment or Adjustment||||
|896|Product Dimension Maintenance||||
|920|Loss or Damage Claim - General Commodities||||
|924|Loss or Damage Claim - Motor Vehicle||||
|925|Claim Tracer||||
|926|Claim Status Report and Tracer Reply||||
|928|Automotive Inspection Detail||||
|940|Warehouse Shipping Order||||
|943|Warehouse Stock Transfer Shipment Advice||||
|944|Warehouse Stock Transfer Receipt Advice||||
|945|Warehouse Shipping Advice||||
|947|Warehouse Inventory Adjustment Advice||||
|980|Functional Group Totals||||
|990|Response to a Load Tender||||
|996|File Transfer||||
|997|Functional Acknowledgment||||
|998|Set Cancellation||||
